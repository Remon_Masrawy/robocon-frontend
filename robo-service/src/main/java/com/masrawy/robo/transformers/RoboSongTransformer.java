package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboSongDTO;
import com.masrawy.robo.models.RoboSongVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboSongTransformer extends DTOTransformer<RoboSongVO, RoboSongDTO>{

}
