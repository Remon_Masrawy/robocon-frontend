var headers = {};

var languages = new Array();

var $song 		= "/robo-web/web/song";
var $artist 	= "/robo-web/web/artist";
var $album 		= "/robo-web/web/album";
var $provider 	= "/robo-web/web/provider";
var $genre 		= "/robo-web/web/genre";
var $event 		= "/robo-web/web/event";
var $frontend 	= "/robo-web/web/frontend";
var $category 	= "/robo-web/web/category";
var $storefront = "/robo-web/web/storefront";
var $country 	= "/robo-web/web/country";
var $operator 	= "/robo-web/web/operator";
var $language 	= "/robo-web/web/language";
var $user		= "/robo-web/web/user";

headers["User-Lang"] = "en";

var $HTTP_METHOD = {GET: "GET", POST: "POST", PUT: "PUT", DELETE: "DELETE"};

/**
 * pagination active by default
 * 
 * */
$enablePanel = true;
$enablePagination = true;
$fill = function(){
	console.log("default");
};

$(document).ready(function(){
	$container 			= $(".templatemo-content-container");
	$preview 			= $(".templatemo-content-container .preview-widget");
	$table 				= $(".templatemo-content-container .templatemo-content-widget .table.data-table");
	
	$maxPaging = 5;
	
	$.ajax({
		url: $language+"/0/0",
		type: $HTTP_METHOD.GET,
		headers: headers,
		success: function(data){
			languages = data;
		},
		error: function(xhr){
			handleError(xhr)
		},
		complete: function(status){
			
		}
	 });
	
	$("table").on("paste", "tr td", function(e){
		e.preventDefault();
		var cd = e.originalEvent.clipboardData;
		var data = cd.getData("text/plain");
		cd.setData("text/plain", data);
		document.execCommand('insertText', false, data);
	})
	
	$(".templatemo-content-container").removeClass("fade-out");
	
	$("body").on("input", ".group-responsive input[type='text']", function(){
		inputWidth = ($(this).val().length + 1) * 10;
		$(this).css({
	        width: inputWidth
	    })
	});
	
	$('.datepicker').datepicker({ 
		dateFormat: 'DD, d MM, yy',
		altFormat: "yyyy-mm-dd",
		changeYear: true,
		changeMonth: true,
		currentText: "Now",
		gotoCurrent: true,
		showAnim: "slideDown"
	});
	
	$(".datepicker").each(function(){
		try{
			var datepicker = this;
			if($(datepicker).val()){
				$(datepicker).datepicker( "setDate", $.datepicker.formatDate('DD, d MM, yy', new Date($(datepicker).val())));
			}
		}catch(error){
			console.log(error);
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .group-responsive>div", function(){
		if($(this).find("input[type='text']").is(":visible")){
			$(this).find("input[type='text']").focus();
		}
	});
	
	$table.on("click", ".templatemo-image-link", function(e){
		if($(".preview").find("img").length){
			$(".preview").find("img").attr("src", $(this).find("img").attr("src"));
			$(".preview").show();
		}else{
			var image = addImage().attr("src", $(this).find("img").attr("src"));
			$(".preview").append(image).show();
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo img", function(e){
		if($(".preview").find("img").length){
			$(".preview").find("img").attr("src", $(this).attr("src"));
			$(".preview").show();
		}else{
			var image = addImage().attr("src", $(this).attr("src"));
			$(".preview").append(image).show();
		}
	});
	
	$(".preview").on("click", ".close", function(){
		$(".preview").hide();
	});
	
	$(".umbrella").on("click", ".close", function(){
		close();
	});
	
	$(".auth-expired").on("click", ".header span.close", function(){
		$(".auth-expired").hide();
	});
	
	$(".server-down").on("click", ".buttons .ok,.header span.close", function(){
		$(".server-down").hide();
	});
	
	$(document).mousedown(function(event) {
	    switch (event.which) {
	        case 1:
	        	// Left Mouse
	        	if(!$(event.target).is(".custom-menu li")){
	        		if($(".custom-menu").is(":visible")){
		        		$(".custom-menu").hide().trigger("change");
		        		return;
		        	}
	        	}
	            break;
	        case 2:
	        	// Middle Mouse
	            break;
	        case 3:
	        	// Right Mouse
	            break;
	        default:
	        	console.log('You have a strange Mouse!');
	    }
	});
	
	$(document).keyup(function(e) {
		// enter
		if (e.keyCode === 13){

		}
		// esc
		if (e.keyCode === 27){
			 close();
		}
	});
	
})

function handleError(xhr){
	if(xhr && xhr.status == 403){
		if($(".auth-expired").is(":hidden")){
			$(".auth-expired").show();
		}
	}else if(xhr && xhr.status == 503){
		if($(".server-down").is(":hidden")){
			$(".server-down").show();
		}
	}else{
		toast(xhr.responseJSON.message);
	}
}

function ajaxCall(url, method, body, headers){
	var call;
	switch(method){
		case $HTTP_METHOD.GET:
			 call = $.ajax({
				url: url,
				type: method,
				headers: headers
			 });
			break;
		default:
			call = $.ajax({
				url: url,
				type: method,
				headers: headers,
				data: JSON.stringify(body),
				contentType: 'application/json; charset=utf-8'
			 });
	}
	
	return call;
}

function mutipartCall(url, method, file, type, name){
	var data = new FormData();
	 data.append('file', file);
	 data.append('type', type);
	 data.append("name", name);
	var call = $.ajax({
		    url: url,
		    data: data,
		    cache: false,
		    contentType: false,
		    processData: false,
		    method: 'POST',
		    type: 'POST', // For jQuery < 1.9
		});
	return call;
}

function addNewMetaRow(meta){
	var selector = getSelector();
	var rows = $preview.find(".preview-widget-content .table-responsive table>tbody>tr").length;
	if(rows == selector.find("option").size()){
		return;
	}
	var selectedOptions = [];
	var nextVal = "en";
	$.each($preview.find(".preview-widget-content .table-responsive table>tbody>tr>td:nth-child(3)>select>option:selected"), function(){
		selectedOptions.push($(this));
	});
	selector.find("option").each(function(){
		var found = false;
		var value = $(this).attr("value");
		$.each(selectedOptions, function(){
			if(value == $(this).attr("value")){
				found = true;
			}
		});
		if(!found){
			nextVal = value;
			return false;
		}
	});
	var row = addTableRow(4).attr("data-id", meta.id?meta.id:'');
	row.children(":first-child()").attr("contenteditable", true).text((meta.name)?meta.name:'');
	row.children(":nth-child(2)").attr("contenteditable", true).text((meta.description)?meta.description:'');
	row.children(":nth-child(3)").append(selector.val((meta.lang)?meta.lang:nextVal));
	row.children(":last-child()").append(addDeleteButton());
	$preview.find(".preview-widget-content .table-responsive table>tbody:last-child").append(row);
}

function reset(){
	$confirmation.off("click", ".confirm-buttons .confirm-cancel-button");
	$confirmation.off("click", ".confirm-buttons .confirm-agree-button");
}

function close(){
	if($(".custom-menu").is(":visible")){
		$(".custom-menu").hide().trigger("change");
		return;
	}
	if($(".preview").is(":visible")){
		$(".preview").hide();
		return;
	}
	if($(".umbrella").is(":visible")){
		$(".umbrella").hide();
	}
	if($(".search-result").is(":visible")){
		$(".search-result").hide();
	}
}

function deleteElement(elementId){
	$("#"+elementId).remove();
}

function addTableRow(cells){
	var row = $("<tr></tr>");
	for(counter=0; counter<cells; counter++){
		row.append(addTableCell());
	}
	return row;
}

function addTableCell(){
	var cell = $("<td></td>");
	return cell;
}

function addDeleteButton(){
	var button = $("<a></a>").attr("href", "javascript:").attr("class", "templatemo-link templatemo-meta-delete-link").text("Delete");
	return button;
}

function addDelButton(){
	var button = $("<a></a>").attr("href", "javascript:").attr("class", "templatemo-link templatemo-del-link").text("Delete");
	return button;
}

function addLink(){
	var link = $("<a></a>")//.attr("href", "javascript:");
	return link;
}

function addVideo(){
	var video = $("<video controls></video>");
	return video;
}

function addAudio(){
	var audio = $("<audio controls></audio>");
	return audio;
}

function addObject(){
	var object = $("<object></object>");
	return object;
}

function addTrack(){
	var track = $("<track></track>");
	return track;
}

function addImage(){
	var img = $("<img></img>");
	return img;
}

function addSource(){
	var source = $("<source></source>");
	return source;
}

function addDiv(){
	var div = $("<div></div>");
	return div;
}

function addCheckbox(){
	var checkbox = $("<input type=\"checkbox\"/>");
	return checkbox;
}

function addSpan(){
	var span = $("<span></span>");
	return span;
}

function addTextBox(){
	var box = $("<input type=\"text\"/>");
	return box;
}

function addTextArea(){
	var textArea = $("<textarea></textarea>");
	return textArea;
}

function addList(){
	var list = $("<ul></ul>");
	return list;
}

function listItem(){
	var item = $("<li></li>");
	return item;
}

function addSelect(){
	var selector = $("<select></select>");
	return selector;
}

function getSelector(){
	var selector = $("<select></select>");
	languages.forEach(function(language, index){
		var option = $("<option></option>").attr("value", language.code).text(language.code);
		selector.append(option);
	});
	return selector;
}

function randomNumberFromRange(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

function toast(message){
	toastr.error(message, 'Error', {positionClass: 'toast-bottom-right'});
}
function info(message){
	toastr.info(message, 'Warning', {positionClass: 'toast-bottom-right'});
}
