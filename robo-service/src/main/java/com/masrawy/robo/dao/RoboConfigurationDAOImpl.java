package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboConfigurationVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboConfigurationDAO")
public class RoboConfigurationDAOImpl extends AbstractWebserviceDAO<RoboConfigurationVO, String> implements RoboConfigurationDAO{

	public RoboConfigurationDAOImpl(){
		super(RoboConfigurationVO.class, new ParameterizedTypeReference<List<RoboConfigurationVO>>() {});
	}
}
