package com.masrawy.robo.services;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dto.RoboArtistDTO;
import com.masrawy.robo.models.RoboArtistVO;

public interface RoboArtistService extends PgrlService<RoboArtistVO, RoboArtistDTO, Long>{

	public RoboArtistDTO createArtist(RoboArtistDTO roboArtistDTO);
	public RoboArtistDTO updateArtist(RoboArtistDTO roboArtistDTO);
	public RoboArtistDTO uploadPhoto(MultipartFile file, Long artistId, String name);
	public RoboArtistDTO getArtist(Long albumId);
	public List<RoboArtistDTO> getArtists(Integer page, Integer limit);
	public Map<String, String> getSocials();
	public void deleteArtist(Long artistId);
}
