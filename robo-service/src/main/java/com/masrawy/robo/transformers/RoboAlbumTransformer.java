package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboAlbumDTO;
import com.masrawy.robo.models.RoboAlbumVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboAlbumTransformer extends DTOTransformer<RoboAlbumVO, RoboAlbumDTO>{

}
