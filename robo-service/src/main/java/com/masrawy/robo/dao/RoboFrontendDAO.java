package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboFrontendVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboFrontendDAO extends DAO<RoboFrontendVO, Long>{

}
