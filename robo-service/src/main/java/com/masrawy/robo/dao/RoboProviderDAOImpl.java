package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboProviderVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboProviderDAO")
public class RoboProviderDAOImpl extends AbstractWebserviceDAO<RoboProviderVO, Long> implements RoboProviderDAO{

	public RoboProviderDAOImpl(){
		super(RoboProviderVO.class, new ParameterizedTypeReference<List<RoboProviderVO>>(){});
	}
}
