package com.masrawy.robo.services;

import java.util.List;

import com.masrawy.robo.dto.RoboConfigurationDTO;
import com.masrawy.robo.models.RoboConfigurationVO;

public interface RoboConfigurationService extends PgrlService<RoboConfigurationVO, RoboConfigurationDTO, String>{

	public List<RoboConfigurationDTO> getConfigurationList();
	
	public void updateConfiguration(RoboConfigurationDTO roboConfigurationDTO);
}
