package com.masrawy.robo.dto;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class RoboTagDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotEmpty
	@NotBlank
	private String tag;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
}
