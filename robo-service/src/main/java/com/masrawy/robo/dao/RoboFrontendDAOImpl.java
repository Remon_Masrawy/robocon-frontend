package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboFrontendVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboFrontendDAO")
public class RoboFrontendDAOImpl extends AbstractWebserviceDAO<RoboFrontendVO, Long> implements RoboFrontendDAO{

	public RoboFrontendDAOImpl(){
		super(RoboFrontendVO.class, new ParameterizedTypeReference<List<RoboFrontendVO>>() {});
	}
}
