package com.masrawy.robo.models;

import com.masrawy.robo.ws.AbstractVO;

public class RoboOperatorVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String mcc;
	private String mnc;
	private String name;
	private String description;
	private Boolean allowed;
	private RoboCountryVO country;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMcc() {
		return mcc;
	}
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	public String getMnc() {
		return mnc;
	}
	public void setMnc(String mnc) {
		this.mnc = mnc;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getAllowed() {
		return allowed;
	}
	public void setAllowed(Boolean allowed) {
		this.allowed = allowed;
	}
	public RoboCountryVO getCountry() {
		return country;
	}
	public void setCountry(RoboCountryVO country) {
		this.country = country;
	}
}
