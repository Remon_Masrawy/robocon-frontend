package com.masrawy.robo.exceptions;

public class ErrorConstants {

	public static final int ROBO_INTERNAL_ERROR = 500;
	
	public static final int ROBO_VALIDATION_ERROR = 405;
	
	public static final int ROBO_CONFIGURATION_ERROR = 301;
	
	public static final int ROBO_CLIENT_ERROR = 400;
	
	public static final int ROBO_ACCESS_DENIED = 403;
	
	public static final int ROBO_NOT_FOUND = 404;
	
	public static final int ROBO_REQUEST_TIMEOUT = 408;
	
}
