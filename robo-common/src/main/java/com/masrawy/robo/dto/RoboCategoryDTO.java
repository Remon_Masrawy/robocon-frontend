package com.masrawy.robo.dto;

import java.util.List;
import java.util.Map;

import org.hibernate.validator.constraints.NotEmpty;

public class RoboCategoryDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotEmpty
	private List<RoboMetaDTO> meta;
	@NotEmpty
	private Map<Long, String> storefronts;
	private Map<Long, Map<String, String>> operators;
	private Map<Long, String> countries;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaDTO> meta) {
		this.meta = meta;
	}
	public Map<Long, String> getStorefronts() {
		return storefronts;
	}
	public void setStorefronts(Map<Long, String> storefronts) {
		this.storefronts = storefronts;
	}
	public Map<Long, Map<String, String>> getOperators() {
		return operators;
	}
	public void setOperators(Map<Long, Map<String, String>> operators) {
		this.operators = operators;
	}
	public Map<Long, String> getCountries() {
		return countries;
	}
	public void setCountries(Map<Long, String> countries) {
		this.countries = countries;
	}
}
