package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.ARTISTS_PAGE;
import static com.masrawy.robo.constants.RoboPages.ARTIST_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboArtistDTO;
import com.masrawy.robo.services.RoboArtistService;
import com.masrawy.robo.services.RoboLanguageService;

@Controller
@RequestMapping(value = "/web/artist")
public class ArtistController {

	@Autowired
	private RoboArtistService roboArtistService;
	
	@Autowired
	private RoboLanguageService roboLanguageService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView artistsPage(){
		ModelAndView model = new ModelAndView(ARTISTS_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{artistId}", method = RequestMethod.GET)
	public ModelAndView artistPage(@PathVariable("artistId") String artistId){
		ModelAndView model = new ModelAndView(ARTIST_PAGE);
		try{
			Long id = Long.valueOf(artistId);
			if(id != 0){
				model.addObject("artist", roboArtistService.getArtist(id));
			}
			model.addObject("languages", roboLanguageService.getLanguageList(0, 0));
		}catch(Exception e){
			model = new ModelAndView("redirect:/web/artist");
		}
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboArtistDTO artistCreate(@RequestBody RoboArtistDTO roboArtistDTO){
		return roboArtistService.createArtist(roboArtistDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboArtistDTO artistUpdate(@RequestBody RoboArtistDTO roboArtistDTO){
		return roboArtistService.updateArtist(roboArtistDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{artistId}", method = RequestMethod.POST)
	public @ResponseBody RoboArtistDTO artistPhoto(@RequestParam(value = "file", required = true) MultipartFile file, @RequestParam(value = "name", required = true) String name, @NotNull @PathVariable("artistId") Long artistId){
		return roboArtistService.uploadPhoto(file, artistId, name);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboArtistDTO> artists(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboArtistService.getArtists(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/socials", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> socials(){
		return roboArtistService.getSocials();
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{artistId}", method = RequestMethod.DELETE)
	public @ResponseBody void artistDelete(@PathVariable("artistId") Long artistId){
		roboArtistService.deleteArtist(artistId);
	}
}
