<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:url var="dashboard" value="/web/dashboard"/>
<c:url var="songs" value="/web/song"/>
<c:url var="artists" value="/web/artist"/>
<c:url var="album" value="/web/album"/>
<c:url var="events" value="/web/event"/>
<c:url var="provider" value="/web/provider"/>
<c:url var="genre" value="/web/genre"/>
<c:url var="frontend" value="/web/frontend"/>
<c:url var="category" value="/web/category"/>
<c:url var="configuration" value="/web/configuration"/>
<c:url var="language" value="/web/language/0/0"/>
<c:url var="logout" value="/logout"/>

<header class="templatemo-site-header">
	<div class="square"></div>
	<h1><spring:message code="page.header"/></h1>
</header>
<div class="profile-photo-container">
	<img src='<c:url value="/images/profile/mazika_2.jpg"/>' alt="Profile Photo"	class="img-responsive">
	<div class="profile-photo-overlay"></div>
</div>
<!-- Search box -->
<div class="templatemo-search-form">
	<div class="input-group">
		<button type="button" class="fa fa-search"></button>
		<input type="text" class="form-control search-input" placeholder="Search" name="srch-term" id="srch-term">
		<button type="button" class="fa fa-cog"></button>
	</div>
</div>
<div class="mobile-menu-icon">
	<i class="fa fa-bars"></i>
</div>
<nav class="templatemo-left-nav">
	<ul>
		<li><a id="dashboard" href="${dashboard}"><i class="fa fa-home fa-fw"></i><spring:message code="robo-pages.dashboard-keyword"/></a></li>
		<sec:authorize access="hasRole('ROLE_CONTENT_ADMIN')">
			<li><a id="songs" href="${songs}"><i class="fa fa-music fa-fw"></i><spring:message code="robo-pages.songs-keyword"/></a></li>
			<li><a id="albums" href="${album}"><i class="fa fa-folder fa-fw"></i><spring:message code="robo-pages.albums-keyword"/></a></li>
			<li><a id="artists" href="${artists}"><i class="fa fa-user fa-fw"></i><spring:message code="robo-pages.artists-keyword"/></a></li>
			<li><a id="events" href="${events}"><i class="fa fa-calendar-alt fa-fw"></i><spring:message code="robo-pages.events-keyword"/></a></li>
			<li><a id="frontend" href="${frontend}"><i class="fa fa-window-restore fa-fw"></i><spring:message code="robo-pages.frontends-keyword"/></a></li>
			<li><a id="genre" href="${genre}"><i class="fa fal fa-bullhorn fa-fw"></i><spring:message code="robo-pages.genres-keyword"/></a></li>
			<li><a id="provider" href="${provider}"><i class="fa fa-building fa-fw"></i><spring:message code="robo-pages.providers-keyword"/></a></li>
			<li><a id="category" href="${category}"><i class="fa fa-list-alt fa-fw"></i><spring:message code="robo-pages.categories-keyword"/></a></li>
		</sec:authorize>
		<sec:authorize access="hasRole('ROLE_SYSTEM')">
			<li><a id="configuration" href="${configuration}"><i class="fa fa-sliders-h fa-fw"></i><spring:message code="menu.configurations"/></a></li>
		</sec:authorize>
		<li><a href="${logout}"><i class="fa fa-eject fa-fw"></i><spring:message code="menu.sign-out"/></a></li>
	</ul>
</nav>