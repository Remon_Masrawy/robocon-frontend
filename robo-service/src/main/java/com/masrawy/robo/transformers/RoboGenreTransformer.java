package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboGenreDTO;
import com.masrawy.robo.models.RoboGenreVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboGenreTransformer extends DTOTransformer<RoboGenreVO, RoboGenreDTO>{

}
