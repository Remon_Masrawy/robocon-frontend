
$(document).ready(function(){
	$container 	= $(".templatemo-content-container");
	
	$table 		= $(".templatemo-content-container .templatemo-content-widget .table.data-table");
	
	$preview 		= $(".templatemo-content-container .preview-widget");
	
	$confirmation = $(".confirmation");
	
	$container.on("click", ".new-btn", function(){
		window.location.href = $provider+"/0";
	});
	
	$table.find(".templatemo-link.templatemo-del-link").click(function(){
		var id = $(this).closest("tr").attr("data-id");
		deleteProvider(id);
	});
	
	$preview.find(".preview-widget-buttons>.new-button").click(function(){
		addNewMetaRow({});
	});
	
	$preview.find(".preview-widget-resposive-buttons>div.save-button").click(function(){
		saveProvider();
	});
	
	$preview.delegate("a.templatemo-link.templatemo-meta-delete-link", "click", function(){
		$(this).closest("tr").remove();
	})
	
	if($container.find(".data-table").hasClass("providers-table")){
		$enablePanel = true;
	}else{
		$enablePagination = false;
		$enablePanel = false;
	}
	
});

$fill = function fillProvider(){
	$.when(ajaxCall($provider+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, provider){
			var url = $provider+"/"+provider.id;
			var row = addTableRow(5).attr("data-id", provider.id);
			row.children("td:first-child()").addClass("tc").text(provider.id);
			row.children("td:nth-child(2)").append(addLink().attr("href", url).addClass("templatemo-keyword-link").text(provider.meta[0].name));
			row.children("td:nth-child(3)").addClass("tc").append(addLink().attr("href", url).addClass("templatemo-edit-btn").text("Edit"));
			row.children("td:nth-child(4)").addClass("tc").append(addLink().addClass("templatemo-link").text("Action"));
			row.children("td:last-child()").addClass("tc").append(addLink().addClass("templatemo-link templatemo-del-link").text("Delete"));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveProvider(){
	var valid = validate();
	if(!valid){
		return;
	}
	var provider = {};
	provider.id = parseInt($preview.attr("data-id"));
	var metaList = new Array();
	$preview.find(".preview-widget-content table>tbody").children("tr").each(function(){
		var meta = {};
		if($(this).attr("data-id")){
			meta.id = parseInt($(this).attr("data-id").trim());
		}
		meta.name = $(this).children(":nth-child(1)").text().trim();
		meta.description = $(this).children(":nth-child(2)").text().trim();
		meta.lang = $(this).children(":nth-child(3)").children("select").val().trim();
		metaList.push(meta);
	});
	provider.meta = metaList;
	var method;
	if($preview.attr("data-id")){
		method = $HTTP_METHOD.PUT;
	}else{
		method = $HTTP_METHOD.POST;
	}
	
	$.when(ajaxCall($provider, method, provider, headers)).then(function(data){
		$preview.find(".preview-widget-content table>tbody").empty();
		$preview.attr("data-id", data.id);
		data.meta.forEach(function(meta, index){
			addNewMetaRow(meta);
		});
		toastr.success("Provider Successfully Saved.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	});
}

function deleteProvider(providerId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteProvider(providerId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteProvider(providerId){
	
	$.when(ajaxCall($provider+"/"+providerId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+providerId+"']").remove();
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function validate(){
	var valid = true;
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(2)").is(":empty")){
			$(this).children("td:nth-child(2)").addClass("error");
			valid = false;
		}
	});
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(3)").is(":empty")){
			$(this).children("td:nth-child(3)").addClass("error");
		}
	});
	
	return valid;
}

