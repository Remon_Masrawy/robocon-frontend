package com.masrawy.robo.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dto.RoboAuthDTO;
import com.masrawy.robo.models.RoboAuthVO;

public interface RoboUserService extends PgrlService<RoboAuthVO, RoboAuthDTO, Long>{

	public RoboAuthDTO getUser();
	public RoboAuthDTO getUser(Long userId);
	public RoboAuthDTO updateUser(RoboAuthDTO roboAuthDTO);
	public RoboAuthDTO updateUserInfo(RoboAuthDTO roboAuthDTO);
	public RoboAuthDTO updateUserPhoto(MultipartFile file, String name);
	public List<RoboAuthDTO> getUsersList(Integer page, Integer limit);
	public void deleteUser(Long id);
}
