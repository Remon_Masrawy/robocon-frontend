package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboProviderDTO;
import com.masrawy.robo.models.RoboProviderVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboProviderTransformer extends DTOTransformer<RoboProviderVO, RoboProviderDTO>{

}
