package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dao.RoboSongDAO;
import com.masrawy.robo.dto.RoboSongDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.exceptions.RoboSongException;
import com.masrawy.robo.model.RoboMimeTypeEnum;
import com.masrawy.robo.models.RoboSongVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboSongTransformer;

@Service("roboSongService")
public class RoboSongServiceImpl extends AbstractPgrlServiceImpl<RoboSongVO, RoboSongDTO, Long> implements RoboSongService{

	@Autowired
	private RoboSongDAO roboSongDAO;
	
	@Autowired
	private RoboSongTransformer roboSongTransformer;
	
	@Value("${robocon.ws.song.url}")
	private String songURL;
	
	@Value("${robocon.ws.play-content.url}")
	private String playContentURL;
	
	@Value("${robocon.ws.print-lyrics.url}")
	private String printLyricsURL;
	
	public RoboSongDTO createSong(RoboSongDTO roboSongDTO){
		logger.debug("Robocon request for creatting song");
		try{
			roboSongDTO = postObject(songURL, roboSongDTO);
			return roboSongDTO;
		}catch(RoboResponseException ex){
			logger.error("Song creation error occured.", ex);
			throw new RoboSongException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Song creation error occured.", ex);
			throw new RoboSongException(ex.getMessage());
		}
	}
	
	public RoboSongDTO updateSong(RoboSongDTO roboSongDTO){
		logger.debug("Robocon request for updatting song");
		try{
			roboSongDTO = updateR(songURL, roboSongDTO);
			return roboSongDTO;
		}catch(RoboResponseException ex){
			logger.error("Song update error occured.", ex);
			throw new RoboSongException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Song update error occured.", ex);
			throw new RoboSongException(ex.getMessage());
		}
	}
	
	@Override
	public RoboSongDTO uploadFile(MultipartFile file, Long songId, String type, String name){
		logger.debug("Robocon request for uploading album photo");
		try{
			RoboMimeTypeEnum roboMimeTypeEnum = RoboMimeTypeEnum.valueOf(type);
			RoboSongDTO song = postFile(String.format(fileUploadURL, songId, roboMimeTypeEnum.getIntValue()), file, name);
			return song;
		}catch(RoboResponseException ex){
			logger.error("Song update error occured.", ex);
			throw new RoboSongException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Song update error occured.", ex);
			throw new RoboSongException(ex.getMessage());
		}
	}
	
	@Override
	public void getContent(Long contentId){
		logger.debug("Robocon request for play content");
		try{
			execute(String.format(playContentURL, contentId));
		}catch(RoboResponseException ex){
			logger.error("Song get content error occured.", ex);
			throw new RoboSongException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Song get content occured.", ex);
			throw new RoboSongException(ex.getMessage());
		}
	}
	
	@Override
	public void getLyrics(Long lyricsId){
		logger.debug("Robocon request for print lyrics");
		try{
			execute(String.format(printLyricsURL, lyricsId));
		}catch(RoboResponseException ex){
			logger.error("Song print lyrics error occured.", ex);
			throw new RoboSongException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Song print lyrics error occured.", ex);
			throw new RoboSongException(ex.getMessage());
		}
	}
	
	@Override
	public RoboSongDTO getSong(Long songId){
		logger.debug("Robocon request for getting song");
		try{
			RoboSongDTO song = getById(songURL, songId);
			return song;
		}catch(RoboResponseException ex){
			logger.error("Song retrieve error occured.", ex);
			throw new RoboSongException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Song retrieve error occured.", ex);
			throw new RoboSongException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboSongDTO> getSongs(Integer page, Integer limit){
		logger.debug("Robocon request for getting song list");
		List<RoboSongDTO> songs = getList(String.format(pagingURL, songURL, page, limit));
		return songs;
	}
	
	@Override
	public List<RoboSongDTO> searchSongs(String keyword, Integer page, Integer limit){
		logger.debug("Robocon request for searching songs for keyword({})", keyword);
		List<RoboSongDTO> songs = getList(String.format(searchURL, songURL, page, limit, keyword));
		return songs;
	}
	
	@Override
	public void deleteSong(Long songId){
		logger.debug("Robocon request for deleting song");
		try{
			deleteById(songURL, songId);
		}catch(RoboResponseException ex){
			logger.error("Song delete error occured.", ex);
			throw new RoboSongException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Song delete error occured.", ex);
			throw new RoboSongException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboSongVO, RoboSongDTO> getTransformer() {
		return roboSongTransformer;
	}

	@Override
	protected DAO<RoboSongVO, Long> getDAO() {
		return roboSongDAO;
	}

}
