<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="new-btn"><a href="javascript:"><spring:message code="robo-pages.new-keyword"/></a></div>
<div class="templatemo-content-widget no-padding">
	<div class="panel panel-default table-responsive">
		<table class="table table-striped table-bordered templatemo-user-table data-table frontends-table">
			<thead>
				<tr>
					<td class="w-50"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.id-keyword"/> <span class="caret"></span></a></td>
					<td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.title-keyword"/> <span class="caret"></span></a></td>
					<td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.type-keyword"/> <span class="caret"></span></a></td>
					<td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.category-keyword"/> <span class="caret"></span></a></td>
					<td class="w-130"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.content-count-keyword"/> <span class="caret"></span></a></td>
					<td class="w-100"><spring:message code="robo-pages.edit-keyword"/></td>
					<td class="w-100"><spring:message code="robo-pages.delete-keyword"/></td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
	
<script type="text/javascript" src='<c:url value="/js/frontend.js"/>'></script>