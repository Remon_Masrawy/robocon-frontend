package com.masrawy.robo.dto;

import org.hibernate.validator.constraints.NotBlank;

public class RoboSocialDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotBlank
	private String social;
	@NotBlank
	private String link;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSocial() {
		return social;
	}
	public void setSocial(String social) {
		this.social = social;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
}
