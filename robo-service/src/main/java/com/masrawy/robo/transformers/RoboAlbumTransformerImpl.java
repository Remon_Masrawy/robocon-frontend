package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboAlbumDTO;
import com.masrawy.robo.models.RoboAlbumVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboAlbumTransformer")
public class RoboAlbumTransformerImpl extends AbstractDTOTransformer<RoboAlbumVO, RoboAlbumDTO> implements RoboAlbumTransformer{

	public RoboAlbumTransformerImpl(){
		super(RoboAlbumVO.class, RoboAlbumDTO.class);
	}
}
