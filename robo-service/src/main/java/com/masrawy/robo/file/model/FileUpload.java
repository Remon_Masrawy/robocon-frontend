package com.masrawy.robo.file.model;

import java.io.Serializable;

public class FileUpload implements Serializable{

	private static final long serialVersionUID = 1L;

	private byte[] bytes;
	
	private String filename;
	private String originalFileName;
	
	private Integer fileType;

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public Integer getFileType() {
		return fileType;
	}

	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}

}
