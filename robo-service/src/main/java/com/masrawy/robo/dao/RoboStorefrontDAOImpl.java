package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboStorefrontVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboStorefrontDAO")
public class RoboStorefrontDAOImpl extends AbstractWebserviceDAO<RoboStorefrontVO, Long> implements RoboStorefrontDAO{

	public RoboStorefrontDAOImpl(){
		super(RoboStorefrontVO.class, new ParameterizedTypeReference<List<RoboStorefrontVO>>() {});
	}
}
