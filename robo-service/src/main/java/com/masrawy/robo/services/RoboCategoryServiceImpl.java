package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboCategoryDAO;
import com.masrawy.robo.dto.RoboCategoryDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboCategoryException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.models.RoboCategoryVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboCategoryTransformer;

@Service("roboCategoryService")
public class RoboCategoryServiceImpl extends AbstractPgrlServiceImpl<RoboCategoryVO, RoboCategoryDTO, Long>  implements RoboCategoryService{

	@Autowired
	private RoboCategoryDAO roboCategoryDAO;
	@Autowired
	private RoboCategoryTransformer roboCategoryTransformer;
	
	@Value("${robocon.ws.category.url}")
	private String categoryURL;
	
	@Override
	public RoboCategoryDTO createCategory(RoboCategoryDTO roboCategoryDTO){
		logger.debug("Robocon request for creating category");
		try{
			roboCategoryDTO = postObject(categoryURL, roboCategoryDTO);
			return roboCategoryDTO;
		}catch(RoboResponseException ex){
			logger.error("Category creation error occured.", ex);
			throw new RoboCategoryException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Category creation error occured.", ex);
			throw new RoboCategoryException(ex.getMessage());
		}
	}
	
	@Override
	public RoboCategoryDTO updateCategory(RoboCategoryDTO roboCategoryDTO){
		logger.debug("Robocon request for updating category");
		try{
			roboCategoryDTO = updateR(categoryURL, roboCategoryDTO);
			return roboCategoryDTO;
		}catch(RoboResponseException ex){
			logger.error("Category creation error occured.", ex);
			throw new RoboCategoryException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Category creation error occured.", ex);
			throw new RoboCategoryException(ex.getMessage());
		}
	}
	
	@Override
	public RoboCategoryDTO getCategory(Long categoryId){
		logger.debug("Robocon request for getting category");
		RoboCategoryDTO category = getById(categoryURL, categoryId);
		return category;
	}
	
	@Override
	public List<RoboCategoryDTO> getCategoryList(Integer page, Integer limit){
		logger.debug("Robocon request for getting categorys list");
		List<RoboCategoryDTO> categorys = getList(String.format(pagingURL, categoryURL, page, limit));
		return categorys;
	}
	
	@Override
	public void deleteCategory(Long categoryId){
		logger.debug("Robocon request for deleting category");
		try{
			deleteById(categoryURL, categoryId);
		}catch(RoboResponseException ex){
			logger.error("Category creation error occured.", ex);
			throw new RoboCategoryException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Category creation error occured.", ex);
			throw new RoboCategoryException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboCategoryVO, RoboCategoryDTO> getTransformer() {
		return roboCategoryTransformer;
	}

	@Override
	protected DAO<RoboCategoryVO, Long> getDAO() {
		return roboCategoryDAO;
	}

}
