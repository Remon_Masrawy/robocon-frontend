package com.masrawy.robo.models;

import com.masrawy.robo.ws.AbstractVO;

public class RoboLanguageVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private String code;

	private String name;

	private String locale;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
}
