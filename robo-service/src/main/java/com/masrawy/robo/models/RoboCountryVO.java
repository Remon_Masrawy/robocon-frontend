package com.masrawy.robo.models;

import com.masrawy.robo.ws.AbstractVO;

public class RoboCountryVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String iso;
	private String code;
	private String name;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIso() {
		return iso;
	}
	public void setIso(String iso) {
		this.iso = iso;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
