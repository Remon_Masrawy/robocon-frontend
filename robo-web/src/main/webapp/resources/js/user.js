$(document).ready(function(){
	
	$profile 			= $(".profile");
	$confirmation 		= $(".confirmation");
	$enablePagination = false;
	$enablePanel = false;
	
	$table = $profile.find(".tab_users .users .table.templatemo-user-table");
	
	$profile.on("click", ".l-collection .l-nav .collectionNav .g-tabs-item a.g-tabs-link", function(){
		$profile.find(".l-collection .l-nav .collectionNav .g-tabs-item a.g-tabs-link.active").removeClass("active");
		$(this).addClass("active");
		$profile.find("section").hide();
		if($(this).closest("li").hasClass("networkTabs__info")){
			$profile.find("section.tab_info").show();
		}else if($(this).closest("li").hasClass("networkTabs__users")){
			$profile.find("section.tab_users").show();
		}
	});
	
	$profile.on("change", "input[value='ROLE_SUPER']", function(){
		if(this.checked) {
	        $(this).closest(".row.form-group").find("input[type='checkbox']:not(input[value='ROLE_SUPER'])").prop('checked', true).attr("disabled", true);
	    }else{
	    	$(this).closest(".row.form-group").find("input[type='checkbox']:not(input[value='ROLE_SUPER'])").attr("disabled", false);
	    }
	});
	
	$profile.on("blur", "input[id='inputPassword'], input[id='inputConfirmPassword']", function(){
		var pass = $("input[id='inputPassword']").val();
	    var repass = $("input[id='inputConfirmPassword']").val();
	    if((pass == 0) || (repass == 0)){
	        $('#inputPassword').addClass('has-error');
	    }
	    else if (pass != repass) {
	        $('#inputPassword').addClass('has-error');
	        $('#inputConfirmPassword').addClass('has-error');
	    }
	    else {
	        $('#inputPassword').removeClass('has-error').addClass('has-success');
	        $('#inputConfirmPassword').removeClass('has-error').addClass('has-success');
	    }
	});
	
	$profile.on("input", "input[id='inputNewPassword'], input[id='inputConfirmNewPassword']", function(){
		var pass = $("input[id='inputNewPassword']").val();
	    var repass = $("input[id='inputConfirmNewPassword']").val();
	    if((pass == 0) && (repass == 0)){
	    	$('#inputNewPassword').removeClass('has-error').removeClass("has-success");
	    	$('#inputConfirmNewPassword').removeClass('has-error').removeClass("has-success");
	    }else if (repass != pass) {
	        $('#inputConfirmNewPassword').addClass('has-error').removeClass("has-success");
	    }
	    else {
	        $('#inputNewPassword').removeClass('has-error').addClass('has-success');
	        $('#inputConfirmNewPassword').removeClass('has-error').addClass('has-success');
	    }
	});
	
	$profile.on("click", ".tab_info .user_info button[type='button']", function(){
		updateInfo();
	});
	
	$profile.on("click", ".tab_users .users .table.templatemo-user-table .templatemo-edit-btn", function(){
		$profile.find(".tab_users .users").slideUp();
		var id = $(this).closest("tr").attr("data-id");
		editUser(id);
	});
	
	$profile.on("click", ".tab_users .users .profile-widget-buttons .new-button", function(){
		$("#inputUsername").prop('disabled', false);;
		$("#inputEmail").prop('disabled', false);
		$profile.find(".tab_users .users").slideUp();
		$profile.find(".tab_users .edit_user").slideDown();
	});
	
	$profile.on("click", ".tab_users button[type='button'].save", function(){
		saveUser();
	});
	
	$profile.on("click", ".tab_users button[type='button'].cancel", function(){
		$profile.find(".tab_users .edit_user").slideUp();
		$profile.find(".tab_users .users").slideDown();
		$profile.find(".tab_users .edit_user").removeAttr("data-id");
		$('#edit_user')[0].reset();
	});
	
	$profile.on("click", ".tab_users .users .table.templatemo-user-table tr>td>a.templatemo-del-link", function(){
		var id = $(this).closest("tr").attr("data-id");
		if($.isNumeric(id)){
			deleteUser(id);
		}
	});
	
	$profile.on("click", ".profile-photo-group .profile-photo", function(){
		$(this).closest(".profile-photo-group").find("input[type='file']").click();
	});
	
	$profile.on("change", ".profile-photo-group input[type='file']", function(event){
		if($(this).val().length != 0){
			var image = addImage().attr("src", URL.createObjectURL(event.target.files[0]));
			$(this).closest(".profile-photo-group").find("div.profile-photo img").remove();
			$(this).closest(".profile-photo-group").find("div.profile-photo").append(image);
		}
	});
	
	$profile.on("hover", ".profile-photo-group .profile-photo .add-sign", function(e){
		if(e.type == "mouseenter") {
				$(this).closest(".profile-photo").find("img").css({"opacity": 0.7});
		  }
		  else if (e.type == "mouseleave") {
			  $(this).closest(".profile-photo").find("img").css({"opacity": 1});
	}});
	
	$info();
	if($("li.networkTabs__users").is(":visible")){
		$fill();
	}
});

$info = function loadInfo(){
	$.when(ajaxCall($user+"/info", $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$profile.find(".tab_info .user_info").attr("data-id", res.id);
		$("#inputFirstName").val(res.firstName);
		$("#inputLastName").val(res.lastName);
		$("#inputNewUsername").val(res.username);
		$("#inputNewEmail").val(res.email);
		if(res.photo){
			var image = addImage().attr("src", res.photo.replace("{width}", 200).replace("{height}", 200)).attr("uploaded", "true");
			$profile.find(".tab_info .user_info .profile-photo-group .profile-photo").append(image);
		}
	},function(xhr){
		handleError(xhr);
	});
	
}

$fill = function loadUsers(){
	$.when(ajaxCall($user+"/0/0", $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, user){
			var row = addTableRow(7).attr("data-id", user.id);
			row.children("td:first-child()").append(user.id);
			row.children("td:nth-child(2)").append(user.firstName);
			row.children("td:nth-child(3)").append(user.lastName);
			row.children("td:nth-child(4)").append(user.username);
			row.children("td:nth-child(5)").append(user.email);
			row.children("td:nth-child(6)").addClass("tc").append(addLink().addClass("templatemo-edit-btn").text("Edit"));
			row.children("td:last-child()").addClass("tc").append(addLink().addClass("templatemo-link templatemo-del-link").text("Delete"));
			if(user.id == 2){
				row.children("td:last-child()").find("a").addClass('disabled');
			}
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
	},
	function(xhr){
		handleError(xhr)
	});
}

function editUser(id){
	$.when(ajaxCall($user+"/"+id, $HTTP_METHOD.GET, {}, headers)).then(function(data){
		$profile.find(".tab_users .edit_user").attr("data-id", data.id);
		$("#inputUsername").val(data.username).prop('disabled', true);;
		$("#inputEmail").val(data.email).prop('disabled', true);
		$("input[name='role']").prop('checked', false);
		$.each(data.roleCodes, function(index, role){
			$("input[value='"+role+"']").prop('checked', true);
		});
		$("input[name='active']").prop('checked', data.isActive);
		$profile.find(".tab_users .edit_user").slideDown();
	},
	function(xhr){
		handleError(xhr)
	});
}

function updateInfo(){
	var valid = validate();
	if(!valid){
		return;
	}
	var user = {};
	user.id = $profile.find(".tab_info .user_info").attr("data-id");
	user.firstName = $("#inputFirstName").val();
	user.lastName = $("#inputLastName").val();
	user.password = $("#inputNewPassword").val();
	$.when(ajaxCall($user+"/info", $HTTP_METHOD.PUT, user, headers)).then(function(res, status, xhr){
		toastr.success("User Info Successfully Updated.", 'Success', {positionClass: 'toast-bottom-right'});
		var image = $profile.find(".profile-photo-group .profile-photo img");
		var uploaded = image.attr("uploaded");
		if(image && (typeof uploaded === typeof undefined || uploaded === false)){
			var xhr = new XMLHttpRequest();
			xhr.open( "GET", image.attr("src"), true );
			xhr.responseType = 'arraybuffer';
			xhr.onload = function( e ) {
				var arrayBufferView = new Uint8Array( this.response );
				var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
				$.when(mutipartCall($user + "/photo", $HTTP_METHOD.POST, blob, type, name)).then(function(data){
					image.attr("uploaded", "true");
					toastr.success("Photo Successfully Uploaded.", 'Success', {positionClass: 'toast-bottom-right'});
				},
				function(error){
					toast(error.responseJSON.message);
				});
			};
			xhr.send();
		}
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveUser(){
	var valid = validate();
	if(!valid){
		return;
	}
	var user = {};
	user.id = $profile.find(".tab_users .edit_user").attr("data-id");
	user.username = $("#inputUsername").val();
	user.email = $("#inputEmail").val();
	user.password = $("#inputPassword").val();
	roles = [];
	$profile.find("input[name='role']:checked").each(function(){
		roles.push($(this).val());
	});
	user.roleCodes = roles;
	user.isActive = $("input[name='active']").is(':checked');
	var method;
	if(user.id){
		method = $HTTP_METHOD.PUT;
	}else{
		method = $HTTP_METHOD.POST;
	}
	
	$.when(ajaxCall($user, method, user, headers)).then(function(res, status, xhr){
		$fill();
		$profile.find(".tab_users .edit_user").slideUp();
		$profile.find(".tab_users .users").slideDown();
		$profile.find(".tab_users .edit_user").removeAttr("data-id");
		$('#edit_user')[0].reset();
	},
	function(xhr){
		handleError(xhr)
	});
}

function deleteUser(id){
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		$.when(ajaxCall($user+"/"+id, $HTTP_METHOD.DELETE, {}, headers)).then(function(res, status, xhr){
			if(xhr.status == 200){
				$profile.find(".tab_users .users .table.templatemo-user-table tr[data-id='"+id+"']").remove();
			}
		},
		function(error){
			console.log(error);
			toast(error.responseJSON.message);
		});
		$confirmation.hide(300);
	});
	
	$confirmation.show(300);
	
}

function validate(){
	if($profile.find("section.tab_info").is(":visible")){
		if($('#inputNewPassword').hasClass('has-error') || $('#inputConfirmNewPassword').hasClass('has-error')){
			toast("Please Handle Errors Before submit.");
			return false;
		}
	}else if($profile.find("section.tab_users").is(":visible")){
		if(!$('#inputUsername').val().trim()){
			toast("Username is required.");
			return false;
		}
		if(!$('#inputEmail').val().trim()){
			toast("Email is required.");
			return false;
		}
		if(!$profile.find(".tab_users .edit_user").attr("data-id")){
			if(!$('#inputPassword').val().trim()){
				toast("Password is required.");
				return false;
			}
		}
		if($('#inputPassword').hasClass('has-error') || $('#inputConfirmPassword').hasClass('has-error')){
			toast("Please Handle Errors Before submit.");
			return false;
		}
	}
	return true;
}