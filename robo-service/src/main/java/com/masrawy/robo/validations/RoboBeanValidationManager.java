package com.masrawy.robo.validations;

import com.masrawy.robo.exceptions.BeanValidationException;

public interface RoboBeanValidationManager {

	public void validate(Object obj) throws BeanValidationException;
}
