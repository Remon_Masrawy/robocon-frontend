package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.USER_PAGE;
import static com.masrawy.robo.constants.RoleConstants.ROLE_SUPER;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboAuthDTO;
import com.masrawy.robo.services.RoboRegistrationService;
import com.masrawy.robo.services.RoboUserService;

@Controller
@RequestMapping(value = "/web/user")
public class UserController {
	
	@Autowired
	private RoboRegistrationService roboRegistrationService;
	
	@Autowired
	private RoboUserService roboUserService;

	@Secured({ CONTENT_ADMIN })
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView userPage(){
		ModelAndView model = new ModelAndView(USER_PAGE);
		return model;
	}
	
	@Secured({ ROLE_SUPER , CONTENT_ADMIN })
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public @ResponseBody RoboAuthDTO getUser(){
		return roboUserService.getUser();
	}
	
	@Secured({ ROLE_SUPER })
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public @ResponseBody RoboAuthDTO getUser(@PathVariable("userId") Long userId){
		return roboUserService.getUser(userId);
	}
	
	@Secured({ ROLE_SUPER })
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboAuthDTO signUp(@RequestBody RoboAuthDTO RoboAuthDTO){
		return roboRegistrationService.signUpUser(RoboAuthDTO);
	}
	
	@Secured({ ROLE_SUPER })
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboAuthDTO updateUser(@RequestBody RoboAuthDTO RoboAuthDTO){
		return roboUserService.updateUser(RoboAuthDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@RequestMapping(value = "/info", method = RequestMethod.PUT)
	public @ResponseBody RoboAuthDTO updateUserInfo(@RequestBody RoboAuthDTO RoboAuthDTO){
		return roboUserService.updateUserInfo(RoboAuthDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@RequestMapping(value = "/photo", method = RequestMethod.POST)
	public @ResponseBody RoboAuthDTO userPhoto(@RequestParam(value = "file", required = true) MultipartFile file, @RequestParam(value = "name", required = true) String name){
		return roboUserService.updateUserPhoto(file, name);
	}
	
	@Secured({ ROLE_SUPER })
	@RequestMapping(value="/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboAuthDTO> listUsers(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboUserService.getUsersList(page, limit);
	}
	
	@Secured({ ROLE_SUPER })
	@RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteUser(@PathVariable("userId") Long userId){
		roboUserService.deleteUser(userId);
	}
}