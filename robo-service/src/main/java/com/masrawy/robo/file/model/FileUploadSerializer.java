package com.masrawy.robo.file.model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;

@JacksonStdImpl
public class FileUploadSerializer extends StdScalarSerializer<FileUpload>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FileUploadSerializer(){
		super(FileUpload.class);
	}

	@Override
	public void serialize(FileUpload value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeObject(value);
	}
}
