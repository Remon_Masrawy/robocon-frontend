package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboSongVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboSongDAO extends DAO<RoboSongVO, Long>{

}
