package com.masrawy.robo.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class RoboOperatorDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotBlank
	private String mcc;
	@NotBlank
	private String mnc;
	@NotBlank
	private String name;
	private String description;
	private Boolean allowed;
	@NotNull
	private RoboCountryDTO country;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMcc() {
		return mcc;
	}
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	public String getMnc() {
		return mnc;
	}
	public void setMnc(String mnc) {
		this.mnc = mnc;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getAllowed() {
		return allowed;
	}
	public void setAllowed(Boolean allowed) {
		this.allowed = allowed;
	}
	public RoboCountryDTO getCountry() {
		return country;
	}
	public void setCountry(RoboCountryDTO country) {
		this.country = country;
	}
}
