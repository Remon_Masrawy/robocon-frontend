package com.masrawy.robo.security;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.client.ResourceAccessException;

import com.masrawy.robo.dto.RoboAuthDTO;
import com.masrawy.robo.services.RoboRegistrationService;

public class CustomUserDetailsByUsernamePasswordService implements AuthenticationUserDetailsService<UsernamePasswordAuthenticationToken> {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsByUsernamePasswordService.class);

	@Value(value = "${security.allowNonVerifiedUsers}")
	private boolean allowNonVerifiedUsers;
	
	@Autowired
	private RoboRegistrationService manager;
	
	@Override
	public UserDetails loadUserDetails(UsernamePasswordAuthenticationToken token) throws UsernameNotFoundException {
		String username = token.getPrincipal().toString();
		String password = token.getCredentials().toString();
		try{
			RoboAuthDTO roboAuthDTO = manager.retreiveUser(username, password);
			
			Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
			
			for(String role : roboAuthDTO.getRoleCodes()){
				grantedAuthorities.add(new SimpleGrantedAuthority(role));
			}
			
			boolean isEnabled = allowNonVerifiedUsers ? roboAuthDTO.getIsActive() : roboAuthDTO.getIsActive() && roboAuthDTO.isVerified();
			
			CustomUserDetails userDetails = new CustomUserDetails(username, password, isEnabled, isEnabled, isEnabled, isEnabled, grantedAuthorities);
			
			userDetails.setAuthToken(roboAuthDTO.getAuthToken());
			userDetails.setPhoto(roboAuthDTO.getPhoto());
			
			return userDetails;
		}catch(AccessDeniedException ex){
			logger.error("No Authentication found with username '{}'", username);
			throw new UsernameNotFoundException("No Authentication found with username '" + username + "'");
		}catch(ResourceAccessException ex){
			String msg = "Authentication request could not be processed due to a system problem";
			logger.error(msg);
			throw new AuthenticationServiceException(msg);
		}catch(Throwable ex){
			String msg = "Authentication request is rejected because the credentials are not sufficiently trusted";
			logger.error(msg);
			throw new InsufficientAuthenticationException(msg);
		}
	}

}
