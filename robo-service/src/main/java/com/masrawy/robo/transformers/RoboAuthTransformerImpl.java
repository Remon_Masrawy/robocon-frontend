package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboAuthDTO;
import com.masrawy.robo.models.RoboAuthVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboAuthTransformer")
public class RoboAuthTransformerImpl extends AbstractDTOTransformer<RoboAuthVO, RoboAuthDTO> implements RoboAuthTransformer{

	public RoboAuthTransformerImpl(){
		super(RoboAuthVO.class, RoboAuthDTO.class);
	}
}
