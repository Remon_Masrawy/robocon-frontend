package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboConfigurationDTO;
import com.masrawy.robo.models.RoboConfigurationVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboConfigurationTransformer extends DTOTransformer<RoboConfigurationVO, RoboConfigurationDTO>{

}
