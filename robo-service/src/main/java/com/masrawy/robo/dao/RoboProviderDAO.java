package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboProviderVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboProviderDAO extends DAO<RoboProviderVO, Long>{

}
