package com.masrawy.robo.controllers;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.masrawy.robo.constants.RoleConstants.ROLE_SYSTEM;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robo.constants.RoboPages.DASHBOARD_PAGE;

@Controller
@RequestMapping(value = "/web/dashboard")
public class HomeController {

	@Secured({ ROLE_SYSTEM, CONTENT_ADMIN })
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView model = new ModelAndView(DASHBOARD_PAGE);
		return model;
	}
	
}