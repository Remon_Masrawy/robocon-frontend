package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboConfigurationDAO;
import com.masrawy.robo.dto.RoboConfigurationDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.ConfigurationException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.models.RoboConfigurationVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboConfigurationTransformer;

@Service("roboConfigurationService")
public class RoboConfigurationServiceImpl extends AbstractPgrlServiceImpl<RoboConfigurationVO, RoboConfigurationDTO, String> implements RoboConfigurationService{

	@Autowired
	private RoboConfigurationDAO roboConfigurationDAO;
	
	@Autowired
	private RoboConfigurationTransformer roboConfigurationTransformer;
	
	@Value("${robocon.ws.configuration.url}")
	private String configurationURL;
	
	@Override
	public List<RoboConfigurationDTO> getConfigurationList(){
		logger.debug("Robocon request for getting configuration list");
		List<RoboConfigurationDTO> configList = getList(configurationURL);
		return configList;
	}
	
	@Override
	public void updateConfiguration(RoboConfigurationDTO roboConfigurationDTO){
		logger.debug("Robocon request for updating configuration param");
		try{
			update(configurationURL, roboConfigurationDTO);
		}catch(RoboResponseException ex){
			logger.error("Configuration update error occured.", ex);
			throw new ConfigurationException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Configuration update error occured.", ex);
			throw new ConfigurationException(ex.getMessage());
		}
	}
	
	@Override
	protected DAO<RoboConfigurationVO, String> getDAO() {
		return roboConfigurationDAO;
	}

	@Override
	protected DTOTransformer<RoboConfigurationVO, RoboConfigurationDTO> getTransformer() {
		return roboConfigurationTransformer;
	}
	
}
