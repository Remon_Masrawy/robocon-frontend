package com.masrawy.robo.models;

import com.masrawy.robo.ws.AbstractVO;

public class RoboTagVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String tagTitle;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTagTitle() {
		return tagTitle;
	}
	public void setTagTitle(String tagTitle) {
		this.tagTitle = tagTitle;
	}
}
