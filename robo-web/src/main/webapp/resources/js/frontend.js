$(document).ready(function(){
	$container = $(".templatemo-content-container");
	
	$table = $(".templatemo-content-container .templatemo-content-widget .table");
	
	$preview = $(".templatemo-content-container .preview-widget");
	
	$confirmation = $(".confirmation");
	
	$search = $(".search");
	
	$enablePagination = true;
	
	var $previous;
	
	$container.on("click", ".new-btn", function(){
		window.location.href = $frontend+"/0";
	});
	
	$table.on("click", ".templatemo-link.templatemo-del-link", function(){
		var id = $(this).closest("tr").attr("data-id");
		deleteFrontend(id);
	});
	
	$preview.on("click", ".preview-widget-buttons>.new-button", function(){
		addNewMetaRow({});
	});
	
	$preview.on("click", ".preview-widget-resposive-buttons>input.add-button", function(){
		if($(this).hasClass("active")){
			$search.hide();
			$(this).removeClass("active");
			return;
		}
		if($search.is(":hidden")){
			type = $preview.find(".preview-widget-content .group-responsive .type select option:selected").val();
			switch(type.toLowerCase()){
				case 'song':
					$(".search select[name='type']").val("song").trigger("change");
					break;
				case 'album':
					$(".search select[name='type']").val("album").trigger("change");
					break;
				default:
					return;
			}
			$(this).addClass("active");
			$search.off("click", ".search-buttons input[name='add'].add-btn", add);
			$search.on("click", ".search-buttons input[name='add'].add-btn", add);
			$(".search select[name='type']").attr("disabled", "disabled");
			$search.show();
			$preview.find(".preview-widget-resposive-buttons>input.delete-button").removeClass("active");
		}
	});
	
	$(".search").on("click", ".control-buttons .close-btn", function(e){
		$preview.find(".preview-widget-resposive-buttons>input.add-button").removeClass("active");
	});
	
	$(".search-result").on("click", ".result-body .table-responsive table>tbody>tr", function(e){
		if($preview.find(".preview-widget-resposive-buttons>input.add-button").hasClass("active")){
			var selectedRow = $(this);
			var id = selectedRow.attr("data-id");
			$(".custom-menu li[value='add']").remove();
			$(".custom-menu ul").append($("<li>", {
				value: "add",
		        text : "Add" 
			}));
			if($(this).closest("fieldset").parent().hasClass("songs")){
				$(".custom-menu ul li[value='add']").on("click", function(){
					switch($preview.find(".preview-widget-content .group-responsive .type select option:selected").val().toLowerCase()){
						case 'song':
							var exist = false;
							$preview.find(".preview-widget-content .assign-responsive table.assign-song-table tbody").children("tr").each(function(){
								if($(this).attr("data-id") === id){
									exist = true;
									info("Song already exist");
								}
							});
							if(!exist){
								var name = selectedRow.children("td:nth-child(3)").text();
								var album = selectedRow.children("td:nth-child(4)").text();
								var artist = selectedRow.children("td:nth-child(5)").text();
								var photo = selectedRow.children("td:nth-child(2)").find("img").attr("src");
								
								var row = addTableRow(5).attr("data-id", id);
								row.children(":first-child()").text(id);
								row.children(":nth-child(2)").append(addImage().attr("src", photo));
								row.children(":nth-child(3)").text(name);
								row.children(":nth-child(4)").text(album);
								row.children(":last-child()").text(artist);
								
								$preview.find(".preview-widget-content .assign-responsive table.assign-song-table tbody").append(row);
								var size = $preview.find(".preview-widget-content .assign-responsive table.assign-song-table tbody").children("tr").length;
								$preview.find(".preview-widget-content .assign-responsive fieldset legend span").text(size);
							}
							break;
						default:
							
					}
				});
			}
			if($(this).closest("fieldset").parent().hasClass("albums")){
				$(".custom-menu ul li[value='add']").on("click", function(){
					switch($preview.find(".preview-widget-content .group-responsive .type select option:selected").val().toLowerCase()){
					case 'album':
						var exist = false;
						$preview.find(".preview-widget-content .assign-responsive table.assign-album-table tbody").children("tr").each(function(){
							if($(this).attr("data-id") === id){
								exist = true;
								info("Album already exist");
							}
						});
						if(!exist){
							var name = selectedRow.children("td:nth-child(3)").text();
							var artist = selectedRow.children("td:nth-child(4)").text();
							var photo = selectedRow.children("td:nth-child(2)").find("img").attr("src");
							
							var row = addTableRow(4).attr("data-id", id);;
							row.children(":first-child()").text(id);
							row.children(":nth-child(2)").append(addImage().attr("src", photo));
							row.children(":nth-child(3)").text(name);
							row.children(":last-child()").text(artist);
							
							$preview.find(".preview-widget-content .assign-responsive table.assign-album-table tbody").append(row);
							var size = $preview.find(".preview-widget-content .assign-responsive table.assign-album-table tbody").children("tr").length;
							$preview.find(".preview-widget-content .assign-responsive fieldset legend span").text(size);
						}
						break;
					default:
						
					}
				});
			}
		}
	});
	
	$preview.on("click", ".preview-widget-resposive-buttons>input.delete-button", function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			return;
		}
		if($search.is(":visible")){
			$search.hide();
			$preview.find(".preview-widget-resposive-buttons>input.add-button").removeClass("active");
		}
		$(this).addClass("active");
	});
	
	$preview.on("click", ".preview-widget-resposive-buttons>div.save-button,.preview-widget-buttons>div.save-button", function(){
		saveFrontend();
	});
	
	$preview.delegate("a.templatemo-link.templatemo-meta-delete-link", "click", function(){
		$(this).closest("tr").remove();
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .render-list .selected-list .item-close", function(){
		var id = $(this).closest("span.item").attr("data-id");
		$(this).closest(".render-list").find("select").children("option[value='"+id+"']").removeAttr("hidden");
		$(this).closest("span.item").remove();
	});
	$preview.find(".preview-widget-content .assign-responsive table tbody").sortable();
	
	$preview.on("hover", ".preview-widget-content .assign-responsive table.assign-song-table tbody tr", function(){
		
	});
	
	$preview.find(".preview-widget-content .group-responsive>div").each(function(){
		if($(this).hasClass("type")){
			if($(this).find('.selected-list .item').length == 0){
				$(this).find('input[name="album"]').show();
	    	}
		}else{
			$(this).find('input').show();
		}
	});
	
	$preview.on("change", ".preview-widget-content .group-responsive .type select", function(){
		updateTable();
	});
	
	if($container.find(".data-table").hasClass("frontends-table")){
		$enablePanel = true;
	}else{
		$enablePagination = false;
		$enablePanel = false;
		updateTable();
	}
	
	$preview.on("click", ".preview-widget-content .assign-responsive table tbody tr", function(){
		if($preview.find(".preview-widget-resposive-buttons>input.delete-button").hasClass("active")){
			deleteRow($(this));
		}else{
			var id = $(this).attr("data-id");
			if($(this).closest("table").hasClass("assign-song-table")){
				window.location.href = $song+"/"+id;
			}else if($(this).closest("table").hasClass("assign-album-table")){
				window.location.href = $album+"/"+id;
			}
		}
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .category .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $category + "/" + $(this).parent().attr("data-id");
		}
	});
	
	$preview.find('.group-responsive .category input[name="category"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchCategories(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	$preview.find('.group-responsive .category .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	})
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .category .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .category input[name="category"]').val("");
	    }
	});
	
});

$fill = function fillFrontends(){
	$.when(ajaxCall($frontend+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, frontend){
			var url = $frontend+"/"+frontend.id;
			var row = addTableRow(7).attr("data-id", frontend.id);
			row.children("td:first-child()").addClass("tc").text(frontend.id);
			row.children("td:nth-child(2)").append(addLink().attr("href", url).addClass("templatemo-keyword-link").text(frontend.meta[0].name));
			row.children("td:nth-child(3)").addClass("tc").text(frontend.type);
			$.each(frontend.categories, function(key, value){
				var innerUrl = $category + "/" + key;
				if(row.children("td:nth-child(4)").text()){
					row.children("td:nth-child(4)").append("- ");
				}
				row.children("td:nth-child(4)").append(addLink().addClass("templatemo-keyword-link").attr("href", innerUrl).attr("data-id", key).text(value));
			});
			switch(frontend.type){
			case "SONG":
				if(frontend.songs){
					row.children("td:nth-child(5)").addClass("tc").text(frontend.songs.length);
				}else{
					row.children("td:nth-child(5)").addClass("tc").text("0");
				}
				break;
			case "ALBUM":
				if(frontend.albums){
					row.children("td:nth-child(5)").addClass("tc").text(frontend.albums.length);
				}else{
					row.children("td:nth-child(5)").addClass("tc").text("0");
				}
				break;
			default:
				
			}
			row.children("td:nth-child(6)").addClass("tc").append(addLink().attr("href", url).addClass("templatemo-edit-btn").text("Edit"));
			row.children("td:last-child()").addClass("tc").append(addLink().addClass("templatemo-link templatemo-del-link").text("Delete"));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveFrontend(){
	var valid = validate();
	if(!valid){
		return;
	}
	var frontend = {};
	frontend.id = parseInt($preview.attr("data-id"));
	var metaList = new Array();
	$preview.find(".preview-widget-content .table-responsive table>tbody").children("tr").each(function(){
		var meta = {};
		if($(this).attr("data-id")){
			meta.id = parseInt($(this).attr("data-id").trim());
		}
		meta.name = $(this).children(":nth-child(1)").text().trim();
		meta.description = $(this).children(":nth-child(2)").text().trim();
		meta.lang = $(this).children(":nth-child(3)").children("select").val().trim();
		metaList.push(meta);
	});
	type = $preview.find(".preview-widget-content .group-responsive .type select option:selected").val();
	if(type){
		frontend.type = type;
	}
	var categoriesMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .category .selected-list").children("span.item").each(function(){
		categoriesMap[$(this).attr("data-id")] = "";
	});
	var items = new Array();
	if(type){
		if(type.toLowerCase() == "song"){
			var songs = new Array();
			$preview.find(".preview-widget-content .assign-responsive table.assign-song-table tbody tr").each(function(){
				var song = {};
				song.id = $(this).attr("data-id");
				songs.push(song);
			});
			frontend.songs = songs;
		}else if(type.toLowerCase() == "album"){
			var albums = new Array();
			$preview.find(".preview-widget-content .assign-responsive table.assign-album-table tbody tr").each(function(){
				var album = {};
				album.id = $(this).attr("data-id");
				albums.push(album);
			});
			frontend.albums = albums;
		}
	}
	frontend.meta = metaList;
	frontend.categories = categoriesMap;
	var method;
	if($preview.attr("data-id")){
		method = $HTTP_METHOD.PUT;
	}else{
		method = $HTTP_METHOD.POST;
	}
	
	$.when(ajaxCall($frontend, method, frontend, headers)).then(function(data){
		$preview.find(".preview-widget-content .table-responsive table>tbody").empty();
		$preview.attr("data-id", data.id);
		data.meta.forEach(function(meta, index){
			addNewMetaRow(meta);
		});
		$preview.find(".preview-widget-content .group-responsive .type select option[value!="+data.type+"]").remove();
		toastr.success("Frontend Successfully Saved.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	}).done(function(){
		
	});
}

function add(){
	type = $preview.find(".preview-widget-content .group-responsive .type select option:selected").val();
	var searchType = $(".search select[name='type']").val();
	if(type && searchType){
		if(type.toLowerCase() === searchType.toLowerCase()){
			var id = $('.search input[name="name"]').attr("data-id");
			var name = $('.search input[name="name"]').attr("data-name");
			switch(type.toLowerCase()){
			case 'song':
				var exist = false;
				$preview.find(".preview-widget-content .assign-responsive table.assign-song-table tbody").children("tr").each(function(){
					if($(this).attr("data-id") === id){
						exist = true;
						info("Song already exist");
					}
				});
				if(!exist){
					var album = $('.search input[name="name"]').attr("data-album");
					var artist = $('.search input[name="name"]').attr("data-artist");
					var photo = $('.search input[name="name"]').attr("data-photo");
					
					var row = addTableRow(5).attr("data-id", id);
					row.children(":first-child()").text(id);
					row.children(":nth-child(2)").append(addImage().attr("src", photo));
					row.children(":nth-child(3)").text(name);
					row.children(":nth-child(4)").text(album);
					row.children(":last-child()").text(artist);
					
					$preview.find(".preview-widget-content .assign-responsive table.assign-song-table tbody").append(row);
					var size = $preview.find(".preview-widget-content .assign-responsive table.assign-song-table tbody").children("tr").length;
					$preview.find(".preview-widget-content .assign-responsive fieldset legend span").text(size);
				}
				break;
			case 'album':
				var exist = false;
				$preview.find(".preview-widget-content .assign-responsive table.assign-album-table tbody").children("tr").each(function(){
					if($(this).attr("data-id") === id){
						exist = true;
						info("Album already exist");
					}
				});
				if(!exist){
					var artist = $('.search input[name="name"]').attr("data-artist");
					var photo = $('.search input[name="name"]').attr("data-photo");
					
					var row = addTableRow(4).attr("data-id", id);;
					row.children(":first-child()").text(id);
					row.children(":nth-child(2)").append(addImage().attr("src", photo));
					row.children(":nth-child(3)").text(name);
					row.children(":last-child()").text(artist);
					
					$preview.find(".preview-widget-content .assign-responsive table.assign-album-table tbody").append(row);
					var size = $preview.find(".preview-widget-content .assign-responsive table.assign-album-table tbody").children("tr").length;
					$preview.find(".preview-widget-content .assign-responsive fieldset legend span").text(size);
				}
				break;
			}
		}
	}
}

function updateTable(){
	type = $preview.find(".preview-widget-content .group-responsive .type select option:selected").val();
	if(type){
		switch(type.toLowerCase()){
			case 'song':
				$preview.find(".preview-widget-content .assign-responsive table:not(.assign-song-table)").hide();
				$preview.find(".preview-widget-content .assign-responsive .assign-song-table").show();
				var size = $preview.find(".preview-widget-content .assign-responsive table.assign-song-table tbody").children("tr").length;
				if(size == 0){
					$preview.find(".preview-widget-resposive-buttons>input.delete-button").removeClass("active");
				}
				$preview.find(".preview-widget-content .assign-responsive fieldset legend span").text(size);
				break;
			case 'album':
				$preview.find(".preview-widget-content .assign-responsive table:not(.assign-album-table)").hide();
				$preview.find(".preview-widget-content .assign-responsive .assign-album-table").show();
				var size = $preview.find(".preview-widget-content .assign-responsive table.assign-album-table tbody").children("tr").length;
				if(size == 0){
					$preview.find(".preview-widget-resposive-buttons>input.delete-button").removeClass("active");
				}
				$preview.find(".preview-widget-content .assign-responsive fieldset legend span").text(size);
				break;
			default:
				$preview.find(".preview-widget-content .assign-responsive table").hide();
		}
	}
	$preview.find(".preview-widget-resposive-buttons>input.add-button").removeClass("active");
	$search.hide();
}

function searchCategories(term, suggest){
	try{
		$.when(ajaxCall($category+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, category){
				var item = {};
				var data = {};
				item.id = category.id;
				data.name = category.meta[0].name;
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function deleteFrontend(frontendId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteFrontend(frontendId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteFrontend(frontendId){
	
	$.when(ajaxCall($frontend+"/"+frontendId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+frontendId+"']").remove();
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function deleteRow(row){
	var globalMsg = $confirmation.find(".confirm-msg").text();
	$confirmation.one("click", ".confirm-buttons .confirm-cancel-button", function(){
		$confirmation.find(".confirm-msg").text(globalMsg);
		$confirmation.hide(300, reset);
	});
	
	$confirmation.one("click", ".confirm-buttons .confirm-agree-button", function(){
		row.remove();
		updateTable();
		$confirmation.find(".confirm-msg").text(globalMsg);
		$confirmation.hide(300, reset);
	});
	var msg = "Do you want to confirm unassign (" + row.children(":nth-child(3)").text() +")";
	$confirmation.find(".confirm-msg").text(msg);
	$confirmation.show(300);
}

function validate(){
	var valid = true;
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(2)").is(":empty")){
			$(this).children("td:nth-child(2)").addClass("error");
			valid = false;
		}
	});
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(3)").is(":empty")){
			$(this).children("td:nth-child(3)").addClass("error");
		}
	});
	
	return valid;
}