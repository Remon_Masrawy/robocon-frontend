package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.GENRE_PAGE;
import static com.masrawy.robo.constants.RoboPages.GENRES_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboGenreDTO;
import com.masrawy.robo.services.RoboGenreService;
import com.masrawy.robo.services.RoboLanguageService;

@Controller
@RequestMapping(value = "/web/genre")
public class GenreController {

	@Autowired
	private RoboGenreService roboGenreService;
	
	@Autowired
	private RoboLanguageService roboLanguageService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView genres(){
		ModelAndView model = new ModelAndView(GENRES_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{genreId}", method = RequestMethod.GET)
	public ModelAndView genre(@PathVariable("genreId") String genreId){
		ModelAndView model = new ModelAndView(GENRE_PAGE);
		try{
			Long id = Long.valueOf(genreId);
			if(id != 0){
				model.addObject("genre", roboGenreService.getGenre(id));
			}
			model.addObject("languages", roboLanguageService.getLanguageList(0, 0));
		}catch(Exception e){
			model = new ModelAndView("redirect:/web/genre");
		}
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboGenreDTO create(@RequestBody RoboGenreDTO roboGenreDTO){
		return roboGenreService.createGenre(roboGenreDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboGenreDTO update(@RequestBody RoboGenreDTO roboGenreDTO){
		return roboGenreService.updateGenre(roboGenreDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value="/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboGenreDTO> genres(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboGenreService.getGenreList(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id") Long id){
		roboGenreService.deleteGenre(id);
	}
}
