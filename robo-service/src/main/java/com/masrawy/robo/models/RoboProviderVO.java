package com.masrawy.robo.models;

import java.util.List;

import com.masrawy.robo.ws.AbstractVO;

public class RoboProviderVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String keyword;
	private List<RoboMetaVO> meta;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public List<RoboMetaVO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaVO> meta) {
		this.meta = meta;
	}
	
}
