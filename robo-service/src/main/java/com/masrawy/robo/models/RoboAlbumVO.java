package com.masrawy.robo.models;

import java.util.List;
import java.util.Map;

import com.masrawy.robo.ws.AbstractVO;

public class RoboAlbumVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private List<RoboMetaVO> meta;
	private List<RoboPhotoVO> photos;
	private List<RoboTagVO> tags;
	private RoboProviderVO provider;
	private Map<Long, String> artists;
	private Map<Long, String> songs;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboMetaVO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaVO> meta) {
		this.meta = meta;
	}
	public List<RoboPhotoVO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboPhotoVO> photos) {
		this.photos = photos;
	}
	public List<RoboTagVO> getTags() {
		return tags;
	}
	public void setTags(List<RoboTagVO> tags) {
		this.tags = tags;
	}
	public RoboProviderVO getProvider() {
		return provider;
	}
	public void setProvider(RoboProviderVO provider) {
		this.provider = provider;
	}
	public Map<Long, String> getArtists() {
		return artists;
	}
	public void setArtists(Map<Long, String> artists) {
		this.artists = artists;
	}
	public Map<Long, String> getSongs() {
		return songs;
	}
	public void setSongs(Map<Long, String> songs) {
		this.songs = songs;
	}
}
