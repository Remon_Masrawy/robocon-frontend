<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="qualityText"><spring:message code="robo-pages.quality-keyword"/></c:set>
<c:set var="pixelText"><spring:message code="robo-pages.pixel-keyword"/></c:set>
<input id="qualityText" type="hidden" value="${qualityText}"/>
<input id="pixelText" type="hidden" value="${pixelText}"/>


<div class="preview-widget" data-id="${song.id}">
	<c:if test="${not empty song && not empty song.meta}">
		<div class="preview-widget-title">
			<span title="Type"><spring:message code="robo-pages.song-keyword"/></span>
			<span title="ID">${song.id}</span>
			<span title="${song.meta[0].name}">${song.meta[0].name}</span>
		</div>
	</c:if>
	<div class="preview-widget-buttons">
		<div class="new-button"><i class="fa fas fa-plus fa-fw"></i><spring:message code="robo-pages.new-meta-data-keyword"/></div>
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="preview-widget-container">
		<div class="preview-widget-content">
			<div class="templatemo-content-container">
				<div class="templatemo-content-widget no-padding">
				
				<!-- Meta -->
	            <div class="panel panel-default table-responsive">
	              <table class="table table-striped table-bordered templatemo-user-table">
	                <thead>
	                  <tr>
	                    <td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.name-keyword"/> <span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.description-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.lang-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><spring:message code="robo-pages.delete-keyword"/></td>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:if test="${not empty song && not empty song.meta}">
							<c:forEach items="${song.meta}" var="meta">
								<tr data-id="${meta.id}">
									<td contenteditable="true">${meta.name}</td>
									<td contenteditable="true">${meta.description}</td>
									<td>
										<select>
											<c:if test="${not empty languages}">
												<c:forEach items="${languages}" var="language">
													<option value="${language.code}" ${language.code == meta.lang? 'selected' : ''}>${language.code}</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
									<td><a href="javascript:" class="templatemo-link templatemo-meta-delete-link">Delete</a></td>
								</tr>
							</c:forEach>
						</c:if>
	                </tbody>
	              </table>    
	            </div> 
	            
	            <!-- Basic Info -->
	            <div class="panel-default basic-info-responsive">
	            	<div class="general">
						<div>
							<label for="isrc"><spring:message code="robo-pages.isrc-keyword"/>:</label>
							<input type="text" name="isrc" value="${song.isrc}" maxlength="12">
						</div>
						<div>
							<label for="createdDate"><spring:message code="robo-pages.release-date-keyword"/>:</label>
							<input type="text" name="createdDate" class="datepicker" contenteditable="false" value="${song.createdDate}">
						</div>
						<div>
							<label for="year"><spring:message code="robo-pages.year-keyword"/>:</label>
							<input type="number" name="year" value="${song.year}" maxlength="4">
						</div>
						<div>
							<label for="link"><spring:message code="robo-pages.video-link-keyword"/>:</label>
							<input type="text" name="link" value="${song.videoLink}">
						</div>
					</div>
	            </div>
	            
	            <div class="panel-default group-responsive">
	            	<div class="artist">
	            		<label for="artist"><spring:message code="robo-pages.artists-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty song.artists}">
		            				<c:forEach var="artist" items="${song.artists}">
		            					<span class="item" data-id="${artist.key}">
		            						<span title="value">${artist.value}</span>
		            						<span class="item-close"></span>
		            					</span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="artist" name="artist" type="text" placeholder="Search..." autocomplete="off">
            			</div>
	            	</div>
	            	<div class="main-artist">
	            		<label for="artist"><spring:message code="robo-pages.original-artists-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty song.mainArtists}">
		            				<c:forEach var="artist" items="${song.mainArtists}">
		            					<span class="item" data-id="${artist.key}">
			            					<span title="value">${artist.value}</span>
			            					<span class="item-close"></span>
		            					</span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="mainartist" name="artist" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            	<div class="genre">
	            		<label for="genre"><spring:message code="robo-pages.genres-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty song.genres}">
		            				<c:forEach var="genre" items="${song.genres}">
		            					<span class="item" data-id="${genre.key}">
			            					<span title="value">${genre.value}</span>
			            					<span class="item-close"></span>
		            					</span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="genre" name="genre" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            	<div class="album">
	            		<label for="album"><spring:message code="robo-pages.album-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty song.album}">
		            				<c:forEach var="album" items="${song.album}">
		            					<span class="item" data-id="${album.key}">
			            					<span title="value">${album.value}</span>
			            					<span class="item-close"></span>
		            					</span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="album" name="album" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            	<div class="provider">
	            		<label for="provider"><spring:message code="robo-pages.provider-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty song.provider}">
		            				<c:forEach var="provider" items="${song.provider}">
		            					<span class="item" data-id="${provider.key}">
			            					<span title="value">${provider.value}</span>
			            					<span class="item-close"></span>
		            					</span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="provider" name="provider" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            </div>
	            
	             <!-- Content -->
	            <div class="panel-default content-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.song-content-keyword"/> <span><spring:message code="robo-pages.content.note"/></span></legend>
	            		<div class="content">
	            			<c:if test="${not empty song.content}">
	            				<c:forEach var="content" items="${song.content}">
	            					<div class="item" data-id="${content.id}">
	            						<audio controls>
	            							<source src='<c:url value="/web/song/content/${content.id}/${content.fileName}"/>'>
	            						</audio>
	            						<span class="quality"><spring:message code="robo-pages.quality-keyword"/>: <span>${content.bitRate }</span><spring:message code="robo-pages.pixel-keyword"/></span>
	            						<span class="close"></span>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            		<div class="add-file-button">
	            			<div class="add-sign"></div>
	            		</div>
	            		<input type="file" accept="audio/mp3,audio/mpeg,audio/ogg,audio/wav" title="Choose an audio to upload">
	            	</fieldset>
	            </div>
	            
	            <!-- Lyrics -->
	            <div class="panel-default lyrics-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.song-lyrics-keyword"/> <span><spring:message code="robo-pages.lyrics.note"/></span></legend>
	            		<div class="lyrics">
	            			<c:if test="${not empty song.lyrics}">
	            				<c:forEach var="lyrics" items="${song.lyrics}">
	            					<div class="item" data-id="${lyrics.id}" title="${lyrics.fileName}">
	            						<textarea data-url='<c:url value="/web/song/lyrics/${lyrics.id}"/>'></textarea>
	            						<span class="close"></span>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            		<div class="add-file-button ${not empty song.lyrics ? 'hidden' : '' }">
	            			<div class="add-sign"></div>
	            		</div>
	            		<input type="file" accept="text/plain" title="Choose a text file to upload">
	            	</fieldset>
	            </div>
	            
	            <!-- Photos -->
	            <div class="panel-default photos-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.song-photos-keyword"/> <span><spring:message code="robo-pages.photos.note"/></span></legend>
	            		<div class="photos">
	            			<c:if test="${not empty song.photos}">
	            				<c:forEach items="${song.photos}" var="photo" varStatus="status">
	            					<div class="photo" data-id="${photo.id}">
	            						<c:set var="suburl" value="${fn:replace(photo.url, '{width}', '500')}"/>
	            						<c:set var="url" value="${fn:replace(suburl, '{height}', '500')}"/>
	            						<img alt="song" src="${url}">
	            						<span class="close"></span>
	            						<input type="checkbox" name="main" class="main-photo-checkbox" title="Set as main photo" ${photo.isMainPhoto ? 'checked' : ''}>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            		<div class="add-file-button">
	            			<div class="add-sign"></div>
	            		</div>
	            		<input type="file" accept="image/*" title="Choose an image to upload">
	            	</fieldset>
	            </div>
	            
	            <!-- Tags -->
	            <div class="panel-default tags-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.song-tags-keyword"/></legend>
	            		<div class="tags">
	            			<c:if test="${not empty song.tags}">
	            				<c:forEach items="${song.tags}" var="tag" varStatus="status">
	            					<div class="tag" data-id="${tag.id}">
	            						<span title="tag">${tag.tag}</span>
	            						<span class="close"></span>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            	</fieldset>
	            </div>
	          </div> 
          </div>
		</div>
	</div>
	<div class="preview-widget-resposive-buttons">
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="clear"></div>
</div>
	
<script type="text/javascript" src='<c:url value="/js/song.js"/>'></script>