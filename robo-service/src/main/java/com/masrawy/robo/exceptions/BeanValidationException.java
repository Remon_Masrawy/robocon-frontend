package com.masrawy.robo.exceptions;

import static com.masrawy.robo.exceptions.ErrorConstants.ROBO_VALIDATION_ERROR;

import java.util.List;

import org.springframework.util.StringUtils;

public class BeanValidationException extends BaseRuntimeException implements ClientError{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String ERROR_MSG = "Validation exception.";
	private List<String> errorMessages;
	
	public BeanValidationException(){
		super(ERROR_MSG);
	}

	public BeanValidationException(List<String> errorMessages){
		super(StringUtils.collectionToCommaDelimitedString(errorMessages));
		this.errorMessages = errorMessages;
	}
	
	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public String getErrorMessage(){
		return StringUtils.collectionToCommaDelimitedString(errorMessages);
	}
	
	@Override
	public int getErrorCode() {
		return ROBO_VALIDATION_ERROR;
	}
	
}
