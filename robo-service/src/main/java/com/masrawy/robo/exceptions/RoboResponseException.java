package com.masrawy.robo.exceptions;

import java.nio.charset.Charset;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class RoboResponseException extends HttpStatusCodeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RoboResponseException(HttpStatus statusCode, String statusText,
			HttpHeaders responseHeaders, byte[] responseBody, Charset responseCharset){
		super(statusCode, statusText, responseHeaders, responseBody, responseCharset);
	}
	
	public String getErrorMessage(){
		if(getResponseHeaders() != null){
			if(getResponseHeaders().containsKey("Error-Msg")){
				return getResponseHeaders().get("Error-Msg").get(0);
			}
		}
		
		return null;
	}
}
