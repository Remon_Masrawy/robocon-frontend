package com.masrawy.robo.dto;

public class RoboLyricsDTO extends RoboFileDTO{

	private static final long serialVersionUID = 1L;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
