package com.masrawy.robo.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class RoboArtistDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotBlank
	private String birthDate;
	private String deathDate;
	private String geneder;
	private Double rate;
	@Valid
	@NotEmpty
	private List<RoboMetaDTO> meta;
	private List<RoboPhotoDTO> photos;
	private List<RoboTagDTO> tags;
	@Valid
	private List<RoboSocialDTO> socials;
	private Map<Long, Map<String, String>> events;
	private Integer songs;
	private Integer albums;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}
	public String getGeneder() {
		return geneder;
	}
	public void setGeneder(String geneder) {
		this.geneder = geneder;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public List<RoboMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaDTO> meta) {
		this.meta = meta;
	}
	public List<RoboPhotoDTO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboPhotoDTO> photos) {
		this.photos = photos;
	}
	public List<RoboTagDTO> getTags() {
		return tags;
	}
	public void setTags(List<RoboTagDTO> tags) {
		this.tags = tags;
	}
	public List<RoboSocialDTO> getSocials() {
		return socials;
	}
	public void setSocials(List<RoboSocialDTO> socials) {
		this.socials = socials;
	}
	public Map<Long, Map<String, String>> getEvents() {
		return events;
	}
	public void setEvents(Map<Long, Map<String, String>> events) {
		this.events = events;
	}
	public Integer getSongs() {
		return songs;
	}
	public void setSongs(Integer songs) {
		this.songs = songs;
	}
	public Integer getAlbums() {
		return albums;
	}
	public void setAlbums(Integer albums) {
		this.albums = albums;
	}
}
