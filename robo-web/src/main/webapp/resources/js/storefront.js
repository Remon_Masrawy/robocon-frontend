$(document).ready(function(){
	$container = $(".templatemo-content-container");
	
	$table = $(".templatemo-content-container .templatemo-content-widget .table");
	
	$confirmation = $(".confirmation");
	
	$container.on("click", ".new-btn", function(){
		addNewRow();
	});
	
	$container.on("click", ".save-btn", function(){
		saveStorefront();
	});
	
});

$fill = function fillStorefronts(){
	$.when(ajaxCall($storefront+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, storefront){
			var url = $storefront+"/"+storefront.id;
			var row = addTableRow(4).attr("data-id", storefront.id);
			row.children("td:first-child()").addClass("tlc").text(storefront.id);
			row.children("td:nth-child(2)").addClass("keyword tll").text(storefront.keyword);
			row.children("td:nth-child(3)").addClass("title tll").text(storefront.name);
			row.children("td:last-child()").addClass("activation tlc").append(addCheckbox().prop('checked', storefront.active));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}


function saveStorefront(){
	var valid = validate();
	if(!valid){
		return;
	}
	var errors = new Array();
	$table.find("tbody tr").each(function(){
		var storefront = {};
		storefront.id = parseInt($(this).attr("data-id"));
		storefront.keyword = $(this).find("td.keyword").text();
		storefront.name = $(this).find("td.title").text();
		storefront.active = $(this).find("td.activation input[type='checkbox']").is(":checked");
		var method;
		if($(this).attr("data-id")){
			method = $HTTP_METHOD.PUT;
		}else{
			method = $HTTP_METHOD.POST;
		}
		var that = $(this);
		$.when(ajaxCall($storefront, method, storefront, headers)).then(function(data){
			that.attr("data-id", data.id);
			that.find("td:first-child()").text(data.id);
			that.find("td.keyword").removeAttr("contenteditable");
		},
		function(error){
			errors.push(error);
			toast(error.responseJSON.message);
		});
	});
	if(errors.length == 0){
		toastr.success("Storefront Successfully Saved.", 'Success', {positionClass: 'toast-bottom-right'});
	}
}

function addNewRow(){
	var row = addTableRow(4);
	row.children(":first-child()").addClass("tlc");
	row.children(":nth-child(2)").addClass("keyword tll").attr("contenteditable", true);
	row.children(":nth-child(3)").addClass("title tll").attr("contenteditable", true);
	row.children(":last-child()").addClass("activation tlc").append(addCheckbox());
	$table.find("tbody:last-child").append(row);
}

function deleteStorefront(storefrontId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteStorefront(storefrontId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteStorefront(storefrontId){
	
	$.when(ajaxCall($storefront+"/"+storefrontId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+storefrontId+"']").remove();
		toastr.success("Storefront Successfully deleted.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function validate(){
	var valid = true;
	$table.find("tbody tr").each(function(){
		if($(this).find("td.keyword").text()){
			$(this).find("td.keyword").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td.keyword").css({"border":"2px solid red"});
			valid = false;
		}
		if($(this).find("td.title").text()){
			$(this).find("td.title").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td.title").css({"border":"2px solid red"});
			valid = false;
		}
	})
	return valid;
}