package com.masrawy.robo.services;

import java.util.List;

import com.masrawy.robo.dto.RoboOperatorDTO;
import com.masrawy.robo.models.RoboOperatorVO;

public interface RoboOperatorService extends PgrlService<RoboOperatorVO, RoboOperatorDTO, Long>{

	public RoboOperatorDTO createOperator(RoboOperatorDTO roboOperatorDTO);
	public RoboOperatorDTO updateOperator(RoboOperatorDTO roboOperatorDTO);
	public List<RoboOperatorDTO> getOperatorList(Integer page, Integer limit);
}
