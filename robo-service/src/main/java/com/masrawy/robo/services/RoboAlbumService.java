package com.masrawy.robo.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dto.RoboAlbumDTO;
import com.masrawy.robo.models.RoboAlbumVO;

public interface RoboAlbumService extends PgrlService<RoboAlbumVO, RoboAlbumDTO, Long>{

	public RoboAlbumDTO createAlbum(RoboAlbumDTO roboAlbumDTO);
	public RoboAlbumDTO updateAlbum(RoboAlbumDTO roboAlbumDTO);
	public RoboAlbumDTO uploadPhoto(MultipartFile file, Long albumId, String name);
	public RoboAlbumDTO getAlbum(Long albumId);
	public List<RoboAlbumDTO> getAlbums(Integer page, Integer limit);
	public void deleteAlbum(Long albumId);
}
