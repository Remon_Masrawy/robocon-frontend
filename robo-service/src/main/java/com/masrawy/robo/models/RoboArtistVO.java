package com.masrawy.robo.models;

import java.util.List;
import java.util.Map;

import com.masrawy.robo.ws.AbstractVO;

public class RoboArtistVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String birthDate;
	private String deathDate;
	private String geneder;
	private Double rate;
	private List<RoboMetaVO> meta;
	private List<RoboPhotoVO> photos;
	private List<RoboTagVO> tags;
	private List<RoboSocialVO> socials;
	private Map<Long, Map<String, String>> events;
	
	private Integer songs;
	private Integer albums;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}
	public String getGeneder() {
		return geneder;
	}
	public void setGeneder(String geneder) {
		this.geneder = geneder;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public List<RoboMetaVO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaVO> meta) {
		this.meta = meta;
	}
	public List<RoboPhotoVO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboPhotoVO> photos) {
		this.photos = photos;
	}
	public List<RoboTagVO> getTags() {
		return tags;
	}
	public void setTags(List<RoboTagVO> tags) {
		this.tags = tags;
	}
	public List<RoboSocialVO> getSocials() {
		return socials;
	}
	public void setSocials(List<RoboSocialVO> socials) {
		this.socials = socials;
	}
	public Map<Long, Map<String, String>> getEvents() {
		return events;
	}
	public void setEvents(Map<Long, Map<String, String>> events) {
		this.events = events;
	}
	public Integer getSongs() {
		return songs;
	}
	public void setSongs(Integer songs) {
		this.songs = songs;
	}
	public Integer getAlbums() {
		return albums;
	}
	public void setAlbums(Integer albums) {
		this.albums = albums;
	}
}
