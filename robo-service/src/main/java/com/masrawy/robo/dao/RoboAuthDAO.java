package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboAuthVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboAuthDAO extends DAO<RoboAuthVO, Long>{

}
