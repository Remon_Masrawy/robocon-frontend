package com.masrawy.robo.dto;

import org.hibernate.validator.constraints.NotBlank;

public class RoboStorefrontDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotBlank
	private String keyword;
	@NotBlank
	private String name;
	private Boolean active;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
}
