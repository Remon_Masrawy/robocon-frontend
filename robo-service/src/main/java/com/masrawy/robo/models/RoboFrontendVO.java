package com.masrawy.robo.models;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.masrawy.robo.ws.AbstractVO;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
	@Type(value = RoboSongFrontendVO.class, name = "SONG"),
	@Type(value = RoboAlbumFrontendVO.class, name = "ALBUM")
})
public abstract class RoboFrontendVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String type;
	private List<RoboMetaVO> meta;
	private Map<Long, String> categories;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<RoboMetaVO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaVO> meta) {
		this.meta = meta;
	}
	public Map<Long, String> getCategories() {
		return categories;
	}
	public void setCategories(Map<Long, String> categories) {
		this.categories = categories;
	}
}
