package com.masrawy.robo.validations;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ClassUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;

import com.masrawy.robo.exceptions.BeanValidationException;

@Component("roboBeanValidationManager")
public class RoboBeanValidationManagerImpl implements RoboBeanValidationManager{

	@Autowired
	private Validator validator;
	
	@Autowired
	private MessageSourceAccessor messageSourceAccessor;
	
	@Override
	public void validate(Object obj) throws BeanValidationException {
		BindingResult errors = new BeanPropertyBindingResult(obj, ClassUtils.getShortClassName(obj.getClass()));
		validator.validate(obj, errors);
		List<String> errorMessages = new ArrayList<String>();
		if(errors.hasErrors()){
			List<FieldError> fieldErrors = errors.getFieldErrors();
			for(FieldError fieldError : fieldErrors){
				String message = messageSourceAccessor.getMessage(fieldError);
				errorMessages.add(message);
			}
			throw new BeanValidationException(errorMessages);
		}
	}

}
