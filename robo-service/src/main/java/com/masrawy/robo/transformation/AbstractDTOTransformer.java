package com.masrawy.robo.transformation;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.masrawy.robo.dozer.DozerMapper;
import com.masrawy.robo.dto.DTO;
import com.masrawy.robo.ws.VO;

public abstract class AbstractDTOTransformer<V extends VO, D extends DTO> implements DTOTransformer<V, D>{

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	protected DozerMapper dozerMapper;
	
	private Class<V> valueClass;
	private Class<D> dtoClass;
	
	public AbstractDTOTransformer(Class<V> valueClass, Class<D> dtoClass){
		this.valueClass = valueClass;
		this.dtoClass = dtoClass;
	}

	// ********************** VO TO DTO transformation methods ****************************
	
	@Override
	public D transformVOToDTO(V sourceObject) {
		D destinationDTO = (D) dozerMapper.map(sourceObject, dtoClass);
		doTransformVOToDTO(sourceObject, destinationDTO);
		return destinationDTO;
	}

	@Override
	public void transformVOToDTO(V sourceObject, D destinationDTO) {
		dozerMapper.map(sourceObject, destinationDTO);
		doTransformVOToDTO(sourceObject, destinationDTO);
	}

	@Override
	public List<D> transformVOToDTO(List<V> Objects) {
		List<D> dtoObjects = new ArrayList<D>();
		for(V sourceObject : Objects){
			dtoObjects.add(transformVOToDTO(sourceObject));
		}
		return dtoObjects;
	}

	// ********************** DTO TO VO transformation methods ****************************
	
	@Override
	public V transformDTOToVO(D sourceDTO) {
		V destinationObject = (V) dozerMapper.map(sourceDTO, valueClass);
		doTransformDTOToVO(sourceDTO, destinationObject);
		return destinationObject;
	}
	
	@Override
	public void transformDTOToVO(D sourceDTO, V destinationObject) {
		dozerMapper.map(sourceDTO, destinationObject);
		doTransformDTOToVO(sourceDTO, destinationObject);
	}

	@Override
	public List<V> transformDTOToVO(List<D> dtos) {
		List<V> voObjects = new ArrayList<V>();
		for(D sourceDTO : dtos){
			voObjects.add(transformDTOToVO(sourceDTO));
		}
		return voObjects;
	}

	protected void doTransformVOToDTO(V sourceObject, D destinationDTO) {
		// to be implemented to custom
	}

	protected void doTransformDTOToVO(D sourceDTO, V destinationObject) {
		// to be implemented to custom
	}
}
