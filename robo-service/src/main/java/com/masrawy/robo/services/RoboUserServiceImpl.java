package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dao.RoboAuthDAO;
import com.masrawy.robo.dto.RoboAuthDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.exceptions.RoboUserException;
import com.masrawy.robo.models.RoboAuthVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboAuthTransformer;

@Service("roboUserService")
public class RoboUserServiceImpl extends AbstractPgrlServiceImpl<RoboAuthVO, RoboAuthDTO, Long> implements RoboUserService{

	@Autowired
	private RoboAuthDAO roboAuthDAO;
	
	@Autowired
	private RoboRegistrationService manager;
	
	@Autowired
	private RoboAuthTransformer roboAuthTransformer;
	
	@Value("${robocon.ws.user.url}")
	private String userURL;
	
	@Value("${robocon.ws.user-info.url}")
	private String userInfoURL;
	
	@Value("${robocon.ws.user-photo.url}")
	private String userPhotoURL;
	
	@Override
	public RoboAuthDTO getUser(){
		logger.debug("Robocon request for getting user Info");
		RoboAuthVO roboAuthVO = roboAuthDAO.getObject(userURL);
		RoboAuthDTO roboAuthDTO = roboAuthTransformer.transformVOToDTO(roboAuthVO);
		return roboAuthDTO;
	}
	
	@Override
	public RoboAuthDTO getUser(Long userId){
		logger.debug("Robocon request for getting user by id");
		RoboAuthDTO user = getById(userURL, userId);
		return user;
	}
	
	@Override
	public RoboAuthDTO updateUser(RoboAuthDTO roboAuthDTO){
		try{
			RoboAuthVO roboAuthVO = roboAuthTransformer.transformDTOToVO(roboAuthDTO);
			roboAuthVO = roboAuthDAO.updateObjectR(userURL, roboAuthVO);
			roboAuthDTO = roboAuthTransformer.transformVOToDTO(roboAuthVO);
			return roboAuthDTO;
		}catch(RoboResponseException ex){
			logger.error("Update User error occured.", ex);
			throw new RoboUserException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Update User error occured.", ex);
			throw new RoboUserException(ex.getMessage());
		}
	}
	
	@Override
	public RoboAuthDTO updateUserInfo(RoboAuthDTO roboAuthDTO){
		try{
			RoboAuthVO roboAuthVO = roboAuthTransformer.transformDTOToVO(roboAuthDTO);
			roboAuthVO = roboAuthDAO.updateObjectR(userInfoURL, roboAuthVO);
			roboAuthDTO = roboAuthTransformer.transformVOToDTO(roboAuthVO);
			return roboAuthDTO;
		}catch(RoboResponseException ex){
			logger.error("Update User Info error occured.", ex);
			throw new RoboUserException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Update User Info error occured.", ex);
			throw new RoboUserException(ex.getMessage());
		}
	}
	
	public RoboAuthDTO updateUserPhoto(MultipartFile file, String name){
		try{
			RoboAuthDTO roboAuthDTO = postFile(userPhotoURL, file, name);
			manager.updateUserDetails(roboAuthDTO);
			return roboAuthDTO;
		}catch(RoboResponseException ex){
			logger.error("Update User Info error occured.", ex);
			throw new RoboUserException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Update User Info error occured.", ex);
			throw new RoboUserException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboAuthDTO> getUsersList(Integer page, Integer limit){
		logger.debug("Robocon request for getting users list");
		List<RoboAuthDTO> users = getList(String.format(pagingURL, userURL, page, limit));
		return users;
	}
	
	@Override
	public void deleteUser(Long id){
		logger.debug("Robocon request for deleting User by id");
		try{
			deleteById(userURL, id);
		}catch(RoboResponseException ex){
			logger.error("User delete error occured.", ex);
			throw new RoboUserException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("User delete error occured.", ex);
			throw new RoboUserException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboAuthVO, RoboAuthDTO> getTransformer() {
		return roboAuthTransformer;
	}

	@Override
	protected DAO<RoboAuthVO, Long> getDAO() {
		return roboAuthDAO;
	}

}
