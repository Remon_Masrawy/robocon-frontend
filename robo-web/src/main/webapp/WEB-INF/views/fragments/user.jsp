<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="templatemo-content-widget white-bg profile">

	<div class="l-collection">
		<div class="l-nav">
			<ul class="collectionNav g-tabs g-tabs-large">
				<li class="g-tabs-item networkTabs__info">
					<a class="g-tabs-link active"> <spring:message code="robo-pages.info-keyword"/> </a>
				</li>
				<sec:authorize access="hasRole('ROLE_SUPER')">
					<li class="g-tabs-item networkTabs__users">
						<a class="g-tabs-link "> <spring:message code="robo-pages.users-keyword"/> </a>
					</li>
				</sec:authorize>
			</ul>
		</div>
	</div>

	<section class="tab_info">
		<form action="#" id="user_info" class="user_info">
			<div class="row form-group profile-photo-group">
				<div class="col-lg-12">
					<label class="control-label templatemo-block"><spring:message code="robo-pages.profile-photo-keyword"/></label>
					<div class="profile-photo">
						<div class="add-sign"></div>
					</div>
					<input type="file" name="fileToUpload" id="fileToUpload"
						class="filestyle" data-buttonName="btn-primary"
						data-buttonBefore="true" data-icon="false" accept="image/jpeg,.jpg, .jpeg">
					<p><spring:message code="robo-pages.max-upload-5-keyword"/></p>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-lg-6 col-md-6 form-group">
					<label for="inputFirstName"><spring:message code="robo-pages.first-name-keyword"/></label> <input type="text"
						class="form-control" id="inputFirstName" placeholder="John">
				</div>
				<div class="col-lg-6 col-md-6 form-group">
					<label for="inputLastName"><spring:message code="robo-pages.last-name-keyword"/></label> <input type="text"
						class="form-control" id="inputLastName" placeholder="Smith">
				</div>
			</div>
			<div class="row form-group">
				<div class="col-lg-6 col-md-6 form-group">
					<label for="inputNewUsername"><spring:message code="robo-pages.username-keyword"/></label> <input type="text"
						class="form-control" id="inputNewUsername" placeholder="Admin" disabled="disabled">
				</div>
				<div class="col-lg-6 col-md-6 form-group">
					<label for="inputNewEmail"><spring:message code="robo-pages.email-keyword"/></label> <input type="email"
						class="form-control" id="inputNewEmail"
						placeholder="admin@mazika.com" disabled="disabled">
				</div>
			</div>
			<div class="row form-group">
				<div class="col-lg-6 col-md-6 form-group">
					<label for="inputNewPassword"><spring:message code="robo-pages.new-password-keyword"/></label> <input
						type="password" class="form-control" id="inputNewPassword">
				</div>
				<div class="col-lg-6 col-md-6 form-group">
					<label for="inputConfirmNewPassword"><spring:message code="robo-pages.confirm-new-keyword"/></label> <input
						type="password" class="form-control" id="inputConfirmNewPassword">
				</div>
			</div>
			<div class="form-group text-right">
				<button type="button" class="templatemo-blue-button"><spring:message code="robo-pages.save-keyword"/></button>
			</div>
		</form>
	</section>
	
	<sec:authorize access="hasRole('ROLE_SUPER')">
		
		<section class="tab_users">
			
			<div class="users">
				<div class="profile-widget-buttons">
					<div class="new-button"><i class="fa fas fa-plus fa-fw"></i><spring:message code="robo-pages.user-keyword"/></div>
				</div>
			
				<div class="panel panel-default table-responsive">
	              <table class="table table-striped table-bordered templatemo-user-table">
	                <thead>
	                  <tr>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.id-keyword"/><span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.first-name-keyword"/><span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.last-name-keyword"/><span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.username-keyword"/><span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.email-keyword"/><span class="caret"></span></a></td>
	                    <td><spring:message code="robo-pages.edit-keyword"/></td>
	                    <td><spring:message code="robo-pages.delete-keyword"/></td>
	                  </tr>
	                </thead>
	                <tbody>
	                </tbody>
	              </table>    
	            </div>  
			</div>
			<form action="#" id="edit_user" class="edit_user">
				<div class="row form-group">
					<div class="col-lg-6 col-md-6 form-group">
						<label for="inputUsername"><spring:message code="robo-pages.username-keyword"/></label> 
						<input type="text" class="form-control" id="inputUsername" placeholder="Admin">
					</div>
					<div class="col-lg-6 col-md-6 form-group">
					<label for="inputEmail"><spring:message code="robo-pages.email-keyword"/></label> <input type="email"
						class="form-control" id="inputEmail"
						placeholder="admin@company.com">
				</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-6 col-md-6 form-group">
						<label for="inputPassword"><spring:message code="robo-pages.password-keyword"/></label> 
						<input type="password" class="form-control" id="inputPassword">
					</div>
					<div class="col-lg-6 col-md-6 form-group">
						<label for="inputConfirmPassword"><spring:message code="robo-pages.confirm-password-keyword"/></label> 
						<input type="password" class="form-control" id="inputConfirmPassword">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-12 form-group">
						<div class="margin-right-15 templatemo-inline-block">
							<input type="checkbox" name="role" id="c3" value="ROLE_SUPER">
							<label for="c3" class="font-weight-400"><span></span><spring:message code="robo-pages.super-admin-keyword"/></label>
						</div>
					</div>
					<div class="col-lg-12 form-group">
						<div class="margin-right-15 templatemo-inline-block">
							<input type="checkbox" name="role" id="c4"
								value="ROLE_CONTENT_ADMIN"> <label for="c4"
								class="font-weight-400"><span></span><spring:message code="robo-pages.content-admin-keyword"/></label>
						</div>
					</div>
					<div class="col-lg-12 form-group">
						<div class="margin-right-15 templatemo-inline-block">
							<input type="checkbox" name="role" id="c5"
								value="ROLE_REPORTS_ADMIN"> <label for="c5"
								class="font-weight-400"><span></span><spring:message code="robo-pages.reports-admin-keyword"/></label>
						</div>
					</div>
					<div class="col-lg-12 form-group">
						<div class="margin-right-15 templatemo-inline-block">
							<input type="checkbox" name="role" id="c6" value="ROLE_ADMIN">
							<label for="c6" class="font-weight-400"><span></span><spring:message code="robo-pages.cms-admin-keyword"/></label>
						</div>
					</div>
					<div class="col-lg-12 form-group">
						<div class="margin-right-15 templatemo-inline-block">
							<input type="checkbox" name="role" id="c7" value="ROLE_USER" checked> 
							<label for="c7" class="font-weight-400"><span></span><spring:message code="robo-pages.user-keyword"/></label>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-12 form-group">
						<div class="margin-right-15 templatemo-inline-block">
							<input type="checkbox" name="active" id="c1" value="active" checked>
							<label for="c1" class="font-weight-400"><span></span><spring:message code="robo-pages.active-keyword"/></label>
						</div>
					</div>
				</div>
				<div class="form-group text-right">
					<button type="button" class="templatemo-white-button cancel"><spring:message code="robo-pages.cancel-keyword"/></button>
					<button type="button" class="templatemo-blue-button save"><spring:message code="robo-pages.save-keyword"/></button>
				</div>
			</form>
		</section>
	</sec:authorize>
</div>
<script type="text/javascript" src='<c:url value="/js/user.js"/>'></script>