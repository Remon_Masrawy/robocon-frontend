package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboStorefrontDTO;
import com.masrawy.robo.models.RoboStorefrontVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboStorefrontTransformer")
public class RoboStorefrontTransformerImpl extends AbstractDTOTransformer<RoboStorefrontVO, RoboStorefrontDTO> implements RoboStorefrontTransformer{

	public RoboStorefrontTransformerImpl(){
		super(RoboStorefrontVO.class, RoboStorefrontDTO.class);
	}
}
