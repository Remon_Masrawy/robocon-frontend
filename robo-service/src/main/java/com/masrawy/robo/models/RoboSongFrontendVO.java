package com.masrawy.robo.models;

import java.util.List;

public class RoboSongFrontendVO extends RoboFrontendVO{

	private static final long serialVersionUID = 1L;

	private List<RoboSongVO> songs;

	public List<RoboSongVO> getSongs() {
		return songs;
	}

	public void setSongs(List<RoboSongVO> songs) {
		this.songs = songs;
	}
}
