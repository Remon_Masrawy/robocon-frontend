package com.masrawy.robo.services;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dto.DTO;
import com.masrawy.robo.ws.VO;

public interface PgrlService<V extends VO, D extends DTO, PK extends Serializable> {

	public D postObject(String url, D d);
	public D postFile(String url, MultipartFile file, String fileName);
	public List<D> getList(String url);
	public Map<String, String> getMap(String url);
	public D getById(String url, Long id);
	public void execute(String url);
	public void update(String url, D d);
	public D updateR(String url, D d);
	public void deleteById(String url, Long id);
}
