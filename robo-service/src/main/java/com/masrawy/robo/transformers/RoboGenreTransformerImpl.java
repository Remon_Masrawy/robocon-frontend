package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboGenreDTO;
import com.masrawy.robo.models.RoboGenreVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboGenreTransformer")
public class RoboGenreTransformerImpl extends AbstractDTOTransformer<RoboGenreVO, RoboGenreDTO> implements RoboGenreTransformer{

	public RoboGenreTransformerImpl(){
		super(RoboGenreVO.class, RoboGenreDTO.class);
	}
}
