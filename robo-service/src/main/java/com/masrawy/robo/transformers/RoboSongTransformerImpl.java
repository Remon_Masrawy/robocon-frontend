package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboSongDTO;
import com.masrawy.robo.models.RoboSongVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboSongTransformer")
public class RoboSongTransformerImpl extends AbstractDTOTransformer<RoboSongVO, RoboSongDTO> implements RoboSongTransformer{

	public RoboSongTransformerImpl(){
		super(RoboSongVO.class, RoboSongDTO.class);
	}
}
