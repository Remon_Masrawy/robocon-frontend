package com.masrawy.robo.services;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dto.DTO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.validations.RoboBeanValidationManager;
import com.masrawy.robo.ws.VO;

public abstract class AbstractPgrlServiceImpl<V extends VO, D extends DTO, PK extends Serializable> implements PgrlService<V, D, PK>{

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private RoboBeanValidationManager validation;
	
	@Value("${robocon.id.url}")
	protected String idURL;
	
	@Value("${robocon.ws.search.url}")
	protected String searchURL;
	
	@Value("${robocon.paging.url}")
	protected String pagingURL;
	
	@Value("${robocon.ws.component.url}")
	protected String componentURL;
	
	@Value("${robocon.file-upload}")
	protected String fileUploadURL;
	
	@Override
	public final D postObject(String url, D d){
		validation.validate(d);
		V vo = getTransformer().transformDTOToVO(d);
		V v = getDAO().postObject(url, vo);
		D dto = getTransformer().transformVOToDTO(v);
		return dto;
	}
	
	@Override
	public final D postFile(String url, MultipartFile file, String fileName){
		V v = getDAO().postFile(url, file, fileName);
		D dto = getTransformer().transformVOToDTO(v);
		return dto;
	}
	
	@Override
	public final List<D> getList(String url){
		List<V> list = getDAO().getObjectList(url);
		List<D> dtoList = getTransformer().transformVOToDTO(list);
		return dtoList;
	}
	
	@Override
	public final Map<String, String> getMap(String url){
		Map<String, String> map = getDAO().getMap(url);
		return map;
	}
	
	@Override
	public final D getById(String url, Long id){
		String formatedUrl = String.format(idURL, url, id);
		V v = getDAO().getObject(formatedUrl);
		D d = getTransformer().transformVOToDTO(v);
		return d;
	}
	
	public final void execute(String url){
		getDAO().execute(url);
	}
	
	@Override
	public final void update(String url, D d){
		validation.validate(d);
		V vo = getTransformer().transformDTOToVO(d);
		getDAO().updateObject(url, vo);
	}
	
	@Override
	public final D updateR(String url, D d){
		validation.validate(d);
		V vo = getTransformer().transformDTOToVO(d);
		V v = getDAO().updateObjectR(url, vo);
		D dto = getTransformer().transformVOToDTO(v);
		return dto;
	}
	
	@Override
	public final void deleteById(String url, Long id){
		String formatedUrl = String.format(idURL, url, id);
		getDAO().deleteObject(formatedUrl);
	}
	
	protected abstract DTOTransformer<V, D> getTransformer();
	
	protected abstract DAO<V, PK> getDAO();
}
