<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title><spring:message code="robo-pages.robocon-keyword"/></title>
  
	<c:url var="logincss" value="/css/login.css"/>
	<c:url var="validation" value="/js/validation.js"/>
	<c:url var="formChecker" value="/js/formChecker.js"/>
	
	<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:600'>
  	<link rel="stylesheet" href="${logincss}"/>
  	<link rel="shortcut icon" type="image/png" href="<c:url value="/images/mazika.png"/>"/>
  
  	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
  	<script type="text/javascript" src="${formChecker}"></script>
  	<script type="text/javascript">
	  	function doBeforeSignIn(){
	  		var valid = validateSignInForm();
	  		if(valid){
	  			console.log("submit");
	  			$('#login-form').submit();
	  		}
	  	}
	  </script>

</head>

<body>
  <div class="login-wrap">
	<div class="login-html">
		<!-- ============================== Error & Info login messages ============================ -->
		<c:if test="${error}">
			<div class="login-error-msg"><strong><spring:message code="login.invalid-username-password.error-msg"/></strong></div>
		</c:if>
		<c:if test="${fail}">
			<c:if test="${not empty failMsg}">
				<div class="login-error-msg"><strong>${failMsg}</strong></div>
			</c:if>
		</c:if>
		<c:if test="${info}">
			<c:if test="${not empty infoMsgCode}">
				<div class="login-info-msg"><strong><spring:message code="${infoMsgCode}"/></strong></div>
			</c:if>
		</c:if>
		
		<div class="login-wrapper">
			<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
			
			<div class="login-form">
				<c:url var="login" value="/login"/>
				<form id="login-form" action="${login}" method="POST" novalidate>
					<div class="sign-in-htm">
						<div class="group">
							<label for="user" class="label"><spring:message code="robo-pages.username-keyword"/></label>
							<input id="sign-in-username" name="username" type="text" class="input">
							<div class="checker">
								<div class="content"></div>
							</div>
						</div>
						<div class="group">
							<label for="pass" class="label"><spring:message code="robo-pages.username-keyword"/></label>
							<input id="sign-in-password" name="password" type="password" class="input" data-type="password">
							<div class="checker">
								<div class="content"></div>
							</div>
						</div>
						<div class="group">
							<input id="check" type="checkbox" class="check" checked>
							<label for="sign-in-check"><span class="icon"></span><spring:message code="robo-pages.keep-me-sign-in-keyword"/></label>
						</div>
						<div class="group">
							<input type="submit" class="button" value="Sign In">
						</div>
						<div class="hr"></div>
						<div class="foot-lnk">
							<!-- <a href="#forgot">Forgot Password?</a> -->
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="${validation}" ></script>
<script type="text/javascript">
{
	element["sign-in-username"]	= {
			checker : "default",
			mandatory : true,
			errorMsg : {
				msg1 : "<spring:message code='login.form-username-field.error-msg'/>" 
			}
	};
	
	element["sign-in-password"]	= {
			checker : "default",
			mandatory : true,
			errorMsg : {
				msg1 : "<spring:message code='login.form-password-field.error-msg'/>" 
			}
	};
	
}

function validateSignInForm(){
	nameFields = new Array('sign-in-username', 'sign-in-password');
	if(!globalControl(nameFields))
		return false;
	return true;
}

$('#login-form .button').click(function(e){
	e.preventDefault();
	doBeforeSignIn();
})

</script>
  
</body>
</html>
