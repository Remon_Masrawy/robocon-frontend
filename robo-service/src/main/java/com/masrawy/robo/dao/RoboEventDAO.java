package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboEventVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboEventDAO extends DAO<RoboEventVO, Long>{

}
