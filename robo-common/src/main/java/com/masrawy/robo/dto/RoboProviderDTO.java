package com.masrawy.robo.dto;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

public class RoboProviderDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@Valid
	@NotEmpty
	private List<RoboMetaDTO> meta;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaDTO> meta) {
		this.meta = meta;
	}
	
}
