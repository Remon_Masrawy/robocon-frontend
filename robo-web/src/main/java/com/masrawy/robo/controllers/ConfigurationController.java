package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.CONFIGURATION_PAGE;
import static com.masrawy.robo.constants.RoleConstants.ROLE_SYSTEM;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboConfigurationDTO;
import com.masrawy.robo.services.RoboConfigurationService;

@Controller
@RequestMapping(value="/web/configuration")
public class ConfigurationController {

	@Autowired
	private RoboConfigurationService roboConfigurationService;
	
	@Secured({ROLE_SYSTEM})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView configuration(){
		ModelAndView model = new ModelAndView(CONFIGURATION_PAGE);
		model.addObject("params", roboConfigurationService.getConfigurationList());
		return model;
	}
	
	@Secured({ROLE_SYSTEM})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody void updateConfiguration(@RequestBody RoboConfigurationDTO configureParam){
		roboConfigurationService.updateConfiguration(configureParam);
	}
}
