package com.masrawy.robo.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class RoboEventMetaDTO extends RoboMetaDTO{

	private static final long serialVersionUID = 1L;

	@NotEmpty
	private String location;
	@NotEmpty
	private String city;
	@NotEmpty
	private String country;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
