package com.masrawy.robo.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public  class SecurityUtils {

	public static String getUsername(){
		CustomUserDetails user = getUser();
		return user != null ? user.getUsername() : null;
	}
	
	public static String getAuthToken(){
		CustomUserDetails user = getUser();
		return user != null ? user.getAuthToken() : null;
	}
	
	public static boolean isAuthenticated() {
		CustomUserDetails user = getUser();
		return user != null;
	}
	
	public static boolean isVerifiedUser() {
		CustomUserDetails user = getUser();
		return user != null ? user.isAccountNonExpired() : false;
	}
	
	private static CustomUserDetails getUser(){
		SecurityContext cxt = SecurityContextHolder.getContext();
		Authentication authentication = cxt.getAuthentication();
		if (authentication == null || authentication.getPrincipal() == null) {
			return null;
		}
		if (!(authentication.getPrincipal() instanceof CustomUserDetails)) {
			return null;
		}
		CustomUserDetails user = (CustomUserDetails) authentication.getPrincipal();
		return user;
	}
}
