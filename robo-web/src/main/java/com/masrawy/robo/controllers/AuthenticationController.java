package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.LOGIN_PAGE;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuthenticationController {
	
	@RequestMapping(value = {"/login", "", "/"}, method = RequestMethod.GET)
	public ModelAndView login(final Principal principal){
		if(principal != null){
			ModelAndView model = new ModelAndView("redirect:/web/dashboard");
			return model;
		}
		ModelAndView model = new ModelAndView(LOGIN_PAGE);
		return model;
	}
	
	@RequestMapping(value = "/login/failed", method = RequestMethod.GET)
	public ModelAndView loginFailed(final Principal principal){
		if(principal != null){
			ModelAndView model = new ModelAndView("redirect:/web/dashboard");
			return model;
		}
		ModelAndView model = new ModelAndView(LOGIN_PAGE);
		model.addObject("error", true);
		return model;
	}
}
