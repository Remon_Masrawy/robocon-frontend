
$(document).ready(function(){
	$container 	= $(".templatemo-content-container");
	
	$table 		= $(".templatemo-content-container .templatemo-content-widget .table.data-table");
	
	$preview 		= $(".templatemo-content-container .preview-widget");
	
	$confirmation = $(".confirmation");
	
	$container.on("click", ".new-btn", function(){
		window.location.href = $category+"/0";
	});
	
	$table.on("click", ".templatemo-link.templatemo-del-link", function(){
		var id = $(this).closest("tr").attr("data-id");
		deleteCategory(id);
	});
	
	$preview.on("click", ".preview-widget-buttons>.new-button", function(){
		addNewMetaRow({});
	});
	
	$preview.on("click" ,".preview-widget-resposive-buttons>div.save-button", function(){
		saveCategory();
	});
	
	$preview.delegate("a.templatemo-link.templatemo-meta-delete-link", "click", function(){
		$(this).closest("tr").remove();
	})
	
	$preview.on("click", ".preview-widget-content .group-responsive .render-list .selected-list .item-close", function(){
		var id = $(this).closest("span.item").attr("data-id");
		$(this).closest(".render-list").find("select").children("option[value='"+id+"']").removeAttr("hidden");
		$(this).closest("span.item").remove();
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .storefront .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $storefront //+ "/" + $(this).parent().attr("data-id");
		}
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .operator .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $operator //+ "/" + $(this).parent().attr("data-id");
		}
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .country .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $country //+ "/" + $(this).parent().attr("data-id");
		}
	});
	
	if($container.find(".data-table").hasClass("categories-table")){
		$enablePanel = true;
	}else{
		$enablePagination = false;
		$enablePanel = false;
	}
	
	$preview.find(".preview-widget-content .group-responsive>div").each(function(){
		$(this).find('input').show();
	});
	
	$preview.find('.group-responsive .storefront input[name="storefront"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchStorefronts(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	$preview.find('.group-responsive .storefront .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	})
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .storefront .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .storefront input[name="storefront"]').val("");
	    }
	});
	
	$preview.find('.group-responsive .operator input[name="operator"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchOperators(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	$preview.find('.group-responsive .operator .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	})
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .operator .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .operator input[name="operator"]').val("");
	    }
	});
	
	$preview.find('.group-responsive .country input[name="country"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchCountries(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	$preview.find('.group-responsive .country .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	})
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .country .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .country input[name="country"]').val("");
	    }
	});
	
});

$fill = function fillCategory(){
	$.when(ajaxCall($category+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, category){
			var url = $category+"/"+category.id;
			var row = addTableRow(7).attr("data-id", category.id);
			row.children("td:first-child()").addClass("tc").text(category.id);
			row.children("td:nth-child(2)").append(addLink().attr("href", url).addClass("templatemo-keyword-link").text(category.meta[0].name));
			$.each(category.storefronts, function(key, value){
				row.children("td:nth-child(3)").append(addSpan().attr("data-id", key).text(value));
			});
			$.each(category.operators, function(key, value){
				row.children("td:nth-child(4)").append(addSpan().attr("data-id", key).text(value['name']+" | "+value['country']));
			});
			$.each(category.countries, function(key, value){
				row.children("td:nth-child(5)").append(addSpan().attr("data-id", key).text(value));
			});
			row.children("td:nth-child(6)").addClass("tc").append(addLink().attr("href", url).addClass("templatemo-edit-btn").text("Edit"));
			row.children("td:last-child()").addClass("tc").append(addLink().addClass("templatemo-link templatemo-del-link").text("Delete"));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveCategory(){
	var valid = validate();
	if(!valid){
		return;
	}
	var category = {};
	category.id = parseInt($preview.attr("data-id"));
	var metaList = new Array();
	$preview.find(".preview-widget-content table>tbody").children("tr").each(function(){
		var meta = {};
		if($(this).attr("data-id")){
			meta.id = parseInt($(this).attr("data-id").trim());
		}
		meta.name = $(this).children(":nth-child(1)").text().trim();
		meta.description = $(this).children(":nth-child(2)").text().trim();
		meta.lang = $(this).children(":nth-child(3)").children("select").val().trim();
		metaList.push(meta);
	});
	var storefrontsMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .storefront .selected-list").children("span.item").each(function(){
		storefrontsMap[$(this).attr("data-id")] = "";
	});
	var operatorsMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .operator .selected-list").children("span.item").each(function(){
		operatorsMap[$(this).attr("data-id")] = new Object();
	});
	var countriesMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .country .selected-list").children("span.item").each(function(){
		countriesMap[$(this).attr("data-id")] = "";
	});
	category.meta = metaList;
	category.storefronts = storefrontsMap;
	category.operators = operatorsMap;
	category.countries = countriesMap;
	var method;
	if($preview.attr("data-id")){
		method = $HTTP_METHOD.PUT;
	}else{
		method = $HTTP_METHOD.POST;
	}
	
	$.when(ajaxCall($category, method, category, headers)).then(function(data){
		$preview.find(".preview-widget-content table>tbody").empty();
		$preview.attr("data-id", data.id);
		data.meta.forEach(function(meta, index){
			addNewMetaRow(meta);
		});
		toastr.success("Category Successfully Saved.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	});
}

function searchStorefronts(term, suggest){
	try{
		$.when(ajaxCall($storefront+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, storefront){
				var item = {};
				var data = {};
				item.id = storefront.id;
				data.name = storefront.name;
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function searchOperators(term, suggest){
	try{
		$.when(ajaxCall($operator+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, operator){
				var item = {};
				var data = {};
				item.id = operator.id;
				data.name = operator.name + " | " + operator.country.name;
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function searchCountries(term, suggest){
	try{
		$.when(ajaxCall($country+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, country){
				var item = {};
				var data = {};
				item.id = country.id;
				data.name = country.name;
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function deleteCategory(categoryId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteCategory(categoryId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteCategory(categoryId){
	
	$.when(ajaxCall($category+"/"+categoryId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+categoryId+"']").remove();
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function validate(){
	var valid = true;
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(2)").is(":empty")){
			$(this).children("td:nth-child(2)").addClass("error");
			valid = false;
		}
	});
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(3)").is(":empty")){
			$(this).children("td:nth-child(3)").addClass("error");
		}
	});
	
	return valid;
}

