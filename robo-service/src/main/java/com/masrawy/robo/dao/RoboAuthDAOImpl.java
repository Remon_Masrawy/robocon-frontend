package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboAuthVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboAuthDAO")
public class RoboAuthDAOImpl extends AbstractWebserviceDAO<RoboAuthVO, Long> implements RoboAuthDAO{

	public RoboAuthDAOImpl(){
		super(RoboAuthVO.class, new ParameterizedTypeReference<List<RoboAuthVO>>() {});
	}
}
