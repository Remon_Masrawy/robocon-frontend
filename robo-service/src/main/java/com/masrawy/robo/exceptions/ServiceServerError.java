package com.masrawy.robo.exceptions;

import static com.masrawy.robo.exceptions.ErrorConstants.ROBO_INTERNAL_ERROR;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ServiceServerError extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Web Service Server Error exception occured.";
	
	public ServiceServerError(){
		super(ERROR_MSG);
	}
	
	public ServiceServerError(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBO_INTERNAL_ERROR;
	}

}
