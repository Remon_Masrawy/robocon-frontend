$(document).ready(function(){
	$container = $(".templatemo-content-container");
	
	$table = $(".templatemo-content-container .templatemo-content-widget .table");
	
	$confirmation = $(".confirmation");
	
	$container.on("click", ".new-btn", function(){
		addNewRow();
	});
	
	$container.on("click", ".save-btn", function(){
		saveCountry();
	});
	
});

$fill = function fillCountries(){
	$.when(ajaxCall($country+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, country){
			var url = $country+"/"+country.id;
			var row = addTableRow(4).attr("data-id", country.id);
			row.children("td:first-child()").addClass("tlc").text(country.id);
			row.children("td:nth-child(2)").addClass("tll").attr("title","iso").text(country.iso);
			row.children("td:nth-child(3)").addClass("tll").attr("title","code").text(country.code);
			row.children("td:last-child()").addClass("tll").attr("title","name").text(country.name);
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveCountry(){
	var valid = validate();
	if(!valid){
		return;
	}
	var errors = new Array();
	$table.find("tbody tr").each(function(){
		var country = {};
		country.id = parseInt($(this).attr("data-id"));
		country.iso = $(this).find("td[title='iso']").text();
		country.code = $(this).find("td[title='code']").text();
		country.name = $(this).find("td[title='name']").text();
		var method;
		if($(this).attr("data-id")){
			method = $HTTP_METHOD.PUT;
		}else{
			method = $HTTP_METHOD.POST;
		}
		var that = $(this);
		$.when(ajaxCall($country, method, country, headers)).then(function(data){
			that.attr("data-id", data.id);
			that.find("td:first-child()").text(data.id);
			that.css({"border":"none"});
		},
		function(error){
			errors.push(error);
			that.css({"border":"2px solid red"});
			toast(error.responseJSON.message);
		});
		
	});
}

function addNewRow(){
	var row = addTableRow(4);
	row.children(":first-child()").addClass("tlc");
	row.children(":nth-child(2)").attr("title", "iso").addClass("tll").attr("contenteditable", true);
	row.children(":nth-child(3)").attr("title", "code").addClass("tll").attr("contenteditable", true);
	row.children(":last-child()").attr("title", "name").addClass("tll").attr("contenteditable", true);
	$table.find("tbody:last-child").append(row);
}

function deleteCountry(countryId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteCountry(countryId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteCountry(countryId){
	
	$.when(ajaxCall($country+"/"+countryId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+countryId+"']").remove();
		toastr.success("Country Successfully deleted.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function validate(){
	var valid = true;
	$table.find("tbody tr").each(function(){
		if($(this).find("td[title='iso']").text()){
			$(this).find("td[title='iso']").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td[title='iso']").css({"border":"2px solid red"});
			valid = false;
		}
		if($(this).find("td[title='code']").text()){
			$(this).find("td[title='code']").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td[title='code']").css({"border":"2px solid red"});
			valid = false;
		}
		if($(this).find("td[title='name']").text()){
			$(this).find("td[title='name']").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td[title='name']").css({"border":"2px solid red"});
			valid = false;
		}
	})
	return valid;
}