<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="preview-widget" data-id="${artist.id}">
	<c:if test="${not empty artist && not empty artist.meta}">
		<div class="preview-widget-title">
			<span title="Type"><spring:message code="robo-pages.artist-keyword"/></span>
			<span title="ID">${artist.id}</span>
			<span title="${artist.meta[0].name}">${artist.meta[0].name}</span>
		</div>
	</c:if>
	<div class="preview-widget-buttons">
		<div class="new-button"><i class="fa fas fa-plus fa-fw"></i><spring:message code="robo-pages.new-meta-data-keyword"/></div>
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="preview-widget-container">
		<div class="preview-widget-content">
			<div class="templatemo-content-container">
				<div class="templatemo-content-widget no-padding">
				
				<!-- Meta -->
	            <div class="panel panel-default table-responsive">
	              <table class="table table-striped table-bordered templatemo-user-table">
	                <thead>
	                  <tr>
	                    <td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.name-keyword"/> <span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.description-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.lang-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><spring:message code="robo-pages.delete-keyword"/></td>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:if test="${not empty artist && not empty artist.meta}">
							<c:forEach items="${artist.meta}" var="meta">
								<tr data-id="${meta.id}">
									<td contenteditable="true">${meta.name}</td>
									<td contenteditable="true">${meta.description}</td>
									<td>
										<select>
											<c:if test="${not empty languages}">
												<c:forEach items="${languages}" var="language">
													<option value="${language.code}" ${language.code == meta.lang? 'selected' : ''}>${language.code}</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
									<td><a href="javascript:" class="templatemo-link templatemo-meta-delete-link">Delete</a></td>
								</tr>
							</c:forEach>
						</c:if>
	                </tbody>
	              </table>    
	            </div> 
	            
	            <!-- Basic Info -->
	            <div class="panel-default basic-info-responsive">
	            	<div class="general">
						<div>
							<label for="birthdate">Birthdate:</label>
							<input type="text" name="birthdate" class="datepicker" contenteditable="false" value="${artist.birthDate}">
						</div>
						<div>
							<label for="deathdate">Deathdate:</label>
							<input type="text" name="deathdate" class="datepicker" contenteditable="false" value="${artist.deathDate}">
						</div>
						<div>
							<label for="gender">Gender:</label>
							<input type="radio" name="gender" value="MALE" ${artist.geneder == 'MALE'? 'checked' : ''}>Male
							<input type="radio" name="gender" value="FEMALE" ${artist.geneder == 'FEMALE'? 'checked' : ''}>Female
						</div>
						<div>
							<label for="rate">Rate:</label>
							<input type="number" name="rate" min="0.0" max="10.0" value="${artist.rate}">
						</div>
					</div>
	            </div>
	            
	            <!-- Photos -->
	            <div class="panel-default photos-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.artist-photos-keyword"/> <span><spring:message code="robo-pages.photos.note"/></span></legend>
	            		<div class="photos">
	            			<c:if test="${not empty artist.photos}">
	            				<c:forEach items="${artist.photos}" var="photo" varStatus="status">
	            					<div class="photo" data-id="${photo.id}">
	            						<c:set var="suburl" value="${fn:replace(photo.url, '{width}', '500')}"/>
	            						<c:set var="url" value="${fn:replace(suburl, '{height}', '500')}"/>
	            						<img alt="artist" src="${url}">
	            						<span class="close"></span>
	            						<input type="checkbox" name="main" class="main-photo-checkbox" title="Set as main photo" ${photo.isMainPhoto ? 'checked' : ''}>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            		<div class="add-file-button">
	            			<div class="add-sign"></div>
	            		</div>
	            		<input type="file" accept="image/*" title="Choose an image to upload">
	            	</fieldset>
	            </div>
	            
	            <!-- Albums -->
	            <c:if test="${artist.albums gt 0}">
		            <div class="panel-default albums-responsive">
		            	<fieldset>
		            		<legend><spring:message code="robo-pages.artist-albums-keyword"/></legend>
		            		<div class="albums">
		            		
		            		</div>
		            		<div class="cu-pagination-wrap">
								<ul class="cu-pagination">
									<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-prev"></i></span></a></li>
									<li><a href="javascript:" class="pages active">1</a></li>
									<li><a href="javascript:" class="pages">2</a></li>
									<li><a href="javascript:" class="pages">3</a></li>
									<li><a href="javascript:" class="pages">4</a></li>
									<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-next"></i></span></a></li>
								</ul>
							</div>
		            	</fieldset>
		            </div>
	            </c:if>
	            
	            <!-- Events -->
	            <c:if test="${not empty artist.events}">
		            <div class="panel-default events-responsive">
		            	<fieldset>
		            		<legend><spring:message code="robo-pages.artist-events-keyword"/></legend>
		            		<div class="events">
            					<c:if test="${not empty artist.events}">
		            				<c:forEach var="event" items="${artist.events}">
		            					<div class="event" data-id="${event.key}">
			            					<a href='<c:url value="/web/event/${event.key}"/>'>
			            						<c:choose>
			            							<c:when test="${not empty event.value['url']}">
			            								<c:set var="photosuburl" value="${fn:replace(event.value['url'], '{width}', '500')}"/>
				            							<c:set var="photourl" value="${fn:replace(photosuburl, '{height}', '500')}"/>
					            						<img alt="Event" src='${photourl}'>
			            							</c:when>
			            							<c:otherwise>
			            								<img alt="Event" src='<c:url value="/images/mazika.png"/>'>
			            							</c:otherwise>
			            						</c:choose>
			            						<span>${event.value['name']}</span>
			            					</a>
				            			</div>
		            				</c:forEach>
		            			</c:if>
		            		</div>
		            	</fieldset>
		            </div>
	            </c:if>
	            
	            <!-- Socials -->
	            <div class="panel-default socials-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.artist-socials-keyword"/></legend>
	            		<div class="socials">
	            			<c:if test="${not empty artist.socials}">
	            				<c:forEach items="${artist.socials}" var="social" varStatus="status">
	            					<div class="social" data-id="${social.id}" id="${social.social}">
	            						<span class="social-close"></span>
	            						<span class="logo fa-co-${fn:toLowerCase(social.social)}"></span>
	            						<input type="text" placeholder="Social Link" contenteditable="false" value="${social.link}">
	            					</div>
	            				</c:forEach>
           					</c:if>
	            		</div>
	            		<div class="add-button"></div>
	            	</fieldset>
	            </div>
	            
	            <!-- Tags -->
	            <div class="panel-default tags-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.tags-keyword"/></legend>
	            		<div class="tags">
	            			<c:if test="${not empty artist.tags}">
	            				<c:forEach items="${artist.tags}" var="tag" varStatus="status">
	            					<div class="tag" data-id="${tag.id}">
	            						<span title="tag">${tag.tag}</span>
	            						<span class="close"></span>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            	</fieldset>
	            </div>
	          </div> 
          </div>
		</div>
	</div>
	<div class="preview-widget-resposive-buttons">
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="clear"></div>
</div>
	
<script type="text/javascript" src='<c:url value="/js/artist.js"/>'></script>