
$(document).ready(function(){
	$container 	= $(".templatemo-content-container");
	
	$table 		= $(".templatemo-content-container .templatemo-content-widget .table.data-table");
	
	$preview 		= $(".templatemo-content-container .preview-widget");
	
	$confirmation = $(".confirmation");
	
	$container.on("click", ".new-btn", function(){
		window.location.href = $event+"/0";
	});
	
	$table.on("click", ".templatemo-link.templatemo-del-link", function(){
		var id = $(this).closest("tr").attr("data-id");
		deleteEvent(id);
	});
	
	$preview.find(".preview-widget-buttons>.new-button").click(function(){
		addNewEventMetaRow({});
	});
	
	$preview.find(".preview-widget-buttons>.delete-button").click(function(){
		if(!$(this).hasClass("hidden")){
			deleteEvent(parseInt($preview.attr("data-id")));
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags", function(){
		if($(this).find("input[type='text']").length){
			$(this).find("input").focus();
		}else{
			var textBox = addTextBox().attr("maxlength", 30).attr("minLength", 10).attr("pattern", "[A-Za-z1-9]{30}");
			$(this).append(textBox);
			textBox.focus();
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive div.add-file-button", function(){
		$(this).closest("fieldset").find("input[type='file']").click();
	});
	
	$preview.on("change", ".preview-widget-container>.preview-widget-content .photos-responsive input[type='file']", function(event){
		if($(this).val().length != 0){
			var image = addImage().attr("src", URL.createObjectURL(event.target.files[0]));
			var closeSpan = addSpan().addClass("close");
			var photoDiv = addDiv().addClass("photo").attr("title", event.target.files[0].name);
			photoDiv.append(image);
			photoDiv.append(closeSpan);
			$(this).closest("fieldset").find("div.photos").append(photoDiv);
		}
	});
	
	$preview.on("hover", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo", function(e){
		if(e.type == "mouseenter") {
			if(!$(this).hasClass("transparent")){
				$(this).find(".close").css({"opacity": 1});
				$(this).find("input[type='checkbox'].main-photo-checkbox").show();
			}
		  }
		  else if (e.type == "mouseleave") {
			  if(!$(this).hasClass("transparent")){
				  $(this).find(".close").css({"opacity": 0});
				  $(this).find("input[type='checkbox'].main-photo-checkbox").hide();
			  }
		  }});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".photo").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags .tag .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".tag").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo input[type='checkbox'].main-photo-checkbox", function(){
		var that = this;
		if($(this).is(":checked")){
			$preview.find(".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo input[type='checkbox'].main-photo-checkbox:checked").not($(that)).prop('checked', false);
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags", function(){
		if($(this).find("input[type='text']").length){
			$(this).find("input").focus();
		}else{
			var textBox = addTextBox().attr("maxlength", 30).attr("minLength", 10).attr("pattern", "[A-Za-z1-9]{30}");
			$(this).append(textBox);
			textBox.focus();
		}
	});
	
	$preview.on("keypress", ".preview-widget-container>.preview-widget-content .tags-responsive .tags input[type='text']", function(event){
		if(event.which == 13) {
			if($(this).val()){
				var tagDiv = addDiv().addClass("tag");
				var textSpan = addSpan().attr("title", "tag").text($(this).val());
				var closeSpan = addSpan().addClass("close");
				tagDiv.append(textSpan);
				tagDiv.append(closeSpan);
				$(this).closest(".tags").children("input").before(tagDiv);
				$(this).val("");
			}
		}
	});
	
	$preview.find(".preview-widget-resposive-buttons>div.save-button,.preview-widget-buttons>div.save-button").click(function(){
		saveEvent();
	});
	
	$preview.delegate("a.templatemo-link.templatemo-meta-delete-link", "click", function(){
		$(this).closest("tr").remove();
	})
	
	$preview.on("click", ".preview-widget-content .group-responsive .render-list .selected-list .item-close", function(){
		var id = $(this).closest("span.item").attr("data-id");
		$(this).closest(".render-list").find("select").children("option[value='"+id+"']").removeAttr("hidden");
		$(this).closest("span.item").remove();
	});
	
	if($container.find(".data-table").hasClass("events-table")){
		$enablePanel = true;
	}else{
		$enablePagination = false;
		$enablePanel = false;
	}
	
	$preview.find(".preview-widget-content .group-responsive>div").each(function(){
		$(this).find('input').show();
	});
	
	/* Artists */
	$preview.find('.group-responsive .artist input[name="artist"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchArtists(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	$preview.find('.group-responsive .artist .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	})
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .artist .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .artist input[name="artist"]').val("");
	    }
	});
	
});

$fill = function fillEvent(){
	$.when(ajaxCall($event+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, event){
			var url = $event+"/"+event.id;
			var row = addTableRow(6).attr("data-id", event.id);
			row.children("td:first-child()").addClass("tc").text(event.id);
			row.children("td:nth-child(2)").append(addLink().attr("href", url).addClass("templatemo-keyword-link").text(event.meta[0].name));
			$.each(event.artists, function(key, value){
				var link = addLink().attr("href", $artist+"/"+key).addClass("templatemo-keyword-link").text(value);
				var div = addDiv().append(link);
				row.children("td:nth-child(3)").append(div);
			});
			var image = addImage();
			if(event.photos && event.photos.length !== 0){
				$.each(event.photos, function(index_, photo){
					if(photo.isMainPhoto){
						image.attr("src", photo.url.replace("{width}", 500).replace("{height}", 500));
					}
				});
				var src = image.attr("src");
				if(typeof src == typeof undefined || src == false || src == null){
					image.attr("src", event.photos[0].url.replace("{width}", 500).replace("{height}", 500));
				}
			}
			row.children("td:nth-child(4)").addClass("tc").append(addLink().addClass("templatemo-image-link").append(image));
			row.children("td:nth-child(5)").addClass("tc").append(addLink().attr("href", url).addClass("templatemo-edit-btn").text("Edit"));
			row.children("td:last-child()").addClass("tc").append(addLink().addClass("templatemo-link templatemo-del-link").text("Delete"));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveEvent(){
	var valid = validate();
	if(!valid){
		return;
	}
	var event = {};
	event.id = parseInt($preview.attr("data-id"));
	var metaList = new Array();
	$preview.find(".preview-widget-content table>tbody").children("tr").each(function(){
		var meta = {};
		if($(this).attr("data-id")){
			meta.id = parseInt($(this).attr("data-id").trim());
		}
		meta.name = $(this).children(":first-child()").text().trim();
		meta.description = $(this).children(":nth-child(2)").text().trim();
		meta.location = $(this).children(":nth-child(3)").text().trim();
		meta.city = $(this).children(":nth-child(4)").text().trim();
		meta.country = $(this).children(":nth-child(5)").text().trim();
		meta.lang = $(this).children(":nth-child(6)").children("select").val().trim();
		metaList.push(meta);
	});
	var beginDate = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .dates input[name='beginDate']").val();
	var endDate = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .dates input[name='endDate']").val();
	var beginTime = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .dates input[name='beginTime']").val();
	var endTime = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .dates input[name='endTime']").val();
	var latitude = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .place input[name='latitude']").val();
	var longitude = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .place input[name='longitude']").val();
	var bookingURL = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .url input[name='bookingURL']").val();
	var telephone = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .tel input[name='telephone']").val();
	var active = $preview.find(".preview-widget-container>.preview-widget-content .basic-info-responsive .tel input[name='active']").is(':checked');
	event.meta = metaList;
	if(beginDate && $.trim(beginDate) !== ""){
		event.beginDate = $.datepicker.formatDate('yy-mm-dd', new Date(beginDate));;
	}
	if(endDate && $.trim(endDate) !== ""){
		event.endDate = $.datepicker.formatDate('yy-mm-dd', new Date(endDate));;
	}
	if(beginTime && $.trim(beginTime) !== ""){
		event.beginTime = beginTime;
	}
	if(endTime && $.trim(endTime) !== ""){
		event.endTime = endTime;
	}
	if(latitude && $.trim(latitude) !== ""){
		event.latitude = latitude;
	}
	if(longitude && $.trim(longitude) !== ""){
		event.longitude = longitude;
	}
	if(bookingURL && $.trim(bookingURL) !== ""){
		event.bookingTicketUrl = bookingURL;
	}
	if(telephone && $.trim(telephone) !== ""){
		event.telephone = telephone;
	}
	event.isActive = active;
	var artistsMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .artist .selected-list").children("span.item").each(function(){
		artistsMap[$(this).attr("data-id")] = "";
	});
	var photoList = new Array();
	$preview.find(".preview-widget-content .photos-responsive .photos").children(".photo").each(function(){
		var dataId = $(this).attr('data-id');
		if(typeof dataId !== typeof undefined && dataId !== false){
			var photo = {};
			photo.id = parseInt(dataId.trim());
			photo.isMainPhoto = $(this).find("input[type='checkbox'].main-photo-checkbox").is(":checked");
			photoList.push(photo);
		}
	});
	var tagList = new Array();
	$preview.find(".preview-widget-content .tags-responsive .tags").children("div.tag").each(function(){
		var tag = {};
		tag.id = $(this).attr("data-id");
		tag.tag = $(this).find("span[title='tag']").text();
		tagList.push(tag);
	});
	event.artists = artistsMap;
	event.photos = photoList;
	event.tags = tagList;
	var method;
	if($preview.attr("data-id")){
		method = $HTTP_METHOD.PUT;
	}else{
		method = $HTTP_METHOD.POST;
	}
	
	$.when(ajaxCall($event, method, event, headers)).then(function(data){
		$preview.attr("data-id", data.id);
		$preview.find(".preview-widget-buttons>.delete-button").removeClass("hidden");
		updatePopup(data);
		$preview.find(".preview-widget-content .photos-responsive .photos").children(".photo").each(function(index){
			$(this).delay(500*index);
			var photoDiv = $(this);
			var photo = $(this);
			var dataId = photo.attr('data-id');
			var type = "EVENT_IMAGE";
			if(typeof dataId === typeof undefined || dataId === false){
				var eventId = parseInt(data.id);
				var image = $(this).children("img");
				var name = $(this).attr("title");
				var xhr = new XMLHttpRequest();
				xhr.open( "GET", image.attr("src"), true );
				xhr.responseType = 'arraybuffer';
				xhr.onload = function( e ) {
					var arrayBufferView = new Uint8Array( this.response );
					var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
					var loadSpan = addSpan().addClass("load");
					var errorSpan = addSpan().addClass("error");
					var checkbox = addCheckbox().attr("name", "main").attr("title", "Set as main photo").addClass("main-photo-checkbox");
					photoDiv.append(loadSpan);
					$.when(mutipartCall($event + "/" + eventId, $HTTP_METHOD.POST, blob, type, name)).then(function(data){
						loadSpan.remove();
						photoDiv.removeClass("transparent");
						photoDiv.append(checkbox);
						photo.attr("data-id", data.photos[data.photos.length-1].id);
						toastr.success("Photo Successfully Uploaded.", 'Success', {positionClass: 'toast-bottom-right'});
					},
					function(error){
						loadSpan.remove();
						photoDiv.removeClass("transparent");
						photoDiv.append(errorSpan);
						toast(error.responseJSON.message);
					});
				};
				xhr.send();
			}
		});
		toastr.success("Event Successfully Saved.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	});
}

function searchArtists(term, suggest){
	try{
		$.when(ajaxCall($artist+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, artist){
				var item = {};
				var data = {};
				item.id = artist.id;
				data.name = artist.meta[0].name;
				if(artist.photos){
					$.each(artist.photos, function(index2, photo){
						if(photo.isMainPhoto){
							data.photo = photo.url;
						}
					});
					if(!item.photo){
						data.photo = artist.photos[0].url;
					}
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		toastr.error('Error occured.', 'Error', {positionClass: 'toast-bottom-right'});
	}
}

function addNewEventMetaRow(meta){
	var selector = getSelector();
	var row = addTableRow(7).attr("data-id", (meta.id)?meta.id:'');
	row.children(":first-child()").attr("contenteditable", true).text((meta.name)?meta.name:'');
	row.children(":nth-child(2)").attr("contenteditable", true).text((meta.description)?meta.description:'');
	row.children(":nth-child(3)").attr("contenteditable", true).text((meta.location)?meta.location:'');
	row.children(":nth-child(4)").attr("contenteditable", true).text((meta.city)?meta.city:'');
	row.children(":nth-child(5)").attr("contenteditable", true).text((meta.country)?meta.country:'');
	row.children(":nth-child(6)").append(selector.val((meta.lang)?meta.lang:'en'));
	row.children(":last-child()").append(addDeleteButton());
	$preview.find(".preview-widget-content table>tbody:last-child").append(row);
}

function deleteEvent(eventId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteEvent(eventId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteEvent(eventId){
	
	$.when(ajaxCall($event+"/"+eventId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		if($container.find(".data-table").hasClass("events-table")){
			$table.find("tr[data-id='"+eventId+"']").remove();
		}else{
			window.location.href = $event;
		}
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function validate(){
	var valid = true;
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(2)").is(":empty")){
			$(this).children("td:nth-child(2)").addClass("error");
			valid = false;
		}
	});
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(3)").is(":empty")){
			$(this).children("td:nth-child(3)").addClass("error");
		}
	});
	
	return valid;
}

function updatePopup(data){
	$preview.find(".preview-widget-content table>tbody").empty();
	data.meta.forEach(function(meta, index){
		addNewEventMetaRow(meta);
	});
	
	$preview.find(".tags-responsive").find("div.tags").empty();
	data.tags.forEach(function(tag, index){
		var tagDiv = addDiv().addClass("tag").attr("data-id", tag.id);
		var textSpan = addSpan().attr("title", "tag").text(tag.tag);
		var closeSpan = addSpan().addClass("close");
		tagDiv.append(textSpan);
		tagDiv.append(closeSpan);
		$preview.find(".tags-responsive").find("div.tags").append(tagDiv);
	});
}

function refreshPagination(){
	if(!$pagination.parent().hasClass("pagination-hidden")){
		$totalPages = Math.ceil($totalItems / $itemsPerPage.val());
		$pagination.children("li:not(:last)").remove();
		for(page=1; page<=$totalPages; page++){
			var $listItem = listItem().append(addLink().attr("data-item", page).text(page));
			$pagination.children("li:last-child()").before($listItem);
		}
		$pagination.find("li:nth-child("+$currentPage+")").addClass("active");
	}
	
}
