package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboOperatorDTO;
import com.masrawy.robo.models.RoboOperatorVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboOperatorTransformer extends DTOTransformer<RoboOperatorVO, RoboOperatorDTO>{

}
