<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <c:set var="title"><tiles:getAsString name='title'/></c:set>
    <c:set var="item"><tiles:getAsString name='item'/></c:set>
    <title><spring:message code="page.title"/> - <c:out value="${fn:toUpperCase(fn:substring(title,0,1))}${fn:toLowerCase(fn:substring(title,1,fn:length(title)))}"/></title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">
    <link rel="shortcut icon" type="image/png" href="<c:url value="/images/mazika.png"/>"/>

    <link href='<c:url value="/css/font-family.css"/>' rel='stylesheet' type='text/css'>
    <link href='<c:url value="/custom/jquery-ui-1.12.1/jquery-ui.css"/>' rel="stylesheet">
    <link href='<c:url value="/custom/jquery-ui-1.12.1/jquery-ui.min.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/font-awesome-all.css"/>' rel="stylesheet">
    <!-- <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"> -->
    <link href='<c:url value="/css/bootstrap.min.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/templatemo-style.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/autocomplete-dropdown.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/jquery.auto-complete.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/robocon.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/tablet.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/mobile.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/toastr.css"/>' rel="stylesheet">
    
    <script src='<c:url value="/js/jquery-1.11.2.min.js"/>'></script>      <!-- jQuery -->
    <%-- <script src='<c:url value="/custom/jquery-ui-1.12.1/jquery-ui.js"/>'></script> --%>
    <script src='<c:url value="/custom/jquery-ui-1.12.1/jquery-ui.min.js"/>'></script>
    <script src='<c:url value="/js/validation.js"/>'></script>
    <script src='<c:url value="/js/formChecker.js"/>'></script>
    <script type="text/javascript" src='<c:url value="/js/robocon.js"/>'></script>
    
  </head>
  <body>  
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <tiles:insertAttribute name="menu" />
      </div>
      
      
      <!-- Main content -->
      <div class="templatemo-content col-1 light-gray-bg">
        <tiles:insertAttribute name="header" />
        <div class="templatemo-content-container fade-out">
       	 	<tiles:insertAttribute name="body" />
       	 	<tiles:insertAttribute name="notifications" /> 
       	</div>
      	<tiles:insertAttribute name="footer" /> 
      </div>
    </div>
    
    <!-- JS -->
    <script src='<c:url value="/js/jquery.autocomplete-dropdown-min.js"/>'></script>
    <script src='<c:url value="/js/jquery.auto-complete.js"/>'></script>
    <script src='<c:url value="/js/jquery.auto-complete.min.js"/>'></script>
    <script src='<c:url value="/js/jquery-migrate-1.2.1.min.js"/>'></script> <!--  jQuery Migrate Plugin -->
    <script type="text/javascript" src='<c:url value="/js/google-chart.js"/>'></script> <!-- Google Chart -->
    <script type="text/javascript" src='<c:url value="/js/templatemo-script.js"/>'></script>      <!-- Templatemo Script -->
    <script type="text/javascript" src='<c:url value="/js/toastr.js"/>'></script>
    <script type="text/javascript" src='<c:url value="/js/pagination.js"/>'></script>
	<script type="text/javascript">
	
	$(document).ready(function(){
		try {
			$('.templatemo-sidebar .templatemo-left-nav li a#${item}').addClass("active");
		} catch (e) {
			console.log("Item ${item} not reconized.")
		}
	});
	
	</script>

  </body>
</html>