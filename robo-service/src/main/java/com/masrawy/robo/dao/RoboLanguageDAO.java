package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboLanguageVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboLanguageDAO extends DAO<RoboLanguageVO, String>{

}
