package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboStorefrontVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboStorefrontDAO extends DAO<RoboStorefrontVO, Long>{

}
