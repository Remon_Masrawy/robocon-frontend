package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboOperatorVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboOperatorDAO extends DAO<RoboOperatorVO, Long>{

}
