package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboArtistDTO;
import com.masrawy.robo.models.RoboArtistVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboArtistTransformer extends DTOTransformer<RoboArtistVO, RoboArtistDTO>{

}
