package com.masrawy.robo.json;

import java.io.ByteArrayInputStream;
import java.io.File;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ByteArraySerializer;
import com.fasterxml.jackson.databind.ser.std.FileSerializer;
import com.masrawy.robo.file.model.FileUpload;
import com.masrawy.robo.file.model.FileUploadSerializer;
import com.masrawy.robo.serializers.ByteArrayInputStreamSerializer;
import com.masrawy.robo.serializers.CommonsMultipartFileSerializer;
import com.masrawy.robo.serializers.DiskFileItemSerializer;

public class JacksonModule extends SimpleModule{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JacksonModule(){
		addSerializer(new ByteArraySerializer());
		addSerializer(File.class, new FileSerializer());
		addSerializer(FileUpload.class, new FileUploadSerializer());
		addSerializer(ByteArrayInputStream.class, new ByteArrayInputStreamSerializer());
		addSerializer(CommonsMultipartFile.class, new CommonsMultipartFileSerializer());
		addSerializer(DiskFileItem.class, new DiskFileItemSerializer());
		addSerializer(new ByteArraySerializer());
	}
}
