package com.masrawy.robo.dto;

import java.util.List;

public class RoboSongFrontendDTO extends RoboFrontendDTO{

	private static final long serialVersionUID = 1L;

	private List<RoboSongDTO> songs;

	public List<RoboSongDTO> getSongs() {
		return songs;
	}

	public void setSongs(List<RoboSongDTO> songs) {
		this.songs = songs;
	}
}
