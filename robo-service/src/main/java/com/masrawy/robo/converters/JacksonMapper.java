package com.masrawy.robo.converters;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.masrawy.robo.json.JacksonModule;

@Component("objectMapper")
public class JacksonMapper extends ObjectMapper{

	private static final long serialVersionUID = 1L;
	
	public JacksonMapper(){
		this.registerModule(new JacksonModule());
		this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		this.configure(MapperFeature.AUTO_DETECT_FIELDS, false);
	}

}
