package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.FRONTENDS_PAGE;
import static com.masrawy.robo.constants.RoboPages.FRONTEND_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboFrontendDTO;
import com.masrawy.robo.services.RoboCategoryService;
import com.masrawy.robo.services.RoboFrontendService;
import com.masrawy.robo.services.RoboLanguageService;

@Controller
@RequestMapping(value = "/web/frontend")
public class FrontendController {
	
	@Autowired
	private RoboFrontendService roboFrontendService;
	
	@Autowired
	private RoboCategoryService roboCategoryService;
	
	@Autowired
	private RoboLanguageService roboLanguageService;

	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView frontendPage(){
		ModelAndView model = new ModelAndView(FRONTENDS_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{frontendId}", method = RequestMethod.GET)
	public ModelAndView frontend(@PathVariable("frontendId") String frontendId){
		ModelAndView model = new ModelAndView(FRONTEND_PAGE);
		try {
			Long id = Long.valueOf(frontendId);
			if(id != 0){
				model.addObject("frontend", roboFrontendService.getFrontend(id));
			}
			model.addObject("types", roboFrontendService.getFrontendTypes());
			model.addObject("categories", roboCategoryService.getCategoryList(0, 0));
			model.addObject("languages", roboLanguageService.getLanguageList(0, 0));
		} catch (Exception e) {
			model = new ModelAndView("redirect:/web/frontend");
		}
		
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboFrontendDTO create(@RequestBody RoboFrontendDTO roboFrontendDTO){
		return roboFrontendService.createFrontend(roboFrontendDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboFrontendDTO update(@RequestBody RoboFrontendDTO roboFrontendDTO){
		return roboFrontendService.updateFrontend(roboFrontendDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value="/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboFrontendDTO> frontends(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboFrontendService.getFrontends(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{frontendId}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("frontendId") Long frontendId){
		roboFrontendService.deleteFrontend(frontendId);
	}
}
