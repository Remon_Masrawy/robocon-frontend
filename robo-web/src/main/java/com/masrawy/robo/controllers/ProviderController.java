package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.PROVIDER_PAGE;
import static com.masrawy.robo.constants.RoboPages.PROVIDERS_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboProviderDTO;
import com.masrawy.robo.services.RoboLanguageService;
import com.masrawy.robo.services.RoboProviderService;

@Controller
@RequestMapping(value = "/web/provider")
public class ProviderController {

	@Autowired
	private RoboProviderService roboProviderService;
	
	@Autowired
	private RoboLanguageService roboLanguageService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView providerPage(){
		ModelAndView model = new ModelAndView(PROVIDERS_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{providerId}", method = RequestMethod.GET)
	public ModelAndView provider(@PathVariable("providerId") String providerId){
		ModelAndView model = new ModelAndView(PROVIDER_PAGE);
		try {
			Long id = Long.valueOf(providerId);
			if(id != 0){
				model.addObject("provider", roboProviderService.getProvider(id));
			}
			model.addObject("languages", roboLanguageService.getLanguageList(0, 0));
		} catch (Exception e) {
			model = new ModelAndView("redirect:/web/provider");
		}
		
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboProviderDTO create(@RequestBody RoboProviderDTO roboProviderDTO){
		return roboProviderService.createProvider(roboProviderDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboProviderDTO update(@RequestBody RoboProviderDTO roboProviderDTO){
		return roboProviderService.updateProvider(roboProviderDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value="/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboProviderDTO> providers(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboProviderService.getProviderList(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{providerId}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("providerId") Long providerId){
		roboProviderService.deleteProvider(providerId);
	}
	
}
