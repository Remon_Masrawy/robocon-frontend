package com.masrawy.robo.services;

import java.util.List;

import com.masrawy.robo.dto.RoboGenreDTO;
import com.masrawy.robo.models.RoboGenreVO;

public interface RoboGenreService extends PgrlService<RoboGenreVO, RoboGenreDTO, Long>{

	public RoboGenreDTO createGenre(RoboGenreDTO roboGenreDTO);
	public RoboGenreDTO updateGenre(RoboGenreDTO roboGenreDTO);
	public RoboGenreDTO getGenre(Long id);
	public List<RoboGenreDTO> getGenreList(Integer page, Integer limit);
	public void deleteGenre(Long id);
}
