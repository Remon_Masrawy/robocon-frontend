<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:url var="logout" value="/logout"/>

<div class="search">
	<div class="search-header"><spring:message code="robo-pages.search-keyword"/></div>
	<section>
		<div>
			<select id="type" name="type">
				<option value=""><spring:message code="robo-pages.choose-type-keyword"/></option>
				<option value="song"><spring:message code="robo-pages.song-keyword"/></option>
				<option value="artist"><spring:message code="robo-pages.artist-keyword"/></option>
				<option value="album"><spring:message code="robo-pages.album-keyword"/></option>
				<option value="frontend"><spring:message code="robo-pages.frontend-keyword"/></option>
				<option value="provider"><spring:message code="robo-pages.provider-keyword"/></option>
				<option value="genre"><spring:message code="robo-pages.genre-keyword"/></option>
				<option value="event"><spring:message code="robo-pages.event-keyword"/></option>
			</select>
		</div>
		<div>
			<label for="code"><spring:message code="robo-pages.code-keyword"/>:</label>
			<fieldset>
				<input id="codefrom" name="code" type="number" autocomplete="off" placeholder="From">
				<input id="codeto" name="code" type="number" autocomplete="off" placeholder="To">
			</fieldset>
		</div>
		<div>
			<label for="name"><spring:message code="robo-pages.name-keyword"/>:</label>
			<input id="name" name="name" type="text" autocomplete="off" class="search-name">
		</div>
		<div>
			<label for="artist"><spring:message code="robo-pages.artist-keyword"/>:</label>
			<input id="artist" name="artist" type="text" autocomplete="off">
		</div>
		<div>
			<label for="album"><spring:message code="robo-pages.album-keyword"/>:</label>
			<input id="album" name="album" type="text" autocomplete="off">
		</div>
		<div>
			<label for="provider"><spring:message code="robo-pages.provider-keyword"/>:</label>
			<input id="provider" name="provider" type="text" autocomplete="off">
		</div>
		<div>
			<label for="genre"><spring:message code="robo-pages.genre-keyword"/>:</label>
			<input id="genre" name="genre" type="text" autocomplete="off">
		</div>
		<div>
			<label for="operator"><spring:message code="robo-pages.operator-keyword"/>:</label>
			<input id="operator" name="operator" type="text" autocomplete="off">
		</div>
		<div>
			<label for="storefront"><spring:message code="robo-pages.storefront-keyword"/>:</label>
			<input id="storefront" name="storefront" type="text" autocomplete="off">
		</div>
		<div>
			<label for="country"><spring:message code="robo-pages.country-keyword"/>:</label>
			<input id="country" name="country" type="text" autocomplete="off">
		</div>
	</section>
	<div class="search-buttons">
		<input type="button" name="search" value="Search" class="search-btn">
		<input type="button" name="add" class="add-btn">
		<input type="button" name="go" value="GO" class="go-btn">
	</div>
	<div class="control-buttons">
		<input type="button" name="close" class="close-btn">
	</div>
</div>

<div class="search-result">
	<div class="result-header">
		<spring:message code="robo-pages.search-result-keyword"/> (<span></span>)
	</div>
	<div class="tab">
	  <button id="songs" class="tablinks"><spring:message code="robo-pages.songs-keyword"/></button>
	  <button id="albums" class="tablinks"><spring:message code="robo-pages.albums-keyword"/></button>
	  <button id="artists" class="tablinks"><spring:message code="robo-pages.artists-keyword"/></button>
	  <button id="events" class="tablinks"><spring:message code="robo-pages.events-keyword"/></button>
	  <button id="frontends" class="tablinks"><spring:message code="robo-pages.frontends-keyword"/></button>
	  <button id="categories" class="tablinks"><spring:message code="robo-pages.categories-keyword"/></button>
	  <button id="providers" class="tablinks"><spring:message code="robo-pages.providers-keyword"/></button>
	  <button id="genres" class="tablinks"><spring:message code="robo-pages.genres-keyword"/></button>
	</div>
	<div class="result-body">
		<div class="songs">
			<fieldset>
				<legend><span><spring:message code="robo-pages.zero-keyword"/></span> <spring:message code="robo-pages.songs-found-keyword"/></legend>
				<div class="items">
					<div class="panel panel-default table-responsive">
						<table class="table table-striped table-bordered templatemo-user-table">
							<thead>
								<tr>
									<th class="w-50"><spring:message code="robo-pages.serial-keyword"/></th>
            						<th class="w-100"><spring:message code="robo-pages.id-keyword"/></th>
            						<th><spring:message code="robo-pages.image-keyword"/></th>
            						<th><spring:message code="robo-pages.name-keyword"/></th>
            						<th><spring:message code="robo-pages.album-keyword"/></th>
            						<th><spring:message code="robo-pages.artists-keyword"/></th>
            					</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination-wrap">
					<ul class="pagination">
						<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
					</ul>
				</div>
			</fieldset>
		</div>
		<div class="albums">
			<fieldset>
				<legend><span><spring:message code="robo-pages.zero-keyword"/></span> <spring:message code="robo-pages.albums-found-keyword"/></legend>
				<div class="items">
					<div class="panel panel-default table-responsive">
						<table class="table table-striped table-bordered templatemo-user-table">
							<thead>
								<tr>
									<th class="w-50"><spring:message code="robo-pages.serial-keyword"/></th>
            						<th class="w-100"><spring:message code="robo-pages.id-keyword"/></th>
            						<th><spring:message code="robo-pages.image-keyword"/></th>
            						<th><spring:message code="robo-pages.name-keyword"/></th>
            						<th><spring:message code="robo-pages.artists-keyword"/></th>
            					</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination-wrap">
					<ul class="pagination">
						<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
					</ul>
				</div>
			</fieldset>
		</div>
		<div class="artists">
			<fieldset>
				<legend><span><spring:message code="robo-pages.zero-keyword"/></span> <spring:message code="robo-pages.artists-found-keyword"/></legend>
				<div class="items">
					<div class="panel panel-default table-responsive">
						<table class="table table-striped table-bordered templatemo-user-table">
							<thead>
								<tr>
									<th class="w-50"><spring:message code="robo-pages.serial-keyword"/></th>
            						<th class="w-100"><spring:message code="robo-pages.id-keyword"/></th>
            						<th><spring:message code="robo-pages.image-keyword"/></th>
            						<th><spring:message code="robo-pages.name-keyword"/></th>
            					</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination-wrap">
					<ul class="pagination">
						<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
					</ul>
				</div>
			</fieldset>
		</div>
		<div class="events">
			<fieldset>
				<legend><span><spring:message code="robo-pages.zero-keyword"/></span> <spring:message code="robo-pages.events-found-keyword"/></legend>
				<div class="items">
					<div class="panel panel-default table-responsive">
						<table class="table table-striped table-bordered templatemo-user-table">
							<thead>
								<tr>
									<th class="w-50"><spring:message code="robo-pages.serial-keyword"/></th>
            						<th class="w-100"><spring:message code="robo-pages.id-keyword"/></th>
            						<th><spring:message code="robo-pages.image-keyword"/></th>
            						<th><spring:message code="robo-pages.name-keyword"/></th>
            						<th><spring:message code="robo-pages.artists-keyword"/></th>
            					</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination-wrap">
					<ul class="pagination">
						<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
					</ul>
				</div>
			</fieldset>
		</div>
		<div class="frontends">
			<fieldset>
				<legend><span><spring:message code="robo-pages.zero-keyword"/></span> <spring:message code="robo-pages.frontends-found-keyword"/></legend>
				<div class="items">
					<div class="panel panel-default table-responsive">
						<table class="table table-striped table-bordered templatemo-user-table">
							<thead>
								<tr>
									<th class="w-50"><spring:message code="robo-pages.serial-keyword"/></th>
            						<th class="w-100"><spring:message code="robo-pages.id-keyword"/></th>
            						<th><spring:message code="robo-pages.name-keyword"/></th>
            						<th><spring:message code="robo-pages.type-keyword"/></th>
            						<th><spring:message code="robo-pages.categories-keyword"/></th>
            					</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination-wrap">
					<ul class="pagination">
						<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
					</ul>
				</div>
			</fieldset>
		</div>
		<div class="categories">
			<fieldset>
				<legend><span><spring:message code="robo-pages.zero-keyword"/></span> <spring:message code="robo-pages.categories-found-keyword"/></legend>
				<div class="items">
					<div class="panel panel-default table-responsive">
						<table class="table table-striped table-bordered templatemo-user-table">
							<thead>
								<tr>
									<th class="w-50"><spring:message code="robo-pages.serial-keyword"/></th>
            						<th class="w-100"><spring:message code="robo-pages.id-keyword"/></th>
            						<th><spring:message code="robo-pages.name-keyword"/></th>
            						<th><spring:message code="robo-pages.storefronts-keyword"/></th>
            						<th><spring:message code="robo-pages.operators-keyword"/></th>
            						<th><spring:message code="robo-pages.countries-keyword"/></th>
            					</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination-wrap">
					<ul class="pagination">
						<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
					</ul>
				</div>
			</fieldset>
		</div>
		<div class="providers">
			<fieldset>
				<legend><span><spring:message code="robo-pages.zero-keyword"/></span> <spring:message code="robo-pages.providers-found-keyword"/></legend>
				<div class="items">
					<div class="panel panel-default table-responsive">
						<table class="table table-striped table-bordered templatemo-user-table">
							<thead>
								<tr>
									<th class="w-50"><spring:message code="robo-pages.serial-keyword"/></th>
            						<th class="w-100"><spring:message code="robo-pages.id-keyword"/></th>
            						<th><spring:message code="robo-pages.name-keyword"/></th>
            					</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination-wrap">
					<ul class="pagination">
						<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
					</ul>
				</div>
			</fieldset>
		</div>
		<div class="genres">
			<fieldset>
				<legend><span><spring:message code="robo-pages.zero-keyword"/></span> <spring:message code="robo-pages.genres-found-keyword"/></legend>
				<div class="items">
					<div class="panel panel-default table-responsive">
						<table class="table table-striped table-bordered templatemo-user-table">
							<thead>
								<tr>
									<th class="w-50"><spring:message code="robo-pages.serial-keyword"/></th>
            						<th class="w-100"><spring:message code="robo-pages.id-keyword"/></th>
            						<th><spring:message code="robo-pages.name-keyword"/></th>
            					</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination-wrap">
					<ul class="pagination">
						<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
					</ul>
				</div>
			</fieldset>
		</div>
	</div>
</div>

<div class="umbrella">
	<span class="close"></span>
</div>

<div class="custom-menu">
	<ul>
	</ul>
</div>

<div class="auth-expired">
	<div class="header">
		<span class="title"><spring:message code="notification.authentication-expired.msg"/></span>
		<span class="close"></span>
	</div>
	<div class="body">
		<div class="icons">
			<span class="warning_icon"></span>
		</div>
	
		<div class="message">
			<p><spring:message code="notification.authentication-expired.error-msg"/></p>
			<br>
			<p><spring:message code="notification.redirect-to-login-page.msg"/></p>
		</div>
	</div>
	<div class="buttons">
		<a class="ok" href="${logout}"><spring:message code="notification.authentication-expired.continue-btn"/></a>
	</div>
</div>

<div class="server-down">
	<div class="header">
		<span class="title"><spring:message code="notification.server-down.msg"/></span>
		<span class="close"></span>
	</div>
	<div class="body">
		<div class="icons">
			<span class="warning_icon"></span>
		</div>
	
		<div class="message">
			<p><spring:message code="notification.server-down.warning-msg"/></p>
		</div>
	</div>
	<div class="buttons">
		<a class="ok"><spring:message code="notification.server-down.ok-btn"/></a>
	</div>
</div>

<div class="info-panel">
	<div><spring:message code="robo-pages.total-keyword"/>: <span class="panel-total"></span></div>
	<div>
		<spring:message code="robo-pages.items-per-page-keyword"/>: 
		<select class="total-per-page">
			<option value="10"><spring:message code="robo-pages.ten-keyword"/></option>
			<option value="20"><spring:message code="robo-pages.twenty-keyword"/></option>
			<option value="30"><spring:message code="robo-pages.thirty-keyword"/></option>
			<option value="40"><spring:message code="robo-pages.forty-keyword"/></option>
			<option value="50"><spring:message code="robo-pages.fifty-keyword"/></option>
			<option value="0"><spring:message code="robo-pages.all-keyword"/></option>
		</select>
	</div>
</div>

<div class="preview">
	<span class="close"></span>
</div>

<div class="confirmation">
	<div class="title"><spring:message code="robo-pages.confirmation-keyword"/></div>
	<div class="confirm-msg"><spring:message code="robo-pages.confirmation-request-keyword"/></div>
	<div class="confirm-buttons">
		<div class="confirm-cancel-button"><spring:message code="robo-pages.cancel-keyword"/></div>
		<div class="confirm-agree-button"><spring:message code="robo-pages.agree-keyword"/></div>
	</div>
</div>

<!-- Second row ends -->
<div class="pagination-wrap global">
	<ul class="pagination">
		<li><a href="javascript:" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-play"></i></span></a></li>
	</ul>
</div>

<script type="text/javascript" src='<c:url value="/js/search.js"/>'></script>