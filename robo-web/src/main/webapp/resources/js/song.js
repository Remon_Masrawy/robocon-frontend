
var mimeType = {"FULL_TRACK":"Full Track", "TRUETONE":"True Tone", "STREAM":"Stream"};

$(document).ready(function(){
	$container 			= $(".templatemo-content-container");
	$table 				= $(".templatemo-content-container .templatemo-content-widget .table.data-table");
	$preview 			= $(".templatemo-content-container .preview-widget");
	$confirmation 		= $(".confirmation");
	$genres				= new Object();
	
	$totalAlbums		= 0;
	$albumsPerPage		= 2;
	$currentAlbumPage	= 1;
	
	$table.on("click", ".templatemo-link.templatemo-del-link", function(){
		var id = $(this).closest("tr").attr("data-id");
		deleteSong(id);
	});
	
	$container.on("click", ".new-btn", function(){
		window.location.href = $song+"/0";
	});
	
	$preview.find(".preview-widget-buttons>.new-button").click(function(){
		addNewMetaRow({});
	});
	
	$preview.find(".preview-widget-resposive-buttons>div.save-button,.preview-widget-buttons>div.save-button").click(function(){
		saveSong();
	});
	
	$preview.delegate("a.templatemo-link.templatemo-meta-delete-link", "click", function(){
		$(this).closest("tr").remove();
	})
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .songs-responsive .assign", function(){
		$(this).removeClass("assign").addClass("unassign");
	});
	$preview.on("click", ".preview-widget-container>.preview-widget-content .songs-responsive .unassign", function(){
		$(this).removeClass("unassign").addClass("assign");
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive div.add-file-button", function(){
		$(this).closest("fieldset").find("input[type='file']").click();
	});
	$preview.on("click", ".preview-widget-container>.preview-widget-content .content-responsive div.add-file-button", function(){
		$(this).closest("fieldset").find("input[type='file']").click();
	});
	$preview.on("click", ".preview-widget-container>.preview-widget-content .lyrics-responsive div.add-file-button", function(){
		$(this).closest("fieldset").find("input[type='file']").click();
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo input[type='checkbox'].main-photo-checkbox", function(){
		var that = this;
		if($(this).is(":checked")){
			$preview.find(".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo input[type='checkbox'].main-photo-checkbox:checked").not($(that)).prop('checked', false);
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags", function(){
		if($(this).find("input[type='text']").length){
			$(this).find("input").focus();
		}else{
			var textBox = addTextBox().attr("maxlength", 30).attr("minLength", 10).attr("pattern", "[A-Za-z1-9]{30}");
			$(this).append(textBox);
			textBox.focus();
		}
	});
	
	$preview.on("keypress", ".preview-widget-container>.preview-widget-content .tags-responsive .tags input[type='text']", function(event){
		if(event.which == 13) {
			if($(this).val()){
				var tagDiv = addDiv().addClass("tag");
				var textSpan = addSpan().attr("title", "tag").text($(this).val());
				var closeSpan = addSpan().addClass("close");
				tagDiv.append(textSpan);
				tagDiv.append(closeSpan);
				$(this).closest(".tags").children("input").before(tagDiv);
				$(this).val("");
			}
		}
	});
	
	$preview.on("change", ".preview-widget-container>.preview-widget-content .photos-responsive input[type='file']", function(event){
		if($(this).val().length != 0){
			var image = addImage().attr("src", URL.createObjectURL(event.target.files[0]));
			var closeSpan = addSpan().addClass("close");
			var photoDiv = addDiv().addClass("photo");
			photoDiv.append(image);
			photoDiv.append(closeSpan);
			$(this).closest("fieldset").find("div.photos").append(photoDiv);
			$(this).val('');
		}
	});
	$preview.on("change", ".preview-widget-container>.preview-widget-content .content-responsive input[type='file']", function(event){
		if($(this).val().length != 0){
			var blobURL = URL.createObjectURL(event.target.files[0]);
			var audio = addAudio();
			var source = addSource().attr("src", blobURL).attr("type", "audio/mpeg");
			audio.append(source);
			var closeSpan = addSpan().addClass("close");
			var contentDiv = addDiv().addClass("item").attr("title", event.target.files[0].name);
			contentDiv.append(audio);
			contentDiv.append(closeSpan);
			$(this).closest("fieldset").find("div.content").append(contentDiv);
			$(this).val('');
		}
	});
	
	$preview.on("change", ".preview-widget-container>.preview-widget-content .lyrics-responsive input[type='file']", function(event){
		if($(this).val().length != 0){
			var textArea = addTextArea();
			var inputBox	= $(this);
			var lyrics = $(this).closest("fieldset").find("div.lyrics");
			var closeSpan = addSpan().addClass("close");
			var itemDiv = addDiv().addClass("item").attr("title", event.target.files[0].name);
			var reader = new FileReader();
            reader.onload = function () {
            	textArea.val($.trim(this.result.replace(/[\t\n]+/g,' ')));
            	itemDiv.append(textArea);
            	itemDiv.append(closeSpan);
            	lyrics.append(itemDiv);
            	inputBox.closest("fieldset").find(".add-file-button").hide();
            };
            reader.onerror = function(event){
            	toast("File could not be read! Code " + event.target.error.code);
            };
            reader.readAsText(event.target.files[0]);
            $(this).val('');
		}
	});
	
	$preview.on("hover", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo", function(e){
		if(e.type == "mouseenter") {
			if(!$(this).hasClass("transparent")){
				$(this).find(".close").css({"opacity": 1});
				$(this).find("input[type='checkbox'].main-photo-checkbox").show();
			}
		  }
		  else if (e.type == "mouseleave") {
			  if(!$(this).hasClass("transparent")){
				  $(this).find(".close").css({"opacity": 0});
				  $(this).find("input[type='checkbox'].main-photo-checkbox").hide();
			  }
		  }
	});
	$preview.on("hover", ".preview-widget-container>.preview-widget-content .content-responsive .content .item", function(e){
		if(e.type == "mouseenter") {
			if(!$(this).hasClass("transparent")){
				$(this).find(".close").css({"opacity": 1});
			}
		  }
		  else if (e.type == "mouseleave") {
			  if(!$(this).hasClass("transparent")){
				  $(this).find(".close").css({"opacity": 0});
			  }
		  }
	});
	$preview.on("hover", ".preview-widget-container>.preview-widget-content .lyrics-responsive .lyrics .item", function(e){
		if(e.type == "mouseenter") {
			if(!$(this).hasClass("transparent")){
				$(this).find(".close").css({"opacity": 1});
			}
		  }
		  else if (e.type == "mouseleave") {
			  if(!$(this).hasClass("transparent")){
				  $(this).find(".close").css({"opacity": 0});
			  }
		  }
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".photo").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	$preview.on("click", ".preview-widget-container>.preview-widget-content .content-responsive .content .item .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".item").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	$preview.on("click", ".preview-widget-container>.preview-widget-content .lyrics-responsive .lyrics .item .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest("fieldset").find(".add-file-button").removeClass("hidden");
			that.closest("fieldset").find(".add-file-button").show();
			that.closest(".item").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .socials-responsive .add-button", function(e){
		var socialDiv = addDiv().addClass("social new");
		var input = addTextBox().attr("placeholder", "Social Link");
		var selector = $("<select></select>").append($("<option></option>").text("Social..."));
		$.each($socials, function(key, value){
			var option = $("<option></option>").val(key).text(value);
			selector.append(option);
		});
		var close = addSpan().addClass("social-close");
		socialDiv.append(close);
		socialDiv.append(selector);
		socialDiv.append(input);
		$(this).closest(".socials-responsive").find(".socials").append(socialDiv);
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .socials-responsive .social-close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".social").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags .tag .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".tag").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	
	$.each($preview.find(".preview-widget-container>.preview-widget-content .lyrics-responsive .lyrics .item"), function(e){
		var textarea = $(this).children("textarea");
		var xhr;
		if (window.XMLHttpRequest){
		        // code for IE7+, Firefox, Chrome, Opera, Safari
			xhr=new XMLHttpRequest();
		}else{
		    // code for IE6, IE5
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xhr.open( "GET", textarea.attr("data-url"), true );
		xhr.onload = function( e ) {
			textarea.val(xhr.responseText);
		}
		xhr.send();
	});
	
	$preview.on("change", ".preview-widget-container>.preview-widget-content .lyrics-responsive .lyrics .item textarea", function(){
		$(this).closest(".item").removeAttr("data-id");
	});
	
	$preview.on("click", ".preview-widget-content .albums-responsive .cu-pagination-wrap .fa-next", function(){
		var currentActive = $(this).closest(".cu-pagination").find("li:has(a.pages.active)");
		var nextElem = currentActive.next("li");
		if(nextElem.children("a.pages").length > 0){
			currentActive.find("a").removeClass("active");
			nextElem.find("a").addClass("active");
			$currentAlbumPage++;
			songAlbums();
		}
	});
	
	$preview.on("click", ".preview-widget-content .albums-responsive .cu-pagination-wrap .fa-prev", function(){
		var currentActive = $(this).closest(".cu-pagination").find("li:has(a.pages.active)");
		var prevElem = currentActive.prev("li");
		if(prevElem.children("a.pages").length > 0){
			currentActive.find("a").removeClass("active");
			prevElem.find("a").addClass("active");
			$currentAlbumPage--;
			songAlbums();
		}
	});
	
	$preview.on("click", ".preview-widget-content .albums-responsive .cu-pagination-wrap li a.pages:not(.active)", function(){
		var currentActive = $(this).closest(".cu-pagination").find("li:has(a.pages.active)");
		var selectedElem = $(this).closest("li");
		currentActive.find("a").removeClass("active");
		selectedElem.find("a").addClass("active");
		$currentAlbumPage = selectedElem.index();
		songAlbums();
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .artist .render-list .selected-list .item>span[title='value'], .preview-widget-content .group-responsive .main-artist .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $artist + "/" + $(this).parent().attr("data-id");
		}
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .genre .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $genre + "/" + $(this).parent().attr("data-id");
		}
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .album .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $album + "/" + $(this).parent().attr("data-id");
		}
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .provider .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $provider + "/" + $(this).parent().attr("data-id");
		}
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .render-list .selected-list .item-close", function(){
		var id = $(this).closest("span.item").attr("data-id");
		if($(this).closest(".render-list").parent().hasClass("album")){
			$(this).closest(".render-list").parent().find('input[name="album"]').show();
		}
		if($(this).closest(".render-list").parent().hasClass("provider")){
			$(this).closest(".render-list").parent().find('input[name="provider"]').show();
		}
		$(this).closest("span.item").remove();
	});
	
	$preview.find(".preview-widget-content .group-responsive>div").each(function(){
		if($(this).hasClass("album")){
			if($(this).find('.selected-list .item').length == 0){
				$(this).find('input[name="album"]').show();
	    	}
		}else if($(this).hasClass("provider")){
			if($(this).find('.selected-list .item').length == 0){
				$(this).find('input[name="provider"]').show();
	    	}
		}else{
			$(this).find('input').show();
		}
	});
	
	if($container.find(".data-table").hasClass("songs-table")){
		$enablePanel = true;
	}else{
		$enablePagination = false;
		$enablePanel = false;
	}
	
	/* Artists */
	$preview.find('.group-responsive .artist input[name="artist"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchArtists(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	$preview.find('.group-responsive .artist .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	})
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .artist .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .artist input[name="artist"]').val("");
	    }
	});
	
	$preview.find('.group-responsive .main-artist input[name="artist"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchArtists(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	$preview.find('.group-responsive .artist .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	});
	    	$preview.find('.group-responsive .main-artist .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	});
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .main-artist .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .main-artist input[name="artist"]').val("");
	    }
	});
	
	$preview.find('.group-responsive .genre input[name="genre"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchGenres(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	$preview.find('.group-responsive .genre .selected-list .item').each(function(){
	    		if($(this).attr("data-id") == item.data('id')){
	    			found = true;
	    		}
	    	})
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .genre .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .genre input[name="genre"]').val("");
	    }
	});
	
	$preview.find('.group-responsive .album input[name="album"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchAlbums(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	if($preview.find('.group-responsive .album .selected-list .item').length > 0){
	    		found = true;
	    	}
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .album .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .album input[name="album"]').val("");
	    	$preview.find('.group-responsive .album input[name="album"]').hide();
	    }
	});
	
	$preview.find('.group-responsive .provider input[name="provider"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchProviders(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	if($preview.find('.group-responsive .provider .selected-list .item').length > 0){
	    		found = true;
	    	}
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .provider .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .provider input[name="provider"]').val("");
	    	$preview.find('.group-responsive .provider input[name="provider"]').hide();
	    }
	});
	
});

$fill = function fillSong(){
	$.when(ajaxCall($song+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, song){
			var url = $song+"/"+song.id;
			var row = addTableRow(9).attr("data-id", song.id);
			row.children("td:first-child()").addClass("tc").text(song.id);
			var keyword;
			$.each(song.meta, function(i, meta){
				keyword = meta.name;
			});
			row.children("td:nth-child(2)").append(addLink().attr("href", url).addClass("templatemo-keyword-link").text(keyword));
			$.each(song.album, function(key, value){
				row.children("td:nth-child(3)").append(addLink().addClass("templatemo-keyword-link").attr("href", "/robo-web/web/album/"+key).attr("album-id", key).text(value));
			});
			$.each(song.artists, function(key, value){
				row.children("td:nth-child(4)").append(addDiv().append(addLink().addClass("templatemo-keyword-link").attr("href", "/robo-web/web/artist/"+key).attr("artist-id", key).text(value)));
			});
			$.each(song.provider, function(key, value){
				var span = addSpan();
				var url = $provider + "/" + key;
				var link = addLink().attr("href", url).text(value);
				span.append(link);
				row.children("td:nth-child(5)").append(span);
			});
			$.each(song.genres, function(key, value){
				var span = addSpan();
				var url = $genre + "/" + key;
				var link = addLink().attr("href", url).text(value);
				span.append(link);
				row.children("td:nth-child(6)").append(span);
			});
			var image = addImage();
			if(song.photos && song.photos.length !== 0){
				$.each(song.photos, function(index_, photo){
					if(photo.isMainPhoto){
						image.attr("src", photo.url.replace("{width}", 500).replace("{height}", 500));
					}
				});
				var src = image.attr("src");
				if(typeof src == typeof undefined || src == false || src == null){
					image.attr("src", song.photos[0].url.replace("{width}", 500).replace("{height}", 500));
				}
			}
			row.children("td:nth-child(7)").addClass("tc").append(addLink().addClass("templatemo-image-link").append(image));
			row.children("td:nth-child(8)").addClass("tc").append(addLink().attr("href", url).addClass("templatemo-edit-btn").text("Edit"));
			row.children("td:last-child()").addClass("tc").append(addLink().addClass("templatemo-link templatemo-del-link").text("Delete"));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveSong(){
	var valid = validate();
	if(!valid){
		return;
	}
	var song = {};
	song.id = parseInt($preview.attr("data-id"));
	var metaList = new Array();
	$preview.find(".preview-widget-content table>tbody").children("tr").each(function(){
		var meta = {};
		if($(this).attr("data-id")){
			meta.id = parseInt($(this).attr("data-id").trim());
		}
		meta.name = $(this).children(":nth-child(1)").text().trim();
		meta.description = $(this).children(":nth-child(2)").text().trim();
		meta.lang = $(this).children(":nth-child(3)").children("select").val().trim();
		metaList.push(meta);
	});
	var artistsMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .artist .selected-list").children("span.item").each(function(){
		artistsMap[$(this).attr("data-id")] = "";
	});
	var mainArtistsMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .main-artist .selected-list").children("span.item").each(function(){
		mainArtistsMap[$(this).attr("data-id")] = "";
	});
	var genresMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .genre .selected-list").children("span.item").each(function(){
		genresMap[$(this).attr("data-id")] = "";
	});
	var albumMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .album .selected-list").children("span.item").each(function(){
		albumMap[$(this).attr("data-id")] = "";
	});
	var providerMap = new Object();
	$preview.find(".preview-widget-content .group-responsive .provider .selected-list").children("span.item").each(function(){
		providerMap[$(this).attr("data-id")] = "";
	});
	var contentList = new Array();
	$preview.find(".preview-widget-content .content-responsive .content").children(".item").each(function(){
		var dataId = $(this).attr('data-id');
		if(typeof dataId !== typeof undefined && dataId !== false){
			var content = {};
			content.id = parseInt(dataId.trim());
			contentList.push(content);
		}
	});
	var lyricsList = new Array();
	$preview.find(".preview-widget-content .lyrics-responsive .lyrics").children(".item").each(function(){
		var dataId = $(this).attr('data-id');
		if(typeof dataId !== typeof undefined && dataId !== false){
			var lyrics = {};
			lyrics.id = parseInt(dataId.trim());
			lyricsList.push(lyrics);
		}
	});
	var photoList = new Array();
	$preview.find(".preview-widget-content .photos-responsive .photos").children(".photo").each(function(){
		var dataId = $(this).attr('data-id');
		if(typeof dataId !== typeof undefined && dataId !== false){
			var photo = {};
			photo.id = parseInt(dataId.trim());
			photo.isMainPhoto = $(this).find("input[type='checkbox'].main-photo-checkbox").is(":checked");
			photoList.push(photo);
		}
	});
	var socialList = new Array();
	$preview.find(".preview-widget-content .socials-responsive .socials").children(".social").each(function(){
		var social = {};
		social.id = $(this).attr("data-id");
		if($(this).find("select").length > 0){
			social.social = $(this).find("select").val();
		}else{
			social.social = $(this).attr("id");
		}
		social.link = $(this).find("input[type='text']").val();
		socialList.push(social);
	});
	var tagList = new Array();
	$preview.find(".preview-widget-content .tags-responsive .tags").children("div.tag").each(function(){
		var tag = {};
		tag.id = $(this).attr("data-id");
		tag.tag = $(this).find("span[title=tag]").text();
		tagList.push(tag);
	});
	song.meta = metaList;
	var isrc 		= $preview.find(".preview-widget-content .basic-info-responsive .general input[name='isrc']").val();
	var createdDate = $preview.find(".preview-widget-content .basic-info-responsive .general input[name='createdDate']").val();
	var year	   	= $preview.find(".preview-widget-content .basic-info-responsive .general input[name='year']").val();
	var videoLink		= $preview.find(".preview-widget-content .basic-info-responsive .general input[name='link']").val();
	
	if(isrc && $.trim(isrc) !== ""){
		song.isrc = isrc;
	}
	if(createdDate && $.trim(createdDate) !== ""){
		song.createdDate = $.datepicker.formatDate('yy-mm-dd', new Date(createdDate));
	}
	if(year && $.trim(year) !== ""){
		song.year = year;
	}
	if(videoLink && $.trim(videoLink) !== ""){
		song.videoLink = videoLink;
	}
	song.artists = artistsMap;
	song.mainArtists = mainArtistsMap;
	song.genres = genresMap;
	song.album = albumMap;
	song.provider = providerMap;
	song.photos = photoList;
	song.socials = socialList;
	song.tags = tagList;
	song.content = contentList;
	song.lyrics = lyricsList;
	var method;
	if($preview.attr("data-id")){
		method = $HTTP_METHOD.PUT;
	}else{
		method = $HTTP_METHOD.POST;
	}
	
	$.when(ajaxCall($song, method, song, headers)).then(function(data){
		$preview.attr("data-id", data.id);
		updatePopup(data);
		$preview.find(".preview-widget-content .photos-responsive .photos").children(".photo").each(function(index){
			$(this).delay(500*index);
			var photoDiv = $(this);
			var photo = $(this);
			var dataId = photo.attr('data-id');
			var type = "SONG_IMAGE";
			if(typeof dataId === typeof undefined || dataId === false){
				var songId = parseInt(data.id);
				var image = $(this).children("img");
				var xhr;
				if (window.XMLHttpRequest){
				        // code for IE7+, Firefox, Chrome, Opera, Safari
					xhr=new XMLHttpRequest();
				}else{
				    // code for IE6, IE5
					xhr=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xhr.open( "GET", image.attr("src"), true );
				xhr.responseType = 'arraybuffer';
				xhr.onload = function( e ) {
					var arrayBufferView = new Uint8Array( this.response );
					var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
					var loadSpan = addSpan().addClass("load");
					var errorSpan = addSpan().addClass("error");
					var checkbox = addCheckbox().attr("name", "main").attr("title", "Set as main photo").addClass("main-photo-checkbox");
					photoDiv.append(loadSpan);
					$.when(mutipartCall($song + "/" + songId, $HTTP_METHOD.POST, blob, type, name)).then(function(data){
						loadSpan.remove();
						photoDiv.removeClass("transparent");
						photoDiv.append(checkbox);
						photoDiv.children("span.error").remove();
						photo.attr("data-id", data.photos[data.photos.length-1].id);
					},
					function(xhr){
						loadSpan.remove();
						photoDiv.removeClass("transparent");
						photoDiv.append(errorSpan);
						handleError(xhr)
					}).done(function(){
						toastr.success("Photo Successfully Uploaded.", 'Success', {positionClass: 'toast-bottom-right'});
					});
				};
				xhr.send();
			}
		});
		
		$preview.find(".preview-widget-content .content-responsive .content").children(".item").each(function(index){
			$(this).delay(500*index);
			var contentDiv = $(this);
			var item = $(this);
			var dataId = item.attr('data-id');
			var type = "FULL_TRACK";
			if(typeof dataId === typeof undefined || dataId === false){
				var songId = parseInt(data.id);
				var audio = $(this).children("audio");
				var name = $(this).attr("title");
				var xhr;
				if (window.XMLHttpRequest){
				        // code for IE7+, Firefox, Chrome, Opera, Safari
					xhr=new XMLHttpRequest();
				}else{
				    // code for IE6, IE5
					xhr=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xhr.open( "GET", audio.children("source").attr("src"), true );
				xhr.responseType = 'blob';
				xhr.onload = function( e ) {
					var blob = this.response
					var loadSpan = addSpan().addClass("load");
					var errorSpan = addSpan().addClass("error");
					var qualitySpan = addSpan().addClass("quality").text($("#qualityText").val() + ": ");
					contentDiv.append(loadSpan);
					$.when(mutipartCall($song + "/" + songId, $HTTP_METHOD.POST, blob, type, name)).then(function(data){
						loadSpan.remove();
						contentDiv.removeClass("transparent");
						item.attr("data-id", data.content[data.content.length-1].id);
						qualitySpan.append(addSpan().text(data.content[data.content.length-1].bitRate));
						qualitySpan.append($("#pixelText").val());
						item.append(qualitySpan);
						toastr.success("Content Successfully Uploaded.", 'Success', {positionClass: 'toast-bottom-right'});
					},
					function(xhr){
						loadSpan.remove();
						contentDiv.removeClass("transparent");
						contentDiv.append(errorSpan);
						handleError(xhr)
					});
				};
				xhr.send();
			}
		});
		
		$preview.find(".preview-widget-content .lyrics-responsive .lyrics").children(".item").each(function(index){
			$(this).delay(500*index);
			var contentDiv = $(this);
			var item = $(this);
			var dataId = item.attr('data-id');
			if(typeof dataId === typeof undefined || dataId === false){
				var songId = parseInt(data.id);
				 var name = $(this).attr("title");
				 var type = "SONG_LYRICS";
				 var blob = new Blob([ $(this).find("textarea").val() ], { type: 'text/plain' });
				 var loadSpan = addSpan().addClass("load");
				 var errorSpan = addSpan().addClass("error");
				 $.when(mutipartCall($song + "/" + songId, $HTTP_METHOD.POST, blob, type, name)).then(function(data){
						loadSpan.remove();
						contentDiv.removeClass("transparent");
						item.attr("data-id", data.lyrics[data.lyrics.length-1].id);
						toastr.success("Content Successfully Uploaded.", 'Success', {positionClass: 'toast-bottom-right'});
					},
					function(xhr){
						loadSpan.remove();
						contentDiv.removeClass("transparent");
						contentDiv.append(errorSpan);
						handleError(xhr)
					});
			}
		});
		
	},
	function(xhr){
		handleError(xhr)
	}).done(function(){
		toastr.success("Song Successfully Saved.", 'Success', {positionClass: 'toast-bottom-right'});
	});;
}

function songAlbums(){
	var songId = parseInt($preview.attr("data-id"));
	$preview.find(".preview-widget-content .albums-responsive .albums").empty();
	$.ajax({
		url: "/robo-web/web/album/"+songId+"/"+$currentAlbumPage+"/"+$albumsPerPage,
		type: $HTTP_METHOD.GET,
		headers: headers,
		success: function(data, status, xhr){
			$.each(data, function(index, album){
				var albumDiv = addDiv().addClass("album");
				var albumPhotoDiv = addDiv().addClass("photo");
				var image = addImage();
				if(album.photos && album.photos.length !== 0){
					$.each(album.photos, function(index_, photo){
						if(photo.isMainPhoto){
							image.attr("src", photo.url.replace("{width}", 500).replace("{height}", 500));
						}
					});
					var src = image.attr("src");
					if(typeof src == typeof undefined || src == false || src == null){
						image.attr("src", album.photos[0].url.replace("{width}", 500).replace("{height}", 500));
					}
				}
				var contentDiv = addDiv().addClass("content");
				var contentHeaderDiv = addDiv().addClass("content-header");
				var headerLink = addLink().attr("href", "/robo-web/web/album/"+album.id);
				var headerSpan = addSpan().addClass("header");
				var songsDiv = addDiv().addClass("songs");
				$.each(album.meta, function(i, meta){
					headerSpan.append(meta.name);
				});
				$.each(album.songs, function(key, value){
					var link = addLink().attr("href", "javascript:");
					if(songId !== parseInt(key)){
						link.attr("href", "/robo-web/web/song/"+key);
					}
					var span = addSpan().addClass("song").text(value);
					link.append(span);
					songsDiv.append(link);
				});
				var contentBodyDiv = addDiv().addClass("content-body");
				var songsList = addList().addClass("songs");
				var counter = 1;
				$.each(album.songs, function(key, value){
					var songItem = listItem().addClass("song");
					var indexSpan = addSpan().addClass("content-index").text(counter);
					var link = addLink().attr("href", "/robo-web/web/song/"+key);
					var span = addSpan().addClass("content-body").text(value);
					link.append(span);
					songItem.append(indexSpan);
					songItem.append(link);
					songsList.append(songItem);
					counter++;
				});
				albumPhotoDiv.append(image);
				contentBodyDiv.append(songsList);
				headerLink.append(headerSpan);
				contentHeaderDiv.append(headerLink);
				contentHeaderDiv.append(songsDiv);
				contentDiv.append(contentHeaderDiv);
				contentDiv.append(contentBodyDiv);
				albumDiv.append(albumPhotoDiv);
				albumDiv.append(contentDiv);
				$preview.find(".preview-widget-content .albums-responsive .albums").append(albumDiv);
			});
			var total = xhr.getResponseHeader("x-data-count");
			if($.isNumeric(total)){
				if($totalAlbums != total){
					$totalAlbums = total;
				}
				if($totalAlbums <= $albumsPerPage){
					$preview.find(".preview-widget-content .albums-responsive .cu-pagination-wrap").hide();
				}else{
					$preview.find(".preview-widget-content .albums-responsive .cu-pagination-wrap").show();
				}
			}
		},
		error: function(xhr){
			handleError(xhr)
		},
		complete: function(status){
			
		}
	 });
}

function searchArtists(term, suggest){
	try{
		$.when(ajaxCall($artist+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, artist){
				var item = {};
				var data = {};
				item.id = artist.id;
				data.name = artist.meta[0].name;
				if(artist.photos.length > 0){
					$.each(artist.photos, function(index2, photo){
						if(photo.isMainPhoto){
							data.photo = photo.url;
						}
					});
					if(!item.photo){
						data.photo = artist.photos[0].url;
					}
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		toastr.error('Error occured.', 'Error', {positionClass: 'toast-bottom-right'});
	}
}

function searchAlbums(term, suggest){
	try{
		$.when(ajaxCall($album+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, album){
				var item = {};
				var data = {};
				var provider = {};
				item.id = album.id;
				data.name = album.meta[0].name;
				provider.id = album.provider.id;
				provider.name = album.provider.meta[0].name;
				$.each(album.artists, function(key, value){
					if(data.artist){
						data.artist.concat("-").concat(value);
					}else{
						data.artist = value;
					}
				});
				if(album.photos.length > 0){
					$.each(album.photos, function(index2, photo){
						if(photo.isMainPhoto){
							data.photo = photo.url;
						}
					});
					if(!item.photo){
						data.photo = album.photos[0].url;
					}
				}
				data.provider = provider;
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	}catch(error){
		
	}
}

function searchProviders(term, suggest){
	try{
		$.when(ajaxCall($provider+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, provider){
				var item = {};
				var data = {};
				item.id = provider.id;
				data.name = provider.meta[0].name;
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function searchGenres(term, suggest){
	try{
		$.when(ajaxCall($genre+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, genre){
				var item = {};
				var data = {};
				item.id = genre.id;
				data.name = genre.meta[0].name;
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function deleteSong(songId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteSong(songId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteSong(songId){
	
	$.when(ajaxCall($song+"/"+songId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+songId+"']").remove();
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function refreshPagination(){
	if(!$pagination.parent().hasClass("pagination-hidden")){
		$totalPages = Math.ceil($totalItems / $itemsPerPage.val());
		$pagination.children("li:not(:last)").remove();
		for(page=1; page<=$totalPages; page++){
			var $listItem = listItem().append(addLink().attr("data-item", page).text(page));
			$pagination.children("li:last-child()").before($listItem);
		}
		$pagination.find("li:nth-child("+$currentPage+")").addClass("active");
	}
	
}

function validate(){
	var valid = true;
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		$(this).children("td:nth-child(2)").removeClass("error");
		if($(this).children("td:nth-child(2)").is(":empty")){
			$(this).children("td:nth-child(2)").addClass("error");
			valid = false;
		}
	});
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		$(this).children("td:nth-child(3)").removeClass("error");
		if($(this).children("td:nth-child(3)").is(":empty")){
			$(this).children("td:nth-child(3)").addClass("error");
		}
	});
	
	return valid;
}

function updatePopup(data){
	$preview.find(".preview-widget-content table>tbody").empty();
	data.meta.forEach(function(meta, index){
		addNewMetaRow(meta);
	});
	
	$preview.find(".tags-responsive").find("div.tags").empty();
	data.tags.forEach(function(tag, index){
		var tagDiv = addDiv().addClass("tag").attr("data-id", tag.id);
		var textSpan = addSpan().attr("title", "tag").text(tag.tag);
		var closeSpan = addSpan().addClass("close");
		tagDiv.append(textSpan);
		tagDiv.append(closeSpan);
		$preview.find(".tags-responsive").find("div.tags").append(tagDiv);
	});
	
}

function mimeTypeSelect(){
	var select = $("<select></select>");
	$.each(mimeType, function(key, value){
		var option = $("<option></option>").val(key).text(value);
		select.append(option);
	});
	return select;
}