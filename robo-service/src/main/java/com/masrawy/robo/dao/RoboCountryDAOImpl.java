package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboCountryVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboCountryDAO")
public class RoboCountryDAOImpl extends AbstractWebserviceDAO<RoboCountryVO, Long> implements RoboCountryDAO{

	public RoboCountryDAOImpl(){
		super(RoboCountryVO.class, new ParameterizedTypeReference<List<RoboCountryVO>>() {});
	}
}
