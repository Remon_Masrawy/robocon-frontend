package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboCountryDTO;
import com.masrawy.robo.models.RoboCountryVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboCountryTransformer")
public class RoboCountryTransformerImpl extends AbstractDTOTransformer<RoboCountryVO, RoboCountryDTO> implements RoboCountryTransformer{

	public RoboCountryTransformerImpl(){
		super(RoboCountryVO.class, RoboCountryDTO.class);
	}
}
