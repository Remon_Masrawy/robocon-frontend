package com.masrawy.robo.models;

import java.util.List;
import java.util.Map;

import com.masrawy.robo.ws.AbstractVO;

public class RoboEventVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String bookingTicketUrl;
	private String beginDate;
	private String endDate;
	private String beginTime;
	private String endTime;
	private Double latitude;
	private Double longitude;
	private String telephone;
	private Boolean isActive;
	private List<RoboEventMetaVO> meta;
	private List<RoboPhotoVO> photos;
	private List<RoboTagVO> tags;
	private Map<Long, String> artists;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBookingTicketUrl() {
		return bookingTicketUrl;
	}
	public void setBookingTicketUrl(String bookingTicketUrl) {
		this.bookingTicketUrl = bookingTicketUrl;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public List<RoboEventMetaVO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboEventMetaVO> meta) {
		this.meta = meta;
	}
	public List<RoboPhotoVO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboPhotoVO> photos) {
		this.photos = photos;
	}
	public List<RoboTagVO> getTags() {
		return tags;
	}
	public void setTags(List<RoboTagVO> tags) {
		this.tags = tags;
	}
	public Map<Long, String> getArtists() {
		return artists;
	}
	public void setArtists(Map<Long, String> artists) {
		this.artists = artists;
	}
}
