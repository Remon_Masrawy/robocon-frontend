package com.masrawy.robo.services;

import java.util.List;

import com.masrawy.robo.dto.RoboStorefrontDTO;
import com.masrawy.robo.models.RoboStorefrontVO;

public interface RoboStorefrontService extends PgrlService<RoboStorefrontVO, RoboStorefrontDTO, Long>{

	public RoboStorefrontDTO createStorefront(RoboStorefrontDTO roboStorefrontDTO);
	public RoboStorefrontDTO updateStorefront(RoboStorefrontDTO roboStorefrontDTO);
	public RoboStorefrontDTO getStorefront(Long id);
	public List<RoboStorefrontDTO> getStorefrontList(Integer page, Integer limit);
	public void deleteStorefront(Long id);
}
