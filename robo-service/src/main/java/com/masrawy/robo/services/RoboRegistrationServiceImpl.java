package com.masrawy.robo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboAuthDAO;
import com.masrawy.robo.dto.RoboAuthDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.exceptions.RoboUserException;
import com.masrawy.robo.models.RoboAuthVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.security.CustomUserDetails;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboAuthTransformer;

@Service("roboRegistrationService")
public class RoboRegistrationServiceImpl extends AbstractPgrlServiceImpl<RoboAuthVO, RoboAuthDTO, Long> implements RoboRegistrationService{

	private static final Logger logger = LoggerFactory.getLogger(RoboRegistrationServiceImpl.class);
	
	@Autowired
	private RoboAuthDAO roboAuthDAO;
	
	@Autowired
	private RoboAuthTransformer roboAuthTransformer;
	
	@Value("${robocon.ws.login.url}")
	private String loginURL;
	
	@Value("${robocon.ws.signup.url}")
	private String signUpURL;
	
	@Override
	public RoboAuthDTO retreiveUser(String username, String password){
			RoboAuthVO roboAuthVO = new RoboAuthVO();
			roboAuthVO.setEmail(username);
			roboAuthVO.setPassword(password);
			roboAuthVO = roboAuthDAO.postObject(loginURL, roboAuthVO);
			RoboAuthDTO roboAuthDTO = roboAuthTransformer.transformVOToDTO(roboAuthVO);
			return roboAuthDTO;
	}
	
	@Override
	public RoboAuthDTO signUpUser(RoboAuthDTO roboAuthDTO){
		logger.debug("Robocon request for creatting user");
		try{
			roboAuthDTO = postObject(signUpURL, roboAuthDTO);
			return roboAuthDTO;
		}catch(RoboResponseException ex){
			logger.error("User creation error occured.", ex);
			throw new RoboUserException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("User creation error occured.", ex);
			throw new RoboUserException(ex.getMessage());
		}
	}
	
	public void updateUserDetails(RoboAuthDTO roboAuthDTO){
		logger.debug("Trying to update user details due to exchange occured by user.");
		try{
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			CustomUserDetails userDetails = (CustomUserDetails) auth.getPrincipal();
			userDetails.setPhoto(roboAuthDTO.getPhoto());
			logger.debug("User details updated successfully.");
		}catch(Throwable e){
			logger.error("Error occured when trying to update user details ", e);
		}
	}
	
	@Override
	protected DTOTransformer<RoboAuthVO, RoboAuthDTO> getTransformer() {
		return roboAuthTransformer;
	}

	@Override
	protected DAO<RoboAuthVO, Long> getDAO() {
		return roboAuthDAO;
	}
}
