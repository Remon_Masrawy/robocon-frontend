package com.masrawy.robo.constants;

public class RoleConstants {

	public static final String ROLE_SYSTEM 		= "ROLE_SYSTEM";
	
	public static final String ROLE_SUPER 		= "ROLE_SUPER";
	
	public static final String ROLE_ADMIN 		= "ROLE_ADMIN";
	
	public static final String CONTENT_ADMIN 	= "ROLE_CONTENT_ADMIN";
	
	public static final String ROLE_USER 		= "ROLE_USER";
	
	
}
