
tabFormChecker = new Array();

/* -------------------------------------------------------------------- */
/* --------------------------- Basic Check ---------------------------- */
/* -------------------------------------------------------------------- */

// Check field not empty
tabFormChecker['default'] = function(field) {
	var result = {};
	
	if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else {
		result.ok = true;
	}
	return result;
}

// Check alpha field
tabFormChecker['alpha'] = function(field) {
	var result = {};
	var regex = new RegExp(/^[a-z\s]*$/i); // Only alphabetic
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	
	return result;
}

//Check password field
tabFormChecker['password'] = function(field) {
	var result = {};
	var aNumber = /[0-9]/;
    var aSpecial = /[!|@|#|$|%|^|&|*|(|)|-|_]/;
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (field.value.length < 10) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else if (!aNumber.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else if (!aSpecial.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	}else {
		result.ok = true;
	}
	
	return result;
}

//Check confirm password field
tabFormChecker['confirm'] = function(field) {
	var result = {};
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (field.value != document.getElementById('sign-up-password').value) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	
	return result;
}

tabFormChecker['captcha'] = function(field) {
	var result = {};
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	}else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	}else if (field.value != document.getElementById('randomfield').value) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	}else {
		result.ok = true;
	}
	return result;
}

// Check numeric field
tabFormChecker['numeric'] = function(field) {
	var result = {};
	var regex = new RegExp(/^\d*$/i); // Only number
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	
	return result;
}

// Check select
tabFormChecker['select'] = function(field) {
	var result = {};
	
	if((field.value == '' || field.value == 0) && !field.mandatory) {
		result.ok = true;
	} else if ((field.value == '' || field.value == 0) && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else {
		result.ok = true;
	}
	return result;
}

// Check input radio
tabFormChecker['radio'] = function(field) {
	var result = {};
	var val = $('input[name=' + field.name + ']:checked').val();
	if (typeof val == "undefined" && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else {
		result.ok = true;
	}
	return result;
}

// Check date
tabFormChecker['date'] = function(field) {
	var result = {};
	
	if (typeof field.dateType != 'undefined' && field.dateType == 'd.m.y') {
		
		var day 	= (typeof field.relation.d != 'undefined' && typeof $('INPUT[name="' + field.relation.d + '"]') != 'undefined' && $('INPUT[name="' + field.relation.d + '"]').val() != '')? parseInt($('INPUT[name="' + field.relation.d + '"]').val(), 10) : '';
		var month 	= (typeof field.relation.m != 'undefined' && typeof $('INPUT[name="' + field.relation.m + '"]') != 'undefined' && $('INPUT[name="' + field.relation.m + '"]').val() != '')? parseInt($('INPUT[name="' + field.relation.m + '"]').val(), 10) : '';
		var year 	= (typeof field.relation.y != 'undefined' && typeof $('INPUT[name="' + field.relation.y + '"]') != 'undefined' && $('INPUT[name="' + field.relation.y + '"]').val() != '')? parseInt($('INPUT[name="' + field.relation.y + '"]').val(), 10) : '';
		
		if(field.mandatory && day == '' && month == '' && year == '') {
			result.ok = false;
			result.msg = field.errorMsg.msg1;
		} else if (!field.mandatory && day == '' && month == '' && year == '') {
			result.ok = true;
		} else if (day == '' || month == '' || year == '') {
			result.ok = false;
			result.msg = field.errorMsg.msg2;
		} else {
			var date = new Date(year,month-1,day);
			if (date.getFullYear() == year && date.getMonth() + 1 == month && date.getDate() == day) {
				result.ok = true;
			} else {
				result.ok = false;
				result.msg = field.errorMsg.msg2;
			}
		}
		
	} else if (typeof field.dateType != 'undefined' && field.dateType == 'm.y') {
		
		var month 	= (typeof field.relation.m != 'undefined' && typeof $('INPUT[name="' + field.relation.m + '"]') != 'undefined' && $('INPUT[name="' + field.relation.m + '"]').val() != '')? parseInt($('INPUT[name="' + field.relation.m + '"]').val(), 10) : '';
		var year 	= (typeof field.relation.y != 'undefined' && typeof $('INPUT[name="' + field.relation.y + '"]') != 'undefined' && $('INPUT[name="' + field.relation.y + '"]').val() != '')? parseInt($('INPUT[name="' + field.relation.y + '"]').val(), 10) : '';
		
		if(field.mandatory && month == '' && year == '') {
			result.ok = false;
			result.msg = field.errorMsg.msg1;
		} else if (!field.mandatory && month == '' && year == '') {
			result.ok = true;
		} else if (month == '' || year == '') {
			result.ok = false;
			result.msg = field.errorMsg.msg2;
		} else {
			var date = new Date(year,month-1);
			if (date.getFullYear() == year && date.getMonth() + 1 == month) {
				result.ok = true;
			} else {
				result.ok = false;
				result.msg = field.errorMsg.msg2;
			}
		}
		
	} else if (typeof field.dateType != 'undefined' && field.dateType == 'creditcard') {
		
		var month 	= (typeof field.relation.m != 'undefined' && typeof $('INPUT[name="' + field.relation.m + '"]') != 'undefined' && $('INPUT[name="' + field.relation.m + '"]').val() != '')? parseInt($('INPUT[name="' + field.relation.m + '"]').val(), 10) : '';
		var year 	= (typeof field.relation.y != 'undefined' && typeof $('INPUT[name="' + field.relation.y + '"]') != 'undefined' && $('INPUT[name="' + field.relation.y + '"]').val() != '')? parseInt($('INPUT[name="' + field.relation.y + '"]').val(), 10) : '';
		
		if(month == '' && year == '') {
			result.ok = false;
			result.msg = field.errorMsg.msg1;
		} else if (month == '' || year == '') {
			result.ok = false;
			result.msg = field.errorMsg.msg2;
		} else {
			var currentDate = new Date();
			var date = new Date(year,month-1);
			var lastDayOfMonth = new Date(date.getFullYear(), date.getMonth()+1, 0);
			if (date.getFullYear() == year && date.getMonth() + 1 == month && lastDayOfMonth >= currentDate) {
				result.ok = true;
			} else {
				result.ok = false;
				result.msg = field.errorMsg.msg2;
			}
		}
		
	}
	
	return result;
}

// Check email
tabFormChecker['email'] = function(field) {
	var result = {};
	var regex = new RegExp(/^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i);
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	
	return result;
}

// Check phone
tabFormChecker['phone'] = function(field) {
	var result = {};
	var regex = new RegExp(/^([0-9]{2}\s?){5}$/); // French format (e.g. 01 23 45 67 89 or 0123456789)
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	
	return result;
}

//Check Country

tabFormChecker['country'] = function(field) {
	var result = {};
	
	if((field.value == '' || field.value == 0) && !field.mandatory) {
		result.ok = true;
	} else if ((field.value == '' || field.value == 0) && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else {
		result.ok = true;
		
		var val = document.getElementById("form-portable").value;
		
		var portable = "";
		
		if(val != null){
			
			var portable = val.replace(/ *\([^)]*\) */g,"");
		}
		
		/*document.getElementById("form-portable").value = "(" + CountryKey[document.getElementById("form-country").options[document.getElementById("form-country").selectedIndex].text].key +") " + portable;*/
	}
	
	return result;
}

tabFormChecker['mobile'] = function(field) {
	var result = {};
	
	var country = document.getElementById("form-country");
	
	if(!$(country).parents('.fieldset').is(':hidden')){
		if(country.value == "0" && field.mandatory){
			result.ok = false;
			result.msg = field.errorMsg.msg3;
		}
	
	else{
		var regex1 = new RegExp("^([\(]{1}[0-9]{2,4}[\)]{1}[ ]{1}[0-9]{4,})$");
		var regex2 = new RegExp("^([\+]{1}[0-9]{2,4}[ ]{1}[0-9]{4,})$");
		var justCode = new RegExp("^([\(]{1}[0-9]{2,4}[\)]{1})$");
		if(field.value == '' && !field.mandatory) {
			result.ok = true;
		}else if(justCode.test(field.value.trim()) && !field.mandatory){
			result.ok = true;
		} else if (field.value == '' && field.mandatory) {
			result.ok = false;
			result.msg = field.errorMsg.msg1;
		} else if (!regex1.test(field.value) && !regex2.test(field.value)) {
			result.ok = false;
			result.msg = field.errorMsg.msg2;
		} else {
			result.ok = true;
		}
		
	}
	}else{
		result.ok = true;
	}
	return result;
}

// Check Postcode
tabFormChecker['postcode'] = function(field) {
	var result = {};
	var regex = new RegExp(/^[0-9]{2}\s?[0-9]{3,}$/); // French format (e.g. 75000 or 75 000)
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	
	return result;
}


/* -------------------------------------------------------------------- */
/* ------------------------- Specific check --------------------------- */
/* -------------------------------------------------------------------- */

// Check number plate
tabFormChecker['numberPlate'] = function(field) {
	var result = {};
	var regex = new RegExp(/^[A-Z]{2}-?\d{3}-?[A-Za-z]{2}$/i); // French format (e.g. AA-123-BB or AA123BB)
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	return result;
}

// Check credit card holder
tabFormChecker['ccHolder'] = function(field) {
	var result = {};
	var regex = new RegExp(/^[a-z\s\.]*$/i);
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	return result;
}

// Check credit card number
tabFormChecker['ccNumber'] = function(field) {
	var result = {};
	var regex = new RegExp(/^(\d{4}\-?){3}\d{4}$/); // Credit card number format (e.g. 1111-1111-1111-1111 or 1111111111111111)
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	return result;
}
//Check checkbox
tabFormChecker['checkbox'] = function(field) {

		var result = {};
	
		var checked = false;
		
		checked = $('input[name=' + field.name + ']').prop("checked");
		
		if(!checked && field.mandatory) {
			result.ok = false;
		} else {
			result.ok = true;
		}
		
	return result;
}
// Check cheking code
tabFormChecker['ccCCode'] = function(field) {
	var result = {};
	var regex = new RegExp(/^\d{3}$/); // Credit card checking code format (e.g. 111)
	
	if(field.value == '' && !field.mandatory) {
		result.ok = true;
	} else if (field.value == '' && field.mandatory) {
		result.ok = false;
		result.msg = field.errorMsg.msg1;
	} else if (!regex.test(field.value)) {
		result.ok = false;
		result.msg = field.errorMsg.msg2;
	} else {
		result.ok = true;
	}
	return result;
}