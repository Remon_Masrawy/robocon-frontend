package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboSongVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboSongDAO")
public class RoboSongDAOImpl extends AbstractWebserviceDAO<RoboSongVO, Long> implements RoboSongDAO{

	public RoboSongDAOImpl(){
		super(RoboSongVO.class, new ParameterizedTypeReference<List<RoboSongVO>>(){});
	}
}
