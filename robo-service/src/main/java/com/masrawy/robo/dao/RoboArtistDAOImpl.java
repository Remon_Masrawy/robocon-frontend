package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboArtistVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboArtistDAO")
public class RoboArtistDAOImpl extends AbstractWebserviceDAO<RoboArtistVO, Long> implements RoboArtistDAO{

	public RoboArtistDAOImpl(){
		super(RoboArtistVO.class, new ParameterizedTypeReference<List<RoboArtistVO>>() {});
	}
}
