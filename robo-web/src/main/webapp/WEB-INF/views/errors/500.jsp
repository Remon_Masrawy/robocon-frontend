<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Robocon</title>
	<meta name="description" content="">  
	<meta name="author" content="">

  <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href='<c:url value="/css/base.css"/>'>
   <link rel="stylesheet" href='<c:url value="/css/main.css"/>'>
   <link rel="stylesheet" href='<c:url value="/css/vendor.css"/>'>     

   <!-- script
   ================================================== -->
	<script src='<c:url value="/js/modernizr.js"/>'></script>

   <!-- favicons
	================================================== -->
	<link rel="icon" type="images/png" href='<c:url value="/images/mazika.png"/>'>

</head>

<body>

	<!-- header 
   ================================================== -->
   <header class="main-header">
   	<div class="row">
   		<div class="logo">
	         <a href="">Robocon</a>
	      </div>   		
   	</div>   

   </header> <!-- /header -->

	<!-- main content
   ================================================== -->
   <main id="main-404-content" class="main-content-static">

   	<div class="content-wrap">

		   <div class="shadow-overlay"></div>

		   <div class="main-content">
		   	<div class="row">
		   		<div class="col-twelve">
			  		
			  			<h1 class="kern-this">500 Error.</h1>
			  			<p>
						Oooooops! Looks like problem occurred at this location.
						Maybe try on of the links below, click on the top menu
						or try a search?
			  			</p>

			  			<div class="search">
				      	<form>
								<input type="text" id="s" name="s" class="search-field" placeholder="Type and hit enter">
							</form>
				      </div>	   			

			   	</div> <!-- /twelve --> 		   			
		   	</div> <!-- /row -->    		 		
		   </div> <!-- /main-content --> 

		   <footer>
		   	<div class="row">

		   		<div class="col-seven tab-full social-links pull-right">
			   	</div>
		   			
		  			<div class="col-five tab-full bottom-links">
			   		<ul class="links">
				   		<li><a href="#">Homepage</a></li>
				         <li><a href="#">About</a></li>
				         <li><a href="mailto:joe@quatro.com">Report Error</a></li>			                    
				   	</ul>

			   	</div>   		   		

		   	</div> <!-- /row -->    		  		
		   </footer>

		</div> <!-- /content-wrap -->
   
   </main> <!-- /main-404-content -->

   <div id="preloader"> 
    	<div id="loader"></div>
   </div> 

   <!-- Java Script
   ================================================== --> 
   <script src='<c:url value="/js/jquery-2.1.3.min.js"/>'></script>
   <script src='<c:url value="/js/plugins.js"/>'></script>
   <script src='<c:url value="/js/main.js"/>'></script>

</body>

</html>