package com.masrawy.robo.exceptions;

import static com.masrawy.robo.exceptions.ErrorConstants.ROBO_ACCESS_DENIED;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class AccessDeniedException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Access Denied Exception Occured.";
	
	public AccessDeniedException(){
		super(ERROR_MSG);
	}
	
	public AccessDeniedException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBO_ACCESS_DENIED;
	}

}
