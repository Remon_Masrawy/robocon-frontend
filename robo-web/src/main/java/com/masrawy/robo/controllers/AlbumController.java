package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.ALBUMS_PAGE;
import static com.masrawy.robo.constants.RoboPages.ALBUM_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboAlbumDTO;
import com.masrawy.robo.services.RoboAlbumService;
import com.masrawy.robo.services.RoboLanguageService;

@Controller
@RequestMapping(value = "/web/album")
public class AlbumController {

	@Autowired
	private RoboAlbumService roboAlbumService;
	
	@Autowired
	private RoboLanguageService roboLanguageService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView albumsPage(){
		ModelAndView model = new ModelAndView(ALBUMS_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{albumId}", method = RequestMethod.GET)
	public ModelAndView albumPage(@PathVariable("albumId") String albumId){
		ModelAndView model = new ModelAndView(ALBUM_PAGE);
		try {
			Long id = Long.valueOf(albumId);
			if(id != 0){
				model.addObject("album", roboAlbumService.getAlbum(id));
			}
			model.addObject("languages", roboLanguageService.getLanguageList(0, 0));
		} catch (Exception e) {
			model = new ModelAndView("redirect:/web/album");
		}
		
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboAlbumDTO albumCreate(@RequestBody RoboAlbumDTO roboAlbumDTO){
		return roboAlbumService.createAlbum(roboAlbumDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboAlbumDTO albumUpdate(@RequestBody RoboAlbumDTO roboAlbumDTO){
		return roboAlbumService.updateAlbum(roboAlbumDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{albumId}", method = RequestMethod.POST)
	public @ResponseBody RoboAlbumDTO albumPhoto(@RequestParam(value = "file", required = true) MultipartFile file, @RequestParam(value = "name", required = true) String name, @NotNull @PathVariable("albumId") Long albumId){
		return roboAlbumService.uploadPhoto(file, albumId, name);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboAlbumDTO> albums(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboAlbumService.getAlbums(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{albumId}", method = RequestMethod.DELETE)
	public @ResponseBody void albumDelete(@PathVariable("albumId") Long albumId){
		roboAlbumService.deleteAlbum(albumId);
	}
	
}
