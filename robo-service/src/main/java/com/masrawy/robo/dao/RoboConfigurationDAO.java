package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboConfigurationVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboConfigurationDAO extends DAO<RoboConfigurationVO, String>{

}
