<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="preview-widget" data-id="${event.id}">
	<c:if test="${not empty event && not empty event.meta}">
		<div class="preview-widget-title">
			<span title="Type"><spring:message code="robo-pages.event-keyword"/></span>
			<span title="ID">${event.id}</span>
			<span title="${event.meta[0].name}">${event.meta[0].name}</span>
		</div>
	</c:if>
	<div class="preview-widget-buttons">
		<div class="new-button"><i class="fa fas fa-plus fa-fw"></i><spring:message code="robo-pages.new-meta-data-keyword"/></div>
		<div class="delete-button ${empty event? 'hidden' : ''}" ><spring:message code="robo-pages.delete-keyword"/></div>
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="preview-widget-container">
		<div class="preview-widget-content">
		<div class="templatemo-content-container">
				<div class="templatemo-content-widget no-padding">
	            <div class="panel panel-default table-responsive">
	              <table class="table table-striped table-bordered templatemo-user-table">
	                <thead>
	                  <tr>
	                    <td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.name-keyword"/> <span class="caret"></span></a></td>
	                    <td class="bold"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.description-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.location-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.city-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.country-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.lang-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><spring:message code="robo-pages.delete-keyword"/></td>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:if test="${not empty event && not empty event.meta}">
							<c:forEach items="${event.meta}" var="meta">
								<tr data-id="${meta.id}">
									<td contenteditable="true">${meta.name}</td>
									<td contenteditable="true">${meta.description}</td>
									<td contenteditable="true">${meta.location}</td>
									<td contenteditable="true">${meta.city}</td>
									<td contenteditable="true">${meta.country}</td>
									<td>
										<select>
											<c:if test="${not empty languages}">
												<c:forEach items="${languages}" var="language">
													<option value="${language.code}" ${language.code == meta.lang? 'selected' : ''}>${language.code}</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
									<td><a href="javascript:" class="templatemo-link templatemo-meta-delete-link"><spring:message code="robo-pages.delete-keyword"/></a></td>
								</tr>
							</c:forEach>
						</c:if>
	                </tbody>
	              </table>    
	            </div>
	            <div class="panel-default basic-info-responsive">
	            	<div class="dates">
	            		<div>
		            		<label for="beginDate"><spring:message code="robo-pages.begin-date.keyword"/></label>
		            		<input type="text" name="beginDate" class="datepicker" contenteditable="false" value="${event.beginDate}">
		            	</div>
		            	<div>
		            		<label for="beginTime"><spring:message code="robo-pages.begin-time.keyword"/></label>
		            		<input type="time" name="beginTime" value="${event.beginTime}">
		            	</div>
		            	<div>
		            		<label for="endDate"><spring:message code="robo-pages.end-date.keyword"/></label>
		            		<input type="text" name="endDate" class="datepicker" contenteditable="false" value="${event.endDate}">
		            	</div>
		            	<div>
		            		<label for="endTime"><spring:message code="robo-pages.end-time.keyword"/></label>
		            		<input type="time" name="endTime" value="${event.endTime}">
		            	</div>
	            	</div>
	            	<div class="place">
	            		<div>
	            			<label for="latitude"><spring:message code="robo-pages.latitude.keyword"/></label>
	            			<input type="number" name="latitude" placeholder="Latitude" value="${event.latitude}">
	            		</div>
	            		<div>
	            			<label for="longitude"><spring:message code="robo-pages.longitude.keyword"/></label>
	            			<input type="number" name="longitude" placeholder="Longitude" value="${event.longitude}">
	            		</div>
	            	</div>
	            	<div class="url">
            			<label for="bookingURL"><spring:message code="robo-pages.booking-ticket-url.keyword"/></label>
            			<input type="url" name="bookingURL" placeholder="Booking Ticket URL" value="${event.bookingTicketUrl}">
            		</div>
	            	<div class="tel">
	            		<div>
	            			<label for="telephone"><spring:message code="robo-pages.telephone.keyword"/></label>
	            			<input type="tel" name="telephone" placeholder="Telephone" value="${event.telephone}">
	            		</div>
	            		<div>
	            			<label for="active"><spring:message code="robo-pages.active.keyword"/></label>
	            			<input type="checkbox" name="active" ${event.isActive ? 'checked' : ''}>
	            		</div>
	            	</div>
	            </div>
	            <div class="panel-default group-responsive">
	            	<div class="artist">
	            		<label for="artist"><spring:message code="robo-pages.artists-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty event.artists}">
		            				<c:forEach var="artist" items="${event.artists}">
		            					<span class="item" data-id="${artist.key}">${artist.value}<span class="item-close"></span></span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="artist" name="artist" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            </div>  
	            <!-- Photos -->
	            <div class="panel-default photos-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.photos-keyword"/> <span><spring:message code="robo-pages.photos.note"/></span></legend>
	            		<div class="photos">
	            			<c:if test="${not empty event.photos}">
	            				<c:forEach items="${event.photos}" var="photo" varStatus="status">
	            					<div class="photo" data-id="${photo.id}">
	            						<c:set var="suburl" value="${fn:replace(photo.url, '{width}', '500')}"/>
	            						<c:set var="url" value="${fn:replace(suburl, '{height}', '500')}"/>
	            						<img alt="artist" src="${url}">
	            						<span class="close"></span>
	            						<input type="checkbox" name="main" class="main-photo-checkbox" title="Set as main photo" ${photo.isMainPhoto ? 'checked' : ''}>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            		<div class="add-file-button">
	            			<div class="add-sign"></div>
	            		</div>
	            		<input type="file" accept="image/jpg,image/jpeg" title="Choose an image to upload">
	            	</fieldset>
	            </div>  
	            <!-- Tags -->
	            <div class="panel-default tags-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.tags-keyword"/></legend>
	            		<div class="tags">
	            			<c:if test="${not empty event.tags}">
	            				<c:forEach items="${event.tags}" var="tag" varStatus="status">
	            					<div class="tag" data-id="${tag.id}">
	            						<span title="tag">${tag.tag}</span>
	            						<span class="close"></span>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            	</fieldset>
	            </div>                   
	          </div> 
          </div>
        </div>
	</div>
	<div class="preview-widget-resposive-buttons">
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="clear"></div>
</div>
	
<script type="text/javascript" src='<c:url value="/js/event.js"/>'></script>