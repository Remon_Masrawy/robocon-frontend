<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript">
	var elementIds = new Array();
</script>

<div class="templatemo-content-container fade-out">
	<div class="templatemo-content-widget no-padding">
		<div class="panel panel-default table-responsive">
			<table
				class="table table-striped table-bordered templatemo-user-table">
				<thead>
					<tr>
						<td><a href="javascript:" class="white-text templatemo-sort-by">Name <span class="caret"></span></a></td>
						<td><a href="javascript:" class="white-text templatemo-sort-by">Type <span class="caret"></span></a></td>
						<td><a href="javascript:" class="white-text templatemo-sort-by">Value <span class="caret"></span></a></td>
						<td><a href="javascript:" class="white-text templatemo-sort-by">Description <span class="caret"></span></a></td>
						<td>Edit</td>
					</tr>
				</thead>
				<tbody>
					<c:if test="${not empty params}">
						<c:forEach items="${params}" var="element" varStatus="status">
							<tr>
								<td id="name">${element.name}</td>
								<td id="type">${element.type}</td>
								<td id="value" class="group">
								<c:choose>
									<c:when test="${element.type.equalsIgnoreCase('INTEGER') }">
										<input id="param_${status.index}" disabled="disabled" type="number" value="${element.value}">
										
										<script type="text/javascript">
											{
												element["param_${status.index}"] = {
														checker : "numeric",
														mandatory : true,
														errorMsg : {
															msg1 : "<spring:message code='configuration.value-field.error-msg'/>" 
														}
												};
												elementIds.push("param_${status.index}");
											}
										</script>
										
									</c:when>
									<c:when test="${element.type.equalsIgnoreCase('LONG') }">
										<input id="param_${status.index}" disabled="disabled" type="number" value="${element.value}">
										
										<script type="text/javascript">
											{
												element["param_${status.index}"] = {
														checker : "numeric",
														mandatory : true,
														errorMsg : {
															msg1 : "<spring:message code='configuration.value-field.error-msg'/>" 
														}
												};
												elementIds.push("param_${status.index}");
											}
										</script>
										
									</c:when>
									<c:when test="${element.type.equalsIgnoreCase('BOOLEAN') }">
										<c:choose>
											<c:when test="${element.value.equalsIgnoreCase('TRUE') }">
												<input id="param_${status.index}" disabled="disabled" type="checkbox" checked="checked">
											</c:when>
											<c:otherwise>
												<input id="param_${status.index}" disabled="disabled" type="checkbox">
											</c:otherwise>
										</c:choose>
										<script type="text/javascript">
											{
												element["param_${status.index}"] = {
														checker : "checkbox",
														mandatory : false,
														errorMsg : {
															msg1 : "<spring:message code='configuration.value-field.error-msg'/>" 
														}
												};
												elementIds.push("param_${status.index}");
											}
										</script>
									</c:when>
									<c:otherwise>
										<input type="text" id="param_${status.index}" disabled="disabled" value="${element.value}">
										
										<script type="text/javascript">
											{
												element["param_${status.index}"] = {
														checker : "default",
														mandatory : true,
														errorMsg : {
															msg1 : "<spring:message code='configuration.value-field.error-msg'/>" 
														}
												};
												elementIds.push("param_${status.index}");
											}
										</script>
										
									</c:otherwise>
								</c:choose>
								
								<div class="checker valid">
									<div class="content">Arpu Plus Content Workflow Management System, Arpu Plus Content Workflow Management System</div>
								</div>
								
								</td>
								<td id="description" class="group">
									<input type="text" id="param_desc_${status.index}" disabled="disabled" value="${element.description}">
									
									<div class="checker valid">
										<div class="content">Arpu Plus Content Workflow Management System, Arpu Plus Content Workflow Management System</div>
									</div>
									
									<script type="text/javascript">
										{
											element["param_desc_${status.index}"] = {
													checker : "default",
													mandatory : true,
													errorMsg : {
														msg1 : "<spring:message code='configuration.value-field.error-msg'/>" 
													}
											};
											elementIds.push("param_desc_${status.index}");
										}
									</script>
										
								</td>
								<td><c:if test="${element.editabled}"><a href="javascript:" class="templatemo-edit-btn">Edit</a></c:if></td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript" src="<c:url value="/js/configuration.js"/>"></script>
