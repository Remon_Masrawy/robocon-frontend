package com.masrawy.robo.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

public class RoboEventDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotEmpty
	private String bookingTicketUrl;
	@NotEmpty
	private String beginDate;
	@NotEmpty
	private String endDate;
	@NotEmpty
	private String beginTime;
	@NotEmpty
	private String endTime;
	private Double latitude;
	private Double longitude;
	private String telephone;
	private Boolean isActive;
	@Valid
	@NotEmpty
	private List<RoboEventMetaDTO> meta;
	private List<RoboPhotoDTO> photos;
	private List<RoboTagDTO> tags;
	@NotEmpty
	private Map<Long, String> artists;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBookingTicketUrl() {
		return bookingTicketUrl;
	}
	public void setBookingTicketUrl(String bookingTicketUrl) {
		this.bookingTicketUrl = bookingTicketUrl;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public List<RoboEventMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboEventMetaDTO> meta) {
		this.meta = meta;
	}
	public List<RoboPhotoDTO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboPhotoDTO> photos) {
		this.photos = photos;
	}
	public List<RoboTagDTO> getTags() {
		return tags;
	}
	public void setTags(List<RoboTagDTO> tags) {
		this.tags = tags;
	}
	public Map<Long, String> getArtists() {
		return artists;
	}
	public void setArtists(Map<Long, String> artists) {
		this.artists = artists;
	}
}
