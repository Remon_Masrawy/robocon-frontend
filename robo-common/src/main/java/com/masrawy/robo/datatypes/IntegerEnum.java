package com.masrawy.robo.datatypes;

public interface IntegerEnum {

	int getIntValue();
}
