<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="preview-widget" data-id="${frontend.id}">
	<c:if test="${not empty frontend && not empty frontend.meta}">
		<div class="preview-widget-title">
			<span title="Type"><spring:message code="robo-pages.frontend-keyword"/></span>
			<span title="ID">${frontend.id}</span>
			<span title="${frontend.meta[0].name}">${frontend.meta[0].name}</span>
		</div>
	</c:if>
	<div class="preview-widget-buttons">
		<div class="new-button"><i class="fa fas fa-plus fa-fw"></i><spring:message code="robo-pages.new-meta-data-keyword"/></div>
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="preview-widget-container">
		<div class="preview-widget-content">
		<div class="templatemo-content-container">
				<div class="templatemo-content-widget no-padding">
	            <div class="panel panel-default table-responsive">
	              <table class="table table-striped table-bordered templatemo-user-table">
	                <thead>
	                  <tr>
	                    <td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.name-keyword"/> <span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.description-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.lang-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><spring:message code="robo-pages.delete-keyword"/></td>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:if test="${not empty frontend && not empty frontend.meta}">
							<c:forEach items="${frontend.meta}" var="meta">
								<tr data-id="${meta.id}">
									<td contenteditable="true">${meta.name}</td>
									<td contenteditable="true">${meta.description}</td>
									<td>
										<select>
											<c:if test="${not empty languages}">
												<c:forEach items="${languages}" var="language">
													<option value="${language.code}" ${language.code == meta.lang? 'selected' : ''}>${language.code}</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
									<td><a href="javascript:" class="templatemo-link templatemo-meta-delete-link">Delete</a></td>
								</tr>
							</c:forEach>
						</c:if>
	                </tbody>
	              </table>    
	            </div>  
	            <div class="panel-default group-responsive">
	            
	            	<div class="type">
	            		<label for="type">Type</label>
	            			<select id="type">
	            				<c:choose>
	            					<c:when test="${not empty frontend.type }">
	            						<option value="${frontend.type}" selected>${types[frontend.type]}</option>
	            					</c:when>
	            					<c:otherwise>
	            						<option><spring:message code="robo-pages.choose-keyword"/></option>
	            						<c:if test="${not empty types}">
				            				<c:forEach var="type" items="${types}">
				            					<option value="${type.key}">${type.value}</option>
				            				</c:forEach>
				            			</c:if>
	            					</c:otherwise>
	            				</c:choose>
		            		</select>
	            	</div>
	            	
	            	<div class="category">
	            		<label for="category">Categories</label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty frontend.categories}">
		            				<c:forEach var="category" items="${frontend.categories}">
		            					<span class="item" data-id="${category.key}">
			            					<span title="value">${category.value}</span>
			            					<span class="item-close"></span>
		            					</span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="category" name="category" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            </div>     
	            <div class="panel-default assign-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.assigned-keyword"/> (<span>0</span>)</legend>
	            		<div>
	            			<table class="assign-song-table">
	            				<thead>
	            					<tr>
	            						<th><spring:message code="robo-pages.id-keyword"/></th>
	            						<th><spring:message code="robo-pages.image-keyword"/></th>
	            						<th><spring:message code="robo-pages.name-keyword"/></th>
	            						<th><spring:message code="robo-pages.album-keyword"/></th>
	            						<th><spring:message code="robo-pages.artists-keyword"/></th>
	            					</tr>
	            				</thead>
	            				<tbody>
	            					<c:if test="${frontend.type == 'SONG'}">
	            						<c:if test="${not empty frontend.songs }">
		            						<c:forEach var="song" items="${frontend.songs}">
		            							<tr data-id="${song.id}">
			            							<td>${song.id}</td>
			            							<td>
				            							<c:if test="${not empty song.photos }">
				            								<c:forEach var="photo" items="${song.photos}">
				            									<c:if test="${photo.isMainPhoto}">
				            										<c:set var="suburl" value="${fn:replace(photo.url, '{width}', '500')}"/>
				            										<c:set var="url" value="${fn:replace(suburl, '{height}', '500')}"/>
				            									</c:if>
				            								</c:forEach>
				            								<c:choose>
				            									<c:when test="${not empty url}">
				            										<img src="${url}">
				            									</c:when>
				            									<c:otherwise>
				            										<c:forEach var="photo" items="${song.photos}" end="0">
					            										<c:set var="suburl" value="${fn:replace(photo.url, '{width}', '500')}"/>
					            										<c:set var="url" value="${fn:replace(suburl, '{height}', '500')}"/>
					            										<img src="${url}">
						            								</c:forEach>
				            									</c:otherwise>
				            								</c:choose>
				            								<c:remove var="url"/>
				            							</c:if>
			            							</td>
			            							<td>${song.meta[0].name}</td>
			            							<c:forEach var="album" items="${song.album}" end="0">
			            								<td>${album.value}</td>
			            							</c:forEach>
			            							<td>
				            							<c:forEach var="artist" items="${song.artists}" varStatus="status">
				            								<c:choose>
				            									<c:when test="${status.index == 0}">
				            										${artist.value}
				            									</c:when>
				            									<c:otherwise>
				            										, ${artist.value}
				            									</c:otherwise>
				            								</c:choose>
				            							</c:forEach>
			            							</td>
			            						</tr>
		            						</c:forEach>
	            						</c:if>
	            					</c:if>
	            				</tbody>
	            			</table>
	            			<table class="assign-album-table">
	            				<thead>
	            					<tr>
	            						<th><spring:message code="robo-pages.id-keyword"/></th>
	            						<th><spring:message code="robo-pages.image-keyword"/></th>
	            						<th><spring:message code="robo-pages.name-keyword"/></th>
	            						<th><spring:message code="robo-pages.artists-keyword"/></th>
	            					</tr>
	            				</thead>
	            				<tbody>
	            					<c:if test="${frontend.type == 'ALBUM'}">
	            						<c:if test="${not empty frontend.albums }">
		            						<c:forEach var="album" items="${frontend.albums}">
		            							<tr data-id="${album.id}">
			            							<td>${album.id}</td>
			            							<td>
				            							<c:if test="${not empty album.photos }">
				            								<c:forEach var="photo" items="${album.photos}">
				            									<c:if test="${photo.isMainPhoto}">
				            										<c:set var="suburl" value="${fn:replace(photo.url, '{width}', '500')}"/>
				            										<c:set var="url" value="${fn:replace(suburl, '{height}', '500')}"/>
				            									</c:if>
				            								</c:forEach>
				            								<c:choose>
				            									<c:when test="${not empty url}">
				            										<img src="${url}">
				            									</c:when>
				            									<c:otherwise>
				            										<c:forEach var="photo" items="${album.photos}" end="0">
					            										<c:set var="suburl" value="${fn:replace(photo.url, '{width}', '500')}"/>
					            										<c:set var="url" value="${fn:replace(suburl, '{height}', '500')}"/>
					            										<img src="${url}">
						            								</c:forEach>
				            									</c:otherwise>
				            								</c:choose>
				            								<c:remove var="url"/>
				            							</c:if>
			            							</td>
			            							<td>${album.meta[0].name}</td>
			            							<td>
				            							<c:forEach var="artist" items="${album.artists}" varStatus="status">
				            								<c:choose>
				            									<c:when test="${status.index == 0}">
				            										${artist.value}
				            									</c:when>
				            									<c:otherwise>
				            										, ${artist.value}
				            									</c:otherwise>
				            								</c:choose>
				            							</c:forEach>
			            							</td>
			            						</tr>
		            						</c:forEach>
	            						</c:if>
	            					</c:if>
	            				</tbody>
	            			</table>
	            		</div>
	            	</fieldset>
	            </div>
	          </div> 
          </div>
        </div>
	</div>
	<div class="preview-widget-resposive-buttons">
		<input type="button" class="add-button">
		<input type="button" class="delete-button">
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="clear"></div>
</div>
	
<script type="text/javascript" src='<c:url value="/js/frontend.js"/>'></script>