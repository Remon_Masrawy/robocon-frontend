package com.masrawy.robo.transformation;

import java.util.List;

import com.masrawy.robo.dto.DTO;
import com.masrawy.robo.ws.VO;

public interface DTOTransformer<V extends VO, D extends DTO> {

	// ********************** Entity TO DTO transformation methods ****************************
	public D transformVOToDTO(V sourceObject);

	public void transformVOToDTO(V sourceObject, D destinationDTO);

	public List<D> transformVOToDTO(List<V> Objects);

	// ********************** DTO TO Entity transformation methods ****************************
	public void transformDTOToVO(D sourceDTO, V destinationObject);

	public V transformDTOToVO(D sourceDTO);

	public List<V> transformDTOToVO(List<D> dtos);
}
