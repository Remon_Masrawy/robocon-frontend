
$(document).ready(function(){
	$container 			= $(".templatemo-content-container");
	$table 				= $(".templatemo-content-container .templatemo-content-widget .table.data-table");
	$preview 			= $(".templatemo-content-container .preview-widget");
	$confirmation 		= $(".confirmation");
	
	$totalAlbums		= 0;
	$albumsPerPage		= 2;
	$currentAlbumPage	= 1;
	
	$table.on("click", ".templatemo-link.templatemo-del-link", function(){
		var id = $(this).closest("tr").attr("data-id");
		deleteArtist(id);
	});
	
	$container.on("click", ".new-btn", function(){
		window.location.href = $artist+"/0";
	});
	
	$preview.find(".preview-widget-buttons>.new-button").click(function(){
		addNewMetaRow({});
	});
	
	$preview.find(".preview-widget-resposive-buttons>div.save-button,.preview-widget-buttons>div.save-button").click(function(){
		saveArtist();
	});
	
	$preview.delegate("a.templatemo-link.templatemo-meta-delete-link", "click", function(){
		$(this).closest("tr").remove();
	})
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .songs-responsive .assign", function(){
		$(this).removeClass("assign").addClass("unassign");
	});
	$preview.on("click", ".preview-widget-container>.preview-widget-content .songs-responsive .unassign", function(){
		$(this).removeClass("unassign").addClass("assign");
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive div.add-file-button", function(){
		$(this).closest("fieldset").find("input[type='file']").click();
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo input[type='checkbox'].main-photo-checkbox", function(){
		var that = this;
		if($(this).is(":checked")){
			$preview.find(".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo input[type='checkbox'].main-photo-checkbox:checked").not($(that)).prop('checked', false);
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags", function(){
		if($(this).find("input[type='text']").length){
			$(this).find("input").focus();
		}else{
			var textBox = addTextBox().attr("maxlength", 30).attr("minLength", 10).attr("pattern", "[A-Za-z1-9]{30}");
			$(this).append(textBox);
			textBox.focus();
		}
	});
	
	$preview.on("keypress", ".preview-widget-container>.preview-widget-content .tags-responsive .tags input[type='text']", function(event){
		if(event.which == 13) {
			if($(this).val()){
				var tagDiv = addDiv().addClass("tag");
				var textSpan = addSpan().attr("title", "tag").text($(this).val());
				var closeSpan = addSpan().addClass("close");
				tagDiv.append(textSpan);
				tagDiv.append(closeSpan);
				$(this).closest(".tags").children("input").before(tagDiv);
				$(this).val("");
			}
		}
	});
	
	$preview.on("change", ".preview-widget-container>.preview-widget-content .photos-responsive input[type='file']", function(event){
		if($(this).val().length != 0){
			var image = addImage().attr("src", URL.createObjectURL(event.target.files[0]));
			var closeSpan = addSpan().addClass("close");
			var photoDiv = addDiv().addClass("photo").attr("title", event.target.files[0].name);
			photoDiv.append(image);
			photoDiv.append(closeSpan);
			$(this).closest("fieldset").find("div.photos").append(photoDiv);
		}
	});
	
	$preview.on("hover", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo", function(e){
		if(e.type == "mouseenter") {
			if(!$(this).hasClass("transparent")){
				$(this).find(".close").css({"opacity": 1});
				$(this).find("input[type='checkbox'].main-photo-checkbox").show();
			}
		  }
		  else if (e.type == "mouseleave") {
			  if(!$(this).hasClass("transparent")){
				  $(this).find(".close").css({"opacity": 0});
				  $(this).find("input[type='checkbox'].main-photo-checkbox").hide();
			  }
		  }});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".photo").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	
	$preview.on("hover", ".preview-widget-container>.preview-widget-content .events-responsive .events .event", function(e){
		if(e.type == "mouseenter"){
			$(this).find("a>span").css({"transform": "translateY(-50px)"});
		}else if (e.type == "mouseleave"){
			$(this).find("a>span").css({"transform": ""});
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .socials-responsive .add-button", function(e){
		var socialDiv = addDiv().addClass("social new");
		var input = addTextBox().attr("placeholder", "Social Link");
		var selector = $("<select></select>").append($("<option></option>").text("Social..."));
		$.each($socials, function(key, value){
			var option = $("<option></option>").val(key).text(value);
			selector.append(option);
		});
		var close = addSpan().addClass("social-close");
		socialDiv.append(close);
		socialDiv.append(selector);
		socialDiv.append(input);
		$(this).closest(".socials-responsive").find(".socials").append(socialDiv);
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .socials-responsive .social-close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".social").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags .tag .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".tag").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	
	$preview.on("click", ".preview-widget-content .albums-responsive .cu-pagination-wrap .fa-next", function(){
		var currentActive = $(this).closest(".cu-pagination").find("li:has(a.pages.active)");
		var nextElem = currentActive.next("li");
		if(nextElem.children("a.pages").length > 0){
			currentActive.find("a").removeClass("active");
			nextElem.find("a").addClass("active");
			$currentAlbumPage++;
			artistAlbums();
		}
	});
	
	$preview.on("click", ".preview-widget-content .albums-responsive .cu-pagination-wrap .fa-prev", function(){
		var currentActive = $(this).closest(".cu-pagination").find("li:has(a.pages.active)");
		var prevElem = currentActive.prev("li");
		if(prevElem.children("a.pages").length > 0){
			currentActive.find("a").removeClass("active");
			prevElem.find("a").addClass("active");
			$currentAlbumPage--;
			artistAlbums();
		}
	});
	
	$preview.on("click", ".preview-widget-content .albums-responsive .cu-pagination-wrap li a.pages:not(.active)", function(){
		var currentActive = $(this).closest(".cu-pagination").find("li:has(a.pages.active)");
		var selectedElem = $(this).closest("li");
		currentActive.find("a").removeClass("active");
		selectedElem.find("a").addClass("active");
		$currentAlbumPage = selectedElem.index();
		artistAlbums();
	});
	
	if($container.find(".data-table").hasClass("artists-table")){
		$enablePanel = true;
	}else{
		$enablePagination = false;
		$enablePanel = false;
		$.ajax({
			url: "/robo-web/web/artist/socials",
			type: $HTTP_METHOD.GET,
			headers: headers,
			success: function(data){
				$socials = data;
			},
			error: function(status, error){
				toastr.error('Error occured.', 'Error', {positionClass: 'toast-bottom-right'});
			},
			complete: function(status){
				
			}
		 });
		
		if($preview.attr("data-id")){
			artistAlbums();
		}
	}
	
});

$fill = function fillArtist(){
	$.when(ajaxCall($artist+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, artist){
			var url = $artist+"/"+artist.id;
			var row = addTableRow(8).attr("data-id", artist.id);
			row.children("td:first-child()").addClass("tc").text(artist.id);
			var keyword;
			$.each(artist.meta, function(i, meta){
				keyword = meta.name;
			});
			row.children("td:nth-child(2)").append(addLink().attr("href", url).addClass("templatemo-keyword-link").text(keyword));
			var image = addImage();
			if(artist.photos && artist.photos.length !== 0){
				$.each(artist.photos, function(index_, photo){
					if(photo.isMainPhoto){
						image.attr("src", photo.url.replace("{width}", 500).replace("{height}", 500));
					}
				});
				var src = image.attr("src");
				if(typeof src == typeof undefined || src == false || src == null){
					image.attr("src", artist.photos[0].url.replace("{width}", 500).replace("{height}", 500));
				}
			}
			row.children("td:nth-child(3)").addClass("tc").text(artist.albums);
			row.children("td:nth-child(4)").addClass("tc").text(artist.songs);
			row.children("td:nth-child(5)").addClass("tc").append(addLink().addClass("templatemo-image-link").append(image));
			row.children("td:nth-child(6)").addClass("tc").append(addLink().attr("href", url).addClass("templatemo-edit-btn").text("Edit"));
			row.children("td:nth-child(7)").addClass("tc").append(addLink().addClass("templatemo-link").text("Action"));
			row.children("td:last-child()").addClass("tc").append(addLink().addClass("templatemo-link templatemo-del-link").text("Delete"));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveArtist(){
	var valid = validate();
	if(!valid){
		return;
	}
	var artist = {};
	artist.id = parseInt($preview.attr("data-id"));
	var metaList = new Array();
	$preview.find(".preview-widget-content table>tbody").children("tr").each(function(){
		var meta = {};
		if($(this).attr("data-id")){
			meta.id = parseInt($(this).attr("data-id").trim());
		}
		meta.name = $(this).children(":first-child()").text().trim();
		meta.description = $(this).children(":nth-child(2)").text().trim();
		meta.lang = $(this).children(":nth-child(3)").children("select").val().trim();
		metaList.push(meta);
	});
	var photoList = new Array();
	$preview.find(".preview-widget-content .photos-responsive .photos").children(".photo").each(function(){
		var dataId = $(this).attr('data-id');
		if(typeof dataId !== typeof undefined && dataId !== false){
			var photo = {};
			photo.id = parseInt(dataId.trim());
			photo.isMainPhoto = $(this).find("input[type='checkbox'].main-photo-checkbox").is(":checked");
			photoList.push(photo);
		}
	});
	var socialList = new Array();
	$preview.find(".preview-widget-content .socials-responsive .socials").children(".social").each(function(){
		var social = {};
		social.id = $(this).attr("data-id");
		if($(this).find("select").length > 0){
			social.social = $(this).find("select").val();
		}else{
			social.social = $(this).attr("id");
		}
		social.link = $(this).find("input[type='text']").val();
		socialList.push(social);
	});
	var tagList = new Array();
	$preview.find(".preview-widget-content .tags-responsive .tags").children("div.tag").each(function(){
		var tag = {};
		tag.id = $(this).attr("data-id");
		tag.tag = $(this).find("span[title=tag]").text();
		tagList.push(tag);
	});
	artist.meta = metaList;
	var birthdate = $preview.find(".preview-widget-content .basic-info-responsive .general input[name='birthdate']").val();
	var deathDate = $preview.find(".preview-widget-content .basic-info-responsive .general input[name='deathdate']").val();
	var geneder   = $preview.find(".preview-widget-content .basic-info-responsive .general input[name='gender']:checked").val();
	var rate		 = $preview.find(".preview-widget-content .basic-info-responsive .general input[name='rate']").val();
	
	if(birthdate && $.trim(birthdate) !== ""){
		artist.birthDate = $.datepicker.formatDate('yy-mm-dd', new Date(birthdate));
	}
	if(deathDate && $.trim(deathDate) !== ""){
		artist.deathDate = $.datepicker.formatDate('yy-mm-dd', new Date(deathDate));
	}
	if(geneder && $.trim(geneder) !== ""){
		artist.geneder = geneder;
	}
	if(rate && $.trim(rate) !== ""){
		artist.rate = rate;
	}
	artist.photos = photoList;
	artist.socials = socialList;
	artist.tags = tagList;
	var providerId = $preview.find(".provider-responsive .select-div>select").val();
	if(providerId){
		var provider = {};
		provider.id = providerId;
		artist.provider = provider;
	}
	var method;
	if($preview.attr("data-id")){
		method = $HTTP_METHOD.PUT;
	}else{
		method = $HTTP_METHOD.POST;
	}
	
	$.when(ajaxCall($artist, method, artist, headers)).then(function(data){
		$preview.attr("data-id", data.id);
		updatePopup(data);
		$preview.find(".preview-widget-content .photos-responsive .photos").children(".photo").each(function(){
			var photoDiv = $(this);
			var photo = $(this);
			var dataId = photo.attr('data-id');
			var type = "ARTIST_IMAGE";
			if(typeof dataId === typeof undefined || dataId === false){
				var artistId = parseInt(data.id);
				var image = $(this).children("img");
				var name = $(this).attr("title");
				var xhr = new XMLHttpRequest();
				xhr.open( "GET", image.attr("src"), true );
				xhr.responseType = 'arraybuffer';
				xhr.onload = function( e ) {
					var arrayBufferView = new Uint8Array( this.response );
					var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
					var loadSpan = addSpan().addClass("load");
					var errorSpan = addSpan().addClass("error");
					var checkbox = addCheckbox().attr("name", "main").attr("title", "Set as main photo").addClass("main-photo-checkbox");
					photoDiv.append(loadSpan);
					$.when(mutipartCall($artist + "/" + artistId, $HTTP_METHOD.POST, blob, type, name)).then(function(data){
						loadSpan.remove();
						photoDiv.removeClass("transparent");
						photoDiv.append(checkbox);
						photo.attr("data-id", data.photos[data.photos.length-1].id);
						toastr.success("Photo Successfully Uploaded.", 'Success', {positionClass: 'toast-bottom-right'});
					},
					function(error){
						loadSpan.remove();
						photoDiv.removeClass("transparent");
						photoDiv.append(errorSpan);
						errors.push(error.responseJSON.message);
						toast(error.responseJSON.message);
					});
				};
				xhr.send();
			}
		});
		toastr.success("Artist Successfully Saved.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	});
}

function artistAlbums(){
	var artistId = parseInt($preview.attr("data-id"));
	$preview.find(".preview-widget-content .albums-responsive .albums").empty();
	var header = {};
	headers["Artist-Id"]=artistId;
	$.ajax({
		url: "/robo-web/web/album/"+$currentAlbumPage+"/"+$albumsPerPage,
		type: $HTTP_METHOD.GET,
		headers: headers,
		success: function(data, status, xhr){
			$.each(data, function(index, album){
				var albumDiv = addDiv().addClass("album");
				var albumPhotoDiv = addDiv().addClass("photo");
				var image = addImage();
				if(album.photos && album.photos.length !== 0){
					$.each(album.photos, function(index_, photo){
						if(photo.isMainPhoto){
							image.attr("src", photo.url.replace("{width}", 500).replace("{height}", 500));
						}
					});
					var src = image.attr("src");
					if(typeof src == typeof undefined || src == false || src == null){
						image.attr("src", album.photos[0].url.replace("{width}", 500).replace("{height}", 500));
					}
				}
				var contentDiv = addDiv().addClass("content");
				var contentHeaderDiv = addDiv().addClass("content-header");
				var headerLink = addLink().attr("href", "/robo-web/web/album/"+album.id);
				var headerSpan = addSpan().addClass("header");
				var artistsDiv = addDiv().addClass("artists");
				$.each(album.meta, function(i, meta){
					headerSpan.append(meta.name);
				});
				$.each(album.artists, function(key, value){
					var link = addLink().attr("href", "javascript:");
					if(artistId !== parseInt(key)){
						link.attr("href", "/robo-web/web/artist/"+key);
					}
					var span = addSpan().addClass("artist").text(value);
					link.append(span);
					artistsDiv.append(link);
				});
				var contentBodyDiv = addDiv().addClass("content-body");
				var songsList = addList().addClass("songs");
				var counter = 1;
				$.each(album.songs, function(key, value){
					var songItem = listItem().addClass("song");
					var indexSpan = addSpan().addClass("content-index").text(counter);
					var link = addLink().attr("href", "/robo-web/web/song/"+key);
					var span = addSpan().addClass("content-body").text(value);
					link.append(span);
					songItem.append(indexSpan);
					songItem.append(link);
					songsList.append(songItem);
					counter++;
				});
				albumPhotoDiv.append(image);
				contentBodyDiv.append(songsList);
				headerLink.append(headerSpan);
				contentHeaderDiv.append(headerLink);
				contentHeaderDiv.append(artistsDiv);
				contentDiv.append(contentHeaderDiv);
				contentDiv.append(contentBodyDiv);
				albumDiv.append(albumPhotoDiv);
				albumDiv.append(contentDiv);
				$preview.find(".preview-widget-content .albums-responsive .albums").append(albumDiv);
			});
			var total = xhr.getResponseHeader("x-data-count");
			if($.isNumeric(total)){
				if($totalAlbums != total){
					$totalAlbums = total;
				}
				if($totalAlbums <= $albumsPerPage){
					$preview.find(".preview-widget-content .albums-responsive .cu-pagination-wrap").hide();
				}else{
					$preview.find(".preview-widget-content .albums-responsive .cu-pagination-wrap").show();
				}
			}
		},
		error: function(xhr){
			handleError(xhr)
		},
		complete: function(status){
			
		}
	 });
}

function deleteArtist(artistId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteArtist(artistId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteArtist(artistId){
	
	$.when(ajaxCall($artist+"/"+artistId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+artistId+"']").remove();
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function refreshPagination(){
	if(!$pagination.parent().hasClass("pagination-hidden")){
		$totalPages = Math.ceil($totalItems / $itemsPerPage.val());
		if($totalPages == 1){
			$pagination.parent().addClass("pagination-hidden");
			return;
		}
		$pagination.children("li:not(:last)").remove();
		for(page=1; page<=$totalPages; page++){
			var $listItem = listItem().append(addLink().attr("data-item", page).text(page));
			$pagination.children("li:last-child()").before($listItem);
		}
		$pagination.find("li:nth-child("+$currentPage+")").addClass("active");
	}
	
}

function validate(){
	var valid = true;
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		$(this).children("td:nth-child(2)").removeClass("error");
		if($(this).children("td:nth-child(2)").is(":empty")){
			$(this).children("td:nth-child(2)").addClass("error");
			valid = false;
		}
	});
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		$(this).children("td:nth-child(3)").removeClass("error");
		if($(this).children("td:nth-child(3)").is(":empty")){
			$(this).children("td:nth-child(3)").addClass("error");
		}
	});
	
	return valid;
}

function updatePopup(data){
	$preview.find(".preview-widget-content table>tbody").empty();
	data.meta.forEach(function(meta, index){
		addNewMetaRow(meta);
	});
	
	$preview.find(".tags-responsive").find("div.tags").empty();
	data.tags.forEach(function(tag, index){
		var tagDiv = addDiv().addClass("tag").attr("data-id", tag.id);
		var textSpan = addSpan().attr("title", "tag").text(tag.tag);
		var closeSpan = addSpan().addClass("close");
		tagDiv.append(textSpan);
		tagDiv.append(closeSpan);
		$preview.find(".tags-responsive").find("div.tags").append(tagDiv);
	});
	
	$preview.find(".songs-responsive").find("div.songs").empty();
	var counter = 1;
	$.each(data.songs, function(key, value){
		var songDiv = addDiv().addClass("song").attr("data-id", key);
		var indexSpan = addSpan().addClass("index").text(counter);
		var textSpan = addSpan().addClass("content").text(value);
		songDiv.append(indexSpan).append(textSpan);
		$preview.find(".songs-responsive").find("div.songs").append(songDiv);
		counter++;
	});
	$preview.find(".socials-responsive").find("div.socials").empty();
	data.socials.forEach(function(social, index){
		var socialDiv = addDiv().addClass("social").attr("data-id", social.id).attr("id", social.social);
		var closeSpan = addSpan().addClass("social-close");
		var logoSpan = addSpan().addClass("logo").addClass("fa-co-"+social.social.toLowerCase());
		var textBox = addTextBox().attr("placeholder", "Social Link").attr("contenteditable", "false").val(social.link);
		socialDiv.append(closeSpan).append(logoSpan).append(textBox);
		$preview.find(".socials-responsive").find("div.socials").append(socialDiv);
	});
}