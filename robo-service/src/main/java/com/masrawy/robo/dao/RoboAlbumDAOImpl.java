package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboAlbumVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboAlbumDAO")
public class RoboAlbumDAOImpl extends AbstractWebserviceDAO<RoboAlbumVO, Long> implements RoboAlbumDAO{

	public RoboAlbumDAOImpl(){
		super(RoboAlbumVO.class, new ParameterizedTypeReference<List<RoboAlbumVO>>() {});
	}
}
