var countries = new Array();

$enablePagination = false;

$(document).ready(function(){
	$container = $(".templatemo-content-container");
	
	$table = $(".templatemo-content-container .templatemo-content-widget .table");
	
	$confirmation = $(".confirmation");
	
	$enablePagination = true;
	
	$container.on("click", ".new-btn", function(){
		addNewRow();
	});
	
	$container.on("click", ".save-btn", function(){
		saveOperator();
	});
	
	$.ajax({
		url: $country+"/0/0",
		type: $HTTP_METHOD.GET,
		headers: headers,
		success: function(data){
			countries = data;
		},
		error: function(status, error){
			toastr.error('Error occured.', 'Error', {positionClass: 'toast-bottom-right'});
		},
		complete: function(status){
			
		}
	 });
	
});

$fill = function fillOperator(){
	$.when(ajaxCall($operator+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, operator){
			var row = addTableRow(7).attr("data-id", operator.id);
			row.children("td:first-child()").addClass("tlc").text(operator.id);
			row.children("td:nth-child(2)").addClass("tll").attr("contenteditable", "true").attr("title", "name").text(operator.name);
			row.children("td:nth-child(3)").addClass("tll").attr("contenteditable", "true").attr("title", "description").text(operator.description);
			row.children("td:nth-child(4)").addClass("tlc").attr("contenteditable", "true").attr("title", "mcc").text(operator.mcc);
			row.children("td:nth-child(5)").addClass("tlc").attr("contenteditable", "true").attr("title", "mnc").text(operator.mnc);
			row.children("td:nth-child(6)").addClass("tlc").attr("title", "country").append(countrySelector().val(operator.country.id));
			row.children("td:last-child()").addClass("tlc").attr("title", "allowed").append(addCheckbox().prop('checked', operator.allowed));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveOperator(){
	var valid = validate();
	if(!valid){
		return;
	}
	var errors = new Array();
	$table.find("tbody tr").each(function(){
		var operator = {};
		operator.id = parseInt($(this).attr("data-id"));
		operator.name = $(this).find("td[title='name']").text();
		operator.description = $(this).find("td[title='description']").text();
		operator.mcc = $(this).find("td[title='mcc']").text();
		operator.mnc = $(this).find("td[title='mnc']").text();
		operator.allowed = $(this).find("td[title='allowed'] input[type='checkbox']").is(":checked");
		var countryId = $(this).find("td[title='country'] select>option:selected").val();
		if(countryId){
			var country = {};
			country.id = countryId;
			operator.country = country;
		}
		
		var method;
		if($(this).attr("data-id")){
			method = $HTTP_METHOD.PUT;
		}else{
			method = $HTTP_METHOD.POST;
		}
		var that = $(this);
		$.when(ajaxCall($operator, method, operator, headers)).then(function(data){
			that.attr("data-id", data.id);
			that.find("td:first-child()").text(data.id);
			that.css({"border":"none"});
		},
		function(error){
			errors.push(error);
			that.css({"border":"2px solid red"});
			toast(error.responseJSON.message);
		});
		
	});
}

function addNewRow(){
	var row = addTableRow(7);
	row.children(":first-child()").addClass("tlc");
	row.children(":nth-child(2)").attr("title", "name").addClass("tll").attr("contenteditable", true);
	row.children(":nth-child(3)").attr("title", "description").addClass("tll").attr("contenteditable", true);
	row.children(":nth-child(4)").attr("title", "mcc").addClass("tlc").attr("contenteditable", true);
	row.children(":nth-child(5)").attr("title", "mnc").addClass("tlc").attr("contenteditable", true);
	row.children(":nth-child(6)").attr("title", "country").addClass("tlc").append(countrySelector());
	row.children(":last-child()").attr("title", "allowed").addClass("tlc").append(addCheckbox().attr("checked", "checked"));
	$table.find("tbody:last-child").append(row);
}

function countrySelector(){
	var selector = $("<select></select>");
	selector.append($("<option></option>").text($("#chooseMsg").val()));
	countries.forEach(function(country, index){
		var option = $("<option></option>").attr("value", country.id).text(country.name);
		selector.append(option);
	});
	return selector;
}

function deleteOperator(operatorId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteOperator(operatorId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteOperator(operatorId){
	
	$.when(ajaxCall($operator+"/"+operatorId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+operatorId+"']").remove();
		toastr.success("Operator Successfully deleted.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function validate(){
	var valid = true;
	$table.find("tbody tr").each(function(){
		if($(this).find("td[title='name']").text()){
			$(this).find("td[title='name']").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td[title='name']").css({"border":"2px solid red"});
			valid = false;
		}
		if($(this).find("td[title='mcc']").text()){
			$(this).find("td[title='mcc']").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td[title='mcc']").css({"border":"2px solid red"});
			valid = false;
		}
		if($(this).find("td[title='mnc']").text()){
			$(this).find("td[title='mnc']").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td[title='mnc']").css({"border":"2px solid red"});
			valid = false;
		}
		if($(this).find("td[title='country'] select>option:selected").val()){
			$(this).find("td[title='country']").css({"border":"1px solid #ddd"});
		}else{
			$(this).find("td[title='country']").css({"border":"2px solid red"});
			valid = false;
		}
	})
	return valid;
}