package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboEventDTO;
import com.masrawy.robo.models.RoboEventVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboEventTransformer")
public class RoboEventTransformerImpl extends AbstractDTOTransformer<RoboEventVO, RoboEventDTO> implements RoboEventTransformer{

	public RoboEventTransformerImpl(){
		super(RoboEventVO.class, RoboEventDTO.class);
	}
}
