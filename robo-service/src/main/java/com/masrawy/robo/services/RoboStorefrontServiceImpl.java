package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboStorefrontDAO;
import com.masrawy.robo.dto.RoboStorefrontDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboStorefrontException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.models.RoboStorefrontVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboStorefrontTransformer;

@Service("roboStorefrontService")
public class RoboStorefrontServiceImpl extends AbstractPgrlServiceImpl<RoboStorefrontVO, RoboStorefrontDTO, Long> implements RoboStorefrontService{

	@Autowired
	private RoboStorefrontDAO roboStorefrontDAO;
	
	@Autowired
	private RoboStorefrontTransformer roboStorefrontTransformer;
	
	@Value("${robocon.ws.storefront.url}")
	private String storefrontURL;
	
	@Override
	public RoboStorefrontDTO createStorefront(RoboStorefrontDTO roboStorefrontDTO){
		logger.debug("Robocon request for creating storefront");
		try{
			RoboStorefrontDTO storefront = postObject(storefrontURL, roboStorefrontDTO);
			return storefront;
		}catch(RoboResponseException ex){
			logger.error("Storefront creation error occured.", ex);
			throw new RoboStorefrontException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Storefront creation error occured.", ex);
			throw new RoboStorefrontException(ex.getMessage());
		}
	}
	
	@Override
	public RoboStorefrontDTO updateStorefront(RoboStorefrontDTO roboStorefrontDTO){
		logger.debug("Robocon request for updating storefront");
		try{
			RoboStorefrontDTO storefront = updateR(storefrontURL, roboStorefrontDTO);
			return storefront;
		}catch(RoboResponseException ex){
			logger.error("Storefront update error occured.", ex);
			throw new RoboStorefrontException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Storefront update error occured.", ex);
			throw new RoboStorefrontException(ex.getMessage());
		}
	}
	
	@Override
	public RoboStorefrontDTO getStorefront(Long id){
		logger.debug("Robocon request for getting storefront by id");
		RoboStorefrontDTO storefront = getById(storefrontURL, id);
		return storefront;
	}
	
	@Override
	public List<RoboStorefrontDTO> getStorefrontList(Integer page, Integer limit){
		logger.debug("Robocon request for getting storefronts list");
		List<RoboStorefrontDTO> storefronts = getList(String.format(pagingURL, storefrontURL, page, limit));
		return storefronts;
	}
	
	@Override
	public void deleteStorefront(Long id){
		logger.debug("Robocon request for deleting storefront by id");
		try{
			deleteById(storefrontURL, id);
		}catch(RoboResponseException ex){
			logger.error("Storefront delete error occured.", ex);
			throw new RoboStorefrontException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Storefront delete error occured.", ex);
			throw new RoboStorefrontException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboStorefrontVO, RoboStorefrontDTO> getTransformer() {
		return roboStorefrontTransformer;
	}

	@Override
	protected DAO<RoboStorefrontVO, Long> getDAO() {
		return roboStorefrontDAO;
	}

}
