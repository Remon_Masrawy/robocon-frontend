package com.masrawy.robo.exceptions;

import static com.masrawy.robo.exceptions.ErrorConstants.ROBO_CLIENT_ERROR;

public class RoboFrontendException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Frontend exception occured.";
	
	public RoboFrontendException(){
		super(ERROR_MSG);
	}
	
	public RoboFrontendException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBO_CLIENT_ERROR;
	}

}
