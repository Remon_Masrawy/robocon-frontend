package com.masrawy.robo.serializers;

import java.io.IOException;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;

@JacksonStdImpl
public class CommonsMultipartFileSerializer extends StdScalarSerializer<CommonsMultipartFile>{

	private static final long serialVersionUID = 1L;

	public CommonsMultipartFileSerializer(){
		super(CommonsMultipartFile.class);
	}

	@Override
	public void serialize(CommonsMultipartFile value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeBinary(provider.getConfig().getBase64Variant(), value.getInputStream(), (int)value.getSize());
	}
}
