package com.masrawy.robo.holders;

public class RestResponseHolder {

	private static String collectionCount;

	public static String getCollectionCount() {
		return collectionCount;
	}

	public static void setCollectionCount(String collectionCount) {
		RestResponseHolder.collectionCount = collectionCount;
	}
	
	public static boolean isCollectionCountNotBlank(){
		if(RestResponseHolder.collectionCount == null){
			return false;
		}
		if(RestResponseHolder.collectionCount.isEmpty()){
			return false;
		}
		return true;
	}
	
}
