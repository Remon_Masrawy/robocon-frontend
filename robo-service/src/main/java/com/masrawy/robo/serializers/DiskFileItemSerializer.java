package com.masrawy.robo.serializers;

import java.io.IOException;

import org.apache.commons.fileupload.disk.DiskFileItem;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;

@JacksonStdImpl
public class DiskFileItemSerializer extends StdScalarSerializer<DiskFileItem>{

	private static final long serialVersionUID = 1L;

	public DiskFileItemSerializer(){
		super(DiskFileItem.class);
	}

	@Override
	public void serialize(DiskFileItem value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeBinary(value.getInputStream(), 3000);
	}
}
