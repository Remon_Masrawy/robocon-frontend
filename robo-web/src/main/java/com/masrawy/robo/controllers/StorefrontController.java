package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.STOREFRONT_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboStorefrontDTO;
import com.masrawy.robo.services.RoboStorefrontService;

@Controller
@RequestMapping(value = "/web/storefront")
public class StorefrontController {

	@Autowired
	private RoboStorefrontService roboStorefrontService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView storefronts(){
		ModelAndView model = new ModelAndView(STOREFRONT_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{storefrontId}", method = RequestMethod.GET)
	public ModelAndView storefront(@PathVariable("storefrontId") String storefrontId){
		ModelAndView model = new ModelAndView(STOREFRONT_PAGE);
		try{
			Long id = Long.valueOf(storefrontId);
			if(id != 0){
				model.addObject("storefront", roboStorefrontService.getStorefront(id));
			}
		}catch(Exception e){
			model = new ModelAndView("redirect:/web/storefront");
		}
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboStorefrontDTO create(@RequestBody RoboStorefrontDTO roboStorefrontDTO){
		return roboStorefrontService.createStorefront(roboStorefrontDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboStorefrontDTO update(@RequestBody RoboStorefrontDTO roboStorefrontDTO){
		return roboStorefrontService.updateStorefront(roboStorefrontDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value="/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboStorefrontDTO> storefronts(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboStorefrontService.getStorefrontList(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id") Long id){
		roboStorefrontService.deleteStorefront(id);
	}
}
