package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboCategoryDTO;
import com.masrawy.robo.models.RoboCategoryVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboCategoryTransformer extends DTOTransformer<RoboCategoryVO, RoboCategoryDTO>{

}
