package com.masrawy.robo.dto;

public class RoboPhotoDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String checksum;
	private Long size;
	private String originalName;
	private String contentType;
	private String url;
	private Boolean isMainPhoto;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public String getOriginalName() {
		return originalName;
	}
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Boolean getIsMainPhoto() {
		return isMainPhoto;
	}
	public void setIsMainPhoto(Boolean isMainPhoto) {
		this.isMainPhoto = isMainPhoto;
	}
}
