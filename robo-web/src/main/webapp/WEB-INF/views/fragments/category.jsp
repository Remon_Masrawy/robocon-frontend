<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="preview-widget" data-id="${category.id}">
	<c:if test="${not empty category && not empty category.meta}">
		<div class="preview-widget-title">
			<span title="Type"><spring:message code="robo-pages.category-keyword"/></span>
			<span title="ID">${category.id}</span>
			<span title="${category.meta[0].name}">${category.meta[0].name}</span>
		</div>
	</c:if>
	<div class="preview-widget-buttons">
		<div class="new-button"><i class="fa fas fa-plus fa-fw"></i><spring:message code="robo-pages.new-meta-data-keyword"/></div>
	</div>
	<div class="preview-widget-container">
		<div class="preview-widget-content">
		<div class="templatemo-content-container">
				<div class="templatemo-content-widget no-padding">
	            <div class="panel panel-default table-responsive">
	              <table class="table table-striped table-bordered templatemo-user-table">
	                <thead>
	                  <tr>
	                    <td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.name-keyword"/> <span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.description-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.lang-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><spring:message code="robo-pages.delete-keyword"/></td>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:if test="${not empty category && not empty category.meta}">
							<c:forEach items="${category.meta}" var="meta">
								<tr data-id="${meta.id}">
									<td contenteditable="true">${meta.name}</td>
									<td contenteditable="true">${meta.description}</td>
									<td>
										<select>
											<c:if test="${not empty languages}">
												<c:forEach items="${languages}" var="language">
													<option value="${language.code}" ${language.code == meta.lang? 'selected' : ''}>${language.code}</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
									<td><a href="javascript:" class="templatemo-link templatemo-meta-delete-link">Delete</a></td>
								</tr>
							</c:forEach>
						</c:if>
	                </tbody>
	              </table>    
	            </div>   
	            <div class="panel-default group-responsive">
	            	<div class="storefront">
	            		<label for="storefront"><spring:message code="robo-pages.storefronts-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty category.storefronts}">
		            				<c:forEach var="storefront" items="${category.storefronts}">
		            					<span class="item" data-id="${storefront.key}">
		            						<span title="value">${storefront.value}</span>
			            					<span class="item-close">
		            					</span></span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="storefront" name="storefront" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            	
	            	<div class="operator">
	            		<label for="operator"><spring:message code="robo-pages.operators-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty category.operators}">
		            				<c:forEach var="operator" items="${category.operators}">
		            					<span class="item" data-id="${operator.key}">
		            					<span title="value">${operator.value['name']} | ${operator.value['country']}</span>
		            					<span class="item-close"></span>
		            					</span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="operator" name="operator" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            	
	            	<div class="country">
	            		<label for="country"><spring:message code="robo-pages.countries-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty category.countries}">
		            				<c:forEach var="country" items="${category.countries}">
		            					<span class="item" data-id="${country.key}">
		            					<span title="value">${country.value}</span>
		            					<span class="item-close"></span>
		            					</span>
		            				</c:forEach>
		            			</c:if>
	            			</div>
	            			<input id="country" name="country" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            	
	            </div>                       
	          </div> 
          </div>
        </div>
	</div>
	<div class="preview-widget-resposive-buttons">
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="clear"></div>
</div>
	
<script type="text/javascript" src='<c:url value="/js/category.js"/>'></script>