package com.masrawy.robo.persistence;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.ws.VO;

public interface DAO<V extends VO, PK extends Serializable> {

	public V postObject(String url, Object request);
	public V postFile(String url, MultipartFile file, String fileName);
	public List<V> getObjectList(String url);
	public Map<String, String> getMap(String url);
	public V getObject(String url);
	public void execute(String url);
	public void updateObject(String url, Object request);
	public V updateObjectR(String url, Object request);
	public void deleteObject(String url);
}
