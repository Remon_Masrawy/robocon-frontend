package com.masrawy.robo.models;

import java.util.List;
import java.util.Map;

import com.masrawy.robo.ws.AbstractVO;

public class RoboSongVO extends AbstractVO{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String isrc;
	private String createdDate;
	private Long year;
	private String videoLink;
	private List<RoboMetaVO> meta;
	private Map<Long, String> artists;
	private Map<Long, String> mainArtists;
	private Map<Long, String> album;
	private Map<Long, String> genres;
	private Map<Long, String> provider;
	private List<RoboPhotoVO> photos;
	private List<RoboTagVO> tags;
	private List<RoboContentVO> content;
	private List<RoboLyricsVO> lyrics;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIsrc() {
		return isrc;
	}
	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Long getYear() {
		return year;
	}
	public void setYear(Long year) {
		this.year = year;
	}
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	public List<RoboMetaVO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaVO> meta) {
		this.meta = meta;
	}
	public Map<Long, String> getArtists() {
		return artists;
	}
	public void setArtists(Map<Long, String> artists) {
		this.artists = artists;
	}
	public Map<Long, String> getMainArtists() {
		return mainArtists;
	}
	public void setMainArtists(Map<Long, String> mainArtists) {
		this.mainArtists = mainArtists;
	}
	public Map<Long, String> getAlbum() {
		return album;
	}
	public void setAlbum(Map<Long, String> album) {
		this.album = album;
	}
	public Map<Long, String> getGenres() {
		return genres;
	}
	public void setGenres(Map<Long, String> genres) {
		this.genres = genres;
	}
	public Map<Long, String> getProvider() {
		return provider;
	}
	public void setProvider(Map<Long, String> provider) {
		this.provider = provider;
	}
	public List<RoboPhotoVO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboPhotoVO> photos) {
		this.photos = photos;
	}
	public List<RoboTagVO> getTags() {
		return tags;
	}
	public void setTags(List<RoboTagVO> tags) {
		this.tags = tags;
	}
	public List<RoboContentVO> getContent() {
		return content;
	}
	public void setContent(List<RoboContentVO> content) {
		this.content = content;
	}
	public List<RoboLyricsVO> getLyrics() {
		return lyrics;
	}
	public void setLyrics(List<RoboLyricsVO> lyrics) {
		this.lyrics = lyrics;
	}
}
