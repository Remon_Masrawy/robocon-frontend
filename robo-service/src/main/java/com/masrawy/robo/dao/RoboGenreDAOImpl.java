package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboGenreVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboGenreDAO")
public class RoboGenreDAOImpl extends AbstractWebserviceDAO<RoboGenreVO, Long> implements RoboGenreDAO{

	public RoboGenreDAOImpl(){
		super(RoboGenreVO.class, new ParameterizedTypeReference<List<RoboGenreVO>>() {});
	}
}
