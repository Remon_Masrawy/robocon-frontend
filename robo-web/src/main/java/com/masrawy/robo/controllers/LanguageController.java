package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.masrawy.robo.dto.RoboLanguageDTO;
import com.masrawy.robo.services.RoboLanguageService;

@Controller
@RequestMapping(value = "/web/language")
public class LanguageController {

	@Autowired
	private RoboLanguageService roboLanguageService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboLanguageDTO> getLanguages(@PathVariable("pageNum") Integer pageNum, @PathVariable("pageLimit") Integer pageLimit){
		return roboLanguageService.getLanguageList(pageNum, pageLimit);
	}
}
