package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dao.RoboAlbumDAO;
import com.masrawy.robo.dto.RoboAlbumDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboAlbumException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.model.RoboMimeTypeEnum;
import com.masrawy.robo.models.RoboAlbumVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboAlbumTransformer;

@Service("roboAlbumService")
public class RoboAlbumServiceImpl extends AbstractPgrlServiceImpl<RoboAlbumVO, RoboAlbumDTO, Long> implements RoboAlbumService{

	@Autowired
	private RoboAlbumDAO roboAlbumDAO;
	
	@Autowired
	private RoboAlbumTransformer roboAlbumTransformer;
	
	@Value("${robocon.ws.album.url}")
	private String albumURL;
	
	@Value("${robocon.ws.album-artist.url}")
	private String albumArtistURL;
	
	public RoboAlbumDTO createAlbum(RoboAlbumDTO roboAlbumDTO){
		logger.debug("Robocon request for creatting album");
		try{
			roboAlbumDTO = postObject(albumURL, roboAlbumDTO);
			return roboAlbumDTO;
		}catch(RoboResponseException ex){
			logger.error("Album creation error occured.", ex);
			throw new RoboAlbumException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Album creation error occured.", ex);
			throw new RoboAlbumException(ex.getMessage());
		}
	}
	
	@Override
	public RoboAlbumDTO updateAlbum(RoboAlbumDTO roboAlbumDTO){
		logger.debug("Robocon request for updatting album");
		try{
			roboAlbumDTO = updateR(albumURL, roboAlbumDTO);
			return roboAlbumDTO;
		}catch(RoboResponseException ex){
			logger.error("Album update error occured.", ex);
			throw new RoboAlbumException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Album update error occured.", ex);
			throw new RoboAlbumException(ex.getMessage());
		}
	}
	
	@Override
	public RoboAlbumDTO uploadPhoto(MultipartFile file, Long albumId, String name){
		logger.debug("Robocon request for uploading album photo");
		try{
			RoboAlbumDTO album = postFile(String.format(fileUploadURL, albumId, RoboMimeTypeEnum.ALBUM_IMAGE.getIntValue()), file, name);
			return album;
		}catch(RoboResponseException ex){
			logger.error("Album update error occured.", ex);
			throw new RoboAlbumException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Album update error occured.", ex);
			throw new RoboAlbumException(ex.getMessage());
		}
	}
	
	@Override
	public RoboAlbumDTO getAlbum(Long albumId){
		logger.debug("Robocon request for getting album");
		try{
			RoboAlbumDTO album = getById(albumURL, albumId);
			return album;
		}catch(RoboResponseException ex){
			logger.error("Album retrieve error occured.", ex);
			throw new RoboAlbumException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Album retrieve error occured.", ex);
			throw new RoboAlbumException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboAlbumDTO> getAlbums(Integer page, Integer limit){
		logger.debug("Robocon request for getting albums list");
		List<RoboAlbumDTO> albums = getList(String.format(pagingURL, albumURL, page, limit));
		return albums;
	}
	
	@Override
	public void deleteAlbum(Long albumId){
		logger.debug("Robocon request for deleting album");
		try{
			deleteById(albumURL, albumId);
		}catch(RoboResponseException ex){
			logger.error("Album delete error occured.", ex);
			throw new RoboAlbumException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Album delete error occured.", ex);
			throw new RoboAlbumException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboAlbumVO, RoboAlbumDTO> getTransformer() {
		return roboAlbumTransformer;
	}

	@Override
	protected DAO<RoboAlbumVO, Long> getDAO() {
		return roboAlbumDAO;
	}

}
