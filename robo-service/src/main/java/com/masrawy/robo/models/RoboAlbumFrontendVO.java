package com.masrawy.robo.models;

import java.util.List;

public class RoboAlbumFrontendVO extends RoboFrontendVO{

	private static final long serialVersionUID = 1L;

	private List<RoboAlbumVO> albums;

	public List<RoboAlbumVO> getAlbums() {
		return albums;
	}

	public void setAlbums(List<RoboAlbumVO> albums) {
		this.albums = albums;
	}
}
