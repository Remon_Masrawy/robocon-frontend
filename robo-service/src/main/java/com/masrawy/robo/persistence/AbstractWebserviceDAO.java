package com.masrawy.robo.persistence;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.masrawy.robo.exceptions.AccessDeniedException;
import com.masrawy.robo.exceptions.RoboClientException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.exceptions.ServiceNotFoundException;
import com.masrawy.robo.exceptions.ServiceRequestTimeoutException;
import com.masrawy.robo.exceptions.ServiceServerError;
import com.masrawy.robo.ws.VO;

@Repository
public abstract class AbstractWebserviceDAO<V extends VO, PK extends Serializable> implements DAO<V, PK>{

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private HttpServletResponse httpResponse;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private HttpHeaders headers;
	
	@Autowired
	private LinkedMultiValueMap<String, String> parameters;
	
	private Class<V> valueObjectClass;
	
	private ParameterizedTypeReference<List<V>> valueObjectListClass;
	
	private static final String X_DATA_COUNT = "x-data-count";
	
	protected AbstractWebserviceDAO(Class<V> valueObjectClass, ParameterizedTypeReference<List<V>> valueObjectListClass){
		this.valueObjectClass = valueObjectClass;
		this.valueObjectListClass = valueObjectListClass;
	}
	
	@Override
	public V postObject(String url, Object request) throws RoboResponseException, RoboClientException{
		logger.info("Start calling web service for url {}, on {}.", url, new Date());
		try{
			HttpEntity<Object> entity = new HttpEntity<Object>(request, headers);
			V v = (V) restTemplate.postForObject(url, entity, valueObjectClass);
			logger.info("Successfully end calling web service for url {}, on {}", url, new Date());
			return v;
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
	}
	
	public V postFile(String url, MultipartFile file, String fileName) throws RoboResponseException, RoboClientException{
		try{
			
			String charset = "UTF-8";
			String CRLF = "\r\n";
			String boundary = Long.toHexString(System.currentTimeMillis());
			
			File convFile = new File(file.getOriginalFilename());
		    file.transferTo(convFile);
			
			URLConnection connection = new URL(url).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
			for(Entry<String, List<String>> header : headers.entrySet()){
				connection.setRequestProperty(header.getKey(), header.getValue().get(0));
			}
			OutputStream output = connection.getOutputStream();
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
			
			writer.append("--" + boundary).append(CRLF);
		    writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + fileName + "\"").append(CRLF);
		    writer.append("Content-Type: " + file.getContentType()).append(CRLF); // Text file itself must be saved in this charset!
		    writer.append(CRLF).flush();
		    Files.copy(convFile.toPath(), output);
		    output.flush(); // Important before continuing with writer!
		    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
		    writer.append("--" + boundary + "--").append(CRLF).flush();
		    writer.close();
		    int responseCode = ((HttpURLConnection) connection).getResponseCode();
		    if(responseCode == HttpStatus.OK.value()){
		    	BufferedReader br = new BufferedReader(new InputStreamReader(((HttpURLConnection) connection).getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                V v = objectMapper.readValue(sb.toString(), valueObjectClass);
                return v;
		    }else{
		    	logger.error("Error: cannot post file due to error {}", "");
		    	HttpHeaders responseHeaders = new HttpHeaders();
		    	String errorMsg = ((HttpURLConnection) connection).getHeaderField("Error-Msg");
		    	responseHeaders.add("Error-Msg", errorMsg);
				throw new RoboResponseException(HttpStatus.valueOf(responseCode), HttpStatus.valueOf(responseCode).name(), responseHeaders, null, Charset.defaultCharset());
		    }
			
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
		
	}
	
	@Override
	public List<V> getObjectList(String url){
		logger.info("Start calling web service for url {}, on {}.", url, new Date());
		try{
			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
			builder.queryParams(parameters);
			HttpEntity<Object> entity = new HttpEntity<Object>(headers);
			HttpEntity<List<V>> response = (HttpEntity<List<V>>) restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, entity, valueObjectListClass);
			HttpHeaders headers = response.getHeaders();
			if(headers.containsKey(X_DATA_COUNT)){
				httpResponse.addHeader(X_DATA_COUNT, headers.getFirst(X_DATA_COUNT));
			}
			logger.info("Successfully end calling web service for url {}, on {}", url, new Date());
			return (List<V>) response.getBody();
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, String> getMap(String url){
		logger.info("Start calling web service for url {}, on {}.", url, new Date());
		try{
			HttpEntity<Object> entity = new HttpEntity<Object>(headers);
			HttpEntity<Map<String, String>> response = (HttpEntity<Map<String, String>>) restTemplate.exchange(url, HttpMethod.GET, entity, (Class<Map<String, String>>) (Class) Map.class);
			logger.info("Successfully end calling web service for url {}, on {}", url, new Date());
			return (Map<String, String>) response.getBody();
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
	}
	
	@Override
	public V getObject(String url){
		logger.info("Start calling web service for url {}, on {}.", url, new Date());
		try{
			HttpEntity<Object> entity = new HttpEntity<Object>(headers);
			ResponseEntity<V> response = (ResponseEntity<V>) restTemplate.exchange(url, HttpMethod.GET, entity, valueObjectClass);
			logger.info("Successfully end calling web service for url {}, on {}", url, new Date());
			return (V) response.getBody();
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
	}
	
	@Override
	public void execute(String url){
		logger.info("Start calling web service for url {}, on {}.", url, new Date());
		try{
			HttpEntity<Object> entity = new HttpEntity<Object>(headers);
			ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);
			if(response.getStatusCode().equals(HttpStatus.OK)){
				IOUtils.write(response.getBody(), httpResponse.getOutputStream());
			}
			logger.info("Successfully end calling web service for url {}, on {}", url, new Date());
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
	}
	
	@Override
	public void updateObject(String url, Object request){
		logger.info("Start calling web service for url {}, on {}.", url, new Date());
		try{
			HttpEntity<Object> entity = new HttpEntity<Object>(request, headers);
			restTemplate.put(url, entity);
			logger.info("Successfully end calling web service for url {}, on {}", url, new Date());
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
	}
	
	@Override
	public V updateObjectR(String url, Object request){
		logger.info("Start calling web service for url {}, on {}.", url, new Date());
		try{
			HttpEntity<Object> entity = new HttpEntity<Object>(request, headers);
			ResponseEntity<V> response = (ResponseEntity<V>) restTemplate.exchange(url, HttpMethod.PUT, entity, valueObjectClass);
			logger.info("Successfully end calling web service for url {}, on {}", url, new Date());
			return (V) response.getBody();
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
	}
	
	@Override
	public void deleteObject(String url){
		logger.info("Start calling web service for url {}, on {}.", url, new Date());
		try{
			HttpEntity<Object> entity = new HttpEntity<Object>(headers);
			restTemplate.exchange(url, HttpMethod.DELETE, entity, valueObjectClass);
			logger.info("Successfully end calling web service for url {}, on {}", url, new Date());
		}catch(Throwable ex){
			logger.error("Error: cannot post object due to {}", ex);
			throw runtimeException(ex);
		}
	}
	
	private RuntimeException runtimeException(Throwable e){
		if(e instanceof HttpStatusCodeException){
			switch(((HttpStatusCodeException) e).getStatusCode()){
				case FORBIDDEN:
					return new AccessDeniedException();
				case INTERNAL_SERVER_ERROR:
					return new ServiceServerError();
				case REQUEST_TIMEOUT:
					return new ServiceRequestTimeoutException();
				case NOT_FOUND:
					return new ServiceNotFoundException();
				case BAD_REQUEST:
					return new RoboResponseException(((HttpStatusCodeException) e).getStatusCode(), ((HttpStatusCodeException) e).getStatusText(), ((HttpStatusCodeException) e).getResponseHeaders(), ((HttpStatusCodeException) e).getResponseBodyAsByteArray(), null);
				default:
					return new RoboResponseException(((HttpStatusCodeException) e).getStatusCode(), ((HttpStatusCodeException) e).getStatusText(), ((HttpStatusCodeException) e).getResponseHeaders(), ((HttpStatusCodeException) e).getResponseBodyAsByteArray(), null);
			}
		}else if(e instanceof ResourceAccessException){
			return (ResourceAccessException) e;
		}else{
			return new RoboClientException();
		}
	}
	
	

}
