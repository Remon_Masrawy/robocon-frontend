package com.masrawy.robo.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dao.RoboArtistDAO;
import com.masrawy.robo.dto.RoboArtistDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboArtistException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.model.RoboMimeTypeEnum;
import com.masrawy.robo.models.RoboArtistVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboArtistTransformer;

@Service("roboArtistService")
public class RoboArtistServiceImpl extends AbstractPgrlServiceImpl<RoboArtistVO, RoboArtistDTO, Long> implements RoboArtistService{

	@Value("${robocon.ws.artist.url}")
	private String artistURL;
	
	@Autowired
	private RoboArtistDAO roboArtistDAO;
	
	@Autowired
	private RoboArtistTransformer roboArtistTransformer;
	
	public RoboArtistDTO createArtist(RoboArtistDTO roboArtistDTO){
		logger.debug("Robocon request for creatting album");
		try{
			roboArtistDTO = postObject(artistURL, roboArtistDTO);
			return roboArtistDTO;
		}catch(RoboResponseException ex){
			logger.error("Artist creation error occured.", ex);
			throw new RoboArtistException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Artist creation error occured.", ex);
			throw new RoboArtistException(ex.getMessage());
		}
	}
	
	@Override
	public RoboArtistDTO updateArtist(RoboArtistDTO roboArtistDTO) {
		logger.debug("Robocon request for updatting artist");
		try{
			roboArtistDTO = updateR(artistURL, roboArtistDTO);
			return roboArtistDTO;
		}catch(RoboResponseException ex){
			logger.error("Artist update error occured.", ex);
			throw new RoboArtistException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Artist update error occured.", ex);
			throw new RoboArtistException(ex.getMessage());
		}
	}
	
	@Override
	public RoboArtistDTO uploadPhoto(MultipartFile file, Long artistId, String name) {
		logger.debug("Robocon request for uploading artist photo");
		try{
			RoboArtistDTO artist = postFile(String.format(fileUploadURL, artistId, RoboMimeTypeEnum.ARTIST_IMAGE.getIntValue()), file, name);
			return artist;
		}catch(RoboResponseException ex){
			logger.error("Artist update error occured.", ex);
			throw new RoboArtistException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Artist update error occured.", ex);
			throw new RoboArtistException(ex.getMessage());
		}
	}
	
	public RoboArtistDTO getArtist(Long artistId){
		logger.debug("Robocon request for getting artist");
		try{
			RoboArtistDTO artist = getById(artistURL, artistId);
			return artist;
		}catch(RoboResponseException ex){
			logger.error("Artist retrieve error occured.", ex);
			throw new RoboArtistException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Artist retrieve error occured.", ex);
			throw new RoboArtistException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboArtistDTO> getArtists(Integer page, Integer limit){
		logger.debug("Robocon request for getting artist list");
		List<RoboArtistDTO> artists = getList(String.format(pagingURL, artistURL, page, limit));
		return artists;
	}
	
	@Override
	public Map<String, String> getSocials(){
		logger.debug("Robocon request for getting artist socials list");
		Map<String, String> socials = getMap(String.format(componentURL, artistURL, "socials"));
		return socials;
	}
	
	@Override
	public void deleteArtist(Long artistId) {
		logger.debug("Robocon request for deleting artist");
		try{
			deleteById(artistURL, artistId);
		}catch(RoboResponseException ex){
			logger.error("Artist delete error occured.", ex);
			throw new RoboArtistException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Artist delete error occured.", ex);
			throw new RoboArtistException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboArtistVO, RoboArtistDTO> getTransformer() {
		return roboArtistTransformer;
	}

	@Override
	protected DAO<RoboArtistVO, Long> getDAO() {
		return roboArtistDAO;
	}

}
