package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboEventVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboEventDAO")
public class RoboEventDAOImpl extends AbstractWebserviceDAO<RoboEventVO, Long> implements RoboEventDAO{

	public RoboEventDAOImpl(){
		super(RoboEventVO.class, new ParameterizedTypeReference<List<RoboEventVO>>() {});
	}
}
