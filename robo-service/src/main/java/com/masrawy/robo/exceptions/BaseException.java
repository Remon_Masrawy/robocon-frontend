package com.masrawy.robo.exceptions;

import java.util.List;

public interface BaseException {

	public List<String> getMessageparameters();

	public String getMessage();
	
	public int getErrorCode();
}
