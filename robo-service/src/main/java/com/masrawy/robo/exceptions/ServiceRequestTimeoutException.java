package com.masrawy.robo.exceptions;

import static com.masrawy.robo.exceptions.ErrorConstants.ROBO_REQUEST_TIMEOUT;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.REQUEST_TIMEOUT)
public class ServiceRequestTimeoutException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Web Service Request Timeout exception occured.";
	
	public ServiceRequestTimeoutException(){
		super(ERROR_MSG);
	}
	
	public ServiceRequestTimeoutException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBO_REQUEST_TIMEOUT;
	}

}
