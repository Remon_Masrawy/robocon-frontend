package com.masrawy.robo.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dto.RoboSongDTO;
import com.masrawy.robo.models.RoboSongVO;

public interface RoboSongService extends PgrlService<RoboSongVO, RoboSongDTO, Long>{

	public RoboSongDTO createSong(RoboSongDTO roboSongDTO);
	public RoboSongDTO updateSong(RoboSongDTO roboSongDTO);
	public RoboSongDTO uploadFile(MultipartFile file, Long songId, String type, String name);
	public void getContent(Long contentId);
	public void getLyrics(Long lyricsId);
	public RoboSongDTO getSong(Long songId);
	public List<RoboSongDTO> getSongs(Integer page, Integer limit);
	public List<RoboSongDTO> searchSongs(String keyword, Integer page, Integer limit);
	public void deleteSong(Long songId);
}
