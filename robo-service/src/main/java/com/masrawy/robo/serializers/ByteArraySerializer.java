package com.masrawy.robo.serializers;

import java.io.IOException;

import org.springframework.core.io.ByteArrayResource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;

@JacksonStdImpl
public class ByteArraySerializer extends StdScalarSerializer<ByteArrayResource>{

	private static final long serialVersionUID = 1L;

	public ByteArraySerializer(){
		super(ByteArrayResource.class);
	}

	@Override
	public void serialize(ByteArrayResource value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeBinary(value.getByteArray());
	}
}
