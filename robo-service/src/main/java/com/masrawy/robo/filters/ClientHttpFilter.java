package com.masrawy.robo.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import com.masrawy.robo.holders.RestResponseHolder;

public class ClientHttpFilter extends OncePerRequestFilter{

	private static final String X_DATA_COUNT = "x-data-count";
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		filterChain.doFilter(request, response);

		if(RestResponseHolder.isCollectionCountNotBlank()){
			response.setHeader(X_DATA_COUNT, RestResponseHolder.getCollectionCount());
		}
	}
	
}
