package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboArtistDTO;
import com.masrawy.robo.models.RoboArtistVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboArtistTransformer")
public class RoboArtistTransformerImpl extends AbstractDTOTransformer<RoboArtistVO, RoboArtistDTO> implements RoboArtistTransformer{

	public RoboArtistTransformerImpl(){
		super(RoboArtistVO.class, RoboArtistDTO.class);
	}
}
