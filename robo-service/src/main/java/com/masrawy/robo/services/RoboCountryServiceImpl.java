package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboCountryDAO;
import com.masrawy.robo.dto.RoboCountryDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboCountryException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.models.RoboCountryVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboCountryTransformer;

@Service("roboCountryService")
public class RoboCountryServiceImpl extends AbstractPgrlServiceImpl<RoboCountryVO, RoboCountryDTO, Long> implements RoboCountryService{

	@Autowired
	private RoboCountryDAO roboCountryDAO;
	@Autowired
	private RoboCountryTransformer roboCountryTransformer;
	
	@Value("${robocon.ws.country.url}")
	private String countryURL;
	
	@Override
	public RoboCountryDTO createCountry(RoboCountryDTO roboCountryDTO){
		logger.debug("Robocon request for creating country");
		try{
			RoboCountryDTO country = postObject(countryURL, roboCountryDTO);
			return country;
		}catch(RoboResponseException ex){
			logger.error("Country creation error occured.", ex);
			throw new RoboCountryException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Country creation error occured.", ex);
			throw new RoboCountryException(ex.getMessage());
		}
	}
	
	@Override
	public RoboCountryDTO updateCountry(RoboCountryDTO roboCountryDTO){
		logger.debug("Robocon request for updating country");
		try{
			RoboCountryDTO country = updateR(countryURL, roboCountryDTO);
			return country;
		}catch(RoboResponseException ex){
			logger.error("Country update error occured.", ex);
			throw new RoboCountryException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Country update error occured.", ex);
			throw new RoboCountryException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboCountryDTO> getCountryList(Integer page, Integer limit){
		logger.debug("Robocon request for getting countrys list");
		List<RoboCountryDTO> countries = getList(String.format(pagingURL, countryURL, page, limit));
		return countries;
	}
	
	@Override
	protected DTOTransformer<RoboCountryVO, RoboCountryDTO> getTransformer() {
		return roboCountryTransformer;
	}

	@Override
	protected DAO<RoboCountryVO, Long> getDAO() {
		return roboCountryDAO;
	}

}
