<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="preview-widget" data-id="${provider.id}">
	<c:if test="${not empty provider && not empty provider.meta}">
		<div class="preview-widget-title">
			<span title="Type"><spring:message code="robo-pages.provider-keyword"/></span>
			<span title="ID">${provider.id}</span>
			<span title="${provider.meta[0].name}">${provider.meta[0].name}</span>
		</div>
	</c:if>
	<div class="preview-widget-buttons">
		<div class="new-button"><i class="fa fas fa-plus fa-fw"></i><spring:message code="robo-pages.new-meta-data-keyword"/></div>
	</div>
	<div class="preview-widget-container">
		<div class="preview-widget-content">
		<div class="templatemo-content-container">
				<div class="templatemo-content-widget no-padding">
	            <div class="panel panel-default table-responsive">
	              <table class="table table-striped table-bordered templatemo-user-table">
	                <thead>
	                  <tr>
	                    <td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.name-keyword"/> <span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.description-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.lang-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><spring:message code="robo-pages.delete-keyword"/></td>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:if test="${not empty provider && not empty provider.meta}">
							<c:forEach items="${provider.meta}" var="meta">
								<tr data-id="${meta.id}">
									<td contenteditable="true">${meta.name}</td>
									<td contenteditable="true">${meta.description}</td>
									<td>
										<select>
											<c:if test="${not empty languages}">
												<c:forEach items="${languages}" var="language">
													<option value="${language.code}" ${language.code == meta.lang? 'selected' : ''}>${language.code}</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
									<td><a href="javascript:" class="templatemo-link templatemo-meta-delete-link">Delete</a></td>
								</tr>
							</c:forEach>
						</c:if>
	                </tbody>
	              </table>    
	            </div>                          
	          </div> 
          </div>
        </div>
	</div>
	<div class="preview-widget-resposive-buttons">
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="clear"></div>
</div>
	
<script type="text/javascript" src='<c:url value="/js/provider.js"/>'></script>