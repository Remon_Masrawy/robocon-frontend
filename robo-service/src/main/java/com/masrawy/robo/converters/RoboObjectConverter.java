package com.masrawy.robo.converters;

import java.util.List;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;

public class RoboObjectConverter extends DozerConverter<List<Object>, List<Object>> implements CustomConverter{

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RoboObjectConverter(){
		super((Class<List<Object>>) (Class) List.class, (Class<List<Object>>) (Class) List.class);
	}

	@Override
	public List<Object> convertFrom(List<Object> arg0, List<Object> arg1) {
		return null;
	}

	@Override
	public List<Object> convertTo(List<Object> arg0, List<Object> arg1) {
		return null;
	}
}
