package com.masrawy.robo.management;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class StartupServiceImpl implements StartupService{
	
	private static final Logger logger = LoggerFactory.getLogger(StartupServiceImpl.class);
	
	private static final String PRODUCTION_PROFILE = "production";
	private static final String DEVELOPMENT_PROFILE = "development";
	
	@Autowired
	private Environment env;
	
	private String profile;
	
	@Value("${app.version}")
	private String version;

	@EventListener({ContextStartedEvent.class, ContextRefreshedEvent.class})
	protected void contextStartupEvent(){
		logger.info("Server version {} has been started at {} with {} profile", getVersion(), new Date(), getProfile());
	}
	
	@EventListener({ContextClosedEvent.class, ContextStoppedEvent.class})
	protected void contextStopEvent(){
		logger.info("Server has been closed at {}", new Date());
	}

	@Override
	public String getProfile() {
		if(profile == null){
			String[] activeProfiles = env.getActiveProfiles();
			if(activeProfiles.length > 0){
				for(String profile : activeProfiles){
					if(this.profile == null){
						this.profile = "";
					}else{
						this.profile.concat(", ");
					}
					this.profile.concat(profile);
				}
			}else{
				this.profile = DEVELOPMENT_PROFILE;
			}
		}
		return profile;
	}
	
	@Override
	public Boolean isProduction(){
		if(profile == null){
			return false;
		}
		return profile.contains(PRODUCTION_PROFILE);
	}
	
	@Override
	public String getVersion(){
		return this.version;			
	}

}
