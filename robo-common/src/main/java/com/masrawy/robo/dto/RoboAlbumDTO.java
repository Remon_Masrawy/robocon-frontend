package com.masrawy.robo.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class RoboAlbumDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@Valid
	@NotEmpty
	private List<RoboMetaDTO> meta;
	private List<RoboPhotoDTO> photos;
	@Valid
	private List<RoboTagDTO> tags;
	@NotNull
	private RoboProviderDTO provider;
	private Map<Long, String> artists;
	private Map<Long, String> songs;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaDTO> meta) {
		this.meta = meta;
	}
	public List<RoboPhotoDTO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboPhotoDTO> photos) {
		this.photos = photos;
	}
	public List<RoboTagDTO> getTags() {
		return tags;
	}
	public void setTags(List<RoboTagDTO> tags) {
		this.tags = tags;
	}
	public RoboProviderDTO getProvider() {
		return provider;
	}
	public void setProvider(RoboProviderDTO provider) {
		this.provider = provider;
	}
	public Map<Long, String> getArtists() {
		return artists;
	}
	public void setArtists(Map<Long, String> artists) {
		this.artists = artists;
	}
	public Map<Long, String> getSongs() {
		return songs;
	}
	public void setSongs(Map<Long, String> songs) {
		this.songs = songs;
	}
}
