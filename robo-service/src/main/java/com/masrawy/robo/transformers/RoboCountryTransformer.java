package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboCountryDTO;
import com.masrawy.robo.models.RoboCountryVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboCountryTransformer extends DTOTransformer<RoboCountryVO, RoboCountryDTO>{

}
