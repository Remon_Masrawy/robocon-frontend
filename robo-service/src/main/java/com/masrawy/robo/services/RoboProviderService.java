package com.masrawy.robo.services;

import java.util.List;


import com.masrawy.robo.dto.RoboProviderDTO;
import com.masrawy.robo.models.RoboProviderVO;

public interface RoboProviderService extends PgrlService<RoboProviderVO, RoboProviderDTO, Long>{

	public RoboProviderDTO createProvider(RoboProviderDTO roboProviderDTO);
	public RoboProviderDTO updateProvider(RoboProviderDTO roboProviderDTO);
	public RoboProviderDTO getProvider(Long providerId);
	public List<RoboProviderDTO> getProviderList(Integer page, Integer limit);
	public void deleteProvider(Long providerId);
}
