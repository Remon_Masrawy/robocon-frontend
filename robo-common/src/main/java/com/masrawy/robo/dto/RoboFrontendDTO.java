package com.masrawy.robo.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
	@Type(value = RoboSongFrontendDTO.class, name = "SONG"),
	@Type(value = RoboAlbumFrontendDTO.class, name = "ALBUM")
})
public abstract class RoboFrontendDTO extends AbstractDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	@Valid
	@NotEmpty
	private List<RoboMetaDTO> meta;
	@NotBlank
	private String type;
	private Map<Long, String> categories;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboMetaDTO> meta) {
		this.meta = meta;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Map<Long, String> getCategories() {
		return categories;
	}
	public void setCategories(Map<Long, String> categories) {
		this.categories = categories;
	}
	
}
