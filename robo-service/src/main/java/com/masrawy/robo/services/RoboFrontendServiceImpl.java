package com.masrawy.robo.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboFrontendDAO;
import com.masrawy.robo.dto.RoboFrontendDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboFrontendException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.models.RoboFrontendVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboFrontendTransformer;

@Service("roboFrontendService")
public class RoboFrontendServiceImpl extends AbstractPgrlServiceImpl<RoboFrontendVO, RoboFrontendDTO, Long> implements RoboFrontendService{

	@Autowired
	private RoboFrontendDAO roboFrontendDAO;
	@Autowired
	private RoboFrontendTransformer roboFrontendTransformer;
	
	@Value("${robocon.ws.frontend.url}")
	private String frontendURL;
	
	private final static String FRONTEND_TYPES_EXE = "/types";
	
	@Override
	public RoboFrontendDTO createFrontend(RoboFrontendDTO roboFrontendDTO){
		logger.debug("Robocon request for creating frontend");
		try{
			RoboFrontendDTO frontend = postObject(frontendURL, roboFrontendDTO);
			return frontend;
		}catch(RoboResponseException ex){
			logger.error("Frontend creation error occured.", ex);
			throw new RoboFrontendException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Frontend creation error occured.", ex);
			throw new RoboFrontendException(ex.getMessage());
		}
	}
	
	@Override
	public RoboFrontendDTO updateFrontend(RoboFrontendDTO roboFrontendDTO){
		logger.debug("Robocon request for updating frontend");
		try{
			RoboFrontendDTO frontend = updateR(frontendURL, roboFrontendDTO);
			return frontend;
		}catch(RoboResponseException ex){
			logger.error("Frontend update error occured.", ex);
			throw new RoboFrontendException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Frontend update error occured.", ex);
			throw new RoboFrontendException(ex.getMessage());
		}
	}
	
	@Override
	public RoboFrontendDTO getFrontend(Long id){
		logger.debug("Robocon request for getting frontend by id");
		try{
			RoboFrontendDTO frontend = getById(frontendURL, id);
			return frontend;
		}catch(Exception ex){
			throw new RoboFrontendException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboFrontendDTO> getFrontends(Integer page, Integer limit){
		logger.debug("Robocon request for getting frontends list");
		List<RoboFrontendDTO> frontends = getList(String.format(pagingURL, frontendURL, page, limit));
		return frontends;
	}
	
	@Override
	public Map<String, String> getFrontendTypes(){
		logger.debug("Robocon request for getting frontend types");
		Map<String, String> types = getMap(frontendURL + FRONTEND_TYPES_EXE);
		return types;
	}
	
	@Override
	public void deleteFrontend(Long id){
		logger.debug("Robocon request for deleting frontend by id");
		try{
		deleteById(frontendURL, id);
		}catch(RoboResponseException ex){
			logger.error("Frontend delete error occured.", ex);
			throw new RoboFrontendException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Frontend delete error occured.", ex);
			throw new RoboFrontendException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboFrontendVO, RoboFrontendDTO> getTransformer() {
		return roboFrontendTransformer;
	}

	@Override
	protected DAO<RoboFrontendVO, Long> getDAO() {
		return roboFrontendDAO;
	}

}
