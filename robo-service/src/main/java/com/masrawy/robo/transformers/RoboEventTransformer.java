package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboEventDTO;
import com.masrawy.robo.models.RoboEventVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboEventTransformer extends DTOTransformer<RoboEventVO, RoboEventDTO>{

}
