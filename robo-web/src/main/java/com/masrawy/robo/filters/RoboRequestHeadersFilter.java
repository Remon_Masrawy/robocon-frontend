package com.masrawy.robo.filters;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.filter.OncePerRequestFilter;

import com.masrawy.robo.exceptions.RoboHeaderException;

public class RoboRequestHeadersFilter extends OncePerRequestFilter{

	@Autowired
	private HttpHeaders headers;
	
	@Autowired
	private LinkedMultiValueMap<String, String> parameters;
	
	private Map<String, Boolean> controlFields;
	
	private Map<String, Boolean> controlParams;
	
	static final String ORIGIN = "Origin";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		try {
			for(String key : controlFields.keySet()){
				headers.remove(key);
			}
			parameters.clear();
			for(Entry<String,Boolean> entry : controlFields.entrySet()){
				String key = entry.getKey();
				String value = request.getHeader(key);
				if(value == null){
					value = request.getParameter(key);
				}
				if(value == null & entry.getValue()){
					logger.error("Error: header ({}) must exist.", key);
					throw new RoboHeaderException("Header " + key + " not exist exception");
				}
				if(value != null){
					headers.add(key, value);
				}
			}
			for(Entry<String,Boolean> entry : controlParams.entrySet()){
				String key = entry.getKey();
				String value = request.getHeader(key);
				if(value == null){
					value = request.getParameter(key);
				}
				if(value == null & entry.getValue()){
					logger.error("Error: header ({}) must exist.", key);
					throw new RoboHeaderException("Header " + key + " not exist exception");
				}
				if(value != null){
					parameters.add(key, URLEncoder.encode(value, "UTF-8"));
				}
			}
			filterChain.doFilter(request, response);
		} catch (Exception e) {
			throw new RoboHeaderException();
		} finally{
			for(String key : controlFields.keySet()){
				headers.remove(key);
			}
			parameters.clear();
		}
	}

	public void setControlFields(Map<String, Boolean> controlFields) {
		this.controlFields = controlFields;
	}

	public void setControlParams(Map<String, Boolean> controlParams) {
		this.controlParams = controlParams;
	}

}
