package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.LOGIN_PAGE;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.exceptions.AccessDeniedException;
import com.masrawy.robo.exceptions.BaseRuntimeException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.exceptions.ServiceNotFoundException;
import com.masrawy.robo.exceptions.ServiceRequestTimeoutException;
import com.masrawy.robo.exceptions.ServiceServerError;

@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler(ServiceServerError.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody BaseRuntimeException Exception(ServiceServerError e){
		return e;
	}
	
	@ExceptionHandler(ServiceRequestTimeoutException.class)
	@ResponseStatus(value = HttpStatus.REQUEST_TIMEOUT)
	public @ResponseBody BaseRuntimeException Exception(ServiceRequestTimeoutException e){
		return e;
	}
	
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public @ResponseBody BaseRuntimeException Exception(AccessDeniedException e){
		return e;
	}
	
	@ExceptionHandler(ServiceNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public @ResponseBody BaseRuntimeException Exception(ServiceNotFoundException e){
		return e;
	}
	
	@ExceptionHandler(BaseRuntimeException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody BaseRuntimeException Exception(BaseRuntimeException e){
		return e;
	}
	
	@ExceptionHandler(RoboResponseException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody RoboResponseException Exception(RoboResponseException e){
		return e;
	}
	
	@ExceptionHandler(ResourceAccessException.class)
	@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
	public @ResponseBody ResourceAccessException Exception(ResourceAccessException e){
		return e;
	}
	
	@ExceptionHandler(org.springframework.security.access.AccessDeniedException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ModelAndView Exception(org.springframework.security.access.AccessDeniedException e){
		ModelAndView model = new ModelAndView(LOGIN_PAGE);
		return model;
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody Exception Exception(Exception e){
		return e;
	}
}
