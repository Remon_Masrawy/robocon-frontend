package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboCountryVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboCountryDAO extends DAO<RoboCountryVO, Long>{

}
