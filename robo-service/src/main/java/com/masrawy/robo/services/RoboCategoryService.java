package com.masrawy.robo.services;

import java.util.List;

import com.masrawy.robo.dto.RoboCategoryDTO;
import com.masrawy.robo.models.RoboCategoryVO;

public interface RoboCategoryService extends PgrlService<RoboCategoryVO, RoboCategoryDTO, Long>{

	public RoboCategoryDTO createCategory(RoboCategoryDTO roboCategoryDTO);
	public RoboCategoryDTO updateCategory(RoboCategoryDTO roboCategoryDTO);
	public RoboCategoryDTO getCategory(Long categoryId);
	public List<RoboCategoryDTO> getCategoryList(Integer page, Integer limit);
	public void deleteCategory(Long categoryId);
}
