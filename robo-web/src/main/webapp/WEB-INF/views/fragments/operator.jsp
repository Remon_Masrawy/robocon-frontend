<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input id="chooseMsg" type="hidden" value='<spring:message code="robo-pages.choose-keyword"/>'>


	<div class="new-btn"><a href="javascript:"><spring:message code="robo-pages.new-keyword"/></a></div>
	<div class="save-btn"><a href="javascript:"><spring:message code="robo-pages.save-keyword"/></a></div>
	<div class="templatemo-content-widget no-padding">
		<div class="panel panel-default table-responsive">
			<table class="table table-striped table-bordered templatemo-user-table operators-table">
				<thead>
					<tr >
						<td class="w-50"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.id-keyword"/> <span class="caret"></span></a></td>
						<td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.name-keyword"/> <span class="caret"></span></a></td>
						<td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.description-keyword"/> <span class="caret"></span></a></td>
						<td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.mcc-keyword"/> <span class="caret"></span></a></td>
						<td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.mnc-keyword"/> <span class="caret"></span></a></td>
						<td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.country-keyword"/> <span class="caret"></span></a></td>
						<td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.allowed-keyword"/> <span class="caret"></span></a></td>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	
<script type="text/javascript" src='<c:url value="/js/operator.js"/>'></script>