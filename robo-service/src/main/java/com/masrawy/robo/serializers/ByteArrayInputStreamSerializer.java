package com.masrawy.robo.serializers;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;

@JacksonStdImpl
public class ByteArrayInputStreamSerializer extends StdScalarSerializer<ByteArrayInputStream>{

	private static final long serialVersionUID = 1L;

	public ByteArrayInputStreamSerializer(){
		super(ByteArrayInputStream.class);
	}
	
	@Override
	public void serialize(ByteArrayInputStream value, JsonGenerator gen, SerializerProvider provider)
			throws IOException {
		 
		 byte[] bytes = new byte[value.available()];
		 int count = value.read(bytes);
		 gen.writeBinary(provider.getConfig().getBase64Variant(), bytes, 0, count);
	}
	
}
