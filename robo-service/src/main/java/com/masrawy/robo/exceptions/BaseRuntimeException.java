package com.masrawy.robo.exceptions;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRuntimeException extends RuntimeException implements BaseException{

	private static final long serialVersionUID = 1L;

	public List<String> messageParameters = new ArrayList<String>();

	public BaseRuntimeException(String message) {
		super(message);
	}

	public BaseRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	protected BaseRuntimeException addMessageParameter(String parameter) {
		messageParameters.add(parameter);
		return this;
	}

	/**
	 * @return the message parameters
	 */
	public List<String> getMessageparameters() {
		return messageParameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	@Override
	public String toString() {
		return "Exception:[" + getClass().getSimpleName() + "] is happened due to " + getMessage()
				+ "] Parameters : " + getMessageparameters();
	}
}
