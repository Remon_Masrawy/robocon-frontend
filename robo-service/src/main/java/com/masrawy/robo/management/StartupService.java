package com.masrawy.robo.management;

public interface StartupService {

	public String getProfile();
	
	public Boolean isProduction();
	
	public String getVersion();
}
