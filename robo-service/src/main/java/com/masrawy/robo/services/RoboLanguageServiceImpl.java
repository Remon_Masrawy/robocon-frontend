package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboLanguageDAO;
import com.masrawy.robo.dto.RoboLanguageDTO;
import com.masrawy.robo.models.RoboLanguageVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboLanguageTransformer;

@Service("roboLanguageService")
public class RoboLanguageServiceImpl extends AbstractPgrlServiceImpl<RoboLanguageVO, RoboLanguageDTO, String> implements RoboLanguageService{

	@Autowired
	private RoboLanguageDAO roboLanguageDAO;
	@Autowired
	private RoboLanguageTransformer roboLanguageTransformer;
	
	@Value("${robocon.ws.language.url}")
	private String languageURL;
	
	@Override
	public List<RoboLanguageDTO> getLanguageList(Integer pageNumber, Integer pageLimit){
		logger.debug("Robocon request for getting langauges list");
		List<RoboLanguageDTO> languages = getList(String.format(pagingURL, languageURL, pageNumber, pageLimit));
		return languages;
	}
	
	@Override
	protected DTOTransformer<RoboLanguageVO, RoboLanguageDTO> getTransformer() {
		return roboLanguageTransformer;
	}

	@Override
	protected DAO<RoboLanguageVO, String> getDAO() {
		return roboLanguageDAO;
	}

}
