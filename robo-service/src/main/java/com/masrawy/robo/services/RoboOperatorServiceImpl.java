package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboOperatorDAO;
import com.masrawy.robo.dto.RoboOperatorDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboOperatorException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.models.RoboOperatorVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboOperatorTransformer;

@Service("roboOperatorService")
public class RoboOperatorServiceImpl extends AbstractPgrlServiceImpl<RoboOperatorVO, RoboOperatorDTO, Long> implements RoboOperatorService{

	@Autowired
	private RoboOperatorDAO roboOperatorDAO;
	@Autowired
	private RoboOperatorTransformer roboOperatorTransformer;
	
	@Value("${robocon.ws.operator.url}")
	private String operatorURL;
	
	@Override
	public RoboOperatorDTO createOperator(RoboOperatorDTO roboOperatorDTO){
		logger.debug("Robocon request for creating operator");
		try{
			RoboOperatorDTO operator = postObject(operatorURL, roboOperatorDTO);
			return operator;
		}catch(RoboResponseException ex){
			logger.error("Operator creation error occured.", ex);
			throw new RoboOperatorException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Operator creation error occured.", ex);
			throw new RoboOperatorException(ex.getMessage());
		}
	}
	
	@Override
	public RoboOperatorDTO updateOperator(RoboOperatorDTO roboOperatorDTO){
		logger.debug("Robocon request for updating operator");
		try{
			RoboOperatorDTO operator = updateR(operatorURL, roboOperatorDTO);
			return operator;
		}catch(RoboResponseException ex){
			logger.error("Operator update error occured.", ex);
			throw new RoboOperatorException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Operator update error occured.", ex);
			throw new RoboOperatorException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboOperatorDTO> getOperatorList(Integer page, Integer limit){
		logger.debug("Robocon request for getting operators list");
		List<RoboOperatorDTO> operators = getList(String.format(pagingURL, operatorURL, page, limit));
		return operators;
	}
	
	@Override
	protected DTOTransformer<RoboOperatorVO, RoboOperatorDTO> getTransformer() {
		return roboOperatorTransformer;
	}

	@Override
	protected DAO<RoboOperatorVO, Long> getDAO() {
		return roboOperatorDAO;
	}

}
