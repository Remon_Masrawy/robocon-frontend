package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboProviderDAO;
import com.masrawy.robo.dto.RoboProviderDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboProviderException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.models.RoboProviderVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboProviderTransformer;

@Service("roboProviderService")
public class RoboProviderServiceImpl extends AbstractPgrlServiceImpl<RoboProviderVO, RoboProviderDTO, Long> implements RoboProviderService{

	@Autowired
	private RoboProviderDAO roboProviderDAO;
	
	@Autowired
	private RoboProviderTransformer roboProviderTransformer;
	
	@Value("${robocon.ws.provider.url}")
	private String providerURL;
	
	@Override
	public RoboProviderDTO createProvider(RoboProviderDTO roboProviderDTO){
		logger.debug("Robocon request for creating provider");
		try{
			roboProviderDTO = postObject(providerURL, roboProviderDTO);
			return roboProviderDTO;
		}catch(RoboResponseException ex){
			logger.error("Provider creation error occured.", ex);
			throw new RoboProviderException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Provider creation error occured.", ex);
			throw new RoboProviderException(ex.getMessage());
		}
	}
	
	@Override
	public RoboProviderDTO updateProvider(RoboProviderDTO roboProviderDTO){
		logger.debug("Robocon request for updating provider");
		try{
			roboProviderDTO = updateR(providerURL, roboProviderDTO);
			return roboProviderDTO;
		}catch(RoboResponseException ex){
			logger.error("Provider creation error occured.", ex);
			throw new RoboProviderException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Provider creation error occured.", ex);
			throw new RoboProviderException(ex.getMessage());
		}
	}
	
	@Override
	public RoboProviderDTO getProvider(Long providerId){
		logger.debug("Robocon request for getting provider");
		RoboProviderDTO provider = getById(providerURL, providerId);
		return provider;
	}
	
	@Override
	public List<RoboProviderDTO> getProviderList(Integer page, Integer limit){
		logger.debug("Robocon request for getting providers list");
		List<RoboProviderDTO> providers = getList(String.format(pagingURL, providerURL, page, limit));
		return providers;
	}
	
	@Override
	public void deleteProvider(Long providerId){
		logger.debug("Robocon request for deleting provider");
		try{
			deleteById(providerURL, providerId);
		}catch(RoboResponseException ex){
			logger.error("Provider creation error occured.", ex);
			throw new RoboProviderException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Provider creation error occured.", ex);
			throw new RoboProviderException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboProviderVO, RoboProviderDTO> getTransformer() {
		return roboProviderTransformer;
	}

	@Override
	protected DAO<RoboProviderVO, Long> getDAO() {
		return roboProviderDAO;
	}

}
