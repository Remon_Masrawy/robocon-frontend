package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboCategoryDTO;
import com.masrawy.robo.models.RoboCategoryVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboCategoryTransformer")
public class RoboCategoryTransformerImpl extends AbstractDTOTransformer<RoboCategoryVO, RoboCategoryDTO> implements RoboCategoryTransformer{

	public RoboCategoryTransformerImpl(){
		super(RoboCategoryVO.class, RoboCategoryDTO.class);
	}
}
