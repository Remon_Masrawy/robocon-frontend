package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.SONGS_PAGE;
import static com.masrawy.robo.constants.RoboPages.SONG_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboSongDTO;
import com.masrawy.robo.services.RoboLanguageService;
import com.masrawy.robo.services.RoboSongService;

@Controller
@RequestMapping(value = "/web/song")
public class SongController {

	@Autowired
	private RoboSongService roboSongService;
	
	@Autowired
	private RoboLanguageService roboLanguageService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView songsPage(){
		ModelAndView model = new ModelAndView(SONGS_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{songId}", method = RequestMethod.GET)
	public ModelAndView songPage(@PathVariable("songId") String songId){
		ModelAndView model = new ModelAndView(SONG_PAGE);
		try{
			Long id = Long.valueOf(songId);
			if(id != 0){
				model.addObject("song", roboSongService.getSong(id));
			}
			model.addObject("languages", roboLanguageService.getLanguageList(0, 0));
		}catch(Exception e){
			model = new ModelAndView("redirect:/web/song");
		}
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboSongDTO songCreate(@RequestBody RoboSongDTO roboSongDTO){
		return roboSongService.createSong(roboSongDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboSongDTO songUpdate(@RequestBody RoboSongDTO roboSongDTO){
		return roboSongService.updateSong(roboSongDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{songId}", method = RequestMethod.POST)
	public @ResponseBody RoboSongDTO uploadFile(@RequestParam(value = "file", required = true) MultipartFile file,@RequestParam(value = "type", required = true) String type, @RequestParam(value = "name", required = true) String name, @NotNull @PathVariable("songId") Long songId){
		return roboSongService.uploadFile(file, songId, type, name);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/content/{contentId}/{filename}", method = RequestMethod.GET)
	public @ResponseBody void downloadContent(@PathVariable("contentId") Long contentId){
		roboSongService.getContent(contentId);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/lyrics/{lyricsId}", method = RequestMethod.GET)
	public @ResponseBody void printLyrics(@PathVariable("lyricsId") Long lyricsId){
		roboSongService.getLyrics(lyricsId);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboSongDTO> artists(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboSongService.getSongs(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{songId}", method = RequestMethod.DELETE)
	public @ResponseBody void albumDelete(@PathVariable("songId") Long songId){
		roboSongService.deleteSong(songId);
	}
	
}
