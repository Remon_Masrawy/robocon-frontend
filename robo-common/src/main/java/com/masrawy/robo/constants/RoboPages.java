package com.masrawy.robo.constants;

public class RoboPages {

	public static final String LOGIN_PAGE 			= "login";
	
	public static final String DASHBOARD_PAGE 		= "fragments/dashboard";
	
	public static final String SONGS_PAGE			= "fragments/songs";
	
	public static final String SONG_PAGE			= "fragments/song";
	
	public static final String ARTISTS_PAGE			= "fragments/artists";
	
	public static final String ARTIST_PAGE			= "fragments/artist";
	
	public static final String EVENTS_PAGE			= "fragments/events";
	
	public static final String EVENT_PAGE			= "fragments/event";
	
	public static final String ALBUMS_PAGE			= "fragments/albums";
	
	public static final String ALBUM_PAGE			= "fragments/album";
	
	public static final String PROVIDER_PAGE 		= "fragments/provider";

	public static final String PROVIDERS_PAGE 		= "fragments/providers";
	
	public static final String GENRE_PAGE 			= "fragments/genre";
	
	public static final String GENRES_PAGE 			= "fragments/genres";
	
	public static final String FRONTEND_PAGE 		= "fragments/frontend";
	
	public static final String FRONTENDS_PAGE 		= "fragments/frontends";
	
	public static final String STOREFRONT_PAGE 		= "fragments/storefront";
	
	public static final String COUNTRY_PAGE 		= "fragments/country";
	
	public static final String OPERATOR_PAGE 		= "fragments/operator";
	
	public static final String CATEGORY_PAGE 		= "fragments/category";
	
	public static final String CATEGORIES_PAGE 		= "fragments/categories";
	
	public static final String CONFIGURATION_PAGE 	= "fragments/configuration";
	
	public static final String CONFIRMATION_PAGE 	= "fragments/confirmation";
	
	public static final String USER_PAGE 			= "fragments/user";
	
	public static final String ERROR_PAGE 			= "error";
}
