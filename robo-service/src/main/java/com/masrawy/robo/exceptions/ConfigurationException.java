package com.masrawy.robo.exceptions;

import static com.masrawy.robo.exceptions.ErrorConstants.ROBO_CONFIGURATION_ERROR;

public class ConfigurationException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Configuration exception occured.";
	
	public ConfigurationException(){
		super(ERROR_MSG);
	}
	
	public ConfigurationException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBO_CONFIGURATION_ERROR;
	}
	
}
