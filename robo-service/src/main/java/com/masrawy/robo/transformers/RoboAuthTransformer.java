package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboAuthDTO;
import com.masrawy.robo.models.RoboAuthVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboAuthTransformer extends DTOTransformer<RoboAuthVO, RoboAuthDTO>{

}
