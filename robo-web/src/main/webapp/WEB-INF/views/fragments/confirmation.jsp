<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<!-- Template by Quackit.com -->
<!-- Images by various sources under the Creative Commons CC0 license and/or the Creative Commons Zero license. 
Although you can use them, for a more unique website, replace these images with your own. -->
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Confirmation</title>

	<c:url var="bootstrap" value="/css/bootstrap.min.css"/>
	<c:url var="custom" value="/css/custom.css"/>
	<c:url var="jquery" value="/js/jquery-1.11.3.min.js"/>
	<c:url var="bootstrapjs" value="/js/bootstrap.min.js"/>
	<c:url var="jqueryeasing" value="/js/jquery.easing.min.js"/>
	<c:url var="customjs" value="/js/custom.js"/>

    <!-- Bootstrap Core CSS -->
    <link href="${bootstrap}" rel="stylesheet">

    <!-- Custom CSS: You can use this stylesheet to override any Bootstrap styles and/or apply your own styles -->
    <link href="${custom}" rel="stylesheet">

    <!-- Custom Fonts from Google -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
</head>

<body>

    <!-- Navigation -->
    <nav id="siteNav" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Logo and responsive toggle -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                	<span class="glyphicon glyphicon-fire"></span> 
                	ROBOCON
                </a>
            </div>
            <!-- Navbar links -->
        </div><!-- /.container -->
    </nav>

	<!-- Header -->
    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>Thank you</h1>
                <p>You have been successfully signed up, waiting for confirmation from administrator, A confirmation message will sent to you on email address : ${email}</p>
            </div>
        </div>
    </header>
    
	<!-- Footer -->
    <footer class="page-footer">
    
    	<!-- Contact Us -->
        <div class="contact">
        	<div class="container">
				<h2 class="section-heading">Contact Us</h2>
				<p><span class="glyphicon glyphicon-earphone"></span><br> +1(23) 456 7890</p>
				<p><span class="glyphicon glyphicon-envelope"></span><br> info@example.com</p>
        	</div>
        </div>
        	
        <!-- Copyright etc -->
        <div class="small-print">
        	<div class="container">
        		<p>Copyright &copy; Example.com 2015</p>
        	</div>
        </div>
        
    </footer>

    <!-- jQuery -->
    <script src="${jquery}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="${bootstrapjs}"></script>

    <!-- Plugin JavaScript -->
    <script src="${jqueryeasing}"></script>
    
    <!-- Custom Javascript -->
    <script src="${customjs}"></script>

</body>

</html>
