package com.masrawy.robo.models;

import com.masrawy.robo.ws.AbstractVO;

public class RoboConfigurationVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private String name;
	private String type;
	private String value;
	private String description;
	private Boolean editabled;
	private Boolean viewed;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getEditabled() {
		return editabled;
	}
	public void setEditabled(Boolean editabled) {
		this.editabled = editabled;
	}
	public Boolean getViewed() {
		return viewed;
	}
	public void setViewed(Boolean viewed) {
		this.viewed = viewed;
	}
	
}
