package com.masrawy.robo.dto;

import java.util.List;

public class RoboAlbumFrontendDTO extends RoboFrontendDTO{

	private static final long serialVersionUID = 1L;

	private List<RoboAlbumDTO> albums;

	public List<RoboAlbumDTO> getAlbums() {
		return albums;
	}

	public void setAlbums(List<RoboAlbumDTO> albums) {
		this.albums = albums;
	}
}
