package com.masrawy.robo.services;

import java.util.List;

import com.masrawy.robo.dto.RoboLanguageDTO;
import com.masrawy.robo.models.RoboLanguageVO;

public interface RoboLanguageService extends PgrlService<RoboLanguageVO, RoboLanguageDTO, String>{

	public List<RoboLanguageDTO> getLanguageList(Integer pageNumber, Integer pageLimit);
}
