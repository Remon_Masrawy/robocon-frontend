package com.masrawy.robo.models;

import com.masrawy.robo.ws.AbstractVO;

public class RoboSocialVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String social;
	private String link;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSocial() {
		return social;
	}
	public void setSocial(String social) {
		this.social = social;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
}
