package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboArtistVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboArtistDAO extends DAO<RoboArtistVO, Long>{

}
