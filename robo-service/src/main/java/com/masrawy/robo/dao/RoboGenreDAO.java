package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboGenreVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboGenreDAO extends DAO<RoboGenreVO, Long>{

}
