package com.masrawy.robo.dozer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.annotation.PostConstruct;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import com.masrawy.robo.exceptions.ConfigurationException;

@Component("dozerMapper")
public class DozerMapperImpl extends DozerBeanMapper implements DozerMapper{

	private final static Logger logger = LoggerFactory.getLogger(DozerMapperImpl.class);
	
	private static final String DOZER_MAPPING_FOLDER = "dozer-mapping";
	
	@PostConstruct
	public void init(){
		initializeMappingFile();
	}
	
	private void initializeMappingFile() {
		try {
			Enumeration<URL> resources = Thread.currentThread().getContextClassLoader()
					.getResources(DOZER_MAPPING_FOLDER);
			if (!resources.hasMoreElements()) {
				String msg = DOZER_MAPPING_FOLDER + " is not found";
				logger.warn(msg);
				return;
				/*throw new ConfigurationException(msg);*/
			}
			while (resources.hasMoreElements()) {
				URL url = (URL) resources.nextElement();
				String protocol = url.getProtocol();
				if (protocol.equalsIgnoreCase("jar")) {
					addMappingFileFromJar(url);
				} else {
					Resource resource = new UrlResource(url);
					File mappingFolder = resource.getFile();
					addMappingFileFromFileSystem(mappingFolder);
				}

			}
		} catch (IOException ex) {
			String msg = "IO Exception is couured while reading dozzer mapping files due to "
					+ ex.getMessage();
			logger.error(msg, ex);
			throw new ConfigurationException(msg);
		}

	}

	private void addMappingFileFromFileSystem(File directory) throws IOException {
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				addMappingFileFromFileSystem(file);
			} else if (file.getPath().endsWith(".xml")) {
				FileInputStream inputStream = new FileInputStream(file);
				addMapping(inputStream);
				inputStream.close();
			}
		}
	}

	private void addMappingFileFromJar(URL url) throws IOException {

		java.net.JarURLConnection conn = (java.net.JarURLConnection) url.openConnection();
		JarFile jarFile = conn.getJarFile();
		Enumeration<JarEntry> jarEntries = jarFile.entries();
		while (jarEntries.hasMoreElements()) {
			JarEntry entry = jarEntries.nextElement();
			if (entry == null) {
				break;
			}
			if (entry.getName().endsWith(".xml") && entry.getName().contains(DOZER_MAPPING_FOLDER)) {
				InputStream inputStream = jarFile.getInputStream(entry);
				addMapping(inputStream);
				inputStream.close();
			}

		}
		jarFile.close();

	}
}
