package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboAlbumVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboAlbumDAO extends DAO<RoboAlbumVO, Long>{

}
