package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboLanguageDTO;
import com.masrawy.robo.models.RoboLanguageVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboLanguageTransformer extends DTOTransformer<RoboLanguageVO, RoboLanguageDTO>{

}
