package com.masrawy.robo.services;

import com.masrawy.robo.dto.RoboAuthDTO;

public interface RoboRegistrationService {

	public RoboAuthDTO retreiveUser(String username, String password);
	
	public RoboAuthDTO signUpUser(RoboAuthDTO roboAuthDTO);
	
	public void updateUserDetails(RoboAuthDTO roboAuthDTO);
	
}
