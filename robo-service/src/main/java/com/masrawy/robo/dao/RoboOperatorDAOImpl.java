package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboOperatorVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboOperatorDAO")
public class RoboOperatorDAOImpl extends AbstractWebserviceDAO<RoboOperatorVO, Long> implements RoboOperatorDAO{

	public RoboOperatorDAOImpl(){
		super(RoboOperatorVO.class, new ParameterizedTypeReference<List<RoboOperatorVO>>() {});
	}
}
