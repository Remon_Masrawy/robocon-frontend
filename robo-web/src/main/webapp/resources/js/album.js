
$(document).ready(function(){
	$container 			= $(".templatemo-content-container");
	$table 				= $(".templatemo-content-container .templatemo-content-widget .table.data-table");
	$preview 			= $(".templatemo-content-container .preview-widget");
	$confirmation 		= $(".confirmation");
	
	$table.on("click", ".templatemo-link.templatemo-del-link", function(){
		var id = $(this).closest("tr").attr("data-id");
		deleteAlbum(id);
	});
	
	$container.on("click", ".new-btn", function(){
		window.location.href = $album+"/0";
	});
	
	$preview.find(".preview-widget-buttons>.new-button").click(function(){
		addNewMetaRow({});
	});
	
	$preview.find(".preview-widget-resposive-buttons>div.save-button,.preview-widget-buttons>div.save-button").click(function(){
		saveAlbum();
	});
	
	$preview.delegate("a.templatemo-link.templatemo-meta-delete-link", "click", function(){
		$(this).closest("tr").remove();
	})
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .songs-responsive .assign", function(){
		$(this).removeClass("assign").addClass("unassign");
	});
	$preview.on("click", ".preview-widget-container>.preview-widget-content .songs-responsive .unassign", function(){
		$(this).removeClass("unassign").addClass("assign");
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive div.add-file-button", function(){
		$(this).closest("fieldset").find("input[type='file']").click();
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo input[type='checkbox'].main-photo-checkbox", function(){
		var that = this;
		if($(this).is(":checked")){
			$preview.find(".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo input[type='checkbox'].main-photo-checkbox:checked").not($(that)).prop('checked', false);
		}
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .provider .render-list .selected-list .item>span[title='value']", function(){
		if($(this).parent().attr("data-id")){
			window.location.href = $provider + "/" + $(this).parent().attr("data-id");
		}
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags", function(){
		if($(this).find("input[type='text']").length){
			$(this).find("input").focus();
		}else{
			var textBox = addTextBox().attr("maxlength", 30).attr("minLength", 10).attr("pattern", "[A-Za-z1-9]{30}");
			$(this).append(textBox);
			textBox.focus();
		}
	});
	
	$preview.on("keypress", ".preview-widget-container>.preview-widget-content .tags-responsive .tags input[type='text']", function(event){
		if(event.which == 13) {
			if($(this).val()){
				var tagDiv = addDiv().addClass("tag");
				var textSpan = addSpan().attr("title", "tag").text($(this).val());
				var closeSpan = addSpan().addClass("close");
				tagDiv.append(textSpan);
				tagDiv.append(closeSpan);
				$(this).closest(".tags").children("input").before(tagDiv);
				$(this).val("");
			}
		}
	});
	
	$preview.on("change", ".preview-widget-container>.preview-widget-content .photos-responsive input[type='file']", function(event){
		if($(this).val().length != 0){
			var image = addImage().attr("src", URL.createObjectURL(event.target.files[0]));
			var closeSpan = addSpan().addClass("close");
			var photoDiv = addDiv().addClass("photo").attr("title", event.target.files[0].name);
			photoDiv.append(image);
			photoDiv.append(closeSpan);
			$(this).closest("fieldset").find("div.photos").append(photoDiv);
		}
	});
	
	$preview.on("hover", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo", function(e){
		if(e.type == "mouseenter") {
			if(!$(this).hasClass("transparent")){
				$(this).find(".close").css({"opacity": 1});
				$(this).find("input[type='checkbox'].main-photo-checkbox").show();
			}
		  }
		  else if (e.type == "mouseleave") {
			  if(!$(this).hasClass("transparent")){
				  $(this).find(".close").css({"opacity": 0});
				  $(this).find("input[type='checkbox'].main-photo-checkbox").hide();
			  }
		  }});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .photos-responsive .photos .photo .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".photo").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	
	$preview.on("click", ".preview-widget-container>.preview-widget-content .tags-responsive .tags .tag .close", function(e){
		var that = $(this);
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
			that.closest(".tag").remove()
			$confirmation.hide(300);
		});
		$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
			$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
			$confirmation.hide(300);
		});
		
		$confirmation.show(300);
		
	});
	
	$preview.on("click", ".preview-widget-content .group-responsive .render-list .selected-list .item-close", function(){
		var id = $(this).closest("span.item").attr("data-id");
		if($(this).closest(".render-list").parent().hasClass("album")){
			$(this).closest(".render-list").parent().find('input[name="album"]').show();
		}
		if($(this).closest(".render-list").parent().hasClass("provider")){
			$(this).closest(".render-list").parent().find('input[name="provider"]').show();
		}
		$(this).closest("span.item").remove();
	});
	
	$preview.find(".preview-widget-content .group-responsive>div").each(function(){
		if($(this).hasClass("album")){
			if($(this).find('.selected-list .item').length == 0){
				$(this).find('input[name="album"]').show();
	    	}
		}else if($(this).hasClass("provider")){
			if($(this).find('.selected-list .item').length == 0){
				$(this).find('input[name="provider"]').show();
	    	}
		}else{
			$(this).find('input').show();
		}
	});
	
	if($container.find(".data-table").hasClass("albums-table")){
		$enablePanel = true;
	}else{
		$enablePagination = false;
		$enablePanel = false;
	}
	
	$preview.find('.group-responsive .provider input[name="provider"]').autoComplete({
	    minChars: 1,
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        searchProviders(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	var found = false;
	    	if($preview.find('.group-responsive .provider .selected-list .item').length > 0){
	    		found = true;
	    	}
	    	if(!found){
	    		var close = addSpan().addClass("item-close");
				var value = addSpan().attr("title", "value").text(item.data('name'));
				var span = addSpan().addClass("item").attr("data-id", item.data('id'));
				span.append(value);
				span.append(close);
				$preview.find('.group-responsive .provider .selected-list').append(span);
	    	}
	    	$preview.find('.group-responsive .provider input[name="provider"]').val("");
	    	$preview.find('.group-responsive .provider input[name="provider"]').hide();
	    }
	});
	
});

$fill = function fillAlbum(){
	$.when(ajaxCall($album+"/"+$currentPage+"/"+$pageLimit, $HTTP_METHOD.GET, {}, headers)).then(function(res, status, xhr){
		$table.find("tbody").empty();
		$.each(res, function(index, album){
			var url = $album+"/"+album.id;
			var row = addTableRow(8).attr("data-id", album.id);
			row.children("td:first-child()").addClass("tc w-50").text(album.id);
			var keyword;
			$.each(album.meta, function(i, meta){
				keyword = meta.name;
			});
			row.children("td:nth-child(2)").append(addLink().attr("href", url).addClass("templatemo-keyword-link").text(keyword));
			$.each(album.artists, function(key, value){
				row.children("td:nth-child(3)").append(addDiv().append(addLink().addClass("templatemo-keyword-link").attr("href", "/robo-web/web/artist/"+key).attr("artist-id", key).text(value)));
			});
			row.children("td:nth-child(4)").addClass("tc w-100").text(Object.keys(album.songs).length);
			var image = addImage();
			if(album.photos && album.photos.length > 0){
				$.each(album.photos, function(index_, photo){
					if(photo.isMainPhoto){
						image.attr("src", photo.url.replace("{width}", 500).replace("{height}", 500));
					}
				});
				var src = image.attr("src");
				if(typeof src == typeof undefined || src == false || src == null){
					image.attr("src", album.photos[0].url.replace("{width}", 500).replace("{height}", 500));
				}
			}
			row.children("td:nth-child(5)").addClass("tc w-100").append(addLink().addClass("templatemo-image-link").append(image));
			row.children("td:nth-child(6)").addClass("tc w-100").append(addLink().attr("href", url).addClass("templatemo-edit-btn").text("Edit"));
			row.children("td:nth-child(7)").addClass("tc w-100").append(addLink().addClass("templatemo-link").text("Action"));
			row.children("td:last-child()").addClass("tc w-100").append(addLink().addClass("templatemo-link templatemo-del-link").text("Delete"));
			$table.find("tbody").append(row);
		});
		
		var total = xhr.getResponseHeader("x-data-count");
		
		if($.isNumeric(total)){
			$displayTotal.text(total);
			if($totalItems != total){
				$totalItems = total;
			}
			refreshPagination();
		}
		
	},
	function(xhr){
		handleError(xhr)
	});
}

function saveAlbum(){
	var valid = validate();
	if(!valid){
		return;
	}
	var album = {};
	album.id = parseInt($preview.attr("data-id"));
	var metaList = new Array();
	$preview.find(".preview-widget-content table>tbody").children("tr").each(function(){
		var meta = {};
		if($(this).attr("data-id")){
			meta.id = parseInt($(this).attr("data-id").trim());
		}
		meta.name = $(this).children(":nth-child(1)").text().trim();
		meta.description = $(this).children(":nth-child(2)").text().trim();
		meta.lang = $(this).children(":nth-child(3)").children("select").val().trim();
		metaList.push(meta);
	});
	var songs = new Map();
	$preview.find(".preview-widget-content .songs-responsive .songs").children(".song").each(function(){
		if($(this).has("div.assign")){
			songs.set(parseInt($(this).attr("data-id").trim()), $(this).find(".content").text());
		}
	});
	var photoList = new Array();
	$preview.find(".preview-widget-content .photos-responsive .photos").children(".photo").each(function(){
		var dataId = $(this).attr('data-id');
		if(typeof dataId !== typeof undefined && dataId !== false){
			var photo = {};
			photo.id = parseInt(dataId.trim());
			photo.isMainPhoto = $(this).find("input[type='checkbox'].main-photo-checkbox").is(":checked");
			photoList.push(photo);
		}
	});
	var tagList = new Array();
	$preview.find(".preview-widget-content .tags-responsive .tags").children("div.tag").each(function(){
		var tag = {};
		tag.id = $(this).attr("data-id");
		tag.tag = $(this).find("span[title='tag']").text();
		tagList.push(tag);
	});
	album.meta = metaList;
	album.photos = photoList;
	album.tags = tagList;
	var providerId = $preview.find(".provider-responsive .provider .selected-list .item").attr("data-id");
	if(providerId){
		var provider = {};
		provider.id = providerId;
		album.provider = provider;
	}
	var method;
	if($preview.attr("data-id")){
		method = $HTTP_METHOD.PUT;
	}else{
		method = $HTTP_METHOD.POST;
	}
	
	$.when(ajaxCall($album, method, album, headers)).then(function(data){
		$preview.attr("data-id", data.id);
		updatePopup(data);
		$preview.find(".preview-widget-content .photos-responsive .photos").children(".photo").each(function(index){
			$(this).delay(500*index);
			var photoDiv = $(this);
			var photo = $(this);
			var dataId = photo.attr('data-id');
			var type = "ALBUM_IMAGE";
			if(typeof dataId === typeof undefined || dataId === false){
				var albumId = parseInt(data.id);
				var image = $(this).children("img");
				var name = $(this).attr("title");
				var xhr = new XMLHttpRequest();
				xhr.open( "GET", image.attr("src"), true );
				xhr.responseType = 'arraybuffer';
				xhr.onload = function( e ) {
					var arrayBufferView = new Uint8Array( this.response );
					var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
					var loadSpan = addSpan().addClass("load");
					var errorSpan = addSpan().addClass("error");
					var checkbox = addCheckbox().attr("name", "main").attr("title", "Set as main photo").addClass("main-photo-checkbox");
					photoDiv.append(loadSpan);
					$.when(mutipartCall($album + "/" + albumId, $HTTP_METHOD.POST, blob, type, name)).then(function(data){
						loadSpan.remove();
						photoDiv.removeClass("transparent");
						photoDiv.append(checkbox);
						photo.attr("data-id", data.photos[data.photos.length-1].id);
						toastr.success("Photo Successfully Uploaded.", 'Success', {positionClass: 'toast-bottom-right'});
					},
					function(error){
						loadSpan.remove();
						photoDiv.removeClass("transparent");
						photoDiv.append(errorSpan);
						errors.push(error.responseJSON.message);
						toast(error.responseJSON.message);
					});
				};
				xhr.send();
			}
		});
		toastr.success("Album Successfully Saved.", 'Success', {positionClass: 'toast-bottom-right'});
	},
	function(xhr){
		handleError(xhr)
	});
}

function searchProviders(term, suggest){
	try{
		$.when(ajaxCall($provider+"/0/10?Keyword="+term, $HTTP_METHOD.GET, {}, headers)).then(function(data){
			var suggestions = [];
			$.each(data, function(index1, provider){
				var item = {};
				var data = {};
				item.id = provider.id;
				data.name = provider.meta[0].name;
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function deleteAlbum(albumId){
	
	$confirmation.find(".confirm-buttons").children(".confirm-cancel-button").click(function(){
		$confirmation.find(".confirm-buttons").children(".confirm-agree-button").off("click");
		$confirmation.hide(300);
	});
	
	$confirmation.find(".confirm-buttons").children(".confirm-agree-button").click(function(){
		confirmDeleteAlbum(albumId);
	});
	
	$confirmation.show(300);
}

function confirmDeleteAlbum(albumId){
	
	$.when(ajaxCall($album+"/"+albumId, $HTTP_METHOD.DELETE, {}, headers)).then(function(data){
		$confirmation.hide(300);
		$table.find("tr[data-id='"+albumId+"']").remove();
	},
	function(xhr){
		handleError(xhr)
	});
	
}

function refreshPagination(){
	if(!$pagination.parent().hasClass("pagination-hidden")){
		$totalPages = Math.ceil($totalItems / $itemsPerPage.val());
		if($totalPages == 1){
			$pagination.parent().addClass("pagination-hidden");
			return;
		}
		$pagination.children("li:not(:last)").remove();
		for(page=1; page<=$totalPages; page++){
			var $listItem = listItem().append(addLink().attr("data-item", page).text(page));
			$pagination.children("li:last-child()").before($listItem);
		}
		$pagination.find("li:nth-child("+$currentPage+")").addClass("active");
	}
	
}

function validate(){
	var valid = true;
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(2)").is(":empty")){
			$(this).children("td:nth-child(2)").addClass("error");
			valid = false;
		}
	});
	$preview.find(".preview-widget-content table>tbody>tr").each(function(){
		if($(this).children("td:nth-child(3)").is(":empty")){
			$(this).children("td:nth-child(3)").addClass("error");
		}
	});
	
	return valid;
}

function updatePopup(data){
	$preview.find(".preview-widget-content table>tbody").empty();
	data.meta.forEach(function(meta, index){
		addNewMetaRow(meta);
	});
	
	$preview.find(".tags-responsive").find("div.tags").empty();
	data.tags.forEach(function(tag, index){
		var tagDiv = addDiv().addClass("tag").attr("data-id", tag.id);
		var textSpan = addSpan().attr("title", "tag").text(tag.tag);
		var closeSpan = addSpan().addClass("close");
		tagDiv.append(textSpan);
		tagDiv.append(closeSpan);
		$preview.find(".tags-responsive").find("div.tags").append(tagDiv);
	});
	
	$preview.find(".songs-responsive").find("div.songs").empty();
	var counter = 1;
	$.each(data.songs, function(key, value){
		var songDiv = addDiv().addClass("song").attr("data-id", key);
		var indexSpan = addSpan().addClass("index").text(counter);
		var link = addLink().attr("href", $song+"/"+key).text(value);
		var contentSpan = addSpan().addClass("content").append(link);
		songDiv.append(indexSpan).append(contentSpan);
		$preview.find(".songs-responsive").find("div.songs").append(songDiv);
		counter++;
	});
}