$(document).ready(function(){
	$container 			= $(".templatemo-content-container");
	
	$limit = 10;			// Number of items per page
	$selectedPage 		= 1;		// Current Selected Page
	$selSongPage  		= 1;
	$selAlbumPage  		= 1;
	$selArtistPage  	= 1;
	$selEventPage  		= 1;
	$selFrontendPage  	= 1;
	$selCategoryPage  	= 1;
	$selProviderPage  	= 1;
	$selGenrePage  		= 1;
	$selOperatorPage	= 1;
	$selStorefrontPage	= 1;
	$selCountryPage		= 1;
	$maxVisibleItems = 5; 	// Pagination Max Visible pages
	
	$(".search").draggable();
	$(".search").on("input", "input", function(){
		if($(this).attr("name") != "name"){
			$('.search input[name="name"]').val("").removeAttr("data-id");
		}
		if($(this).attr("name")=="artist"){
			$('.search input[name="album"]').val("").removeAttr("data-id");
			$('.search input[name="provider"]').val("").removeAttr("data-id").removeAttr("disabled");
		}
		if($(this).attr("name")=="album"){
			$('.search input[name="provider"]').val("").removeAttr("data-id").removeAttr("disabled");
			$('.search input[name="genre"]').val("").removeAttr("data-id")
		}
		$(this).removeAttr("data-id");
		$(this).removeAttr("data-code");
		$(this).removeAttr("data-keyword");
		$(".search .search-buttons input[name='search']").show();
		$(".search .search-buttons input[name='add']").hide();
		$(".search .search-buttons input[name='go']").hide();
	});
	
	$(".search section input:not(input[name='name']):not(input[name='code'])").closest("div").hide();
	$(".search .search-buttons input[name='add']").hide();
	$(".search").on("change", "select[name='type']", function(){
		$('.search input[name="code"]').val("");
		$('.search input[name="name"]').val("").removeAttr("data-id");
		$(".search section input:not(input[name='name']):not(input[name='code'])").closest("div").hide();
		$(".search .search-buttons input[name='add']").hide();
		$(".search .search-buttons input[name='search']").show();
		if($(this).find("option:selected").val() == "song"){
			$('.search input[name="artist"]').closest("div").show();
			$('.search input[name="album"]').closest("div").show();
			$('.search input[name="provider"]').closest("div").show();
			$('.search input[name="genre"]').closest("div").show();
		}else if($(this).find("option:selected").val() == "album"){
			$('.search input[name="artist"]').closest("div").show();
		}else if($(this).find("option:selected").val() == "event"){
			$('.search input[name="artist"]').closest("div").show();
		}else if($(this).find("option:selected").val() == "frontend"){
			$('.search input[name="operator"]').closest("div").show();
			$('.search input[name="storefront"]').closest("div").show();
			$('.search input[name="country"]').closest("div").show();
		}
	});
	
	$("body").on("click", ".templatemo-search-form .input-group .fa-cog", function(){
		$(".search").show();
	});
	
	$(".search-result").on("click", ".tab .tablinks", function(){
		$(this).closest(".tab").find("button.tablinks").removeClass("active");
		$(this).addClass("active");
		$(".search-result").find(".result-body>div").hide();
		switch($(this).attr("id")){
			case 'songs':
				$(".search-result").find(".result-body .songs").show();
				break;
			case 'albums':
				$(".search-result").find(".result-body .albums").show();
				break;
			case 'artists':
				$(".search-result").find(".result-body .artists").show();
				break;
			case 'events':
				$(".search-result").find(".result-body .events").show();
				break;
			case 'frontends':
				$(".search-result").find(".result-body .frontends").show();
				break;
			case 'categories':
				$(".search-result").find(".result-body .categories").show();
				break;
			case 'providers':
				$(".search-result").find(".result-body .providers").show();
				break;
			case 'genres':
				$(".search-result").find(".result-body .genres").show();
				break;
			default:
		}
	});
	
	$("body").on("click", ".search .search-buttons .search-btn,.templatemo-search-form .input-group .fa-search", function(e){
		search(this);
	});
	
	$(".search-result").on("click", ".pagination-wrap ul.pagination li>a[aria-label='Next']", function(){
		var current = $(this).closest("ul.pagination").find("li.active>a").attr("data-item");
		var total = $(this).closest("fieldset").find("legend>span").attr("data-total");
		var pages = Math.ceil(total / $limit);
		var term = $(".search-result").find(".result-header span").text();
		if(current < pages){
			if($(this).closest("fieldset").parent().hasClass("songs")){
				$selSongPage = parseInt($selSongPage)+1;
				getSongs(term, fillSongs);
			}
			if($(this).closest("fieldset").parent().hasClass("albums")){
				$selAlbumPage = parseInt($selAlbumPage)+1;
				getAlbums(term, fillAlbums);
			}
			if($(this).closest("fieldset").parent().hasClass("artists")){
				$selArtistPage = parseInt($selArtistPage)+1;
				getArtists(term, fillAlbums);
			}
			if($(this).closest("fieldset").parent().hasClass("events")){
				$selEventPage = parseInt($selEventPage)+1;
				getEvents(term, fillAlbums);
			}
			if($(this).closest("fieldset").parent().hasClass("frontends")){
				$selFrontendPage = parseInt($selFrontendPage)+1;
				getFrontends(term, fillAlbums);
			}
			if($(this).closest("fieldset").parent().hasClass("categories")){
				$selCategoryPage = parseInt($selCategoryPage)+1;
				getCategories(term, fillAlbums);
			}
			if($(this).closest("fieldset").parent().hasClass("providers")){
				$selProviderPage = parseInt($selProviderPage)+1;
				getProviders(term, fillAlbums);
			}
			if($(this).closest("fieldset").parent().hasClass("genres")){
				$selGenrePage = parseInt($selGenrePage)+1;
				getGenres(term, fillAlbums);
			}
		}
	});
	
	$(".search-result").on("click", ".pagination-wrap ul.pagination li:not(:last-child):not(.active)>a", function(){
		var term = $(".search-result").find(".result-header span").text();
		if($(this).closest("fieldset").parent().hasClass("songs")){
			$selSongPage = parseInt($(this).attr("data-item"));
			getSongs(term, fillSongs);
		}
		if($(this).closest("fieldset").parent().hasClass("albums")){
			$selAlbumPage = parseInt($(this).attr("data-item"));
			getAlbums(term, fillAlbums);
		}
		if($(this).closest("fieldset").parent().hasClass("artists")){
			$selArtistPage = parseInt($(this).attr("data-item"));
			getArtists(term, fillAlbums);
		}
		if($(this).closest("fieldset").parent().hasClass("events")){
			$selEventPage = parseInt($(this).attr("data-item"));
			getEvents(term, fillAlbums);
		}
		if($(this).closest("fieldset").parent().hasClass("frontends")){
			$selFrontendPage = parseInt($(this).attr("data-item"));
			getFrontends(term, fillAlbums);
		}
		if($(this).closest("fieldset").parent().hasClass("categories")){
			$selCategoryPage = parseInt($(this).attr("data-item"));
			getCategories(term, fillAlbums);
		}
		if($(this).closest("fieldset").parent().hasClass("providers")){
			$selProviderPage = parseInt($(this).attr("data-item"));
			getProviders(term, fillAlbums);
		}
		if($(this).closest("fieldset").parent().hasClass("genres")){
			$selGenrePage = parseInt($(this).attr("data-item"));
			getGenres(term, fillAlbums);
		}
	});
	
	$(".search").on("click", ".control-buttons .close-btn", function(e){
		$(".search").hide();
		$(".search select[name='type']").val("").trigger("change");
	});
	
	$(".search").on("click", ".search-buttons input[name='go']", function(){
		var id = $(".search").find("input[name='name']").attr("data-id");
		if(id){
			switch($('.search select[name="type"] option:selected').val()){
		        case 'song':
		        	window.location.href = $song+"/"+id;
		        	break;
		        case 'artist':
		        	window.location.href = $artist+"/"+id;
		        	break;
		        case 'album':
		        	window.location.href = $album+"/"+id;
		        	break;
		        case 'frontend':
		        	window.location.href = $frontend+"/"+id;
		        	break;
		        case 'provider':
		        	window.location.href = $provider+"/"+id;
		        	break;
		        case 'genre':
		        	window.location.href = $genre+"/"+id;
		        	break;
		        case 'event':
		        	window.location.href = $event+"/"+id;
		        	break;
		        default:
		    	
		    }
		}
	});
	
	$(".search-result").on("click", ".result-body .table-responsive table>tbody>tr", function(e){
		var id = $(this).attr("data-id");
		$(".search-result .result-body .table-responsive table>tbody").find("tr.selected").removeClass("selected");
		$(this).addClass("selected");
		$(".custom-menu li[value='open']").remove();
		if($(".custom-menu ul").children("li").length < 1){
			if($(this).closest("fieldset").parent().hasClass("songs")){
				window.location.href = $song+"/"+id;
			}
			if($(this).closest("fieldset").parent().hasClass("albums")){
				window.location.href = $album+"/"+id;
			}
			if($(this).closest("fieldset").parent().hasClass("artists")){
				window.location.href = $artist+"/"+id;
			}
			if($(this).closest("fieldset").parent().hasClass("events")){
				window.location.href = $event+"/"+id;
			}
			if($(this).closest("fieldset").parent().hasClass("frontends")){
				window.location.href = $frontend+"/"+id;
			}
			if($(this).closest("fieldset").parent().hasClass("categories")){
				window.location.href = $category+"/"+id;
			}
			if($(this).closest("fieldset").parent().hasClass("providers")){
				window.location.href = $provider+"/"+id;
			}
			if($(this).closest("fieldset").parent().hasClass("genres")){
				window.location.href = $genre+"/"+id;
			}
		}else{
			$(".custom-menu ul").append($("<li>", {
				value: "open",
		        text : "Open" 
			}));
			if($(this).closest("fieldset").parent().hasClass("songs")){
				$(".custom-menu ul li[value='open']").on("click", function(){
					window.location.href = $song+"/"+id;
				});
			}
			if($(this).closest("fieldset").parent().hasClass("albums")){
				$(".custom-menu ul li[value='open']").on("click", function(){
					window.location.href = $album+"/"+id;
				});
			}
			if($(this).closest("fieldset").parent().hasClass("artists")){
				$(".custom-menu ul li[value='open']").on("click", function(){
					window.location.href = $artist+"/"+id;
				});
			}
			if($(this).closest("fieldset").parent().hasClass("events")){
				$(".custom-menu ul li[value='open']").on("click", function(){
					window.location.href = $event+"/"+id;
				});
			}
			if($(this).closest("fieldset").parent().hasClass("frontends")){
				$(".custom-menu ul li[value='open']").on("click", function(){
					window.location.href = $frontend+"/"+id;
				});
			}
			if($(this).closest("fieldset").parent().hasClass("categories")){
				$(".custom-menu ul li[value='open']").on("click", function(){
					window.location.href = $category+"/"+id;
				});
			}
			if($(this).closest("fieldset").parent().hasClass("providers")){
				$(".custom-menu ul li[value='open']").on("click", function(){
					window.location.href = $provider+"/"+id;
				});
			}
			if($(this).closest("fieldset").parent().hasClass("genres")){
				$(".custom-menu ul li[value='open']").on("click", function(){
					window.location.href = $genre+"/"+id;
				});
			}
			$(".custom-menu").css({
				"top": e.clientY,
				"left": e.clientX
			}).show().trigger("change");
		}
	});
	
	$(document).keyup(function(e) {
		// enter
		if (e.keyCode === 13){
			  if($(e.target).hasClass("search-input")){
				  search(e.target);
			  }
			  if($(e.target).hasClass("search-name")){
				  if($('.search input[name="search"]').is(":visible")){
					  search(e.target);
				  }
			  }
		}
	});
	
	$(".custom-menu").on("change", function(){
		if($(this).is(":visible")){
			$(window).on("scroll mousewheel touchmove",function(e){
				return false;
			});
		}else{
			$(window).off("scroll mousewheel touchmove");
			$(window).on("scroll mousewheel touchmove",function(e){
				return true;
			});
			$(".custom-menu ul").empty();
			$(".search-result .result-body .table-responsive table>tbody").find("tr.selected").removeClass("selected");
		}
	});
	
	/* Name */
	$('.search input[name="name"]').autoComplete({
	    minChars: 0,
	    cache: 0,
	    menuClass: 'fixed-position',
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        switch($('.search select[name="type"] option:selected').val()){
		        case 'song':
		        	getSongs(term, suggest);
		        	break;
		        case 'artist':
		        	getArtists(term, suggest);
		        	break;
		        case 'album':
		        	getAlbums(term, suggest);
		        	break;
		        case 'frontend':
		        	getFrontends(term, suggest);
		        	break;
		        case 'provider':
		        	getProviders(term, suggest);
		        	break;
		        case 'genre':
		        	getGenres(term, suggest);
		        	break;
		        case 'event':
		        	getEvents(term, suggest);
		        	break;
		        default:
	        	
	        }
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        	if(item.data.artist){
	        		div.attr("data-artist", item.data.artist);
	        	}
	        	if(item.data.album){
	        		div.attr("data-album", item.data.album);
	        	}
	        	if(item.data.type){
	        		div.attr("data-type", item.data.type).append(" | " + item.data.type);
	        	}
	        	if(item.data.categories){
	        		div.attr("data-categories", item.data.categories).append(" | " + item.data.categories);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	if(item.data('id')){
	    		$('.search input[name="name"]').attr("data-id", item.data('id'));
	    	}
	    	if(item.data('name')){
	    		$('.search input[name="name"]').val(item.data('name'));
	    		$('.search input[name="name"]').attr("data-name", item.data('name'));
	    	}
	    	if(item.data("album")){
	    		$('.search input[name="name"]').attr("data-album", item.data("album"));
	    	}
	    	if(item.data("artist")){
	    		$('.search input[name="name"]').attr("data-artist", item.data("artist"));
	    	}
	    	if(item.data("photo")){
	    		$('.search input[name="name"]').attr("data-photo", item.data("photo"));
	    	}
	    	if(item.data("type")){
	    		$('.search input[name="name"]').val($('.search input[name="name"]').val() + " | " + item.data('type'));
	    	}
	    	if(item.data("categories")){
	    		$('.search input[name="name"]').val($('.search input[name="name"]').val() + " | " + item.data('categories'));
	    	}
	    	$('.search input[name="search"]').hide();
	    	if($(".templatemo-content-container .preview-widget .preview-widget-resposive-buttons>input.add-button").hasClass("active")){
	    		$('.search input[name="add"]').show();
	    	}else{
	    		$('.search input[name="go"]').show();
	    	}
	    }
	});
	
	/* Artist */
	$('.search input[name="artist"]').autoComplete({
	    minChars: 1,
	    menuClass: 'fixed-position',
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        getArtists(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	if(item.data('id')){
	    		$('.search input[name="artist"]').attr("data-id", item.data('id'));
	    	}
	    	if(item.data('name')){
	    		$('.search input[name="artist"]').val(item.data('name'));
	    		$('.search input[name="artist"]').attr("data-name", item.data('name'));
	    	}
	    	if(item.data("photo")){
	    		$('.search input[name="artist"]').attr("data-photo", item.data("photo"));
	    	}
	    }
	});
	
	/* Album */
	$('.search input[name="album"]').autoComplete({
	    minChars: 0,
	    cache: 0,
	    menuClass: 'fixed-position',
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        getAlbums(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        	if(item.data.provider){
	        		div.attr("data-providerid", item.data.provider.id);
	        		div.attr("data-providername", item.data.provider.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	$('.search input[name="album"]').val(item.data('name')).attr("data-id", item.data('id'));
	    	$('.search input[name="provider"]').val(item.data("providername")).attr("data-id", item.data('providerid')).attr("disabled", "disabled");
	    }
	});
	
	/* Provider */
	$('.search input[name="provider"]').autoComplete({
	    minChars: 1,
	    menuClass: 'fixed-position',
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        getProviders(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        return '<div class="autocomplete-suggestion" data-id="'+item.id+'" data-name="'+item.data.name+'" data-val="'+search+'">'+item.data.name+'</div>';
	    },
	    onSelect: function(e, term, item){
	    	$('.search input[name="provider"]').val(item.data('name')).attr("data-id", item.data('id'));
	    }
	});
	
	/* Genre */
	$('.search input[name="genre"]').autoComplete({
	    minChars: 0,
	    cache: 0,
	    menuClass: 'fixed-position',
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        getGenres(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        var div = addDiv().addClass("autocomplete-suggestion").attr("data-val", search);
	        div.attr("data-id", item.id);
	        if(item.data){
	        	if(item.data.photo){
	        		var src = item.data.photo.replace("{width}","100").replace("{height}", "100");
	        		var img = addImage().attr("src", src);
	        		div.attr("data-photo", src);
	        		div.append(img)
	        	}
	        	if(item.data.name){
	        		div.attr("data-name", item.data.name).append(item.data.name);
	        	}
	        }
	        return div.wrap("<div></div>").parent().html();
	    },
	    onSelect: function(e, term, item){
	    	$('.search input[name="genre"]').val(item.data('name')).attr("data-id", item.data('id'));
	    }
	});
	
	/* Operator */
	$('.search input[name="operator"]').autoComplete({
	    minChars: 1,
	    menuClass: 'fixed-position',
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        getOperators(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        return '<div class="autocomplete-suggestion" data-id="'+item.id+'" data-name="'+item.data.name+'" data-country="'+item.data.country+'" data-val="'+search+'">'+item.data.name+ ' | ' + item.data.country + '</div>';
	    },
	    onSelect: function(e, term, item){
	    	$('.search input[name="operator"]').val(item.data('name') + ' | ' + item.data('country')).attr("data-id", item.data('id'));
	    }
	});
	
	/* Operator */
	$('.search input[name="storefront"]').autoComplete({
	    minChars: 1,
	    menuClass: 'fixed-position',
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        getStorefronts(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        return '<div class="autocomplete-suggestion" data-id="'+item.id+'" data-name="'+item.data.name+'" data-keyword="'+item.data.keyword+'" data-val="'+search+'">'+item.data.name+'</div>';
	    },
	    onSelect: function(e, term, item){
	    	$('.search input[name="storefront"]').val(item.data('name')).attr("data-id", item.data('id')).attr("data-keyword", item.data('keyword'));
	    }
	});
	
	/* Operator */
	$('.search input[name="country"]').autoComplete({
	    minChars: 1,
	    menuClass: 'fixed-position',
	    source: function(term, suggest){
	        term = term.toLowerCase();
	        getCountries(term, suggest);
	    },
	    renderItem: function (item, search){
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        return '<div class="autocomplete-suggestion" data-id="'+item.id+'" data-name="'+item.data.name+'" data-code="'+item.data.code+'" data-val="'+search+'">'+item.data.name+'</div>';
	    },
	    onSelect: function(e, term, item){
	    	$('.search input[name="country"]').val(item.data('name')).attr("data-id", item.data('id')).attr("data-code", item.data('code'));
	    }
	});
});

function getSongs(term, suggest){
	try{
		$.when(ajaxCall($song+"/"+$selSongPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, song){
				var item = {};
				var data = {};
				item.id = song.id;
				data.name = song.meta[0].name;
				$.each(song.album, function(key, value){
					if(data.album){
						data.album.concat("-").concat(value);
					}else{
						data.album = value;
					}
				});
				$.each(song.artists, function(key, value){
					if(data.artist){
						data.artist.concat("-").concat(value);
					}else{
						data.artist = value;
					}
				});
				if(song.photos && song.photos.length > 0){
					$.each(song.photos, function(index2, photo){
						if(song.isMainPhoto){
							data.photo = photo.url;
						}
					});
					if(!item.photo){
						data.photo = song.photos[0].url;
					}
				}
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		console.log(error);
	}
}

function getArtists(term, suggest){
	try{
		$.when(ajaxCall($artist+"/"+$selArtistPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, artist){
				var item = {};
				var data = {};
				item.id = artist.id;
				data.name = artist.meta[0].name;
				if(artist.photos && artist.photos.length > 0){
					$.each(artist.photos, function(index2, photo){
						if(photo.isMainPhoto){
							data.photo = photo.url;
						}
					});
					if(!item.photo){
						data.photo = artist.photos[0].url;
					}
				}
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		toastr.error('Error occured.', 'Error', {positionClass: 'toast-bottom-right'});
	}
}

function getEvents(term, suggest){
	try{
		$.when(ajaxCall($event+"/"+$selEventPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, event){
				var item = {};
				var data = {};
				item.id = event.id;
				data.name = event.meta[0].name;
				$.each(event.artists, function(key, value){
					if(data.artist){
						data.artist.concat("-").concat(value);
					}else{
						data.artist = value;
					}
				});
				if(event.photos && event.photos.length > 0){
					$.each(event.photos, function(index2, photo){
						if(photo.isMainPhoto){
							data.photo = photo.url;
						}
					});
					if(!item.photo){
						data.photo = event.photos[0].url;
					}
				}
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		toastr.error('Error occured.', 'Error', {positionClass: 'toast-bottom-right'});
	}
}

function getAlbums(term, suggest){
	try{
		$.when(ajaxCall($album+"/"+$selAlbumPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, album){
				var item = {};
				var data = {};
				var provider = {};
				item.id = album.id;
				data.name = album.meta[0].name;
				provider.id = album.provider.id;
				provider.name = album.provider.meta[0].name;
				$.each(album.artists, function(key, value){
					if(data.artist){
						data.artist.concat("-").concat(value);
					}else{
						data.artist = value;
					}
				});
				if(album.photos && album.photos.length > 0){
					$.each(album.photos, function(index2, photo){
						if(photo.isMainPhoto){
							data.photo = photo.url;
						}
					});
					if(!item.photo){
						data.photo = album.photos[0].url;
					}
				}
				data.provider = provider;
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	}catch(error){
		
	}
}

function getFrontends(term, suggest){
	try{
		$.when(ajaxCall($frontend+"/"+$selFrontendPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, frontend){
				var item = {};
				var data = {};
				item.id = frontend.id;
				data.name = frontend.meta[0].name;
				data.type = frontend.type;
				$.each(frontend.categories, function(key, value){
					if(data.categories){
						data.categories.concat("-").concat(value);
					}else{
						data.categories = value;
					}
				});
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function getCategories(term, suggest){
	try{
		$.when(ajaxCall($category+"/"+$selCategoryPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, category){
				var item = {};
				var data = {};
				item.id = category.id;
				data.name = category.meta[0].name;
				$.each(category.storefronts, function(key, value){
					if(data.storefronts){
						data.storefronts.concat("-").concat(value);
					}else{
						data.storefronts = value;
					}
				});
				$.each(category.operators, function(key, value){
					if(data.operators){
						data.operators.concat("-").concat(value["name"] + " | " + value["country"]);
					}else{
						data.operators = value["name"] + " | " + value["country"];
					}
				});
				$.each(category.countries, function(key, value){
					if(data.countries){
						data.countries.concat("-").concat(value);
					}else{
						data.countries = value;
					}
				});
				
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function getProviders(term, suggest){
	try{
		$.when(ajaxCall($provider+"/"+$selProviderPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, provider){
				var item = {};
				var data = {};
				item.id = provider.id;
				data.name = provider.meta[0].name;
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function getGenres(term, suggest){
	try{
		$.when(ajaxCall($genre+"/"+$selGenrePage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, genre){
				var item = {};
				var data = {};
				item.id = genre.id;
				data.name = genre.meta[0].name;
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function getOperators(term, suggest){
	try{
		$.when(ajaxCall($operator+"/"+$selOperatorPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, operator){
				var item = {};
				var data = {};
				item.id = operator.id;
				data.name = operator.name;
				if(operator.country.name){
					data.country = operator.country.name;
				}
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function getStorefronts(term, suggest){
	try{
		$.when(ajaxCall($storefront+"/"+$selStorefrontPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, storefront){
				var item = {};
				var data = {};
				item.id = storefront.id;
				data.keyword = storefront.keyword;
				data.name = storefront.name;
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function getCountries(term, suggest){
	try{
		$.when(ajaxCall($country+"/"+$selCountryPage+"/"+$limit+"?Keyword="+term, $HTTP_METHOD.GET, {}, getHeaders())).then(function(data, status, xhr){
			var suggestions = [];
			$.each(data, function(index1, country){
				var item = {};
				var data = {};
				item.id = country.id;
				data.name = country.name;
				data.code = country.iso;
				var total = xhr.getResponseHeader("x-data-count");
				
				if($.isNumeric(total)){
					data.total = total;
				}
				item.data = data;
				suggestions.push(item);
			});
			suggest(suggestions);
		},
		function(xhr){
			handleError(xhr)
		});
	} catch(error){
		
	}
}

function search(elem){
	var term;
	var codeFrom;
	var codeTo;
	if($(elem).hasClass("search-input")){
		term  = $(elem).val();
	}
	if($(elem).hasClass("search-name")){
		term  = $(elem).val();
	}
	if($(elem).hasClass("fa-search")){
		term  = $(elem).closest(".input-group").find("input[name='srch-term']").val();
	}
	if($(elem).hasClass("search-btn")){
		term = $('.search input[name="name"]').val();
	}
	codeFrom = $('.search input[id="codefrom"]').val();
	codeTo = $('.search input[id="codeto"]').val();
	if(term || codeFrom || codeTo){
		$(".search-result").find(".result-header span").text(term);
		term = encodeURIComponent(term);
		$(".search-result").find(".tab").hide();
		$(".search-result").find(".result-body>div").hide();
		$(".search-result").find(".result-body>div>fieldset>legend>span").attr("data-total", "0").text("0");
		$(".search-result").find(".result-body>div .table-responsive table>tbody").empty();
		term = term.toLowerCase();
		switch($('.search select[name="type"] option:selected').val()){
	        case 'song':
	        	getSongs(term, fillSongs);
	        	$(".search-result").find(".result-body>div.songs").show();
	        	break;
	        case 'artist':
	        	getArtists(term, fillArtists);
	        	$(".search-result").find(".result-body>div.artists").show();
	        	break;
	        case 'album':
	        	getAlbums(term, fillAlbums);
	        	$(".search-result").find(".result-body>div.albums").show();
	        	break;
	        case 'provider':
	        	getProviders(term, fillProviders);
	        	$(".search-result").find(".result-body>div.providers").show();
	        	break;
	        case 'genre':
	        	getGenres(term, fillGenres);
	        	$(".search-result").find(".result-body>div.genres").show();
	        	break;
	        case 'event':
	        	getEvents(term, fillEvents);
	        	$(".search-result").find(".result-body>div.events").show();
	        	break;
	        case 'frontend':
	        	getFrontends(term, fillFrontends);
	        	$(".search-result").find(".result-body>div.frontends").show();
	        	break;
	        case 'category':
	        	getCategories(term, fillCategories);
	        	$(".search-result").find(".result-body>div.categories").show();
	        	break;
	        default:
	        	getSongs(term, fillSongs);
	        	setTimeout(
	        			  function() 
	        			  {
	        				  getArtists(term, fillArtists);
	        			  }, 200);
	        	setTimeout(
	        			  function() 
	        			  {
	        				  getAlbums(term, fillAlbums);
	        			  }, 400);
	        	setTimeout(
	        			  function() 
	        			  {
	        				  getEvents(term, fillEvents);
	        			  }, 600);
	        	setTimeout(
	        			  function() 
	        			  {
	        				  getFrontends(term, fillFrontends);
	        			  }, 800);
	        	setTimeout(
	        			  function() 
	        			  {
	        				  getCategories(term, fillCategories);
	        			  }, 1000);
	        	setTimeout(
	        			  function() 
	        			  {
	        				  getProviders(term, fillProviders);
	        			  }, 1200);
	        	setTimeout(
	        			  function() 
	        			  {
	        				  getGenres(term, fillGenres);
	        			  }, 1400);
		        $(".search-result").find(".tab").show();
		        $(".search-result").find(".tab button.tablinks").removeClass("active");
		        $(".search-result").find(".tab #songs").addClass("active");
		        $(".search-result").find(".result-body>div.songs").show();
		}
		$(".umbrella").show();
		$(".search-result").show();
	}
}

function fillSongs(data){
	$(".search-result").find(".result-body .songs .table-responsive table>tbody").empty();
	if(data && data.length){
		$.each(data, function(index, song){
			$(".search-result").find(".result-body .songs>fieldset>legend>span").attr("data-total", song.data.total).text(song.data.total);
			var row = addTableRow(6).attr("data-id", song.id);
			row.children("td:first-child()").addClass("tc").text((index + 1) + (($selSongPage - 1) * ($limit)));
			row.children("td:nth-child(2)").addClass("tc").text(song.id);
			var image = addImage();
			if(song.data.photo){
				image.attr("src", song.data.photo.replace("{width}", 500).replace("{height}", 500));
			}
			row.children("td:nth-child(3)").addClass("photo").append(image);
			row.children("td:nth-child(4)").text(song.data.name);
			row.children("td:nth-child(5)").text(song.data.album);
			row.children("td:last-child()").text(song.data.artist);
			$(".search-result").find(".result-body .songs .table-responsive table>tbody").append(row);
		});
	}
	updatePagenation($(".search-result").find(".result-body .songs"));
}

function fillAlbums(data){
	$(".search-result").find(".result-body .albums .table-responsive table>tbody").empty();
	if(data && data.length){
		$.each(data, function(index, album){
			$(".search-result").find(".result-body .albums>fieldset>legend>span").attr("data-total", album.data.total).text(album.data.total);
			var row = addTableRow(5).attr("data-id", album.id);
			row.children("td:first-child()").addClass("tc").text((index + 1) + (($selAlbumPage - 1) * ($limit)));
			row.children("td:nth-child(2)").addClass("tc").text(album.id);
			var image = addImage();
			if(album.data.photo){
				image.attr("src", album.data.photo.replace("{width}", 500).replace("{height}", 500));
			}
			row.children("td:nth-child(3)").addClass("photo").append(image);
			row.children("td:nth-child(4)").text(album.data.name);
			row.children("td:last-child()").text(album.data.artist);
			$(".search-result").find(".result-body .albums .table-responsive table>tbody").append(row);
		});
	}
	updatePagenation($(".search-result").find(".result-body .albums"));
}

function fillArtists(data){
	$(".search-result").find(".result-body .artists .table-responsive table>tbody").empty();
	if(data && data.length){
		$.each(data, function(index, artist){
			$(".search-result").find(".result-body .artists>fieldset>legend>span").attr("data-total", artist.data.total).text(artist.data.total);
			var row = addTableRow(4).attr("data-id", artist.id);
			row.children("td:first-child()").addClass("tc").text((index + 1) + (($selArtistPage - 1) * ($limit)));
			row.children("td:nth-child(2)").addClass("tc").text(artist.id);
			var image = addImage();
			if(artist.data.photo){
				image.attr("src", artist.data.photo.replace("{width}", 500).replace("{height}", 500));
			}
			row.children("td:nth-child(3)").addClass("photo").append(image);
			row.children("td:last-child()").text(artist.data.name);
			$(".search-result").find(".result-body .artists .table-responsive table>tbody").append(row);
		});
	}
	updatePagenation($(".search-result").find(".result-body .artists"));
}

function fillEvents(data){
	$(".search-result").find(".result-body .events .table-responsive table>tbody").empty();
	if(data && data.length){
		$.each(data, function(index, event){
			$(".search-result").find(".result-body .events>fieldset>legend>span").attr("data-total", event.data.total).text(event.data.total);
			var row = addTableRow(5).attr("data-id", event.id);
			row.children("td:first-child()").addClass("tc").text((index + 1) + (($selEventPage - 1) * ($limit)));
			row.children("td:nth-child(2)").addClass("tc").text(event.id);
			var image = addImage();
			if(event.data.photo){
				image.attr("src", event.data.photo.replace("{width}", 500).replace("{height}", 500));
			}
			row.children("td:nth-child(3)").addClass("photo").append(image);
			row.children("td:nth-child(4)").text(event.data.name);
			row.children("td:last-child()").text(event.data.artist);
			$(".search-result").find(".result-body .events .table-responsive table>tbody").append(row);
		});
	}
	updatePagenation($(".search-result").find(".result-body .events"));
}

function fillFrontends(data){
	$(".search-result").find(".result-body .frontends .table-responsive table>tbody").empty();
	if(data && data.length){
		$.each(data, function(index, frontend){
			$(".search-result").find(".result-body .frontends>fieldset>legend>span").attr("data-total", frontend.data.total).text(frontend.data.total);
			var row = addTableRow(5).attr("data-id", frontend.id);
			row.children("td:first-child()").addClass("tc").text((index + 1) + (($selFrontendPage - 1) * ($limit)));
			row.children("td:nth-child(2)").addClass("tc").text(frontend.id);
			row.children("td:nth-child(3)").text(frontend.data.name);
			row.children("td:nth-child(4)").text(frontend.data.type);
			row.children("td:last-child()").text(frontend.data.categories);
			$(".search-result").find(".result-body .frontends .table-responsive table>tbody").append(row);
		});
	}
	updatePagenation($(".search-result").find(".result-body .frontends"));
}

function fillCategories(data){
	$(".search-result").find(".result-body .categories .table-responsive table>tbody").empty();
	if(data && data.length){
		$.each(data, function(index, category){
			$(".search-result").find(".result-body .categories>fieldset>legend>span").attr("data-total", category.data.total).text(category.data.total);
			var row = addTableRow(6).attr("data-id", category.id);
			row.children("td:first-child()").addClass("tc").text((index + 1) + (($selCategoryPage - 1) * ($limit)));
			row.children("td:nth-child(2)").addClass("tc").text(category.id);
			row.children("td:nth-child(3)").text(category.data.name);
			row.children("td:nth-child(4)").text(category.data.storefronts);
			row.children("td:nth-child(5)").text(category.data.operators);
			row.children("td:nth-child(6)").text(category.data.countries);
			$(".search-result").find(".result-body .categories .table-responsive table>tbody").append(row);
		});
	}
	updatePagenation($(".search-result").find(".result-body .categories"));
}

function fillProviders(data){
	$(".search-result").find(".result-body .providers .table-responsive table>tbody").empty();
	if(data && data.length){
		$.each(data, function(index, provider){
			$(".search-result").find(".result-body .providers>fieldset>legend>span").attr("data-total", provider.data.total).text(provider.data.total);
			var row = addTableRow(3).attr("data-id", provider.id);
			row.children("td:first-child()").addClass("tc").text((index + 1) + (($selProviderPage - 1) * ($limit)));
			row.children("td:nth-child(2)").addClass("tc").text(provider.id);
			row.children("td:last-child()").text(provider.data.name);
			$(".search-result").find(".result-body .providers .table-responsive table>tbody").append(row);
		});
	}
	updatePagenation($(".search-result").find(".result-body .providers"));
}

function fillGenres(data){
	$(".search-result").find(".result-body .genres .table-responsive table>tbody").empty();
	if(data && data.length){
		$.each(data, function(index, genre){
			$(".search-result").find(".result-body .genres>fieldset>legend>span").attr("data-total", genre.data.total).text(genre.data.total);
			var row = addTableRow(3).attr("data-id", genre.id);
			row.children("td:first-child()").addClass("tc").text((index + 1) + (($selGenrePage - 1) * ($limit)));
			row.children("td:nth-child(2)").addClass("tc").text(genre.id);
			row.children("td:last-child()").text(genre.data.name);
			$(".search-result").find(".result-body .genres .table-responsive table>tbody").append(row);
		});
	}
	updatePagenation($(".search-result").find(".result-body .genres"));
}

function updatePagenation(fragment){
	if($maxVisibleItems % 2 == 0){
		$maxVisibleItems = $maxVisibleItems + 1;
	}
	var current = fragment.find(".pagination-wrap ul.pagination li.active").attr("data-item");
	if($.isNumeric(current)){
		if($selectedPage == current){
			return;
		}
	}else{
		current = 1;
	}
	var total = fragment.find("fieldset>legend>span").attr("data-total");
	if(fragment.is(":visible")){
		$('html, body').animate({
	        scrollTop: $(".search-result").offset().top
	    }, 2000);
	}
	if($.isNumeric(total)){
		if(parseInt(total) > $limit){
			var selected = 1;
			if(fragment.hasClass("songs")){
				selected = $selSongPage;
			}else if(fragment.hasClass("albums")){
				selected = $selAlbumPage;
			}else if(fragment.hasClass("artists")){
				selected = $selArtistPage;
			}else if(fragment.hasClass("events")){
				selected = $selEventPage;
			}else if(fragment.hasClass("frontends")){
				selected = $selFrontendPage;
			}else if(fragment.hasClass("categories")){
				selected = $selCategoryPage;
			}else if(fragment.hasClass("providers")){
				selected = $selProviderPage;
			}else if(fragment.hasClass("genres")){
				selected = $selGenrePage;
			}
			var list = [];
			var pages = Math.ceil(total / $limit);
			var firstItem = (($maxVisibleItems - 1) / 2);
			var lastItem = (($maxVisibleItems - 1) / 2);
			if(pages > 1){
				for(var counter=1; counter<=pages; counter++){
					if(counter == selected){
						list.push(item(counter).addClass("active"));
					}else if(counter >= (selected - firstItem) && counter <= (selected + lastItem)){
						list.push(item(counter));
					}else if(selected - firstItem < 1){
						for(var i=1; i<=$maxVisibleItems; i++){
							if(counter == i){
								list.push(item(counter));
							}
						}
					}else if(selected + lastItem > pages){
						for(var i=pages; i>=pages-$maxVisibleItems; i--){
							if(counter == i){
								list.push(item(counter));
							}
						}
					}
				}
			}
			fragment.find(".pagination-wrap ul.pagination").children("li:not(:last)").remove();
			$.each(list, function(index, item){
				fragment.find(".pagination-wrap ul.pagination").children("li:last-child()").before(item);
			})
			fragment.find(".pagination-wrap").show();
		}else{
			fragment.find(".pagination-wrap ul.pagination").children("li:not(:last)").remove();
			fragment.find(".pagination-wrap").hide();
		}
	}
}

function reset(){
	$selSongPage  		= 1;
	$selAlbumPage  		= 1;
	$selArtistPage  	= 1;
	$selEventPage  		= 1;
	$selFrontendPage  	= 1;
	$selCategoryPage  	= 1;
	$selProviderPage  	= 1;
	$selGenrePage  		= 1;
	$selOperatorPage	= 1;
}

function item(id){
	var item = listItem();
	var link = addLink().attr("data-item", id).text(id);
	item.append(link);
	return item;
}

function getHeaders(){
	var headers = {};
	if($('.search input[id="codefrom"]').is(":visible")){
		headers["Code-From"]=$('.search input[id="codefrom"]').val();
	}
	if($('.search input[id="codeto"]').is(":visible")){
		headers["Code-To"]=$('.search input[id="codeto"]').val();
	}
	if($('.search input[name="artist"]').is(":visible")){
		headers["Artist-Id"]=$('.search input[name="artist"]').attr("data-id");
	}
	if($('.search input[name="album"]').is(":visible")){
		headers["Album-Id"]=$('.search input[name="album"]').attr("data-id");
	}
	if($('.search input[name="provider"]').is(":visible")){
		headers["Provider-Id"]=$('.search input[name="provider"]').attr("data-id");
	}
	if($('.search input[name="genre"]').is(":visible")){
		headers["Genre-Id"]=$('.search input[name="genre"]').attr("data-id");
	}
	if($('.search input[name="operator"]').is(":visible")){
		headers["operator-id"]=$('.search input[name="operator"]').attr("data-id");
	}
	if($('.search input[name="storefront"]').is(":visible")){
		headers["storefront"]=$('.search input[name="storefront"]').attr("data-keyword");
	}
	if($('.search input[name="country"]').is(":visible")){
		headers["country"]=$('.search input[name="country"]').attr("data-code");
	}
	return headers;
}