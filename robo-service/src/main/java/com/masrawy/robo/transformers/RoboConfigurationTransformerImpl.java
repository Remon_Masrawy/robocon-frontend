package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboConfigurationDTO;
import com.masrawy.robo.models.RoboConfigurationVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboConfigurationTransformer")
public class RoboConfigurationTransformerImpl extends AbstractDTOTransformer<RoboConfigurationVO, RoboConfigurationDTO> implements RoboConfigurationTransformer{

	public RoboConfigurationTransformerImpl(){
		super(RoboConfigurationVO.class, RoboConfigurationDTO.class);
	}
}
