package com.masrawy.robo.models;

public class RoboEventMetaVO extends RoboMetaVO{

	private static final long serialVersionUID = 1L;

	private String location;
	private String city;
	private String country;
	
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
}
