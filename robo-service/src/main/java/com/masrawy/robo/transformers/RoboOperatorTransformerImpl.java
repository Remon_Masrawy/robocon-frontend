package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboOperatorDTO;
import com.masrawy.robo.models.RoboOperatorVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboOperatorTransformer")
public class RoboOperatorTransformerImpl extends AbstractDTOTransformer<RoboOperatorVO, RoboOperatorDTO> implements RoboOperatorTransformer{

	public RoboOperatorTransformerImpl(){
		super(RoboOperatorVO.class, RoboOperatorDTO.class);
	}
}
