package com.masrawy.robo.models;

public class RoboLyricsVO extends RoboFileVO{

	private static final long serialVersionUID = 1L;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
