package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboFrontendDTO;
import com.masrawy.robo.models.RoboFrontendVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboFrontendTransformer")
public class RoboFrontendTransformerImpl extends AbstractDTOTransformer<RoboFrontendVO, RoboFrontendDTO> implements RoboFrontendTransformer{

	public RoboFrontendTransformerImpl(){
		super(RoboFrontendVO.class, RoboFrontendDTO.class);
	}
}
