package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.OPERATOR_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboOperatorDTO;
import com.masrawy.robo.services.RoboOperatorService;

@Controller
@RequestMapping(value = "/web/operator")
public class OperatorController {

	@Autowired
	private RoboOperatorService roboOperatorService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(){
		ModelAndView model = new ModelAndView(OPERATOR_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboOperatorDTO create(@RequestBody RoboOperatorDTO roboOperatorDTO){
		return roboOperatorService.createOperator(roboOperatorDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboOperatorDTO update(@RequestBody RoboOperatorDTO roboOperatorDTO){
		return roboOperatorService.updateOperator(roboOperatorDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value="/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboOperatorDTO> operators(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboOperatorService.getOperatorList(page, limit);
	}
}
