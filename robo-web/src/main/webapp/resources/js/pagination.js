
$(document).ready(function(){
	
	$container = $(".templatemo-content-container");
	
	$pagination = $("div.pagination-wrap.global>ul.pagination");
	
	$displayTotal	= $container.find(".info-panel .panel-total");
	
	$itemsPerPage = $container.find("select.total-per-page");
	
	$totalItems = 0;
	
	$currentPage= 1;
	
	$totalPages = 1;
	
	$displayPages = 5;
	
	if($enablePagination){
		$pagination.parent().removeClass("pagination-hidden");
	}else{
		$pagination.parent().addClass("pagination-hidden");
		$itemsPerPage.val(0);
	}
	
	$pageLimit = $itemsPerPage.val();
	
	$itemsPerPage.change(function(){
		$pageLimit = $(this).val();
		if($pageLimit == 0){
			$currentPage = 0;
			$pagination.parent().addClass("pagination-hidden");
		}else{
			$currentPage = 1;
			if($pagination.parent().hasClass("pagination-hidden")){
				$pagination.parent().removeClass("pagination-hidden");
			}
		}
		if($pageLimit > $totalItems){
			$currentPage = 0;
			$pagination.parent().addClass("pagination-hidden");
		}
		$fill();
	});
	
	$pagination.on("click", "li>a[aria-label='Next']", function(){
		if($currentPage < $totalPages){
			$currentPage++;
			$fill();
		}
	});
	
	$pagination.on("click", "li:not(:last-child):not(.active)>a", function(){
		$currentPage = parseInt($(this).text());
		$fill();
	});
	
	if($enablePanel){
		$fill();
		$container.find(".info-panel").css({"display": "inline"});
	}
});

function refreshPagination(){
	if(!$pagination.parent().hasClass("pagination-hidden")){
		if($itemsPerPage.val() == 0){
			$pagination.parent().addClass("pagination-hidden");
			return;
		}
		$totalPages = Math.ceil($totalItems / $itemsPerPage.val());
		if($totalPages <= 1){
			$pagination.parent().addClass("pagination-hidden");
			return;
		}
		$pagination.children("li:not(:last)").remove();
		for(page=1; page<=$totalPages; page++){
			var $listItem = listItem();
			if((page == 1 || page == $totalPages || page == $currentPage) ||
					(page == $currentPage - 1 || page == $currentPage + 1) ||
					(($currentPage == 1 || $currentPage == 2) && (page == 2 || page == 3 || page == 4)) ||
					(($currentPage == $totalPages || $currentPage == $totalPages - 1) && (page == $totalPages - 1 || page == $totalPages - 2 || page == $totalPages - 3))){
				$listItem.append(addLink().attr("data-item", page).text(page));
			}else{
				if(page == 2 || page == $totalPages - 1){
					$listItem.append(addLink().addClass("inactive").text("..."));
				}else{
					$listItem.addClass("hidden");
				}
			}
			$pagination.children("li:last-child()").before($listItem);
		}
		$pagination.find("li:nth-child("+$currentPage+")").addClass("active");
		 $("html, body").animate({ scrollTop: 0 }, "slow");
	}
	
}