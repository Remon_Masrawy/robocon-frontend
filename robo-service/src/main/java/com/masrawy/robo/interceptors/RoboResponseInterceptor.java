package com.masrawy.robo.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.masrawy.robo.security.CustomUserDetails;


public class RoboResponseInterceptor extends HandlerInterceptorAdapter{

	@Autowired
	private HttpHeaders headers;
	
	private static final String AUTH_TOKEN = "Auth-Token";
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if(headers.containsKey(AUTH_TOKEN)){
			headers.remove(AUTH_TOKEN);
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication.getPrincipal() instanceof CustomUserDetails){
			String token = ((CustomUserDetails) authentication.getPrincipal()).getAuthToken();
			headers.add(AUTH_TOKEN, token);
		}
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		super.postHandle(request, response, handler, modelAndView);
	}
}
