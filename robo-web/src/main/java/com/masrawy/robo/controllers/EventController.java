package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.EVENTS_PAGE;
import static com.masrawy.robo.constants.RoboPages.EVENT_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboEventDTO;
import com.masrawy.robo.services.RoboArtistService;
import com.masrawy.robo.services.RoboEventService;
import com.masrawy.robo.services.RoboLanguageService;

@Controller
@RequestMapping(value = "/web/event")
public class EventController {

	@Autowired
	private RoboEventService roboEventService;
	
	@Autowired
	private RoboArtistService roboArtistService;
	
	@Autowired
	private RoboLanguageService roboLanguageService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView eventsPage(){
		ModelAndView model = new ModelAndView(EVENTS_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{eventId}", method = RequestMethod.GET)
	public ModelAndView eventPage(@PathVariable("eventId") String eventId){
		ModelAndView model = new ModelAndView(EVENT_PAGE);
		try{
			Long id = Long.valueOf(eventId);
			if(id != 0){
				model.addObject("event", roboEventService.getEvent(id));
			}
			model.addObject("artists", roboArtistService.getArtists(0, 0));
			model.addObject("languages", roboLanguageService.getLanguageList(0, 0));
		}catch(Exception e){
			model = new ModelAndView("redirect:/web/event");
		}
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboEventDTO eventCreate(@RequestBody RoboEventDTO roboEventDTO){
		return roboEventService.createEvent(roboEventDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboEventDTO eventUpdate(@RequestBody RoboEventDTO roboEventDTO){
		return roboEventService.updateEvent(roboEventDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{eventId}", method = RequestMethod.POST)
	public @ResponseBody RoboEventDTO eventPhoto(@RequestParam(value = "file", required = true) MultipartFile file, @RequestParam(value = "name", required = true) String name, @NotNull @PathVariable("eventId") Long eventId){
		return roboEventService.uploadPhoto(file, eventId, name);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboEventDTO> getEvents(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboEventService.getEvents(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{eventId}", method = RequestMethod.DELETE)
	public @ResponseBody void artistDelete(@PathVariable("eventId") Long eventId){
		roboEventService.deleteEvent(eventId);
	}
}
