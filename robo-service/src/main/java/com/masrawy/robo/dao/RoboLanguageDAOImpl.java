package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboLanguageVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboLanguageDAO")
public class RoboLanguageDAOImpl extends AbstractWebserviceDAO<RoboLanguageVO, String> implements RoboLanguageDAO{

	public RoboLanguageDAOImpl(){
		super(RoboLanguageVO.class, new ParameterizedTypeReference<List<RoboLanguageVO>>(){});
	}
}
