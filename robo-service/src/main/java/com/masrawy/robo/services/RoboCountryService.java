package com.masrawy.robo.services;

import java.util.List;

import com.masrawy.robo.dto.RoboCountryDTO;
import com.masrawy.robo.models.RoboCountryVO;

public interface RoboCountryService extends PgrlService<RoboCountryVO, RoboCountryDTO, Long>{

	public RoboCountryDTO createCountry(RoboCountryDTO roboCountryDTO);
	public RoboCountryDTO updateCountry(RoboCountryDTO roboCountryDTO);
	public List<RoboCountryDTO> getCountryList(Integer page, Integer limit);
}
