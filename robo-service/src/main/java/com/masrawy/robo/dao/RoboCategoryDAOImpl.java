package com.masrawy.robo.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import com.masrawy.robo.models.RoboCategoryVO;
import com.masrawy.robo.persistence.AbstractWebserviceDAO;

@Repository("roboCategoryDAO")
public class RoboCategoryDAOImpl extends AbstractWebserviceDAO<RoboCategoryVO, Long> implements RoboCategoryDAO{

	public RoboCategoryDAOImpl(){
		super(RoboCategoryVO.class, new ParameterizedTypeReference<List<RoboCategoryVO>>() {});
	}
}
