package com.masrawy.robo.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dto.RoboEventDTO;
import com.masrawy.robo.models.RoboEventVO;

public interface RoboEventService extends PgrlService<RoboEventVO, RoboEventDTO, Long>{

	public RoboEventDTO createEvent(RoboEventDTO roboEventDTO);
	public RoboEventDTO updateEvent(RoboEventDTO roboEventDTO);
	public RoboEventDTO uploadPhoto(MultipartFile file, Long eventId, String name);
	public RoboEventDTO getEvent(Long eventId);
	public List<RoboEventDTO> getEvents(Integer page, Integer limit);
	public void deleteEvent(Long eventId);
}
