package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.masrawy.robo.dao.RoboEventDAO;
import com.masrawy.robo.dto.RoboEventDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboEventException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.model.RoboMimeTypeEnum;
import com.masrawy.robo.models.RoboEventVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboEventTransformer;

@Service("roboEventService")
public class RoboEventServiceImpl extends AbstractPgrlServiceImpl<RoboEventVO, RoboEventDTO, Long> implements RoboEventService{

	@Autowired
	private RoboEventDAO roboEventDAO;
	@Autowired
	private RoboEventTransformer roboEventTransformer;
	
	@Value("${robocon.ws.event.url}")
	private String roboEventURL;
	
	@Override
	public RoboEventDTO createEvent(RoboEventDTO roboEventDTO){
		logger.debug("Robocon request for creatting event");
		try{
			roboEventDTO = postObject(roboEventURL, roboEventDTO);
			return roboEventDTO;
		}catch(RoboResponseException ex){
			logger.error("Event creation error occured.", ex);
			throw new RoboEventException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Event creation error occured.", ex);
			throw new RoboEventException(ex.getMessage());
		}
	}
	
	@Override
	public RoboEventDTO updateEvent(RoboEventDTO roboEventDTO){
		logger.debug("Robocon request for updatting event");
		try{
			roboEventDTO = updateR(roboEventURL, roboEventDTO);
			return roboEventDTO;
		}catch(RoboResponseException ex){
			logger.error("Event update error occured.", ex);
			throw new RoboEventException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Event update error occured.", ex);
			throw new RoboEventException(ex.getMessage());
		}
	}
	
	@Override
	public RoboEventDTO uploadPhoto(MultipartFile file, Long eventId, String name) {
		logger.debug("Robocon request for uploading artist photo");
		try{
			RoboEventDTO event = postFile(String.format(fileUploadURL, eventId, RoboMimeTypeEnum.EVENT_IMAGE.getIntValue()), file, name);
			return event;
		}catch(RoboResponseException ex){
			logger.error("Event photo upload error occured.", ex);
			throw new RoboEventException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Event photo upload error occured.", ex);
			throw new RoboEventException(ex.getMessage());
		}
	}
	
	@Override
	public RoboEventDTO getEvent(Long eventId){
		logger.debug("Robocon request for getting event");
		try{
			RoboEventDTO event = getById(roboEventURL, eventId);
			return event;
		}catch(RoboResponseException ex){
			logger.error("Event retrieve error occured.", ex);
			throw new RoboEventException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Event retrieve error occured.", ex);
			throw new RoboEventException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboEventDTO> getEvents(Integer page, Integer limit){
		logger.debug("Robocon request for getting artist list");
		List<RoboEventDTO> events = getList(String.format(pagingURL, roboEventURL, page, limit));
		return events;
	}
	
	@Override
	public void deleteEvent(Long eventId) {
		logger.debug("Robocon request for deleting event");
		try{
			deleteById(roboEventURL, eventId);
		}catch(RoboResponseException ex){
			logger.error("Event delete error occured.", ex);
			throw new RoboEventException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Event delete error occured.", ex);
			throw new RoboEventException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboEventVO, RoboEventDTO> getTransformer() {
		return roboEventTransformer;
	}

	@Override
	protected DAO<RoboEventVO, Long> getDAO() {
		return roboEventDAO;
	}

}
