package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboStorefrontDTO;
import com.masrawy.robo.models.RoboStorefrontVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboStorefrontTransformer extends DTOTransformer<RoboStorefrontVO, RoboStorefrontDTO>{

}
