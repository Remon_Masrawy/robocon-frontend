package com.masrawy.robo.dao;

import com.masrawy.robo.models.RoboCategoryVO;
import com.masrawy.robo.persistence.DAO;

public interface RoboCategoryDAO extends DAO<RoboCategoryVO, Long>{

}
