package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboProviderDTO;
import com.masrawy.robo.models.RoboProviderVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboProviderTransformer")
public class RoboProviderTransformerImpl extends AbstractDTOTransformer<RoboProviderVO, RoboProviderDTO> implements RoboProviderTransformer{

	public RoboProviderTransformerImpl(){
		super(RoboProviderVO.class, RoboProviderDTO.class);
	}
}
