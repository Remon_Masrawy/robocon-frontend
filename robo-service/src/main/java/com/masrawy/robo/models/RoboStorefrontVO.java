package com.masrawy.robo.models;

import com.masrawy.robo.ws.AbstractVO;

public class RoboStorefrontVO extends AbstractVO{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String keyword;
	private String name;
	private Boolean active;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
}
