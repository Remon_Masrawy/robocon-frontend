element = new Array();

function formChecker(type, status, elem){
	var field		= new Object();
	field.id		= elem[0].id;
	field.name		= elem.attr('id');
	field.type		= type;
	field.status	= status;
	field.value		= elem.val();
	field.parent	= elem.parents('.group');
	field.checker	= element[field.name].checker;
	field.mandatory	= element[field.name].mandatory;
	field.errorMsg	= element[field.name].errorMsg;
	if(typeof element[field.name].dateType != "undefined")
		field.dateType	= element[field.name].dateType;
	if(typeof element[field.name].relation != "undefined")
		field.relation	= element[field.name].relation;
	
	if($(field.parent).is(":hidden")){
		return true;
	}
	
	if(!$(field.parent).is(":hidden")){
		field.result 	= tabFormChecker[field.checker](field);
		if(field.status == 'off') {
			if(field.result.ok) {
				field.parent.removeClass('error');
				field.parent.find('.checker').removeClass('error').addClass('valid');
			}else{
				field.parent.addClass('error');
				field.parent.find('.checker').removeClass('valid').addClass('error');
				field.parent.find('.checker > .content').html(field.result.msg);
			}
		}
		return field.result.ok;
	}
}

function globalControl(nameFields) {
	ok = true;
	for(name in nameFields) {
		if(!formChecker($('[id="' + nameFields[name] + '"]')[0].type, 'off', $('[id="' + nameFields[name] + '"]')))
			ok = false;
	}
	return ok;
}