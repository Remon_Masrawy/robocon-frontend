package com.masrawy.robo.exceptions;

import static com.masrawy.robo.exceptions.ErrorConstants.ROBO_NOT_FOUND;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ServiceNotFoundException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Service not found exception occured.";
	
	public ServiceNotFoundException(){
		super(ERROR_MSG);
	}
	
	public ServiceNotFoundException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBO_NOT_FOUND;
	}

}
