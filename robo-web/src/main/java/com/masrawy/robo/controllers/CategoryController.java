package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.CATEGORIES_PAGE;
import static com.masrawy.robo.constants.RoboPages.CATEGORY_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboCategoryDTO;
import com.masrawy.robo.services.RoboCategoryService;
import com.masrawy.robo.services.RoboLanguageService;

@Controller
@RequestMapping(value = "/web/category")
public class CategoryController {

	@Autowired
	private RoboCategoryService roboCategoryService;
	
	@Autowired
	private RoboLanguageService roboLanguageService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView categoryPage(){
		ModelAndView model = new ModelAndView(CATEGORIES_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
	public ModelAndView category(@PathVariable("categoryId") String categoryId){
		ModelAndView model = new ModelAndView(CATEGORY_PAGE);
		try {
			Long id = Long.valueOf(categoryId);
			if(id != 0){
				model.addObject("category", roboCategoryService.getCategory(id));
			}
			model.addObject("languages", roboLanguageService.getLanguageList(0, 0));
		} catch (Exception e) {
			model = new ModelAndView("redirect:/web/category");
		}
		
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboCategoryDTO create(@RequestBody RoboCategoryDTO roboCategoryDTO){
		return roboCategoryService.createCategory(roboCategoryDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboCategoryDTO update(@RequestBody RoboCategoryDTO roboCategoryDTO){
		return roboCategoryService.updateCategory(roboCategoryDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value="/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboCategoryDTO> categorys(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboCategoryService.getCategoryList(page, limit);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value = "/{categoryId}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("categoryId") Long categoryId){
		roboCategoryService.deleteCategory(categoryId);
	}
}
