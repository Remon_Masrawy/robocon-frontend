package com.masrawy.robo.services;

import java.util.List;
import java.util.Map;

import com.masrawy.robo.dto.RoboFrontendDTO;
import com.masrawy.robo.models.RoboFrontendVO;

public interface RoboFrontendService extends PgrlService<RoboFrontendVO, RoboFrontendDTO, Long>{

	public RoboFrontendDTO createFrontend(RoboFrontendDTO roboFrontendDTO);
	public RoboFrontendDTO updateFrontend(RoboFrontendDTO roboFrontendDTO);
	public RoboFrontendDTO getFrontend(Long id);
	public List<RoboFrontendDTO> getFrontends(Integer page, Integer limit);
	public Map<String, String> getFrontendTypes();
	public void deleteFrontend(Long id);
}
