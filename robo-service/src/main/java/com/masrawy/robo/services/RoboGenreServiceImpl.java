package com.masrawy.robo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.masrawy.robo.dao.RoboGenreDAO;
import com.masrawy.robo.dto.RoboGenreDTO;
import com.masrawy.robo.exceptions.BeanValidationException;
import com.masrawy.robo.exceptions.RoboGenreException;
import com.masrawy.robo.exceptions.RoboResponseException;
import com.masrawy.robo.models.RoboGenreVO;
import com.masrawy.robo.persistence.DAO;
import com.masrawy.robo.transformation.DTOTransformer;
import com.masrawy.robo.transformers.RoboGenreTransformer;

@Service("roboGenreService")
public class RoboGenreServiceImpl extends AbstractPgrlServiceImpl<RoboGenreVO, RoboGenreDTO, Long> implements RoboGenreService{

	@Autowired
	private RoboGenreDAO roboGenreDAO;
	@Autowired
	private RoboGenreTransformer roboGenreTransformer;
	
	@Value("${robocon.ws.genre.url}")
	private String genreURL;
	
	@Override
	public RoboGenreDTO createGenre(RoboGenreDTO roboGenreDTO){
		logger.debug("Robocon request for creating genre");
		try{
			RoboGenreDTO genre = postObject(genreURL, roboGenreDTO);
			return genre;
		}catch(RoboResponseException ex){
			logger.error("Genre creation error occured.", ex);
			throw new RoboGenreException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Genre creation error occured.", ex);
			throw new RoboGenreException(ex.getMessage());
		}
	}
	
	@Override
	public RoboGenreDTO updateGenre(RoboGenreDTO roboGenreDTO){
		logger.debug("Robocon request for updating genre");
		try{
			RoboGenreDTO genre = updateR(genreURL, roboGenreDTO);
			return genre;
		}catch(RoboResponseException ex){
			logger.error("Genre update error occured.", ex);
			throw new RoboGenreException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Genre update error occured.", ex);
			throw new RoboGenreException(ex.getMessage());
		}
	}
	
	@Override
	public RoboGenreDTO getGenre(Long id){
		logger.debug("Robocon request for getting genre by id");
		RoboGenreDTO genre = getById(genreURL, id);
		return genre;
	}
	
	@Override
	public List<RoboGenreDTO> getGenreList(Integer page, Integer limit){
		logger.debug("Robocon request for getting genres list");
		List<RoboGenreDTO> genres = getList(String.format(pagingURL, genreURL, page, limit));
		return genres;
	}
	
	@Override
	public void deleteGenre(Long id){
		logger.debug("Robocon request for deleting genre by id");
		try{
		deleteById(genreURL, id);
		}catch(RoboResponseException ex){
			logger.error("Genre delete error occured.", ex);
			throw new RoboGenreException(ex.getErrorMessage());
		}catch(BeanValidationException ex){
			logger.error("Genre delete error occured.", ex);
			throw new RoboGenreException(ex.getMessage());
		}
	}
	
	@Override
	protected DTOTransformer<RoboGenreVO, RoboGenreDTO> getTransformer() {
		return roboGenreTransformer;
	}

	@Override
	protected DAO<RoboGenreVO, Long> getDAO() {
		return roboGenreDAO;
	}

	
}
