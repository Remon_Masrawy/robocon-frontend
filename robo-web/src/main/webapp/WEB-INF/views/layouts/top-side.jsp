<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:url var="user" value="/web/user"/>
<c:url var="storefront" value="/web/storefront"/>
<c:url var="country" value="/web/country"/>
<c:url var="operator" value="/web/operator"/>
<c:url var="logout" value="/logout"/>
<c:catch>
	<sec:authentication var="photo" property="principal.photo"/>
</c:catch>
<c:if test="${not empty photo}">
	<c:set var="surl" value="${fn:replace(photo, '{width}', '50')}"/>
	<c:set var="photourl" value="${fn:replace(surl, '{height}', '50')}"/>
</c:if>

<div class="templatemo-top-nav-container">
	<div class="row">
		<nav class="templatemo-top-nav col-lg-12 col-md-12">
			<ul class="text-uppercase">
				<li><a id="storefront" href="${storefront}"><spring:message code="robo-pages.storefront-keyword"/></a></li>
				<li><a id="country" href="${country}"><spring:message code="robo-pages.countries-keyword"/></a></li>
				<li><a id="operator" href="${operator}"><spring:message code="robo-pages.operators-keyword"/></a></li>
			</ul>
		</nav>
		<div class="buttons">
			<div class="profile-btn">
				<a href="${user}">
					<c:choose>
						<c:when test="${not empty photourl}">
							<img alt="Photo" src="${photourl}">
						</c:when>
						<c:otherwise>
							<img alt="Photo" src="<c:url value="/images/mazika.png"/>">
						</c:otherwise>
					</c:choose>
				</a>
			</div>
		</div>
	</div>
</div>