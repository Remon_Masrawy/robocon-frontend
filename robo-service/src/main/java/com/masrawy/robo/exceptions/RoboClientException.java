package com.masrawy.robo.exceptions;

import java.util.List;

public class RoboClientException extends RuntimeException implements BaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String ERROR_MSG = "Unknown exception occured.";
	
	public RoboClientException(){
		super(ERROR_MSG);
	}

	@Override
	public int getErrorCode() {
		return 0;
	}

	@Override
	public List<String> getMessageparameters() {
		return null;
	}
}
