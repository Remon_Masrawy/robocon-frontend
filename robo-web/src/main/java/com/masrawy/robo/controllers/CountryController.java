package com.masrawy.robo.controllers;

import static com.masrawy.robo.constants.RoboPages.COUNTRY_PAGE;
import static com.masrawy.robo.constants.RoleConstants.CONTENT_ADMIN;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.masrawy.robo.dto.RoboCountryDTO;
import com.masrawy.robo.services.RoboCountryService;

@Controller
@RequestMapping(value = "/web/country")
public class CountryController {

	@Autowired
	private RoboCountryService roboCountryService;
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView countries(){
		ModelAndView model = new ModelAndView(COUNTRY_PAGE);
		return model;
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboCountryDTO create(@RequestBody RoboCountryDTO roboCountryDTO){
		return roboCountryService.createCountry(roboCountryDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboCountryDTO update(@RequestBody RoboCountryDTO roboCountryDTO){
		return roboCountryService.updateCountry(roboCountryDTO);
	}
	
	@Secured({CONTENT_ADMIN})
	@RequestMapping(value="/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboCountryDTO> countrys(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboCountryService.getCountryList(page, limit);
	}
}
