package com.masrawy.robo.interceptors;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class RoboLoggingRequestInterceptor implements ClientHttpRequestInterceptor{

	final static Logger logger = LoggerFactory.getLogger(RoboLoggingRequestInterceptor.class);
	
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		traceRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        return response;
	}
	
	private void traceRequest(HttpRequest request, byte[] body) throws IOException {
		logger.debug("===========================request begin================================================");
		logger.debug("URI         : {}", request.getURI());
		logger.debug("Method      : {}", request.getMethod());
		logger.debug("Headers     : {}", request.getHeaders() );
		logger.debug("Request body: {}", new String(body, "UTF-8"));
		logger.debug("==========================request end================================================");
    }

}
