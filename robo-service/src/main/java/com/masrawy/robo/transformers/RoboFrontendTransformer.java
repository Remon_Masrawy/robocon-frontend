package com.masrawy.robo.transformers;

import com.masrawy.robo.dto.RoboFrontendDTO;
import com.masrawy.robo.models.RoboFrontendVO;
import com.masrawy.robo.transformation.DTOTransformer;

public interface RoboFrontendTransformer extends DTOTransformer<RoboFrontendVO, RoboFrontendDTO>{

}
