$(document).ready(function(){
	
	$enablePagination = false;
	$enablePanel = false;
	
	$('table.table.templatemo-user-table > tbody > tr > td a.templatemo-edit-btn').click(function(){
		switch($(this).text()){
			case 'Edit':
				$(this).closest("tr").find('#value').find("input").removeAttr('disabled');
				$(this).closest("tr").find('#description').find("input").removeAttr('disabled');
				$(this).html('Save');
				break;
			case 'Save':
				var ids = new Array();
				var valueId = $(this).closest("tr").find('#value').find("input").attr("id");
				var descriptionId = $(this).closest("tr").find('#description').find("input").attr("id");
				ids.push(valueId);
				ids.push(descriptionId);
				if(!globalControl(ids)){
					return;
				}
				var name = $(this).closest("tr").find('#name').text();
				var type = $(this).closest("tr").find('#type').text();
				var value;
				switch($(this).closest("tr").find('#value').find("input").attr("type")){
					case "number":
						value = $(this).closest("tr").find('#value').find("input[type='number']").val()
						break;
					case "checkbox":
						value = $(this).closest("tr").find('#value').find("input[type='checkbox']").is(":checked");
						break;
					default:
						value = $(this).closest("tr").find('#value').find("input").val();
				}
				var description = $(this).closest("tr").find('#description').find("input").val();
				
				var configureParam = {};
				configureParam.name = name;
				configureParam.value = value;
				configureParam.description = description;
				
				var that = $(this);
				
				var headers = {};
				headers["User-Lang"] = "en";
				
				 $.ajax({
					url: "configuration",
					type: "PUT",
					headers: headers,
					data: JSON.stringify(configureParam),
					contentType: 'application/json; charset=utf-8',
					success: function(status){
						that.closest("tr").find('#value').find("input").attr('disabled','disabled');
						that.closest("tr").find('#description').find("input").attr('disabled','disabled');
						that.html('Edit');
						toastr.success('Saved successfully.', 'Success', {positionClass: 'toast-bottom-right'});
					},
					error: function(status, error){
						toastr.error('Error occured.', 'Error', {positionClass: 'toast-bottom-right'});
					},
					complete: function(status){
						
					}
				 });
				
				break;
			default:
				location.reload();
		}
	});
});