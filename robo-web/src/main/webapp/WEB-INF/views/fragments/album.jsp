<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="preview-widget" data-id="${album.id}">
	<c:if test="${not empty album && not empty album.meta}">
		<div class="preview-widget-title">
			<span title="Type"><spring:message code="robo-pages.album-keyword"/></span>
			<span title="ID">${album.id}</span>
			<span title="${album.meta[0].name}">${album.meta[0].name}</span>
		</div>
	</c:if>
	<div class="preview-widget-buttons">
		<div class="new-button"><i class="fa fas fa-plus fa-fw"></i><spring:message code="robo-pages.new-meta-data-keyword"/></div>
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="preview-widget-container">
		<div class="preview-widget-content">
			<div class="templatemo-content-container">
				<div class="templatemo-content-widget no-padding">
	            <div class="panel panel-default table-responsive">
	              <table class="table table-striped table-bordered templatemo-user-table">
	                <thead>
	                  <tr>
	                    <td class="w-200"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.name-keyword"/> <span class="caret"></span></a></td>
	                    <td><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.description-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><a href="" class="white-text templatemo-sort-by"><spring:message code="robo-pages.lang-keyword"/> <span class="caret"></span></a></td>
	                    <td class="w-100"><spring:message code="robo-pages.delete-keyword"/></td>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:if test="${not empty album && not empty album.meta}">
							<c:forEach items="${album.meta}" var="meta">
								<tr data-id="${meta.id}">
									<td contenteditable="true">${meta.name}</td>
									<td contenteditable="true">${meta.description}</td>
									<td>
										<select>
											<c:if test="${not empty languages}">
												<c:forEach items="${languages}" var="language">
													<option value="${language.code}" ${language.code == meta.lang? 'selected' : ''}>${language.code}</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
									<td><a href="javascript:" class="templatemo-link templatemo-meta-delete-link">Delete</a></td>
								</tr>
							</c:forEach>
						</c:if>
	                </tbody>
	              </table>    
	            </div> 
	            <div class="panel-default group-responsive provider-responsive">
	            	<div class="provider">
	            		<label for="provider"><spring:message code="robo-pages.provider-keyword"/></label>
	            		<div class="render-list">
	            			<div class="selected-list">
	            				<c:if test="${not empty album.provider}">
	            					<span class="item" data-id="${album.provider.id}">
		            					<span title="value">${album.provider.meta[0].name}</span>
		            					<span class="item-close"></span>
	            					</span>
		            			</c:if>
	            			</div>
	            			<input id="provider" name="provider" type="text" placeholder="Search..." autocomplete="off">
	            		</div>
	            	</div>
	            </div>
	            <div class="panel-default songs-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.songs-keyword"/></legend>
	            		<div class="songs">
	            			<c:if test="${not empty album.songs}">
	            				<c:forEach items="${album.songs}" var="song" varStatus="status">
	            					<div class="song" data-id="${song.key}">
	            						<span class="index">${status.index + 1}</span>
	            						<span class="content"><a href='<c:url value="/web/song/${song.key}"/>'>${song.value}</a></span>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            	</fieldset>
	            </div>
	            <div class="panel-default photos-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.photos-keyword"/> <span><spring:message code="robo-pages.photos.note"/></span></legend>
	            		<div class="photos">
	            			<c:if test="${not empty album.photos}">
	            				<c:forEach items="${album.photos}" var="photo" varStatus="status">
	            					<div class="photo" data-id="${photo.id}">
	            						<c:set var="suburl" value="${fn:replace(photo.url, '{width}', '500')}"/>
	            						<c:set var="url" value="${fn:replace(suburl, '{height}', '500')}"/>
	            						<img alt="Album" src="${url}">
	            						<span class="close"></span>
	            						<input type="checkbox" name="main" class="main-photo-checkbox" title="Set as main photo" ${photo.isMainPhoto ? 'checked' : ''}>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            		<div class="add-file-button">
	            			<div class="add-sign"></div>
	            		</div>
	            		<input type="file" accept="image/*" title="Choose an image to upload">
	            	</fieldset>
	            </div>
	            <div class="panel-default tags-responsive">
	            	<fieldset>
	            		<legend><spring:message code="robo-pages.tags-keyword"/></legend>
	            		<div class="tags">
	            			<c:if test="${not empty album.tags}">
	            				<c:forEach items="${album.tags}" var="tag" varStatus="status">
	            					<div class="tag" data-id="${tag.id}">
	            						<span title="tag">${tag.tag}</span>
	            						<span class="close"></span>
	            					</div>
	            				</c:forEach>
	            			</c:if>
	            		</div>
	            	</fieldset>
	            </div>
	          </div> 
          </div>
		</div>
	</div>
	<div class="preview-widget-resposive-buttons">
		<div class="save-button"><spring:message code="robo-pages.save-keyword"/></div>
	</div>
	<div class="clear"></div>
</div>
	
<script type="text/javascript" src='<c:url value="/js/album.js"/>'></script>