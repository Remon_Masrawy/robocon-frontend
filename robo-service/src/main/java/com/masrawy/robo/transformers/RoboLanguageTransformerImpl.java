package com.masrawy.robo.transformers;

import org.springframework.stereotype.Component;

import com.masrawy.robo.dto.RoboLanguageDTO;
import com.masrawy.robo.models.RoboLanguageVO;
import com.masrawy.robo.transformation.AbstractDTOTransformer;

@Component("roboLanguageTransformer")
public class RoboLanguageTransformerImpl extends AbstractDTOTransformer<RoboLanguageVO, RoboLanguageDTO> implements RoboLanguageTransformer{

	public RoboLanguageTransformerImpl(){
		super(RoboLanguageVO.class, RoboLanguageDTO.class);
	}
}
