package com.masrawy.robo.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;

public class UsernamePasswordAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider{

	private AuthenticationUserDetailsService<UsernamePasswordAuthenticationToken> authenticationUserDetailsService = null;
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken token)
			throws AuthenticationException {
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken token)
			throws AuthenticationException {
		return authenticationUserDetailsService.loadUserDetails(token);
	}

	public void setAuthenticationUserDetailsService(
			AuthenticationUserDetailsService<UsernamePasswordAuthenticationToken> authenticationUserDetailsService) {
		this.authenticationUserDetailsService = authenticationUserDetailsService;
	}

}
